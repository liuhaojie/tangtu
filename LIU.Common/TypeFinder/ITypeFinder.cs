﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.Common
{
    /// <summary>
    /// 类型查找器
    /// </summary>
    public interface ITypeFinder
    {

        /// <summary>
        /// 获取所有需要注册的类型
        /// </summary>
        /// <returns></returns>
        List<Type> GetTypes();

        /// <summary>
        /// 获取实现类
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        Type GetType(Type type);
    }
}
