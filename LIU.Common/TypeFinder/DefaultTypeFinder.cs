﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;

namespace LIU.Common
{
    /// <summary>
    /// 默认查找器
    /// </summary>
    public class DefaultTypeFinder : ITypeFinder
    {
        List<Assembly> assemblies = new List<Assembly>();

        /// <summary>
        /// 获取所有需要注册的类型
        /// </summary>
        /// <returns></returns>
        public List<Type> GetTypes()
        {
            if (!assemblies.Any())
                GetAssembly();
            List<Type> types = new List<Type>();
            foreach (var item in assemblies)
            {
                types.AddRange(item.ExportedTypes.Where(p => p.IsInterface && p.GetInterface("IRegister") == typeof(IRegister)).ToList());
            }
            return types;
        }

        /// <summary>
        /// 获取实现类
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public Type GetType(Type type)
        {
            if (!assemblies.Any())
                GetAssembly();
            Type types = null;
            //多个类型目前只返回第一个，后期需要再改
            foreach (var item in assemblies)
            {
                foreach (var t in item.GetTypes())
                {
                    if (type.IsAssignableFrom(t) && t.IsClass)
                    {
                        types = t;
                        break;
                    }
                }
                if (types != null)
                    break;
                //types = item.GetTypes().Where(p => p.GetInterfaces().Contains(type) && p.IsClass).FirstOrDefault();
                //item.GetType("LIU.BLL.System.UserInfoBLL");
                //if (types != null)
                //    continue;
            }

            if (types != null)
                return types;
            else
                throw new Exception("接口没有实现");
        }


        /// <summary>
        /// 获取所有的程序集
        /// </summary>
        private void GetAssembly()
        {
            //DirectoryInfo directory = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory);
            //if (!directory.FullName.ToLower().EndsWith("bin\\"))
            //{
            //    directory = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory + "bin\\");
            //}
            //foreach (var item in directory.GetFiles("*.dll", SearchOption.AllDirectories))
            //{
            //    //if (item.Name.ToUpper().StartsWith("LIU"))
            //    //{
            //    try
            //    {
            //        Assembly assembly = Assembly.LoadFile(item.FullName);
            //        if (assembly.FullName.ToUpper().StartsWith("LIU"))
            //            assemblies.Add(assembly);
            //    }
            //    catch (Exception ex)
            //    {
            //        continue;
            //    }
            //    //}
            //}
            AppDomain.CurrentDomain.GetAssemblies().ToList().ForEach(p =>
            {
                if (p.FullName.ToUpper().StartsWith("LIU"))
                    assemblies.Add(p);
            });
            GetAllAssemblies();
        }

        /// <summary>
        /// 获取所有程序集
        /// </summary>
        /// <returns></returns>
        protected void GetAllAssemblies()
        {
            var dir = new DirectoryInfo(GetBinDirectory());
            var files = dir.GetFiles("*.dll");
            foreach (var file in files)
            {
                var ass = Assembly.Load(AssemblyName.GetAssemblyName(file.FullName));
                assemblies.Add(ass);
            }
        }

        /// <summary>
        /// 获取当前应用程序的程序集存放目录
        /// </summary>
        /// <returns></returns>
        public virtual string GetBinDirectory()
        {
            if (HostingEnvironment.IsHosted)
                return HttpRuntime.BinDirectory;
            else
                return AppDomain.CurrentDomain.BaseDirectory;
        }
    }
}
