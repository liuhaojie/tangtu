﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LIU.Common
{
    /// <summary>
    /// 正则验证
    /// </summary>
    public class ValidateHelper
    {
        /// <summary>
        /// 正则密码验证（6到16为）
        /// </summary>
        /// <param name="mima"></param>
        /// <returns></returns>
        public static bool RegexPassWord(string mima)
        {
            Regex reg = new Regex(@"[\S]{6,16}", RegexOptions.IgnoreCase);
            return reg.Match(mima).Success;
        }

        /// <summary>
        /// 正则验证邮箱
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static bool RegexMail(string email)
        {
            Regex regex = new Regex(@"\w[-\w.+]*@([A-Za-z0-9][-A-Za-z0-9]+\.)+[A-Za-z]{2,14}", RegexOptions.IgnoreCase);
            return regex.Match(email).Success;
        }
    }
}
