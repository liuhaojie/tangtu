﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.Common
{
    /// <summary>
    /// 分页帮助类
    /// </summary>
    public class PageHelper
    {
        /// <summary>
        /// 每页多少行
        /// </summary>
        int _pageSize = 10;

        /// <summary>
        /// 当前第几页
        /// </summary>
        int _pageIndex = 1;

        /// <summary>
        /// 跳过多少行
        /// </summary>
        public int PageCount { get { return _pageSize * (_pageIndex - 1); } }

        /// <summary>
        /// 第几页
        /// </summary>
        public int PageIndex
        {
            get { return _pageIndex; }
        }

        /// <summary>
        /// 每页多少行
        /// </summary>
        public int PageSize
        {
            get { return _pageSize; }
        }

        public PageHelper(int pageSize, int pageIndex)
        {
            _pageSize = pageSize;
            _pageIndex = pageIndex;
        }

        public PageHelper(int pageIndex)
        {
            _pageIndex = pageIndex;
        }

        public PageHelper()
        {
        }
    }
}
