﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.Common
{
    public class MsgHelper
    {
        /// <summary>
        /// 状态 1表示成功 97表示系统内部错误 98表示没有登录 99表示没有权限 
        /// </summary>
        public int flag { get; set; }

        /// <summary>
        /// 信息
        /// </summary>
        public object msg { get; set; }

        /// <summary>
        /// 返回数据2019-01-17添加
        /// </summary>
        public object data { get; set; }
    }
}
