﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace LIU.Common
{
    public class HttpHelper
    {
        /*
         * 有时候请求会重定向，但我们就需要从重定向url获取东西，像QQ登录成功后获取sid，但上面的会自动根据重定向地址跳转。我们可以用:
         * request.AllowAutoRedirect = false;设置重定向禁用，你就可以从headers的Location属性中获取重定向地址
         * 
         */
        public static string HttpPost(string Url, string postDataStr, bool isJson = false)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);
                request.Method = "POST";
                if (isJson)
                    request.ContentType = "application/json";
                else
                    request.ContentType = "application/x-www-form-urlencoded";
                //request.ContentLength = Encoding.UTF8.GetByteCount(postDataStr);
                request.Timeout = 1000 * 60;
                //request.CookieContainer = cookie;
                Stream myRequestStream = request.GetRequestStream();
                StreamWriter myStreamWriter = new StreamWriter(myRequestStream, Encoding.GetEncoding("utf-8"));
                myStreamWriter.Write(postDataStr);
                myStreamWriter.Close();
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                //response.Cookies = cookie.GetCookies(response.ResponseUri);
                Stream myResponseStream = response.GetResponseStream();
                StreamReader myStreamReader = new StreamReader(myResponseStream, Encoding.GetEncoding("utf-8"));
                string retString = myStreamReader.ReadToEnd();
                myStreamReader.Close();
                myResponseStream.Close();
                return retString;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public static string HttpGet(string Url, string postDataStr, bool isUTF8 = false)
        {

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url + (postDataStr == "" ? "" : "?") + postDataStr);
            request.Method = "GET";
            request.ContentType = "text/html;charset=UTF-8";
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream myResponseStream = response.GetResponseStream();
            StreamReader myStreamReader;
            if (isUTF8)
                myStreamReader = new StreamReader(myResponseStream, Encoding.GetEncoding("utf-8"));
            else
                myStreamReader = new StreamReader(myResponseStream, Encoding.GetEncoding("gb2312"));

            string retString = myStreamReader.ReadToEnd();
            myStreamReader.Close();
            myResponseStream.Close();
            return retString;
        }
    }
}
