﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace LIU.Common
{
    public class JsonHelper
    {
        /// <summary>
        /// 对象转换json字符串
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string getJsonString(object obj)
        {
            JavaScriptSerializer jsonSerialize = new JavaScriptSerializer();
            return jsonSerialize.Serialize(obj);
        }

        /// <summary>
        /// json字符串转换对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="josnString"></param>
        /// <returns></returns>
        public static T getObject<T>(string josnString)
        {
            JavaScriptSerializer jsonSerialize = new JavaScriptSerializer();
            return jsonSerialize.Deserialize<T>(josnString);
        }

        /// <summary>
        /// datatable转换成list数组
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static List<Dictionary<string, object>> DataTableToList(DataTable dt)
        {
            List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
            foreach (DataRow dr in dt.Rows)//每一行信息，新建一个Dictionary<string,object>,将该行的每列信息加入到字典
            {
                Dictionary<string, object> result = new Dictionary<string, object>();
                foreach (DataColumn dc in dt.Columns)
                {
                    result.Add(dc.ColumnName, dr[dc].ToString());
                }
                list.Add(result);
            }
            return list;
        }
    }
}
