﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.Common
{
    public class LayuiDataHelper
    {
        /// <summary>
        /// 0表示成功
        /// </summary>
        public int code { get; set; }

        /// <summary>
        /// 如果成功为空
        /// </summary>
        public string msg { get; set; }

        /// <summary>
        /// 总条数
        /// </summary>
        public int count { get; set; }

        /// <summary>
        /// 具体数据
        /// </summary>
        public object data { get; set; }
    }
}
