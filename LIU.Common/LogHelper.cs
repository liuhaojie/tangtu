﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.Common
{
    /// <summary>
    /// 文本日志记录
    /// </summary>
    public class LogHelper
    {
        private static Object obj = new Object();
        

        /// <summary>
        /// 写入错误信息
        /// </summary>
        /// <param name="text"></param>
        public static void Error(string text)
        {
            text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " 【错误】:" + text;
            WerteLog(text);
        }

        /// <summary>
        /// 写入提示信息
        /// </summary>
        /// <param name="text"></param>
        public static void Info(string text)
        {
            text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " 【信息】:" + text;
            WerteLog(text);
        }




        /// <summary>
        /// 写信息到文件
        /// </summary>
        /// <param name="text"></param>
        private static void WerteLog(string text)
        {
            lock (obj)
            {
                string logpath = AppDomain.CurrentDomain.BaseDirectory + @"Log";
                if (!System.IO.Directory.Exists(logpath))
                    Directory.CreateDirectory(logpath);
                logpath += @"\" + DateTime.Now.ToString("yyyy-MM-dd") + ".log";              
                using (StreamWriter sw = new StreamWriter(logpath,true))
                {
                    sw.WriteLine(text);
                    sw.Flush();
                    sw.Close();
                }
            }
        }
    }
}
