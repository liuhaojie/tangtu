﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.Common
{
    /// <summary>
    /// layui 树节点信息
    /// </summary>
    public class MenuTreeHelper
    {
        /// <summary>
        /// 节点名称
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 是否展开状态（默认false）
        /// </summary>
        public bool spread { get; set; }

        /// <summary>
        /// 节点链接（可选），未设则不会跳转
        /// </summary>
        public string href { get; set; }
        public List<MenuTreeHelper> children { get; set; }
        /// <summary>
        /// 主键
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// 菜单地址
        /// </summary>
        public string shref { get; set; }

        /// <summary>
        /// 菜单图标
        /// </summary>
        public string icon { get; set; }
    }
}
