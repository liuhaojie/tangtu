using LIU.IDAL.EBook;
using LIU.Model;
using LIU.Model.EBook;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.DAL.EBook
{
    public class ChapterDAL : BaseDAL<Chapter>, IChapterDAL
    {
        public ChapterDAL(DbContext db) : base(db) { }

        /// <summary>
        /// 获取已经采集到最大的章节
        /// </summary>
        /// <param name="gNovelKey">小说主键</param>
        /// <returns></returns>
        public int getMaxChapter(Guid gNovelKey)
        {
            
            int[] result = dbSet.Where(p => p.gNovelKey == gNovelKey).Select(p => p.iIndex).ToArray();
            return result.Length == 0 ? 1 : result.Max();
        }
    }
}

