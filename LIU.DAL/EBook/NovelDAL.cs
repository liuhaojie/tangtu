using LIU.IDAL.EBook;
using LIU.Model;
using LIU.Model.EBook;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.DAL.EBook
{
    public class NovelDAL:BaseDAL<Novel>,INovelDAL
    {
        public NovelDAL(DbContext db) : base(db) { }
    }
}

