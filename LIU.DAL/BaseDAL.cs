﻿using LIU.Common;
using LIU.IDAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace LIU.DAL
{
    public class BaseDAL<T> : IBaseDAL<T> where T : class
    {

        protected DbContext db;
        protected IDbSet<T> dbSet;

        public BaseDAL(DbContext db)
        {
            this.db = db;
            this.dbSet = db.Set<T>();
        }

        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public virtual T GetModel(Expression<Func<T, bool>> where)
        {
            return dbSet.Where(where).FirstOrDefault();
        }

        /// <summary>
        /// 获取实体类
        /// </summary>
        /// <param name="where">lambda查询条件</param>
        /// <param name="orderBy">lambda排序条件</param>
        /// <param name="isAscending">是否升序</param>
        /// <returns></returns>
        public virtual T GetModel(Expression<Func<T, bool>> where, Expression<Func<T, object>> orderBy, bool isAscending)
        {
            if (isAscending)
                return dbSet.Where(where).OrderBy(orderBy).FirstOrDefault();
            else
                return dbSet.Where(where).OrderByDescending(orderBy).FirstOrDefault();
        }

        /// <summary>
        /// 添加实体
        /// </summary>
        /// <param name="model"></param>
        public virtual void Add(T model)
        {
            dbSet.Add(model);
        }

        /// <summary>
        /// 删除实体
        /// </summary>
        /// <param name="model"></param>
        public virtual void Delete(T model)
        {
            db.Entry(model).State = EntityState.Deleted;
        }

        /// <summary>
        /// 根据条件删除
        /// </summary>
        /// <param name="where"></param>
        public virtual void Delete(Expression<Func<T, bool>> where)
        {
            IEnumerable<T> modelList = dbSet.Where(where).AsEnumerable();
            foreach (var item in modelList)
            {
                Delete(item);
            }
        }

        /// <summary>
        /// 更新实体
        /// </summary>
        /// <param name="model"></param>
        public virtual void Update(T model)
        {
            db.Entry(model).State = EntityState.Modified;
        }

        /// <summary>
        /// 获取实体数组
        /// </summary>
        /// <param name="where">查询条件</param>
        /// <returns></returns>
        public virtual List<T> GetList(Expression<Func<T, bool>> where)
        {
            return dbSet.Where(where).ToList();
        }

        /// <summary>
        /// 获取分页数组
        /// </summary>
        /// <param name="where">查询条件</param>
        /// <param name="orderBys">排序字段</param>
        /// <param name="ph">分页帮助类</param>
        /// <param name="isAsc">是否升序</param>
        /// <returns></returns>
        public virtual List<T> GetList(Expression<Func<T, bool>> where, Expression<Func<T, object>> orderBys, PageHelper ph, bool isAsc)
        {
            if (isAsc)
                return dbSet.Where(where).OrderBy(orderBys).Skip(ph.PageCount).Take(ph.PageSize).ToList();
            else
                return dbSet.Where(where).OrderByDescending(orderBys).Skip(ph.PageCount).Take(ph.PageSize).ToList();
        }

        /// <summary>
        /// 获取分页的总行数
        /// </summary>
        /// <param name="where">查询条件</param>
        /// <returns></returns>
        public virtual int RowCount(Expression<Func<T, bool>> where)
        {
            return dbSet.Where(where).Count();
        }

        /// <summary>
        /// 获取最大或者最小值
        /// </summary>
        /// <param name="where">排序条件</param>
        /// <param name="orderBys">排序规则</param>
        /// <param name="isMax">默认获取最大</param>
        /// <returns></returns>
        public virtual T GetMaxOrMin(Expression<Func<T, bool>> where, Expression<Func<T, object>> orderBys, bool isMax = true)
        {
            if (isMax)
                return dbSet.Where(where).OrderByDescending(orderBys).FirstOrDefault();
            else
                return dbSet.Where(where).OrderBy(orderBys).FirstOrDefault();
        }


        /// <summary>
        /// 执行sql语句
        /// </summary>
        /// <typeparam name="TT"></typeparam>
        /// <param name="procName">语句</param>
        /// <param name="paras">参数</param>
        /// <returns></returns>
        public virtual List<TT> ExcetProc<TT>(string procName, params SqlParameter[] paras)
        {
            return db.Database.SqlQuery<TT>(procName, paras).ToList();
        }

        /// <summary>
        /// 执行sql语句
        /// </summary>
        /// <typeparam name="TT"></typeparam>
        /// <param name="procName">语句</param>
        /// <param name="paras">参数</param>
        /// <returns></returns>
        public virtual int ExcetProc(string procName, params SqlParameter[] paras)
        {
            return db.Database.ExecuteSqlCommand(procName, paras);
        }

        /// <summary>
        /// SqlBulkCopy 导入快速
        /// </summary>
        /// <param name="dataTables"></param>
        public virtual void SqlBulkCopy(DataTable[] dataTables)
        {
            //using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(db.Database.Connection.ConnectionString, SqlBulkCopyOptions.UseInternalTransaction | SqlBulkCopyOptions.FireTriggers))
            //{
            //    foreach (var dataTable in dataTables)
            //    {
            //        if (dataTable != null)
            //        {
            //            sqlBulkCopy.DestinationTableName = dataTable.TableName;
            //            sqlBulkCopy.WriteToServer(dataTable);
            //        }
            //    }
            //}
            using (SqlConnection conn = (SqlConnection)db.Database.Connection)
            {
                conn.Open();
                SqlTransaction tran = conn.BeginTransaction();//开启事务
                SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(conn, SqlBulkCopyOptions.CheckConstraints, tran);
                try
                {
                    foreach (var dataTable in dataTables)
                    {
                        if (dataTable != null)
                        {
                            sqlBulkCopy.DestinationTableName = dataTable.TableName;
                            sqlBulkCopy.WriteToServer(dataTable);
                        }
                    }
                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
                finally
                {
                    sqlBulkCopy.Close();
                    conn.Close();
                }
            }
        }


        /// <summary>
        /// EF SQL 语句返回 dataTable
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public DataTable SqlQueryDataTable(string sql, params SqlParameter[] parameters)
        {
            DataTable table = new DataTable();
            //using (SqlConnection conn = (SqlConnection)db.Database.Connection)//new SqlConnection(db.Database.Connection.ConnectionString))
            //{
            SqlConnection conn = (SqlConnection)db.Database.Connection;
            conn.Open();
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = conn;
                cmd.CommandText = sql;
                if (parameters.Length > 0)
                {
                    foreach (var item in parameters)
                    {
                        cmd.Parameters.Add(item);
                    }
                }
                try
                {
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);

                    adapter.Fill(table);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
                return table;
            }
            //}
        }
    }
}
