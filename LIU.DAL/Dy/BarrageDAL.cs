﻿using LIU.IDAL.Dy;
using LIU.Model.Dy;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.DAL.Dy
{
    public class BarrageDAL : BaseDAL<Barrage>, IBarrageDAL
    {
        public BarrageDAL(DbContext db) : base(db) { }
    }
}
