﻿using LIU.IDAL.Bill;
using LIU.Model.Bill;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.DAL.Bill
{
    public class DetailDAL : BaseDAL<Detail>, IDetailDAL
    {
        public DetailDAL(DbContext db) : base(db) { }
    }
}
