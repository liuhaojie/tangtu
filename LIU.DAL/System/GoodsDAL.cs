﻿using LIU.IDAL.System;
using LIU.Model.System;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.DAL.System
{
    public class GoodsDAL : BaseDAL<Goods>, IGoodsDAL
    {
        public GoodsDAL(DbContext db) : base(db) { }
    }
}
