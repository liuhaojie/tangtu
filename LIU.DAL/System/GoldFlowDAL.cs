﻿using LIU.IDAL.System;
using LIU.Model.System;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.DAL.System
{
    public class GoldFlowDAL : BaseDAL<GoldFlow>, IGoldFlowDAL
    {
        public GoldFlowDAL(DbContext db) : base(db) { }
    }
}
