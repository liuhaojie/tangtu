﻿using LIU.IDAL.System;
using LIU.Model.System;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.DAL.System
{
    public class ValidateDAL : BaseDAL<Validate>, IValidateDAL
    {
        public ValidateDAL(DbContext db) : base(db) { }
    }
}
