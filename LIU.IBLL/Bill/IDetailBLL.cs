﻿using LIU.Common;
using LIU.Model.Bill;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.IBLL.Bill
{
    public interface IDetailBLL : IBaseBLL<Detail>, IRegister
    {
        /// <summary>
        /// SqlBulkCopy方式导入数据库
        /// </summary>
        /// <param name="list"></param>
        void ImportSqlBulkCopy(List<Detail> list);

        /// <summary>
        /// 获取当前查询条件下的统计数据
        /// </summary>
        ///  <param name="income">账单归属人员</param>
        /// <param name="income">支出收入 1收入，2支出，3资产转移</param>
        /// <param name="iTransactionType">交易类型</param>
        /// <param name="iTransactionMode">交易方式</param>
        /// <param name="sTransactionObject">交易对象</param>
        /// <param name="dsTransactionTime">交易时间开始</param>
        /// <param name="deTransactionTime">交易时间结束</param>
        /// <returns></returns>
        string GetHeJi(Guid gkey, int? income, int? iTransactionType, int? iTransactionMode, string sTransactionObject,
           DateTime? dsTransactionTime, DateTime? deTransactionTime);
    }
}
