using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LIU.Common;
using LIU.Model;
using LIU.Model.System;

namespace LIU.IBLL.System
{
    public interface IRoleBLL:IBaseBLL<Role>, IRegister
    {
        /// <summary>
        /// 做级联删除 删除角色对应的删除 角色权限的菜单
        /// </summary>
        /// <param name="glist">菜单主键组</param>
        /// <returns></returns>
        bool CascadeDel(List<Guid> glist);
    }
}

