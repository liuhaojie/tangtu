﻿using LIU.Common;
using LIU.Model.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.IBLL.System
{
    public interface IGoodsBLL : IBaseBLL<Goods>, IRegister
    {
        /// <summary>
        /// SqlBulkCopy方式导入数据库
        /// </summary>
        /// <param name="list"></param>
        void ImportSqlBulkCopy(List<Goods> list);
    }
}
