using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LIU.Common;
using LIU.Model;
using LIU.Model.System;

namespace LIU.IBLL.System
{
    public interface IRoleMenuBLL:IBaseBLL<RoleMenu>, IRegister
    {
        /// <summary>
        /// 配置角色菜单的时候 事务级别 删除添加
        /// </summary>
        /// <param name="groleKey">角色主键</param>
        /// <param name="list">要添加的数组</param>
        /// <returns></returns>
        bool DelAndAdd(Guid groleKey, List<RoleMenu> list);
    }
}

