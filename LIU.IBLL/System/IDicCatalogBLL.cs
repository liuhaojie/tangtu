using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LIU.Common;
using LIU.Model;
using LIU.Model.System;

namespace LIU.IBLL.System
{
    public interface IDicCatalogBLL:IBaseBLL<DicCatalog>, IRegister
    {
        /// <summary>
        /// 级联删除字典信息
        /// </summary>
        /// <param name="gkey">字典目录主键</param>
        /// <returns></returns>
        bool DeleteDicAll(Guid gkey);

        /// <summary>
        /// 更新字典目录变动排序 (交换排序)
        /// </summary>
        /// <param name="dicCatalog">需要更新的字典目录</param>
        /// <param name="oldSotr">旧的排序</param>
        /// <returns></returns>
        bool updateOfSort(DicCatalog dicCatalog, int oldSotr);
    }
}

