﻿using LIU.Common;
using LIU.Model.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.IBLL.System
{
    public interface IUserGoodsBLL : IBaseBLL<UserGoods>, IRegister
    {
        /// <summary>
        /// 买物品
        /// </summary>
        /// <param name="user">用户信息</param>
        /// <param name="goods">物品</param>
        /// <param name="sGoodsFrom">物品来源</param>
        /// <param name="buyCount">购买数量</param>
        /// <param name="msg">返回消息</param>
        /// <returns></returns>
        bool Buy(UserInfo user, Goods goods, out string msg, string sGoodsFrom = "购买", int buyCount = 1);

        /// <summary>
        /// 出售物品
        /// </summary>
        /// <param name="user">用户信息</param>
        /// <param name="goods">物品</param>
        /// <param name="userGoods">用户物品</param>
        /// <returns></returns>
        bool Out(UserInfo user, Goods goods, List<UserGoods> userGoods);
    }
}
