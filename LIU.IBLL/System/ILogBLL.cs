using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LIU.Common;
using LIU.Model;
using LIU.Model.System;

namespace LIU.IBLL.System
{
    public interface ILogBLL : IBaseBLL<Log>, IRegister
    {       
    }
}

