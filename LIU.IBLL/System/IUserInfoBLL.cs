﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LIU.Common;
using LIU.Model;
using LIU.Model.System;

namespace LIU.IBLL.System
{
    public interface IUserInfoBLL : IBaseBLL<UserInfo>, IRegister
    {
        /// <summary>
        /// 更新删除历史vip
        /// </summary>
        /// <param name="model"></param>
        /// <param name="Vip"></param>
        /// <returns></returns>
        bool Update(UserInfo model, VIPHistory Vip);
    }
}
