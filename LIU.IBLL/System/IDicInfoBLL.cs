using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LIU.Common;
using LIU.Model;
using LIU.Model.System;

namespace LIU.IBLL.System
{
    public interface IDicInfoBLL:IBaseBLL<DicInfo>, IRegister
    {
        /// <summary>
        /// 更新字典目录变动排序 (交换排序)
        /// </summary>
        /// <param name="dicInfo">需要更新的字典目录</param>
        /// <param name="oldSotr">旧的排序</param>
        /// <returns></returns>
        bool updateOfSort(DicInfo dicInfo, int oldSotr);
    }
}

