using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LIU.Common;
using LIU.Model;
using LIU.Model.System;

namespace LIU.IBLL.System
{
    public interface IMenuBLL : IBaseBLL<Menu>, IRegister
    {
        /// <summary>
        /// 做级联删除 删除菜单对应的删除 角色权限的菜单
        /// </summary>
        /// <param name="glist">菜单主键组</param>
        /// <returns></returns>
        bool CascadeDel(List<Guid> glist);

        /// <summary>
        /// 获取树状菜单
        /// </summary>
        /// <returns></returns>
        List<MenuTreeHelper> GetMenuTreeList();
    }
}

