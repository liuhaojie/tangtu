﻿using LIU.Common;
using LIU.Model.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.IBLL.System
{
    public interface IHeadFrameBLL : IBaseBLL<HeadFrame>, IRegister
    {
    }
}
