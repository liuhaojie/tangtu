using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LIU.Common;
using LIU.Model;
using LIU.Model.System;

namespace LIU.IBLL.System
{
    public interface INotifyMessageBLL : IBaseBLL<NotifyMessage>, IRegister
    {
        /// <summary>
        /// 领取邮件中的物品
        /// </summary>
        /// <param name="userInfo">用户</param>
        /// <param name="notifyMessages">邮件</param>
        /// <returns></returns>
        bool ReceiveGoods(UserInfo userInfo, List<NotifyMessage> notifyMessages);
    }
}

