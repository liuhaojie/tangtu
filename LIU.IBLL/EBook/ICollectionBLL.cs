using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LIU.Common;
using LIU.Model;
using LIU.Model.EBook;

namespace LIU.IBLL.EBook
{
    public interface ICollectionBLL:IBaseBLL<Collection>, IRegister
    {
    }
}

