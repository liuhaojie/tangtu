using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LIU.Common;
using LIU.Model;
using LIU.Model.DTOModel;
using LIU.Model.EBook;

namespace LIU.IBLL.EBook
{
    public interface IChapterBLL : IBaseBLL<Chapter>, IRegister
    {
        /// <summary>
        /// 获取已经采集到最大的章节
        /// </summary>
        /// <param name="gNovelKey">小说主键</param>
        /// <returns></returns>
        int getMaxChapter(Guid gNovelKey);

        /// <summary>
        /// 获取首页显示的章节
        /// </summary>
        /// <returns></returns>
        List<HomeModel> GetHomeModel();
    }
}

