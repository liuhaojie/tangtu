using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LIU.Common;
using LIU.Model;
using LIU.Model.EBook;

namespace LIU.IBLL.EBook
{
    public interface INovelBLL:IBaseBLL<Novel>, IRegister
    {

        /// <summary>
        /// 联合删除 小说章节 小说配置
        /// </summary>
        /// <param name="gkey">小说主键</param>
        /// <param name="model">小说实体</param>
        /// <returns></returns>
        bool UnionDelete(Guid gkey, Novel model);
    }
}

