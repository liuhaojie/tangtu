﻿using LIU.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace LIU.IBLL
{
    public interface IBaseBLL<T> where T : class
    {
        /// <summary>
        /// 获取实体类
        /// </summary>
        /// <param name="where">lambda查询条件</param>
        /// <returns></returns>
        T GetModel(Expression<Func<T, bool>> where);

        /// <summary>
        /// 获取实体类
        /// </summary>
        /// <param name="where">lambda查询条件</param>
        /// <param name="orderBy">lambda排序条件</param>
        /// <param name="isAscending">是否升序</param>
        /// <returns></returns>
        T GetModel(Expression<Func<T, bool>> where, Expression<Func<T, object>> orderBy, bool isAscending);

        /// <summary>
        /// 分页带排序
        /// </summary>
        /// <param name="where">lambda查询条件</param>
        /// <param name="orderBy">lambda排序条件</param>
        /// <param name="page">分页帮助类</param>
        /// <param name="isAscending">是否升序</param>
        /// <returns></returns>
        List<T> GetList(Expression<Func<T, bool>> where,
            Expression<Func<T, object>> orderBy,
            PageHelper page, bool isAscending);

        /// <summary>
        /// 分页带排序
        /// </summary>
        /// <param name="where">lambda查询条件</param>
        /// <param name="orderBy">lambda排序条件</param>
        /// <param name="page">分页帮助类</param>
        /// <param name="isAscending">是否升序</param>
        /// <param name="rowsCount">返回总行</param>
        /// <returns></returns>
        List<T> GetList(Expression<Func<T, bool>> where,
            Expression<Func<T, object>> orderBy,
            PageHelper page, bool isAscending, ref int rowsCount);



        /// <summary>
        /// 获取实体组
        /// </summary>
        /// <param name="where">lambda查询条件</param>
        /// <returns></returns>
        List<T> GetList(Expression<Func<T, bool>> where);

        /// <summary>
        ///获取符合条件实体数
        /// </summary>
        /// <param name="where">lambda查询条件</param>
        /// <returns></returns>
        int GetRowCount(Expression<Func<T, bool>> where);

        /// <summary>
        /// 更新
        /// </summary>
        /// <returns></returns>
        bool Update();

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool Update(T model);

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool Update(List<T> model);

        /// <summary>
        /// 增加一条数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool Add(T model);

        /// <summary>
        /// 增加一组数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool Add(List<T> model);

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool Delete(T model);

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool Delete(List<T> modelList);

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="where">删除条件</param>
        /// <returns></returns>
        bool Delete(Expression<Func<T, bool>> where);

        /// <summary>
        /// 获取最大或者最小值
        /// </summary>
        /// <param name="where">排序条件</param>
        /// <param name="orderBys">排序规则</param>
        /// <param name="isMax">默认获取最大</param>
        /// <returns></returns>
        T GetMaxOrMin(Expression<Func<T, bool>> where, Expression<Func<T, object>> orderBys, bool isMax = true);

        /// <summary>
        /// 执行sql语句
        /// </summary>
        /// <typeparam name="TT"></typeparam>
        /// <param name="procName">语句</param>
        /// <param name="paras">参数</param>
        /// <returns></returns>
        List<TT> ExcetProc<TT>(string procName, params SqlParameter[] paras);

        /// <summary>
        /// 执行sql语句
        /// </summary>
        /// <typeparam name="TT"></typeparam>
        /// <param name="procName">语句</param>
        /// <param name="paras">参数</param>
        /// <returns></returns>
        int ExcetProc(string procName, params SqlParameter[] paras);

        /// <summary>
        /// EF SQL 语句返回 dataTable
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        DataTable SqlQueryDataTable(string sql, params SqlParameter[] parameters);
    }
}
