﻿using LIU.Common;
using LIU.Model.Dy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.IBLL.Dy
{
    public interface IBarrageBLL : IBaseBLL<Barrage>, IRegister
    {
    }
}
