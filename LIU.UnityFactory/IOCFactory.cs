﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Unity;

namespace LIU.UnityFactory
{
    public class IOCFactory
    {
        public static T GetIOCResolve<T>()
        {
            if (HttpContext.Current.Application["UnityContainer"] != null)
            {
                IUnityContainer ioc = HttpContext.Current.Application["UnityContainer"] as UnityContainer;
                return ioc.Resolve<T>();
            }
            else
            {
                throw new Exception("IOC容器初始化失败");
            }
        }        
    }
}
