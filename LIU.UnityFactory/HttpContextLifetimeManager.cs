﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Unity.Lifetime;

namespace LIU.UnityFactory
{
    public class HttpContextLifetimeManager : LifetimeManager, IDisposable
    {
        private Type type;
        public HttpContextLifetimeManager(Type p) { type = p; }
        public override object GetValue(ILifetimeContainer container = null)
        {
            return HttpContext.Current.Items[type.AssemblyQualifiedName];
        }
        public override void RemoveValue(ILifetimeContainer container = null)
        {
            HttpContext.Current.Items.Remove(type.AssemblyQualifiedName);
        }
        public override void SetValue(object newValue, ILifetimeContainer container = null)
        {
            HttpContext.Current.Items[type.AssemblyQualifiedName] = newValue;
        }
        public void Dispose()
        {
            RemoveValue();
        }

        protected override LifetimeManager OnCreateLifetimeManager()
        {
            throw new NotImplementedException();
        }
    }
}
