using System.Data.Entity.Migrations;

namespace LIU.Model.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<LIU.Model.DB>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "LIU.Model.DB";//必须为数据上下文名称
        }

        protected override void Seed(LIU.Model.DB context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //         
        }
    }
}
