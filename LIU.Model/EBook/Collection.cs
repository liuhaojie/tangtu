﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.Model.EBook
{
    /// <summary>
    /// 采集配置表
    /// </summary>
    [Table("eb_collection")]
    public class Collection
    {
        /// <summary>
        /// 配置主键
        /// </summary>
        [Key]
        public Guid gkey { get; set; }

        /// <summary>
        /// 小说主键
        /// </summary>
        public Guid gNovelKey { get; set; }

        /// <summary>
        /// 正文正则
        /// </summary>
        public string gTextRegex { get; set; }

        /// <summary>
        /// 标题正则
        /// </summary>
        public string gTitleRegex { get; set; }

        /// <summary>
        /// 下一章地址正则
        /// </summary>
        public string gNextRegex { get; set; }

        /// <summary>
        /// 首章地址
        /// </summary>
        public string gStartAdd { get; set; }

        /// <summary>
        /// 下一章地址（下一章地址存在就从下一章地址开始采集）
        /// </summary>
        public string gIndexAdd { get; set; }

        /// <summary>
        /// 采集信息出错信息
        /// </summary>
        public string gErrorMag { get; set; }
    }
}
