﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.Model.EBook
{
    /// <summary>
    /// 章节表
    /// </summary>
    [Table("eb_chapter")]
    public class Chapter
    {
        /// <summary>
        /// 章节主键
        /// </summary>
        [Key]
        public Guid gkey { get; set; }

        /// <summary>
        /// 小说主键
        /// </summary>
        public Guid gNovelKey { get; set; }

        /// <summary>
        /// 第几章
        /// </summary>
        public int iIndex { get; set; }

        /// <summary>
        /// 小说章节标题
        /// </summary>
        public string sTitle { get; set; }

        /// <summary>
        /// 小说内容
        /// </summary>
        public string sText { get; set; }

        /// <summary>
        /// 阅读次数
        /// </summary>
        public int iRead { get; set; }

        /// <summary>
        /// 收录时间
        /// </summary>
        public DateTime dInsertTime { get; set; }

        /// <summary>
        /// 收录人主键
        /// </summary>
        public Guid gInsertUserKey { get; set; }

        /// <summary>
        /// 静态页面地址
        /// </summary>
        [MaxLength(256)]
        public string sHtmlUrl { get; set; }
    }
}
