﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.Model.EBook
{
    /// <summary>
    /// 小说表
    /// </summary>
    [Table("eb_novel")]
    public class Novel
    {
        /// <summary>
        /// 主键
        /// </summary>
        [Key]
        public Guid gkey { get; set; }

        /// <summary>
        /// 小说名
        /// </summary>
        public string sName { get; set; }

        /// <summary>
        /// 作者
        /// </summary>
        public string sAuthor { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime? dUpdateTime { get; set; }

        /// <summary>
        /// 更新人主键
        /// </summary>
        public Guid gUpdateUserKey { get; set; }

        /// <summary>
        /// 收录时间
        /// </summary>
        public DateTime? dInsertTime { get; set; }

        /// <summary>
        /// 收录人主键
        /// </summary>
        public Guid gInsertUserKey { get; set; }

        /// <summary>
        /// 是否完结 1.完结 2.连载中 9.未知
        /// </summary>
        public int iType { get; set; }

        /// <summary>
        /// 采集的网站名称
        /// </summary>
        public string sWebsiteName { get; set; }

        /// <summary>
        /// 采集的网站地址
        /// </summary>
        public string sWebsiteUrl { get; set; }

        /// <summary>
        /// 首章地址
        /// </summary>
        public string gStartAdd { get; set; }

        /// <summary>
        /// 下一章地址（下一章地址存在就从下一章地址开始采集）
        /// </summary>
        public string gIndexAdd { get; set; }

        /// <summary>
        /// 采集信息出错信息
        /// </summary>
        public string gErrorMag { get; set; }
    }
}
