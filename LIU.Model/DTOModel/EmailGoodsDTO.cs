﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.Model.DTOModel
{
    /// <summary>
    /// 邮件里面包含的物品数据
    /// </summary>
    public class EmailGoodsDTO
    {
        /// <summary>
        /// 物品主键  如果为金币 这id=0
        /// </summary>
        public int goodsID { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public int count { get; set; }
    }
}
