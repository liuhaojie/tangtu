﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.Model.Bill
{
    /// <summary>
    /// 账单明细
    /// </summary>
    [Table("bl_detail")]
    public class Detail
    {
        /// <summary>
        /// 主键
        /// </summary>
        [Key]
        public Guid gkey { get; set; }

        /// <summary>
        /// 条账单归属人员
        /// </summary>
        public Guid gBelongKey { get; set; }

        /// <summary>
        /// 标志 0删除，1存在
        /// </summary>
        public int iFlag { get; set; }

        /// <summary>
        /// 金额
        /// </summary>
        public decimal dAmount { get; set; }

        /// <summary>
        /// 支出收入 1收入，2支出
        /// </summary>
        public int income { get; set; }

        /// <summary>
        /// 交易类型 通过字典获取
        /// </summary>
        public int iTransactionType { get; set; }

        /// <summary>
        /// 交易方式 通过字典获取
        /// </summary>
        public int iTransactionMode { get; set; }

        /// <summary>
        /// 交易对象
        /// </summary>
        public string sTransactionObject { get; set; }

        /// <summary>
        /// 说明
        /// </summary>
        public string sExplain { get; set; }

        /// <summary>
        /// 交易时间
        /// </summary>
        public DateTime dTransactionTime { get; set; }

        /// <summary>
        /// 添加时间
        /// </summary>
        public DateTime dAddTime { get; set; }

        /// <summary>
        /// 交易编号
        /// </summary>
        public string sNo { get; set; }

        /// <summary>
        /// 商品名称
        /// </summary>
        public string sGoods { get; set; }
    }
}
