﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.Model.Dy
{
    /// <summary>
    /// 关键字表
    /// </summary>
    [Table("dy_KeyWord")]
    public class KeyWord
    {
        /// <summary>
        /// 主键
        /// </summary>
        [Key]
        public Guid gkey { get; set; }

        /// <summary>
        /// 房间id
        /// </summary>
        public string sRoomId { get; set; }

        /// <summary>
        /// 关键字
        /// </summary>
        public string sWord { get; set; }

        /// <summary>
        /// 总数
        /// </summary>
        public int iCount { get; set; }

        /// <summary>
        /// 第一条时间
        /// </summary>
        public DateTime dStartTime { get; set; }

        /// <summary>
        /// 最后一跳时间
        /// </summary>
        public DateTime dEndTime { get; set; }

        /// <summary>
        /// 属于那一天  yyyy-MM-dd 格式 对应数据库 date字段
        /// </summary>
        public DateTime dIndedxDate { get; set; }
    }
}
