﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.Model.Dy
{
    /// <summary>
    /// 弹幕表
    /// </summary>
    [Table("dy_Barrage")]
    public class Barrage
    {
        /// <summary>
        /// 主键
        /// </summary>
        [Key]
        public Guid gkey { get; set; }

        /// <summary>
        /// 房间id
        /// </summary>
        public string sRoomId { get; set; }

        /// <summary>
        /// 发言人名
        /// </summary>
        public string sName { get; set; }

        /// <summary>
        /// 发育内容
        /// </summary>
        public string sContent { get; set; }

        /// <summary>
        /// 发言时间
        /// </summary>
        public DateTime dSpeakTime { get; set; }

        /// <summary>
        /// 发言人等级
        /// </summary>
        public int sLevel { get; set; }
    }
}
