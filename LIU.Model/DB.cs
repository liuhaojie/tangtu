﻿namespace LIU.Model
{
    using Bill;
    using Dy;
    using global::System.Data.Common;
    using global::System.Data.Entity;
    using LIU.Common;
    using LIU.Model.EBook;
    using LIU.Model.System;

    public class DB : DbContext
    {
        //您的上下文已配置为从您的应用程序的配置文件(App.config 或 Web.config)
        //使用“DB”连接字符串。默认情况下，此连接字符串针对您的 LocalDb 实例上的
        //“LIU.Model.DB”数据库。
        // 
        //如果您想要针对其他数据库和/或数据库提供程序，请在应用程序配置文件中修改“DB”
        //连接字符串。
        private static string str;

        public DB()
        : base(GetConnection())
        {
        }

        public static string GetConnection()
        {
            if (str == null)
            {
                str = AesHelper.AesDecrypt(global::System.Configuration.ConfigurationManager.ConnectionStrings["DB"].ToString());
            }
            return str;
        }

        //为您要在模型中包含的每种实体类型都添加 DbSet。有关配置和使用 Code First  模型
        //的详细信息，请参阅 http://go.microsoft.com/fwlink/?LinkId=390109。

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
        #region 系统模块
        public virtual DbSet<UserInfo> UserInfo { get; set; }
        public virtual DbSet<Menu> Menu { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<RoleMenu> RoleMenu { get; set; }
        public virtual DbSet<DicCatalog> DicCatalog { get; set; }
        public virtual DbSet<DicInfo> DicInfo { get; set; }
        public virtual DbSet<Parameter> Parameter { get; set; }
        public virtual DbSet<MessageBoard> MessageBoard { get; set; }
        public virtual DbSet<NotifyMessage> NotifyMessage { get; set; }
        public virtual DbSet<Log> Log { get; set; }
        public virtual DbSet<Notice> Notice { get; set; }
        public virtual DbSet<HeadFrame> HeadFrame { get; set; }
        public virtual DbSet<GoldFlow> GoldFlow { get; set; }
        public virtual DbSet<Validate> Validate { get; set; }
        public virtual DbSet<Goods> Goods { get; set; }
        public virtual DbSet<UserGoods> UserGoods { get; set; }
        public virtual DbSet<VIPHistory> VIPHistory { get; set; }
        #endregion

        #region 小说模块
        public virtual DbSet<Novel> Novel { get; set; }
        public virtual DbSet<Collection> Collection { get; set; }
        public virtual DbSet<Chapter> Chapter { get; set; }
        #endregion

        #region 斗鱼

        /// <summary>
        /// 弹幕
        /// </summary>
        public virtual DbSet<Barrage> Barrage { get; set; }

        /// <summary>
        /// 关键字
        /// </summary>
        public virtual DbSet<KeyWord> KeyWord { get; set; }
        #endregion

        #region 订单模块
        public virtual DbSet<Detail> Detail { get; set; }
        #endregion
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}