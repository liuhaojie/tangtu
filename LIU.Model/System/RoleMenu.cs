﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.Model.System
{
    /// <summary>
    /// 角色权限表
    /// </summary>
    [Table("sys_role_menu")]
    public class RoleMenu
    {
        /// <summary>
        /// 角色主键
        /// </summary>
        [Key, Column(Order = 0)]
        public Guid gRoleKey { get; set; }

        /// <summary>
        /// 菜单主键
        /// </summary>
        [Key, Column(Order = 1)]
        public Guid gMenuKey { get; set; }

        /// <summary>
        /// 添加人员主键
        /// </summary>
        public Guid? gInsertKey { get; set; }

        /// <summary>
        /// 添加时间
        /// </summary>
        public DateTime? dInsert { get; set; }

        /// <summary>
        /// 修改人员主键
        /// </summary>
        public Guid? gUpdateKey { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? dUpdate { get; set; }
    }
}
