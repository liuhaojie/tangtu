﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.Model.System
{
    /// <summary>
    /// 角色列表
    /// </summary>
    [Table("sys_role")]
    public class Role
    {
        /// <summary>
        /// 主键
        /// </summary>
        [Key]
        public Guid gkey { get; set; }

        /// <summary>
        /// 角色名
        /// </summary>
        [MaxLength(50)]
        public string sName { get; set; }

        /// <summary>
        /// 角色说明
        /// </summary>
        [MaxLength(200)]
        public string sExplain { get; set; }

        /// <summary>
        /// 角色状态 0.停用 1.启用
        /// </summary>
        //public int iType { get; set; }

        /// <summary>
        /// 添加人员主键
        /// </summary>
        public Guid? gInsertKey { get; set; }

        /// <summary>
        /// 添加时间
        /// </summary>
        public DateTime? dInsert { get; set; }

        /// <summary>
        /// 修改人员主键
        /// </summary>
        public Guid? gUpdateKey { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? dUpdate { get; set; }
    }
}
