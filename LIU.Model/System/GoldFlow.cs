﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.Model.System
{
    /// <summary>
    /// 金币流水
    /// </summary>
    [Table("sys_goldFlow")]
    public class GoldFlow
    {
        /// <summary>
        /// 主键 自增长
        /// </summary>
        [Key]
        public int ID { get; set; }

        /// <summary>
        /// 变化前的金币
        /// </summary>
        public int iOldGold { get; set; }

        /// <summary>
        /// 变化后的金币
        /// </summary>
        public int iNewGold { get; set; }

        /// <summary>
        /// 变化的金币数量
        /// </summary>
        public int iGold { get; set; }

        /// <summary>
        /// 用户主键
        /// </summary>
        public Guid gUserKey { get; set; }

        /// <summary>
        /// 产生变化的对象，空默认系统
        /// </summary>
        public Guid? gObjectKey { get; set; }

        /// <summary>
        /// 发生的时间
        /// </summary>
        public DateTime dTime { get; set; }

        /// <summary>
        /// 说明
        /// </summary>
        public string sMemo { get; set; }

        /// <summary>
        /// 类型 1签到，2赠送，3消费，4获得，5,取消，9其他
        /// </summary>
        public int iType { get; set; }

        /// <summary>
        /// 1 增加 2减少
        /// </summary>
        public int iFlag { get; set; }


    }
}
