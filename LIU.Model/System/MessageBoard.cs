﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.Model.System
{
    /// <summary>
    /// 留言板
    /// </summary>
    [Table("sys_messageBoard")]
    public class MessageBoard
    {
        /// <summary>
        /// 主键
        /// </summary>
        [Key]
        public Guid gkey { get; set; }

        /// <summary>
        /// 留言内容
        /// </summary>
        [MaxLength(1000)]
        public string sMessage { get; set; }

        /// <summary>
        /// 回复内容
        /// </summary>
        [MaxLength(1000)]
        public string sReply { get; set; }

        /// <summary>
        /// 留言人员主键
        /// </summary>
        public Guid gInsertKey { get; set; }

        /// <summary>
        /// 留言时间
        /// </summary>
        public DateTime dInsert { get; set; }

        /// <summary>
        /// 回复人员主键
        /// </summary>
        public Guid? gReplyKey { get; set; }

        /// <summary>
        /// 回复时间
        /// </summary>
        public DateTime? dReply { get; set; }

        /// <summary>
        /// 留言类型 根据字典表获取具体类型
        /// </summary>
        public int iType { get; set; }

        /// <summary>
        /// 标志 0删除 1启用
        /// </summary>
        public int iFlag { get; set; }
    }
}
