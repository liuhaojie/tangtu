﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.Model.System
{
    /// <summary>
    /// 系统参数
    /// </summary>
    [Table("sys_parameter")]
    public class Parameter
    {
        /// <summary>
        /// 主键
        /// </summary>
        [Key]
        public Guid gkey { get; set; }

        /// <summary>
        /// 参数名
        /// </summary>
        [MaxLength(100)]
        public string sName { get; set; }

        /// <summary>
        /// 参数代码
        /// </summary>
        [MaxLength(100)]
        public string sCode { get; set; }

        /// <summary>
        /// 参数值
        /// </summary>
        [MaxLength(500)]
        public string sValue { get; set; }

        /// <summary>
        /// 参数说明
        /// </summary>
        [MaxLength(500)]
        public string sRemark { get; set; }

        /// <summary>
        /// 标志 0不启用 1启用
        /// </summary>
        public int iFlag { get; set; }
    }
}
