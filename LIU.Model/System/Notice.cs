﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.Model.System
{
    /// <summary>
    /// 公告表
    /// </summary>
    [Table("sys_Notice")]
    public class Notice
    {

        /// <summary>
        /// 主键
        /// </summary>
        [Key]
        public Guid gkey { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        [MaxLength(1024)]
        public string sContent { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime dCreateTime { get; set; }

        /// <summary>
        /// 创建人id
        /// </summary>
        public Guid gCreateId { get; set; }

        /// <summary>
        /// 0:禁用 1:启用
        /// </summary>
        public int iFlag { get; set; }
    }
}
