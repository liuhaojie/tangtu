﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.Model.System
{
    /// <summary>
    /// 用户表
    /// </summary>
    [Table("sys_validate")]
    public class Validate
    {
        /// <summary>
        /// 主键
        /// </summary>
        [Key]
        public Guid gkey { get; set; }

        /// <summary>
        /// 用户主键
        /// </summary>
        public Guid gUserKey { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime dCreateTime { get; set; }

        /// <summary>
        /// 过期时间
        /// </summary>
        public DateTime dOverdueTime { get; set; }

        /// <summary>
        /// 1.激活 2.邮箱验证 3.密码修改
        /// </summary>
        public int iType { get; set; }

        /// <summary>
        /// 0.已经使用 1.未使用
        /// </summary>
        public int iFlag { get; set; }
    }
}
