﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.Model.System
{
    /// <summary>
    /// 字典目录
    /// </summary>
    [Table("sys_dicCatalog")]
    public class DicCatalog
    {
        /// <summary>
        /// 主键
        /// </summary>
        [Key]
        public Guid gkey { get; set; }

        /// <summary>
        /// 目录名称
        /// </summary>
        [MaxLength(50)]
        public string sName { get; set; }

        /// <summary>
        /// 目录代码
        /// </summary>
        [MaxLength(50)]
        public string sCode { get; set; }

        /// <summary>
        /// 目录备注
        /// </summary>
        [MaxLength(300)]
        public string sRemark { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int iSort { get; set; }

        /// <summary>
        /// 标志 0不启用 1启用
        /// </summary>
        public int iFlag { get; set; }
    }
}
