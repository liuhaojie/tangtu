﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.Model.System
{
    /// <summary>
    /// vip冻结表
    /// </summary>
    [Table("sys_vipHistory")]
    public class VIPHistory
    {
        /// <summary>
        /// 主键
        /// </summary>
        [Key]
        public Guid gkey { get; set; }

        /// <summary>
        /// 用户主键
        /// </summary>
        public Guid gUserKey { get; set; }

        /// <summary>
        /// vip等级
        /// </summary>
        public int iLevel { get; set; }

        /// <summary>
        /// vip剩余天数
        /// </summary>
        public int iDay { get; set; }

        /// <summary>
        /// 添加时间
        /// </summary>
        public DateTime dCreateTime { get; set; }
    }
}
