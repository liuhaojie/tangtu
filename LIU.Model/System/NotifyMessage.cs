﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.Model.System
{
    /// <summary>
    /// 通知消息表
    /// </summary>
    [Table("sys_notifyMessage")]
    public class NotifyMessage
    {
        /// <summary>
        /// 主键
        /// </summary>
        [Key]
        public Guid gkey { get; set; }

        /// <summary>
        /// 消息键 用于区分同次发送的消息 分组作用
        /// </summary>
        public Guid gMsgKey { get; set; }

        /// <summary>
        /// 发送人主键
        /// </summary>
        public Guid gSendKey { get; set; }

        /// <summary>
        /// 发送标题
        /// </summary>
        [MaxLength(100)]
        public string sSendTitle { get; set; }

        /// <summary>
        /// 地址链接  现做邮件附件使用
        /// </summary>
        [MaxLength(300)]
        public string sHref { get; set; }

        /// <summary>
        /// 发送内容
        /// </summary>
        public string sSendContent { get; set; }

        /// <summary>
        /// 发送时间
        /// </summary>
        public DateTime dSendDate { get; set; }

        /// <summary>
        /// 接收人主键
        /// </summary>
        public Guid gReceiveKey { get; set; }

        /// <summary>
        /// 阅读时间
        /// </summary>
        public DateTime? dReadDate { get; set; }

        /// <summary>
        /// 消息状态 0:删除 1未读 2已读 3已领取
        /// </summary>
        public int iFlag { get; set; }

        /// <summary>
        /// 消息类型 1:系统通知 2:非系统通知 3：包含物品
        /// </summary>
        public int iType { get; set; }
    }
}
