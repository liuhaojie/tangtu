﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.Model.System
{
    /// <summary>
    /// 菜单列表
    /// </summary>
    [Table("sys_menu")]
    public class Menu
    {
        /// <summary>
        /// 菜单主键
        /// </summary>
        [Key]
        public Guid gKey { get; set; }

        /// <summary>
        /// 菜单名称
        /// </summary>
        [MaxLength(100)]
        public string sName { get; set; }

        /// <summary>
        /// 菜单图标
        /// </summary>
        [MaxLength(50)]
        public string sIcon { get; set; }

        /// <summary>
        /// 菜单页面地址 或者 按钮请求菜单 
        /// </summary>
        [MaxLength(200)]
        public string sHref { get; set; }

        /// <summary>
        /// 按钮Id
        /// </summary>
        [MaxLength(50)]
        public string sBtnId { get; set; }

        /// <summary>
        /// 父菜单主键 为默认值表示一级菜单
        /// </summary>
        public Guid gParentKey { get; set; }

        /// <summary>
        /// 菜单类型 1.菜单文件 2.菜单页面 3.按钮
        /// </summary>
        public int iType { get; set; }

        /// <summary>
        /// 添加人员主键
        /// </summary>
        public Guid? gInsertKey { get; set; }

        /// <summary>
        /// 添加时间
        /// </summary>
        public DateTime? dInsert { get; set; }

        /// <summary>
        /// 修改人员主键
        /// </summary>
        public Guid? gUpdateKey { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? dUpdate { get; set; }
    }
}
