﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.Model.System
{
    /// <summary>
    /// 用户物品表
    /// </summary>
    [Table("sys_user_goods")]
    public class UserGoods
    {
        /// <summary>
        /// 主键
        /// </summary>
        [Key]
        public Guid gkey { get; set; }

        /// <summary>
        /// 物品表主键
        /// </summary>
        public int GoodsID { get; set; }

        /// <summary>
        /// 用户主键
        /// </summary>
        public Guid gUserKey { get; set; }

        /// <summary>
        /// 使用表示 0未使用，1已使用,2出售
        /// </summary>
        public int iFlag { get; set; }

        /// <summary>
        /// 物品来源
        /// </summary>
        public string sGoodsFrom { get; set; }

        /// <summary>
        /// 物品过期时间
        /// </summary>
        public DateTime? dOverdueTime { get; set; }

        /// <summary>
        /// 添加时间
        /// </summary>
        public DateTime dCreateTime { get; set; }
    }
}
