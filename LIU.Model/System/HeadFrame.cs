﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.Model.System
{

    /// <summary>
    /// 头像框表
    /// </summary>
    [Table("sys_headFrame")]
    public class HeadFrame
    {
        /// <summary>
        /// 主键
        /// </summary>
        [Key]
        public Guid gkey { get; set; }

        /// <summary>
        /// 级别
        /// </summary>
        public int iLevel { get; set; }

        /// <summary>
        /// 图片地址
        /// </summary>
        public string sUrl { get; set; }

        /// <summary>
        /// 图片名
        /// </summary>
        public string sName { get; set; }

        /// <summary>
        /// 排序号
        /// </summary>
        public int iSort { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime dAddTime { get; set; }
    }
}
