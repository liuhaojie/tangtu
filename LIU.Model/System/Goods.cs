﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.Model.System
{
    /// <summary>
    /// 物品表
    /// </summary>
    [Table("sys_goods")]
    public class Goods
    {
        /// <summary>
        /// 主键
        /// </summary>
        [Key]
        public int ID { get; set; }

        /// <summary>
        /// 物品名称
        /// </summary>
        public string sName { get; set; }

        /// <summary>
        /// 物品说明
        /// </summary>
        public string sExplain { get; set; }

        /// <summary>
        /// 分类
        /// </summary>
        public string sClassify { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int iSort { get; set; }

        /// <summary>
        /// 预留参数1 在vip道具中 sValue1做为vip等级  sValue2作为vip天数
        /// </summary>
        public string sValue1 { get; set; }

        /// <summary>
        /// 预留参数2
        /// </summary>
        public string sValue2 { get; set; }

        /// <summary>
        /// 原价
        /// </summary>
        public int iOldPrice { get; set; }

        /// <summary>
        /// 现价
        /// </summary>
        public int iNewPrice { get; set; }

        /// <summary>
        /// 限购类型 all:不限购,only:终身，day:天，week:周，month:月，year:年
        /// </summary>
        public string sRestrictionsType { get; set; }

        /// <summary>
        /// 限购 0表示限
        /// </summary>
        public int iRestrictions { get; set; }

        /// <summary>
        /// 物品图片地址
        /// </summary>
        public string sHref { get; set; }

        /// <summary>
        /// 添加时间
        /// </summary>
        public DateTime dCreateTime { get; set; }

        /// <summary>
        /// 0表示可以购买 1表示不可以购买
        /// </summary>
        public int iType { get; set; }
    }
}
