﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.Model.System
{
    /// <summary>
    /// 字典详情
    /// </summary>
    [Table("sys_dicInfo")]
    public class DicInfo
    {

        /// <summary>
        /// 主键
        /// </summary>
        [Key]
        public Guid gkey { get; set; }

        /// <summary>
        /// 字典目录表主键
        /// </summary>
        public Guid gDicCatalogKey { get; set; }
        
        /// <summary>
        /// 字典代码(对应字典目录代码)
        /// </summary>
        [MaxLength(50)]
        public string sCode { get; set; }

        /// <summary>
        /// 字典名称
        /// </summary>
        [MaxLength(100)]
        public string sName { get; set; }

        /// <summary>
        /// 字典值
        /// </summary>
        [MaxLength(100)]
        public string sValue { get; set; }

        /// <summary>
        /// 字典备注
        /// </summary>
        [MaxLength(300)]
        public string sRemark { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int iSort { get; set; }

        /// <summary>
        /// 标志 0不启用 1启用
        /// </summary>
        public int iFlag { get; set; }
    }
}
