﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.Model.System
{
    /// <summary>
    /// 系统日志
    /// </summary>
    [Table("sys_log")]
    public class Log
    {
        /// <summary>
        /// 主键
        /// </summary>
        [Key]
        public Guid gkey { get; set; }

        /// <summary>
        /// 改变的对象主键
        /// </summary>
        [MaxLength(100)]
        public string sObjectId { get; set; }

        /// <summary>
        /// 操作类型 1:增 2:删 3:改
        /// </summary>
        public int iOperationType { get; set; }

        /// <summary>
        /// 操作类型名
        /// </summary>
        [MaxLength(10)]
        public string sOperationTypeName { get; set; }

        /// <summary>
        /// 改变的内容
        /// </summary>
        public string sChangeContext { get; set; }

        /// <summary>
        /// 操作人员主键
        /// </summary>
        public Guid gOperationKey { get; set; }

        /// <summary>
        /// 操作时间
        /// </summary>
        public DateTime dOperationTime { get; set; }
    }
}
