﻿using LIU.Model.EBook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LIU.CollectionService
{
    /// <summary>
    /// 采集工厂
    /// </summary>
    public class CollectionFactory
    {
        /// <summary>
        /// 采集处理程序静态缓存
        /// </summary>
        private static Type[] types = null;

        /// <summary>
        /// 创建对象
        /// </summary>
        /// <param name="goods"></param>
        /// <returns></returns>
        public static ICollectionHandle CreateICollectionHandle(Novel novel)
        {
            if (types == null || types.Length == 0)
                types = Assembly.Load("LIU.CollectionService").GetTypes().Where(p => p.GetInterfaces().Contains(typeof(ICollectionHandle))).ToArray();
            //types = AppDomain.CurrentDomain.GetAssemblies().SelectMany(a => a.GetTypes().Where(t => t.GetInterfaces().Contains(typeof(IGoodsHandle)))).ToArray();
            foreach (var item in types)
            {
                var att = item.GetCustomAttribute<CollectionAttribute>();
                //var att = (GoodsAttribute)Attribute.GetCustomAttribute(item, typeof(GoodsAttribute));
                if (att.WebsiteName == novel.sWebsiteName)
                {
                    return Activator.CreateInstance(item, novel) as ICollectionHandle;
                }
            }
            throw new Exception("该网站站点未有对应的采集规则");
        }

    }
}
