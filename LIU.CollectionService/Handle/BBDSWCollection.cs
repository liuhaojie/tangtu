﻿using LIU.Model.EBook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LIU.CollectionService.Handle
{
    /// <summary>
    /// 八八读书网
    /// </summary>
    [Collection(WebsiteName = "八八读书网", WebsiteUrl = "https://www.88dush.com/")]
    public class BBDSWCollection : CollectionBase, ICollectionHandle
    {
        public BBDSWCollection(Novel novel) : base(novel)
        { }
        public void CollectionInfo(bool async)
        {
            RegContext = $"<div class=\"yd_text2\">([\\d\\D]*?)</div>";
            RegNext = $"返回目录</a><a href=\"([\\d\\D]*?)\" target=\"_top\">下一章 →</a>";
            RegTitle = $"<h1>([\\D\\d]*?)</h1>";
            httpHerd = url.Substring(0, url.LastIndexOf('/') + 1);
            overUrl = "index.html";
            if (async)
            {
                Thread thread = new Thread(getValue);
                thread.Start();
            }
            else
                getValue();
        }
    }
}
