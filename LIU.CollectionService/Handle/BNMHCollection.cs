﻿using LIU.Common;
using LIU.Model.EBook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace LIU.CollectionService.Handle
{
    [Collection(WebsiteName = "百年漫画", WebsiteUrl = "https://m.bnmanhua.com/")]
    public class BNMHCollection : CollectionBase, ICollectionHandle
    {

        /// <summary>
        /// 小说主键
        /// </summary>
        private Guid novelKey;

        public BNMHCollection(Novel novel) : base(novel)
        {
            novelKey = novel.gkey;
        }

        /// <summary>
        /// 图片头地址正则
        /// </summary>
        private string Reg_yurl = "z_yurl[\\s]?=[\\s]?['|\"](http[s]?://[\\w\\W]+/)['|\"]";

        /// <summary>
        /// 图片正则
        /// </summary>
        private string Reg_img = "z_img[\\s]?=[\\s]?['|\"]\\[([\\w\\W]+)\\]['|\"]";

        /// <summary>
        /// 网站地址
        /// </summary>
        private string WebsiteUrl = "https://m.bnmanhua.com/";

        /// <summary>
        /// 下一章正则
        /// </summary>
        private string Reg_next = "href=['|\"]([\\w\\W][^'^\"]+)['|\"][\\s]?id=['|\"]xurl['|\"]";

        /// <summary>
        /// 名称正则 group 1为详情页 2位名称
        /// </summary>
        private string Reg_Name = "class=\"bo_tit\">[\\w\\W]+href=\"([\\w\\W][^\"]+)\"[\\w\\W][^\"]+</a>([\\w\\W][^<]+)</h4>";

        /// <summary>
        /// 结束时最后一章正则获取的值
        /// </summary>
        private string OverText = "";

        public void CollectionInfo(bool async)
        {
            if (string.IsNullOrWhiteSpace(novel.gIndexAdd) && string.IsNullOrWhiteSpace(novel.gStartAdd))
            {
                novel.gErrorMag = $"采集失败！当前地址或下一章地址为空";
                iNovlBll.Update(novel);
            }
            if (async)
            {
                Thread thread = new Thread(getValue);
                thread.Start();
            }
            else
                getValue();
        }

        string indexUrl = "";
        protected override void getValue()
        {
            try
            {
                chapterIndex = iChapBll.getMaxChapter(novel.gkey);
                string html = "";
                StringBuilder content = new StringBuilder(); ;
                string yurl = "";
                string next = "";
                string img = "";
                MatchCollection name = null;
                indexUrl = string.IsNullOrWhiteSpace(novel.gIndexAdd) ? novel.gStartAdd : novel.gIndexAdd;
                if (chapterIndex > 1)
                {
                    //判断第一章是否存在
                    Chapter chapter = iChapBll.GetModel(p => p.gNovelKey == novelKey && p.iIndex == 1);
                    if (chapter != null)//存在第一章就先获取一次下一章的信息一次
                    {
                        html = HttpHelper.HttpGet(indexUrl, "", true);
                        next = getRegex(Reg_next, html);
                        name = getMatchCollection(Reg_Name, html);
                        if (next == name[0].Groups[1].ToString())
                        {
                            novel.gErrorMag = $"已经采集到最新章节了";
                            return;
                        }
                        indexUrl = WebsiteUrl + next;
                        chapterIndex++;
                    }
                }
                while (true)
                {
                    content.Clear();
                    html = HttpHelper.HttpGet(indexUrl, "", true);
                    next = getRegex(Reg_next, html);
                    yurl = "https://img.yaoyaoliao.com/";//getRegex(Reg_yurl, html);
                    img = getRegex(Reg_img, html);
                    name = getMatchCollection(Reg_Name, html);
                    if (string.IsNullOrWhiteSpace(OverText))
                        OverText = name[0].Groups[1].ToString();
                    content.Append($"<h2> {name[0].Groups[2].ToString().Replace("&nbsp;", "").Replace(">", "")} </h2>");
                    foreach (var item in img.Replace("\"", "").Replace("'", "").Replace("\\", "").Split(new char[] { ',', '，' }))
                    {
                        content.Append($"<img src='{yurl}{item}' >");
                        //content.Append($"<img src='{yurl.Replace("https://", "https://m-bnmanhua-com.mipcdn.com/i/")}{item}' >");
                    }
                    Chapter model = new Chapter
                    {
                        dInsertTime = DateTime.Now,
                        gkey = Guid.NewGuid(),
                        gNovelKey = novelKey,
                        iIndex = chapterIndex,
                        sText = content.ToString(),
                        sTitle = name[0].Groups[2].ToString().Replace("&nbsp;", "").Replace(">", "")
                    };
                    if (iChapBll.Add(model))
                    {
                        chapterIndex++;
                    }
                    else
                    {
                        novel.gErrorMag = $"添加章节出错";

                        return;
                    }
                    if (next == OverText)
                    {
                        novel.gErrorMag = $"已经采集到最新章节了";

                        return;
                    }
                    else
                    {
                        indexUrl = WebsiteUrl + next;
                    }
                }

            }
            catch (Exception ex)
            {
                novel.gErrorMag = $"采集出错：{ex.Message}";
            }
            finally
            {
                novel.gIndexAdd = indexUrl;
                iNovlBll.Update(novel);
            }
        }
    }
}
