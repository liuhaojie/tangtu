﻿using LIU.Common;
using LIU.Model.EBook;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LIU.CollectionService.Handle
{
    /// <summary>
    /// 36漫画网
    /// </summary>
    [Collection(WebsiteName = "36漫画网", WebsiteUrl = "https://www.36mh.com")]
    public class SLMHWCollection : CollectionBase, ICollectionHandle
    {
        public SLMHWCollection(Novel novel) : base(novel)
        {
            novelKey = novel.gkey;
        }

        //找到当前小说网站的id
        //访问 /comic/read/?id=2704 进入第一页
        //通过正则获取页面内容
        //获取下一页

        /// <summary>
        /// 下一章节信息正则
        /// </summary>
        private string RegpNextChapter = "nextChapterData[\\s]=[\\s]([{][\\w\\W][^}]+[}])";

        /// <summary>
        /// 当前章节图片地址
        /// </summary>
        private string RegChapterPath = "chapterPath[\\s]=[\\s][\"|']([\\w\\W][^\"|^']+)[\"|']";

        /// <summary>
        /// 章节图片集合
        /// </summary>
        private string RegChapterImages = "chapterImages[\\s]=[\\s]\\[([\"|'][\\s\\S]+[\"|'])]";

        /// <summary>
        /// 图片根地址
        /// </summary>
        private string RegPageImage = "pageImage[\\s]=[\\s][\"|'](http[s]?://[\\s\\S][^/]+[/]?)";

        /// <summary>
        /// 当前页地址
        /// </summary>
        private string RegPageUrl = "pageUrl[\\s]=[\\s][\"|']([\\w\\W][^\"|^']+)[\"|']";

        /// <summary>
        /// 标题
        /// </summary>
        private string RegTitle = "pageTitle[\\s]=[\\s][\"|']([\\w\\W][^_]+)";

        /// <summary>
        /// 小说主键
        /// </summary>
        private Guid novelKey;

        public void CollectionInfo(bool async)
        {
            if (string.IsNullOrWhiteSpace(novel.gIndexAdd) && string.IsNullOrWhiteSpace(novel.gStartAdd))
            {
                novel.gErrorMag = $"采集失败！当前地址或下一章地址为空";
                iNovlBll.Update(novel);
            }
            if (async)
            {
                Thread thread = new Thread(getValue);
                thread.Start();
            }
            else
                getValue();
        }
        string indexUrl = "";
        protected override void getValue()
        {
            try
            {
                chapterIndex = iChapBll.getMaxChapter(novel.gkey);
                string html = "";
                StringBuilder content = new StringBuilder(); ;
                string pageUrl = "";
                string ChapterPath = "";
                string PageImage = "";
                string chapterImages = "";
                string title = "";
                nextChapterData nextChapterData = new nextChapterData();
                indedxChapterData indedxChapterData = new indedxChapterData();
                indexUrl = string.IsNullOrWhiteSpace(novel.gIndexAdd) ? novel.gStartAdd : novel.gIndexAdd;
                if (chapterIndex > 1)
                {
                    //判断第一章是否存在
                    Chapter chapter = iChapBll.GetModel(p => p.gNovelKey == novelKey && p.iIndex == 1);
                    if (chapter != null)//存在第一章就先获取一次下一章的信息一次
                    {
                        html = HttpHelper.HttpGet(indexUrl, "", true);
                        nextChapterData = JsonConvert.DeserializeObject<nextChapterData>(getRegex(RegpNextChapter, html));
                        pageUrl = getRegex(RegPageUrl, html);

                        PageImage = getRegex(RegPageImage, html);
                        if (string.IsNullOrWhiteSpace(nextChapterData.id))
                        {
                            novel.gErrorMag = $"已经采集到最新章节了";
                            return;
                        }
                        //indexUrl = pageUrl + nextChapterData.id + ".html"; 会出现错误，多采集比楼采集好
                        chapterIndex++;
                    }
                }
                while (true)
                {
                    content.Clear();
                    html = HttpHelper.HttpGet(indexUrl, "", true);
                    nextChapterData = JsonConvert.DeserializeObject<nextChapterData>(getRegex(RegpNextChapter, html));
                    ChapterPath = getRegex(RegChapterPath, html);
                    chapterImages = getRegex(RegChapterImages, html);
                    if (string.IsNullOrWhiteSpace(pageUrl))
                        pageUrl = getRegex(RegPageUrl, html);
                    if (string.IsNullOrWhiteSpace(PageImage))
                        PageImage = getRegex(RegPageImage, html);
                    if (string.IsNullOrWhiteSpace(indedxChapterData.name))
                        title = getRegex(RegTitle, html);
                    else
                        title = indedxChapterData.name;
                    content.Append($"<h2> {title} </h2>");
                    foreach (var item in chapterImages.Replace("\"", "").Replace("'", "").Split(new char[] { ',', '，' }))
                    {
                        content.Append($"<img src='{PageImage}{ChapterPath}{item}' >");
                    }
                    Chapter model = new Chapter
                    {
                        dInsertTime = DateTime.Now,
                        gkey = Guid.NewGuid(),
                        gNovelKey = novelKey,
                        iIndex = chapterIndex,
                        sText = content.ToString(),
                        sTitle = title
                    };
                    if (iChapBll.Add(model))
                    {
                        indedxChapterData.id = nextChapterData.id;
                        indedxChapterData.comic_id = nextChapterData.comic_id;
                        indedxChapterData.comic_name = nextChapterData.comic_name;
                        indedxChapterData.is_end = nextChapterData.is_end;
                        indedxChapterData.name = nextChapterData.name;
                        chapterIndex++;
                    }
                    else
                    {
                        novel.gErrorMag = $"添加章节出错";

                        return;
                    }
                    if (string.IsNullOrWhiteSpace(nextChapterData.id))
                    {
                        novel.gErrorMag = $"已经采集到最新章节了";

                        return;
                    }
                    else
                    {
                        indexUrl = pageUrl + nextChapterData.id + ".html";
                    }
                }

            }
            catch (Exception ex)
            {
                novel.gErrorMag = $"采集出错：{ex.Message}";
            }
            finally
            {
                novel.gIndexAdd = indexUrl;
                iNovlBll.Update(novel);
            }
        }


        /// <summary>
        ///当前章节信息
        /// </summary>
        private class indedxChapterData
        {
            /// <summary>
            /// 章节id
            /// </summary>
            public string id { get; set; }

            /// <summary>
            /// 漫画id
            /// </summary>
            public string comic_id { get; set; }

            /// <summary>
            /// 漫画名称
            /// </summary>
            public string comic_name { get; set; }

            public string is_end { get; set; }

            /// <summary>
            /// 名称
            /// </summary>
            public string name { get; set; }
        }

        /// <summary>
        /// 下一章节信息
        /// </summary>
        private class nextChapterData
        {
            /// <summary>
            /// 章节id
            /// </summary>
            public string id { get; set; }

            /// <summary>
            /// 漫画id
            /// </summary>
            public string comic_id { get; set; }

            /// <summary>
            /// 漫画名称
            /// </summary>
            public string comic_name { get; set; }

            public string is_end { get; set; }

            /// <summary>
            /// 名称
            /// </summary>
            public string name { get; set; }
        }
    }
}
