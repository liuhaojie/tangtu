﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.CollectionService
{
    /// <summary>
    /// 采集处理接口
    /// </summary>
    public interface ICollectionHandle
    {
        /// <summary>
        /// 采集信息
        /// </summary>
        /// <param name="async">异步， true</param>
        void CollectionInfo(bool async = true);
    }
}
