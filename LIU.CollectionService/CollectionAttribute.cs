﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.CollectionService
{
    [AttributeUsage(AttributeTargets.Class)]
    public class CollectionAttribute : Attribute
    {
        /// <summary>
        /// 站点名称
        /// </summary>
        public string WebsiteName { get; set; }

        /// <summary>
        /// 站点地址
        /// </summary>
        public string WebsiteUrl { get; set; }
    }
}
