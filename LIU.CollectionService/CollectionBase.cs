﻿using LIU.BLL;
using LIU.Common;
using LIU.IBLL.EBook;
using LIU.Model.EBook;
using LIU.UnityFactory;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LIU.CollectionService
{
    public class CollectionBase
    {
        protected readonly Novel novel;
        protected readonly DbContext db;
        protected readonly CommitLog cl;
        protected readonly INovelBLL iNovlBll;
        protected readonly IChapterBLL iChapBll;
        public CollectionBase(Novel novel)
        {
            this.novel = novel;
            db = IOCFactory.GetIOCResolve<DbContext>();
            cl = new CommitLog(db);
            iNovlBll = IOCFactory.GetIOCResolve<INovelBLL>();
            iChapBll = IOCFactory.GetIOCResolve<IChapterBLL>();
            url = string.IsNullOrWhiteSpace(novel.gIndexAdd) ? novel.gStartAdd : novel.gIndexAdd;
        }

        /// <summary>
        /// 内容正则
        /// </summary>
        protected string RegContext;//内容正则
        /// <summary>
        /// 下一章地址正则
        /// </summary>
        protected string RegNext;//下一章地址正则
        /// <summary>
        /// 标题正则
        /// </summary>
        protected string RegTitle;//标题正则
        protected int chapterIndex = 1;//当前第几章       
        protected int sIndex = 1;//开始第几章
        protected int eIndex = 0;//结束第几章
        protected string result;//结果
        private string indexUrl;//单前访问地址

        protected string url;//文章地址
        protected string httpHerd;//地址头
        protected string overUrl;//结束地址

        #region 通用采集小说网站
        /// <summary>
        /// 获取值
        /// </summary>
        protected virtual void getValue()
        {
            try
            {
                chapterIndex = iChapBll.getMaxChapter(novel.gkey);
                while (!string.IsNullOrEmpty(url) && (chapterIndex <= eIndex || eIndex == 0))
                {
                    result = "Url:" + url + ";";
                    string html = HttpHelper.HttpGet(url, "");
                    indexUrl = url;
                    Chapter eb = new Chapter();//章节
                    url = "";//表示没有下一章了，如果下面没有对其赋值则退出循环
                    if (!string.IsNullOrEmpty(RegContext))//判断正则是否存在 文章内容
                    {
                        string content = getRegex(RegContext, html);
                        if (string.IsNullOrEmpty(content))//判断是否有匹配的内容
                        {
                            result += string.Format(@"第【{0}】章内容获取失败;", chapterIndex);
                        }
                        else
                        {
                            eb.sText = content;
                            result += string.Format(@"第【{0}】章内容获取成功;", chapterIndex);
                        }
                    }
                    else
                    {
                        result += "内容正则不存在,无法获取内容;";
                    }

                    if (!string.IsNullOrEmpty(RegTitle))//判断正则是否存在 文章标题
                    {
                        string content = getRegex(RegTitle, html);
                        if (string.IsNullOrEmpty(content))//判断是否有匹配的内容
                        {
                            result += string.Format(@"第【{0}】章标题获取失败;", chapterIndex);
                        }
                        else
                        {
                            eb.sTitle = content;
                            result += string.Format(@"第【{0}】章标题获取成功;", chapterIndex);
                        }
                    }
                    else
                    {
                        result += "标题正则不存在,无法获取标题;";
                    }

                    if (!string.IsNullOrEmpty(RegNext))//判断正则是否存在 下一章地址
                    {
                        string content = getRegex(RegNext, html);
                        if (string.IsNullOrEmpty(content))//判断是否有匹配的内容
                        {
                            result += "获取完成";
                        }
                        else
                        {
                            if (url == overUrl)
                            {
                                result += "获取完成";
                                return;
                            }
                            result += @"下一章地址获取成功";
                            if (content.Substring(0, 4) != "http")
                                url = httpHerd + content;
                            else
                                url = content;
                        }
                    }
                    else
                    {
                        result += "下一章正则不存在,无法获取下一章地址";
                    }
                    if (string.IsNullOrEmpty(eb.sText))
                        break;
                    eb.gkey = Guid.NewGuid();
                    eb.gNovelKey = novel.gkey;
                    eb.iIndex = chapterIndex;
                    eb.dInsertTime = DateTime.Now;
                    eb.gInsertUserKey = Guid.Empty;
                    iChapBll.Add(eb);
                    chapterIndex++;
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("远程服务器返回错误: (404) 未找到"))
                    result = "已经采集到最新章节";
                result = ex.Message;
            }
            finally
            {
                novel.gIndexAdd = indexUrl;
                novel.gErrorMag = $"采集完成！{result}";
                iNovlBll.Update(novel);
            }
        }

        #endregion

        /// <summary>
        /// 获取正则结果
        /// </summary>
        /// <param name="regText">正则表达式</param>
        /// <param name="content">待匹配内容</param>
        /// <returns></returns>
        protected string getRegex(string regText, string content)
        {
            Regex reg = new Regex(regText, RegexOptions.IgnoreCase);
            MatchCollection matchs = reg.Matches(content);
            return matchs[0].Groups[1].ToString();
        }


        /// <summary>
        /// 获取正则结果
        /// </summary>
        /// <param name="regText">正则表达式</param>
        /// <param name="content">待匹配内容</param>
        /// <returns></returns>
        protected MatchCollection getMatchCollection(string regText, string content)
        {
            Regex reg = new Regex(regText, RegexOptions.IgnoreCase);
            return reg.Matches(content);
        }
    }
}
