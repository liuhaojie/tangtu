﻿using LIU.Model.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LIU.GoodsService
{
    public class GoodsFactory
    {
        /// <summary>
        /// 物品处理程序静态缓存
        /// </summary>
        private static Type[] types = null;

        /// <summary>
        /// 创建对象
        /// </summary>
        /// <param name="goods"></param>
        /// <returns></returns>
        public static IGoodsHandle CreateIGoodsHandle(Goods goods)
        {
            if (types == null || types.Length == 0)
                types = Assembly.Load("LIU.GoodsService").GetTypes().Where(p => p.GetInterfaces().Contains(typeof(IGoodsHandle))).ToArray();
            //types = AppDomain.CurrentDomain.GetAssemblies().SelectMany(a => a.GetTypes().Where(t => t.GetInterfaces().Contains(typeof(IGoodsHandle)))).ToArray();
            foreach (var item in types)
            {
                var att = item.GetCustomAttribute<GoodsAttribute>();
                //var att = (GoodsAttribute)Attribute.GetCustomAttribute(item, typeof(GoodsAttribute));
                if (att.Classify == goods.sClassify || att.Name == goods.sName)
                {
                    return Activator.CreateInstance(item, goods) as IGoodsHandle;
                }
            }
            throw new Exception("找不到物品对应的处理程序");
        }
    }
}
