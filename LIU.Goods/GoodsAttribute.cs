﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.GoodsService
{
    [AttributeUsage(AttributeTargets.Class)]
    public class GoodsAttribute : Attribute
    {
        /// <summary>
        /// 物品名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 物品分类
        /// </summary>
        public string Classify { get; set; }
    }
}
