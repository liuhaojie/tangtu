﻿using LIU.Model.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.GoodsService
{
    /// <summary>
    /// 物品处理接口
    /// </summary>
    public interface IGoodsHandle
    {
        /// <summary>
        /// 使用物品
        /// </summary>
        /// <param name="ui">使用人</param>
        /// <param name="userGoods">用户物品</param>
        /// <returns></returns>
        bool UseGoods(UserInfo ui, List<UserGoods> userGoods);
    }
}
