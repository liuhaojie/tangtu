﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LIU.Common;
using LIU.IDAL.System;
using LIU.Model.System;
using LIU.UnityFactory;

namespace LIU.GoodsService.Handle
{
    [Goods(Classify = "VIP")]
    public class VIPGoods : GoodsBase, IGoodsHandle
    {

        /// <summary>
        /// 用户信息
        /// </summary>
        private readonly IUserInfoDAL iurDAL = IOCFactory.GetIOCResolve<IUserInfoDAL>();
        /// <summary>
        /// 用户物品
        /// </summary>
        private readonly IUserGoodsDAL iugDAL = IOCFactory.GetIOCResolve<IUserGoodsDAL>();
        /// <summary>
        /// 物品
        /// </summary>
        private readonly IGoodsDAL igDAL = IOCFactory.GetIOCResolve<IGoodsDAL>();
        /// <summary>
        /// vip冻结表
        /// </summary>
        private readonly IVIPHistoryDAL ivipDAL = IOCFactory.GetIOCResolve<IVIPHistoryDAL>();

        public VIPGoods(Goods goods) : base(goods)
        {

        }
        /// <summary>
        /// 使用物品
        /// </summary>
        /// <param name="ui">使用人</param>
        /// <param name="userGoods">用户物品</param>
        /// <returns></returns>
        public bool UseGoods(UserInfo ui, List<UserGoods> userGoods)
        {
            try
            {
                //1.vip等级相同或没有vip的情况下直接增加天数
                //2.增加的vip等级比当前的高，把当前vip剩余天数封存起来，添加新的vip等级
                //3.增加的vip等级比当前的低，不可以兑换
                int day = Convert.ToInt32(goods.sValue2) * userGoods.Count;
                int level = Convert.ToInt32(goods.sValue1);
                if (ui.iLevel == 0)
                {
                    ui.iLevel = level;
                    ui.dLevelTIme = DateTime.Now.AddDays(day);
                }
                else if (ui.iLevel == level)
                {
                    ui.dLevelTIme = Convert.ToDateTime(ui.dLevelTIme).AddDays(day);
                }
                else if (ui.iLevel < level)
                {
                    VIPHistory vip = new VIPHistory()
                    {
                        iLevel = ui.iLevel,
                        dCreateTime = DateTime.Now,
                        gkey = Guid.NewGuid(),
                        gUserKey = ui.gkey,
                        iDay = (Convert.ToDateTime(ui.dLevelTIme) - DateTime.Now).Days
                    };
                    ivipDAL.Add(vip);
                    ui.iLevel = level;
                    ui.dLevelTIme = DateTime.Now.AddDays(day);
                }
                else
                {
                    return false;
                }
                //修改物品已使用
                foreach (var item in userGoods)
                {
                    item.iFlag = 1;
                    iugDAL.Update(item);
                }
                iurDAL.Update(ui);
                return cl.Commit() > 0;
            }
            catch (Exception ex)
            {
                LogHelper.Error($"【道具错误，请联系管理员】{ex.Message}");
                throw new Exception("道具错误，请联系管理员！");
            }
        }
    }
}
