﻿using LIU.BLL;
using LIU.Model.System;
using LIU.UnityFactory;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.GoodsService
{
    public class GoodsBase
    {
        protected readonly Goods goods;
        protected DbContext db;
        protected CommitLog cl;
        public GoodsBase(Goods goods)
        {
            this.goods = goods;
            db = IOCFactory.GetIOCResolve<DbContext>();
            cl = new CommitLog(db);
        }
    }
}
