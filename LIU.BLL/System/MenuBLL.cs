using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LIU.Model;
using LIU.IBLL.System;
using System.Data.Entity;
using LIU.IDAL.System;
using LIU.Model.System;
using LIU.UnityFactory;
using LIU.Common;

namespace LIU.BLL.System
{
    public class MenuBLL : BaseBLL<Menu>, IMenuBLL
    {
        private readonly IRoleMenuDAL irmDAL = IOCFactory.GetIOCResolve<IRoleMenuDAL>();
        //private IMenuDAL iDal;
        public MenuBLL(DbContext db, IMenuDAL iDal) : base(db, iDal)
        {
            this.iDal = iDal;
        }

        /// <summary>
        /// 做级联删除 删除菜单对应的删除 角色权限的菜单
        /// </summary>
        /// <param name="glist">菜单主键组</param>
        /// <returns></returns>
        public bool CascadeDel(List<Guid> glist)
        {
            iDal.Delete(p => glist.Contains(p.gKey));
            irmDAL.Delete(p => glist.Contains(p.gMenuKey));
            //return db.SaveChanges() > 0;
            return cl.Commit() > 0;
        }

        /// <summary>
        /// 获取树状菜单
        /// </summary>
        /// <returns></returns>
        public List<MenuTreeHelper> GetMenuTreeList()
        {
            List<MenuTreeHelper> MTHList = new List<MenuTreeHelper>();
            //获取父菜单
            List<Menu> mlist = GetList(p => p.gParentKey == Guid.Empty && p.iType != 3);
            for (int i = 0; i < mlist.Count; i++)
            {
                MenuTreeHelper mth = new MenuTreeHelper();
                mth.id = mlist[i].gKey.ToString();
                mth.name = mlist[i].sName;
                mth.spread = false;
                mth.href = "";
                mth.icon = mlist[i].sIcon;
                mth.shref= mlist[i].sHref;
                mth.children = GetChildrenList(mlist[i].gKey);
                MTHList.Add(mth);
            }
            return MTHList;
        }

        /// <summary>
        /// 获取子菜单
        /// </summary>
        /// <param name="gParentKey"></param>
        /// <returns></returns>
        private List<MenuTreeHelper> GetChildrenList(Guid gParentKey)
        {
            List<MenuTreeHelper> MTHList = new List<MenuTreeHelper>();
            List<Menu> mlist = GetList(p => p.gParentKey == gParentKey && p.iType != 3);
            for (int i = 0; i < mlist.Count; i++)
            {
                MenuTreeHelper mth = new MenuTreeHelper();
                mth.id = mlist[i].gKey.ToString();
                mth.name = mlist[i].sName;
                mth.spread = false;
                mth.href = "";
                mth.icon = mlist[i].sIcon;
                mth.shref = mlist[i].sHref;
                mth.children = GetChildrenList(mlist[i].gKey);
                MTHList.Add(mth);
            }
            return MTHList;
        }
    }
}

