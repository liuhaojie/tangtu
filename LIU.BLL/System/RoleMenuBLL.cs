using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LIU.Model;
using LIU.IBLL.System;
using System.Data.Entity;
using LIU.IDAL.System;
using LIU.Model.System;

namespace LIU.BLL.System
{
    public class RoleMenuBLL:BaseBLL<RoleMenu>,IRoleMenuBLL
    {
        //private IRoleMenuDAL iDal;
        public RoleMenuBLL(DbContext db,IRoleMenuDAL iDal) : base(db, iDal)
        {
            this.iDal = iDal;
        }

        /// <summary>
        /// 配置角色菜单的时候 事务级别 删除添加
        /// </summary>
        /// <param name="groleKey">角色主键</param>
        /// <param name="list">要添加的数组</param>
        /// <returns></returns>
        public bool DelAndAdd(Guid groleKey, List<RoleMenu> list)
        {
            iDal.Delete(p => p.gRoleKey == groleKey);
            foreach (RoleMenu item in list)
            {
                iDal.Add(item);
            }
            return cl.Commit() > 0;
        }
    }
}

