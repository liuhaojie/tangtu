using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LIU.Model;
using LIU.IBLL.System;
using System.Data.Entity; 
using LIU.IDAL.System;
using LIU.Model.System;

namespace LIU.BLL.System
{
    public class MessageBoardBLL:BaseBLL<MessageBoard>,IMessageBoardBLL
    {
        //private IMessageBoardDAL iDal;
        public MessageBoardBLL(DbContext db,IMessageBoardDAL iDal) : base(db, iDal)
        {
            this.iDal = iDal;
        }
    }
}

