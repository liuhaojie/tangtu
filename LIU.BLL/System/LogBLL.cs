using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LIU.Model;
using LIU.IBLL.System;
using System.Data.Entity;
using LIU.IDAL.System;
using LIU.Model.System;
using LIU.UnityFactory;
using LIU.Common;

namespace LIU.BLL.System
{
    public class LogBLL : BaseBLL<Log>, ILogBLL
    {
        //private ILogDAL iDal;
        public LogBLL(DbContext db, ILogDAL iDal) : base(db, iDal)
        {
            this.iDal = iDal;
        }

    }
}

