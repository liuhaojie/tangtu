﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LIU.Model;
using LIU.IBLL.System;
using System.Data.Entity;
using LIU.IDAL.System;
using LIU.Model.System;
using LIU.UnityFactory;

namespace LIU.BLL.System
{
    public class UserInfoBLL : BaseBLL<UserInfo>, IUserInfoBLL
    {
        IVIPHistoryDAL VIPHistoryDAL = IOCFactory.GetIOCResolve<IVIPHistoryDAL>();
        //private IUserInfoDAL iDal;
        public UserInfoBLL(DbContext db, IUserInfoDAL iDal) : base(db, iDal)
        {
            this.iDal = iDal;
        }


        /// <summary>
        /// 更新删除历史vip
        /// </summary>
        /// <param name="model"></param>
        /// <param name="Vip"></param>
        /// <returns></returns>
        public bool Update(UserInfo model, VIPHistory Vip)
        {
            iDal.Update(model);
            if (Vip != null)
                VIPHistoryDAL.Delete(Vip);
            return cl.Commit() > 0;
        }
    }
}
