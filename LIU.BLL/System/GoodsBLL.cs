﻿using LIU.IBLL.System;
using LIU.IDAL.System;
using LIU.Model.System;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.BLL.System
{
    public class GoodsBLL : BaseBLL<Goods>, IGoodsBLL
    {
        public GoodsBLL(DbContext db, IGoodsDAL iDal) : base(db, iDal)
        {
        }

        /// <summary>
        /// SqlBulkCopy方式导入数据库
        /// </summary>
        /// <param name="list"></param>
        public void ImportSqlBulkCopy(List<Goods> list)
        {
            DataTable dt = GetDataTablestruct();
            dt.TableName = "sys_goods";
            foreach (var item in list)
            {
                var newRow = dt.NewRow();
                newRow["sName"] = item.sName;
                newRow["sExplain"] = item.sExplain;
                newRow["sClassify"] = item.sClassify;
                newRow["iSort"] = item.iSort;
                newRow["sValue1"] = item.sValue1;
                newRow["sValue2"] = item.sValue2;
                newRow["iOldPrice"] = item.iOldPrice;
                newRow["iNewPrice"] = item.iNewPrice;
                newRow["sRestrictionsType"] = item.sRestrictionsType;
                newRow["iRestrictions"] = item.iRestrictions;
                newRow["dCreateTime"] = item.dCreateTime;
                newRow["sHref"] = item.sHref;
                newRow["iType"] = item.iType;
                dt.Rows.Add(newRow);
            }
            iDal.SqlBulkCopy(new DataTable[] { dt });
        }

        /// <summary>
        /// 获取空表结构
        /// </summary>
        /// <returns></returns>
        private DataTable GetDataTablestruct()
        {
            return iDal.SqlQueryDataTable("SELECT * FROM dbo.sys_goods WHERE 1 <> 1");
        }
    }
}
