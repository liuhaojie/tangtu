﻿using LIU.IBLL.System;
using LIU.IDAL.System;
using LIU.Model.System;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.BLL.System
{
    public class HeadFrameBLL : BaseBLL<HeadFrame>, IHeadFrameBLL
    {
        public HeadFrameBLL(DbContext db, IHeadFrameDAL iDal) : base(db, iDal)
        {
            this.iDal = iDal;
        }
    }
}
