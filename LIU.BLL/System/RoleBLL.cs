using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LIU.Model;
using LIU.IBLL.System;
using System.Data.Entity;
using LIU.IDAL.System;
using LIU.Model.System;
using LIU.UnityFactory;

namespace LIU.BLL.System
{
    public class RoleBLL:BaseBLL<Role>,IRoleBLL
    {
        private readonly IRoleMenuDAL irmDAL = IOCFactory.GetIOCResolve<IRoleMenuDAL>();
        //private IRoleDAL iDal;
        public RoleBLL(DbContext db,IRoleDAL iDal) : base(db, iDal)
        {
            this.iDal = iDal;
        }

        /// <summary>
        /// 做级联删除 删除角色对应的删除 角色权限的菜单
        /// </summary>
        /// <param name="glist">角色主键组</param>
        /// <returns></returns>
        public bool CascadeDel(List<Guid> glist)
        {
            iDal.Delete(p => glist.Contains(p.gkey));
            irmDAL.Delete(p => glist.Contains(p.gRoleKey));
            //return db.SaveChanges() > 0;
            return cl.Commit() > 0;
        }
    }
}

