using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LIU.Model;
using LIU.IBLL.System;
using System.Data.Entity;
using LIU.IDAL.System;
using LIU.Model.System;

namespace LIU.BLL.System
{
    public class DicInfoBLL:BaseBLL<DicInfo>,IDicInfoBLL
    {
        //private IDicInfoDAL iDal;
        public DicInfoBLL(DbContext db,IDicInfoDAL iDal) : base(db, iDal)
        {
            this.iDal = iDal;
        }

        /// <summary>
        /// �����ֵ�Ŀ¼�䶯���� (��������)
        /// </summary>
        /// <param name="dicInfo">��Ҫ���µ��ֵ�Ŀ¼</param>
        /// <param name="oldSotr">�ɵ�����</param>
        /// <returns></returns>
        public bool updateOfSort(DicInfo dicInfo, int oldSotr)
        {
            if (dicInfo.iSort == oldSotr)//�ж������Ƿ�ı�
                return Update(dicInfo);
            DicInfo di = GetModel(p => p.iSort == dicInfo.iSort&&p.gDicCatalogKey==dicInfo.gDicCatalogKey);
            if (di == null)//�ж������Ƿ����
                return Update(dicInfo);
            di.iSort = oldSotr;
            iDal.Update(dicInfo);
            iDal.Update(di);
            //return db.SaveChanges() > 0;
            return cl.Commit() > 0;
        }
    }
}

