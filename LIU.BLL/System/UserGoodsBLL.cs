﻿using LIU.IBLL.System;
using LIU.IDAL.System;
using LIU.Model.System;
using LIU.UnityFactory;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.BLL.System
{
    public class UserGoodsBLL : BaseBLL<UserGoods>, IUserGoodsBLL
    {
        /// <summary>
        /// 物品
        /// </summary>
        private readonly IGoodsDAL irmDAL = IOCFactory.GetIOCResolve<IGoodsDAL>();
        /// <summary>
        /// 用户
        /// </summary>
        private readonly IUserInfoDAL iuDAL = IOCFactory.GetIOCResolve<IUserInfoDAL>();
        /// <summary>
        /// 金币流水表
        /// </summary>
        private readonly IGoldFlowDAL igfDAL = IOCFactory.GetIOCResolve<IGoldFlowDAL>();

        public UserGoodsBLL(DbContext db, IUserGoodsDAL iDal) : base(db, iDal)
        {
        }

        /// <summary>
        /// 买物品
        /// </summary>
        /// <param name="user">用户信息</param>
        /// <param name="goods">物品</param>
        /// <param name="sGoodsFrom">物品来源</param>
        /// <param name="buyCount">购买数量</param>
        /// <param name="msg">返回消息</param>
        /// <returns></returns>
        public bool Buy(UserInfo user, Goods goods, out string msg, string sGoodsFrom = "购买", int buyCount = 1)
        {
            int id = goods.ID;
            Guid gUertkey = user.gkey;
            int count = 0;
            DateTime sdt = DateTime.Now;
            if (goods.iType != 0)
            {
                msg = "该物品不可以购买";
                return false;
            }
            if (user.iLevel > 0 && user.dLevelTIme < DateTime.Now)
            {
                msg = "当前VIP等级已经过期，请重新登录";
                return false;
            }

            //先判断是否可以购买
            switch (goods.sRestrictionsType.ToLower())
            {
                case "all":
                    break;
                case "only":
                    count = GetRowCount(p => p.gUserKey == gUertkey && p.GoodsID == id);
                    if ((buyCount + count) > goods.iRestrictions)//需要购买 + 已购买 大于 限购 则不可以购买
                    {
                        msg = $"此物品当前只能购买{goods.iRestrictions - count}次";
                        return false;
                    }
                    break;
                case "day":
                    sdt = DateTime.Now.Date;
                    count = GetRowCount(p => p.gUserKey == gUertkey && p.GoodsID == id && p.dCreateTime >= sdt);
                    if ((buyCount + count) > goods.iRestrictions)//需要购买 + 已购买 大于 限购 则不可以购买
                    {
                        msg = $"此物品当前只能购买{goods.iRestrictions - count}次";
                        return false;
                    }
                    break;
                case "week":
                    int cha = 0;
                    switch (DateTime.Now.DayOfWeek)
                    {
                        case DayOfWeek.Sunday:
                            cha = 6;
                            break;
                        default:
                            cha = (int)DateTime.Now.DayOfWeek - 1;
                            break;
                    }
                    sdt = DateTime.Now.Date.AddDays(-cha);
                    count = GetRowCount(p => p.gUserKey == gUertkey && p.GoodsID == id && p.dCreateTime >= sdt);
                    if ((buyCount + count) > goods.iRestrictions)//需要购买 + 已购买 大于 限购 则不可以购买
                    {
                        msg = $"此物品当前只能购买{goods.iRestrictions - count}次";
                        return false;
                    }
                    break;
                case "month":
                    sdt = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM"));
                    count = GetRowCount(p => p.gUserKey == gUertkey && p.GoodsID == id && p.dCreateTime >= sdt);
                    if ((buyCount + count) > goods.iRestrictions)//需要购买 + 已购买 大于 限购 则不可以购买
                    {
                        msg = $"此物品当前只能购买{goods.iRestrictions - count}次";
                        return false;
                    }
                    break;
                case "year":
                    sdt = Convert.ToDateTime(DateTime.Now.ToString("yyyy"));
                    count = GetRowCount(p => p.gUserKey == gUertkey && p.GoodsID == id && p.dCreateTime >= sdt);
                    if ((buyCount + count) > goods.iRestrictions)//需要购买 + 已购买 大于 限购 则不可以购买
                    {
                        msg = $"此物品当前只能购买{goods.iRestrictions - count}次";
                        return false;
                    }
                    break;
                default:
                    msg = "不存在的限购类型";
                    return false;
            }
            //判断金币是否充足
            int needGold = goods.iNewPrice * buyCount;
            switch (user.iLevel)
            {
                case 1:
                case 2:
                    needGold = (int)(needGold * 0.95);
                    break;
                case 3:
                case 4:
                case 5:
                    needGold = (int)(needGold * 0.9);
                    break;
                case 6:
                case 7:
                    needGold = (int)(needGold * 0.85);
                    break;
                case 8:
                case 9:
                    needGold = (int)(needGold * 0.7);
                    break;
            }
            if (needGold > user.iGold)
            {
                msg = "金币不足,无法购买";
                return false;
            }

            GoldFlow gf = new GoldFlow()
            {
                dTime = DateTime.Now,
                gUserKey = user.gkey,
                iFlag = 2,
                iGold = needGold,
                iNewGold = user.iGold - needGold,
                iOldGold = user.iGold,
                iType = 3,
                sMemo = $"购买物品【{goods.sName}】×{buyCount}"
            };
            igfDAL.Add(gf);

            user.iGold = user.iGold - needGold;

            iuDAL.Update(user);
            for (int i = 0; i < buyCount; i++)
            {
                iDal.Add(new UserGoods()
                {
                    gkey = Guid.NewGuid(),
                    sGoodsFrom = sGoodsFrom,
                    dCreateTime = DateTime.Now,
                    GoodsID = goods.ID,
                    gUserKey = user.gkey,
                    iFlag = 0
                });
            }
            if (cl.Commit() > 0)
            {
                msg = "购买成功";
                return true;
            }
            else
            {
                msg = "购买失败";
                return false;
            }
        }

        /// <summary>
        /// 出售物品
        /// </summary>
        /// <param name="user">用户信息</param>
        /// <param name="goods">物品</param>
        /// <param name="userGoods">用户物品</param>
        /// <returns></returns>
        public bool Out(UserInfo user, Goods goods, List<UserGoods> userGoods)
        {
            int allCount = (int)(goods.iNewPrice * 0.5 * userGoods.Count);//全部兑换的金币
            GoldFlow gf = new GoldFlow()
            {
                dTime = DateTime.Now,
                gUserKey = user.gkey,
                iFlag = 1,
                iGold = allCount,
                iNewGold = user.iGold + allCount,
                iOldGold = user.iGold,
                iType = 4,
                sMemo = $"出售物品【{goods.sName}】×{userGoods.Count}"
            };
            igfDAL.Add(gf);
            user.iGold = user.iGold + allCount;
            foreach (var item in userGoods)
            {
                item.iFlag = 2;
                iDal.Update(item);
            }
            iuDAL.Update(user);
            return cl.Commit() > 0;

        }

    }
}
