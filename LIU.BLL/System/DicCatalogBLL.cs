using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LIU.Model;
using LIU.IBLL.System;
using System.Data.Entity;
using LIU.IDAL.System;
using LIU.Model.System;
using LIU.UnityFactory;

namespace LIU.BLL.System
{
    public class DicCatalogBLL : BaseBLL<DicCatalog>, IDicCatalogBLL
    {

        //private IDicCatalogDAL iDal;
        public DicCatalogBLL(DbContext db, IDicCatalogDAL iDal) : base(db, iDal)
        {
            this.iDal = iDal;
        }

        /// <summary>
        /// ����ɾ���ֵ���Ϣ
        /// </summary>
        /// <param name="gkey">�ֵ�Ŀ¼����</param>
        /// <returns></returns>
        public bool DeleteDicAll(Guid gkey)
        {
            IDicInfoDAL idiDAL = IOCFactory.GetIOCResolve<IDicInfoDAL>();
            idiDAL.Delete(p => p.gDicCatalogKey == gkey);
            iDal.Delete(p => p.gkey == gkey);
            //return db.SaveChanges() > 0;
            return cl.Commit() > 0;
        }

        /// <summary>
        /// �����ֵ�Ŀ¼�䶯���� (��������)
        /// </summary>
        /// <param name="dicCatalog">��Ҫ���µ��ֵ�Ŀ¼</param>
        /// <param name="oldSotr">�ɵ�����</param>
        /// <returns></returns>
        public bool updateOfSort(DicCatalog dicCatalog,int oldSotr)
        {
            if (dicCatalog.iSort == oldSotr)//�ж������Ƿ�ı�
                return Update(dicCatalog);
            DicCatalog catalog = GetModel(p => p.iSort == dicCatalog.iSort);
            if(catalog==null)//�ж������Ƿ����
                return Update(dicCatalog);
            catalog.iSort = oldSotr;
            iDal.Update(dicCatalog);
            iDal.Update(catalog);
            //return db.SaveChanges()>0;
            return cl.Commit() > 0;
        }
    }
}

