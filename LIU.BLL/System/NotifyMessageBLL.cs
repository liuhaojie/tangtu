using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LIU.Model;
using LIU.IBLL.System;
using System.Data.Entity;
using LIU.IDAL.System;
using LIU.Model.System;
using LIU.Common;
using LIU.Model.DTOModel;
using LIU.UnityFactory;

namespace LIU.BLL.System
{
    public class NotifyMessageBLL : BaseBLL<NotifyMessage>, INotifyMessageBLL
    {
        /// <summary>
        /// 物品
        /// </summary>
        private readonly IGoodsDAL igDAL = IOCFactory.GetIOCResolve<IGoodsDAL>();

        /// <summary>
        /// 用户物品
        /// </summary>
        private readonly IUserGoodsBLL iugDAL = IOCFactory.GetIOCResolve<IUserGoodsBLL>();
        /// <summary>
        /// 用户
        /// </summary>
        private readonly IUserInfoDAL iuDAL = IOCFactory.GetIOCResolve<IUserInfoDAL>();
        /// <summary>
        /// 金币流水表
        /// </summary>
        private readonly IGoldFlowDAL igfDAL = IOCFactory.GetIOCResolve<IGoldFlowDAL>();
        //private INotifyMessageDAL iDal;
        public NotifyMessageBLL(DbContext db, INotifyMessageDAL iDal) : base(db, iDal)
        {
            this.iDal = iDal;
        }

        /// <summary>
        /// 领取邮件中的物品
        /// </summary>
        /// <param name="userInfo">用户</param>
        /// <param name="notifyMessages">邮件</param>
        /// <returns></returns>
        public bool ReceiveGoods(UserInfo userInfo, List<NotifyMessage> notifyMessages)
        {
            int addGold = 0;
            foreach (var item in notifyMessages)
            {
                List<EmailGoodsDTO> emailGoods = JsonHelper.getObject<List<EmailGoodsDTO>>(item.sHref);
                if (emailGoods.Where(p => p.goodsID == 0).FirstOrDefault() != null)//判断是否含有金币
                    addGold += emailGoods.Where(p => p.goodsID == 0).FirstOrDefault().count;
                emailGoods = emailGoods.Where(p => p.goodsID != 0).ToList();
                foreach (var dto in emailGoods)//循环添加用户物品
                {
                    for (int i = 0; i < dto.count; i++)
                    {
                        iugDAL.Add(new UserGoods
                        {
                            gkey = Guid.NewGuid(),
                            GoodsID = dto.goodsID,
                            dCreateTime = DateTime.Now,
                            gUserKey = userInfo.gkey,
                            iFlag = 0,
                            sGoodsFrom = "领取邮件"
                        });
                    }
                }
                //对邮件物品标记以兑换
                item.iFlag = 3;
                item.dReadDate = DateTime.Now;
                iDal.Update(item);
            }
            if (addGold > 0)
            { 
                igfDAL.Add(new GoldFlow()
                {
                    dTime = DateTime.Now,
                    gUserKey = userInfo.gkey,
                    iFlag = 1,
                    iGold = addGold,
                    iNewGold = userInfo.iGold + addGold,
                    iOldGold = userInfo.iGold,
                    iType = 4,
                    sMemo = $"批量领取邮件"
                });
                userInfo.iGold += addGold;
                iuDAL.Update(userInfo);
            }
            return cl.Commit() > 0;
        }
    }
}

