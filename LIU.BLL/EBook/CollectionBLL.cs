using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LIU.Model;
using LIU.IBLL.EBook;
using System.Data.Entity;
using LIU.IDAL.EBook;
using LIU.Model.EBook;

namespace LIU.BLL.EBook
{
    public class CollectionBLL:BaseBLL<Collection>,ICollectionBLL
    {
        //private ICollectionDAL iDal;
        public CollectionBLL(DbContext db,ICollectionDAL iDal) : base(db, iDal)
        {
            this.iDal = iDal;
        }
    }
}

