using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LIU.Model;
using LIU.IBLL.EBook;
using System.Data.Entity;
using LIU.IDAL.EBook;
using LIU.Model.EBook;
using LIU.Model.DTOModel;

namespace LIU.BLL.EBook
{
    public class ChapterBLL : BaseBLL<Chapter>, IChapterBLL
    {
        private new IChapterDAL iDal;
        public ChapterBLL(DbContext db, IChapterDAL iDal) : base(db, iDal)
        {
            this.iDal = iDal;
        }

        /// <summary>
        /// 获取已经采集到最大的章节
        /// </summary>
        /// <param name="gNovelKey">小说主键</param>
        /// <returns></returns>
        public int getMaxChapter(Guid gNovelKey)
        {
            return iDal.getMaxChapter(gNovelKey);
        }

        /// <summary>
        /// 获取首页显示的章节
        /// </summary>
        /// <returns></returns>
        public List<HomeModel> GetHomeModel()
        {
            //取最近更新的3本小说最新4章
            string sql = @"SELECT d.sName,c.* FROM (
                            SELECT a.*, ROW_NUMBER() OVER(PARTITION BY a.gNovelKey  ORDER BY iIndex DESC) AS rowId FROM dbo.eb_chapter AS a
                            RIGHT JOIN (SELECT TOP 3 gNovelKey FROM dbo.eb_chapter GROUP BY gNovelKey ORDER BY MAX(dInsertTime) DESC) AS b ON  a.gNovelKey=b.gNovelKey)
                             AS c LEFT JOIN dbo.eb_novel AS d ON c.gNovelKey=d.gkey
                            WHERE c.rowId<5 ";
            return iDal.ExcetProc<HomeModel>(sql);
        }
    }
}

