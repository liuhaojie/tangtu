using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LIU.Model;
using LIU.IBLL.EBook;
using System.Data.Entity;
using LIU.IDAL.EBook;
using LIU.Model.EBook;
using LIU.UnityFactory;

namespace LIU.BLL.EBook
{
    public class NovelBLL : BaseBLL<Novel>, INovelBLL
    {

        //private INovelDAL iDal;
        public NovelBLL(DbContext db, INovelDAL iDal) : base(db, iDal)
        {
            this.iDal = iDal;
        }

        /// <summary>
        /// 联合删除 小说章节 小说配置
        /// </summary>
        /// <param name="gkey">小说主键</param>
        /// <param name="model">小说实体</param>
        /// <returns></returns>
        public bool UnionDelete(Guid gkey, Novel model)
        {
            //配置
            ICollectionDAL iColllDAL = IOCFactory.GetIOCResolve<ICollectionDAL>();
            //章节
            IChapterDAL iChapDAL = IOCFactory.GetIOCResolve<IChapterDAL>();
            iColllDAL.Delete(p => p.gNovelKey == gkey);
            iChapDAL.Delete(p => p.gNovelKey == gkey);
            iDal.Delete(model);
            //return db.SaveChanges() > 0;
            return cl.Commit() > 0;

            //List<Collection> collList = iColllBll.GetList(p => p.gNovelKey == gkey);
            //if (collList.Count() > 0)//判断是否有 小说配置
            //{
            //    if (!iColllBll.Delete(collList))
            //    {
            //        return false;
            //    }
            //}
            //List<Chapter> chapList = iChapBll.GetList(p => p.gNovelKey == gkey);
            //if (chapList.Count() > 0)//判断是否有 小说章节
            //{
            //    if (!iChapBll.Delete(chapList))
            //    {
            //        return false;
            //    }
            //}
            //if (!Delete(model))
            //{
            //    return false;
            //}
            //return true;
        }
    }
}

