﻿using LIU.Common;
using LIU.IBLL.Bill;
using LIU.IDAL.Bill;
using LIU.Model.Bill;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.BLL.Bill
{
    public class DetailBLL : BaseBLL<Detail>, IDetailBLL
    {
        public DetailBLL(DbContext db, IDetailDAL iDal) : base(db, iDal)
        {
        }


        /// <summary>
        /// 获取当前查询条件下的统计数据
        /// </summary>
        /// <param name="income">账单归属人员</param>
        /// <param name="income">支出收入 1收入，2支出，3资产转移</param>
        /// <param name="iTransactionType">交易类型</param>
        /// <param name="iTransactionMode">交易方式</param>
        /// <param name="sTransactionObject">交易对象</param>
        /// <param name="dsTransactionTime">交易时间开始</param>
        /// <param name="deTransactionTime">交易时间结束</param>
        /// <returns></returns>
        public string GetHeJi(Guid gkey,int? income, int? iTransactionType, int? iTransactionMode, string sTransactionObject,
            DateTime? dsTransactionTime, DateTime? deTransactionTime)
        {
            string sql = "SELECT income,SUM(dAmount) FROM dbo.bl_detail where gBelongKey=@gkey ";
            List<SqlParameter> parms = new List<SqlParameter>();
            parms.Add(new SqlParameter("@gkey", gkey));
            if (income != null)
            {
                sql += " and income=@income";
                parms.Add(new SqlParameter("@income", income));
            }
            if (iTransactionType != null)
            {
                sql += " and iTransactionType=@iTransactionType";
                parms.Add(new SqlParameter("@iTransactionType", iTransactionType));
            }
            if (iTransactionMode != null)
            {
                sql += " and iTransactionMode=@iTransactionMode";
                parms.Add(new SqlParameter("@iTransactionMode", iTransactionMode));
            }
            if (!string.IsNullOrWhiteSpace(sTransactionObject))
            {
                sql += " and sTransactionObject like @sTransactionObject";
                parms.Add(new SqlParameter("@sTransactionObject", $"%{sTransactionObject}%"));
            }
            if (dsTransactionTime != null)
            {
                sql += " and dTransactionTime>=@dsTransactionTime";
                parms.Add(new SqlParameter("@dsTransactionTime", dsTransactionTime));
            }
            if (deTransactionTime != null)
            {
                sql += " and dTransactionTime<=@deTransactionTime";
                parms.Add(new SqlParameter("@deTransactionTime", deTransactionTime));
            }
            sql += " GROUP BY income";
            DataTable dt = iDal.SqlQueryDataTable(sql, parms.ToArray());
            Dictionary<string, string> dic = new Dictionary<string, string>();
            string sr = "0", zc = "0", zczy = "0";//收入 支出 资产转移
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                switch (dt.Rows[i][0].ToString())
                {
                    case "1":
                        sr = dt.Rows[i][1].ToString();                       
                        break;
                    case "2":
                        zc = dt.Rows[i][1].ToString();                        
                        break;
                    case "3":
                        zczy = dt.Rows[i][1].ToString();                       
                        break;
                }
            }
            dic.Add("s1", sr);
            dic.Add("s2", zc);
            dic.Add("s3", zczy);
            return JsonHelper.getJsonString(dic);
        }

        /// <summary>
        /// 获取空表结构
        /// </summary>
        /// <returns></returns>
        private DataTable GetDataTablestruct()
        {
            return iDal.SqlQueryDataTable("SELECT * FROM dbo.bl_detail WHERE 1 <> 1");
        }

        /// <summary>
        /// SqlBulkCopy方式导入数据库
        /// </summary>
        /// <param name="list"></param>
        public void ImportSqlBulkCopy(List<Detail> list)
        {
            DataTable dt = GetDataTablestruct();
            dt.TableName = "bl_detail";
            foreach (var item in list)
            {
                var newRow = dt.NewRow();
                newRow["gkey"] = item.gkey;
                newRow["gBelongKey"] = item.gBelongKey;
                newRow["iFlag"] = item.iFlag;
                newRow["dAmount"] = item.dAmount;
                newRow["income"] = item.income;
                newRow["iTransactionType"] = item.iTransactionType;
                newRow["iTransactionMode"] = item.iTransactionMode;
                newRow["sTransactionObject"] = item.sTransactionObject;
                newRow["sExplain"] = item.sExplain;
                newRow["dTransactionTime"] = item.dTransactionTime;
                newRow["dAddTime"] = item.dAddTime;
                newRow["sNo"] = item.sNo;
                newRow["sGoods"] = item.sGoods;
                dt.Rows.Add(newRow);
            }
            iDal.SqlBulkCopy(new DataTable[] { dt });
        }
    }
}
