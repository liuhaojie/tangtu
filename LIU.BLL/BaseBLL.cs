﻿using LIU.Common;
using LIU.IBLL;
using LIU.IDAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace LIU.BLL
{
    public class BaseBLL<T> : IBaseBLL<T> where T : class
    {
        protected IBaseDAL<T> iDal;
        protected DbContext db;
        protected CommitLog cl;
        /// <summary>
        /// 是否记录日志 true 记录
        /// </summary>
        protected bool RecordLog = true;
        public BaseBLL(DbContext db, IBaseDAL<T> iDal)
        {
            this.iDal = iDal;
            this.db = db;
            cl = new CommitLog(db);
        }


        /// <summary>
        /// 获取实体类
        /// </summary>
        /// <param name="where">lambda查询条件</param>
        /// <returns></returns>
        public virtual T GetModel(Expression<Func<T, bool>> where)
        {
            return iDal.GetModel(where);
        }

        /// <summary>
        /// 获取实体类
        /// </summary>
        /// <param name="where">lambda查询条件</param>
        /// <param name="orderBy">lambda排序条件</param>
        /// <param name="isAscending">是否升序</param>
        /// <returns></returns>
        public virtual T GetModel(Expression<Func<T, bool>> where, Expression<Func<T, object>> orderBy, bool isAscending)
        {
            return iDal.GetModel(where, orderBy, isAscending);
        }

        /// <summary>
        /// 分页带排序
        /// </summary>
        /// <param name="where">lambda查询条件</param>
        /// <param name="orderBy">lambda排序条件</param>
        /// <param name="page">分页帮助类</param>
        /// <param name="isAscending">是否升序</param>
        /// <returns></returns>
        public virtual List<T> GetList(Expression<Func<T, bool>> where,
            Expression<Func<T, object>> orderBy,
            PageHelper page, bool isAscending)
        {
            return iDal.GetList(where, orderBy, page, isAscending);
        }


        /// <summary>
        /// 分页带排序
        /// </summary>
        /// <param name="where">lambda查询条件</param>
        /// <param name="orderBy">lambda排序条件</param>
        /// <param name="page">分页帮助类</param>
        /// <param name="isAscending">是否升序</param>
        /// <param name="rowsCount">返回总行</param>
        /// <returns></returns>
        public virtual List<T> GetList(Expression<Func<T, bool>> where,
            Expression<Func<T, object>> orderBy,
            PageHelper page, bool isAscending, ref int rowsCount)
        {
            rowsCount = iDal.RowCount(where);
            return iDal.GetList(where, orderBy, page, isAscending);
        }

        /// <summary>
        /// 获取符合条件实体数
        /// </summary>
        /// <param name="where">查询条件</param>
        /// <returns></returns>
        public virtual int GetRowCount(Expression<Func<T, bool>> where)
        {
            return iDal.RowCount(where);
        }

        /// <summary>
        /// 获取实体组
        /// </summary>
        /// <param name="where">lambda查询条件</param>
        /// <returns></returns>
        public virtual List<T> GetList(Expression<Func<T, bool>> where)
        {
            return iDal.GetList(where);
        }


        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public virtual bool Update()
        {
            if (RecordLog)
                return cl.Commit() > 0;
            else
                return db.SaveChanges() > 0;
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public virtual bool Update(T model)
        {
            iDal.Update(model);
            if (RecordLog)
                return cl.Commit() > 0;
            else
                return db.SaveChanges() > 0;
        }


        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual bool Update(List<T> model)
        {
            foreach (var item in model)
            {
                iDal.Update(item);
            }
            if (RecordLog)
                return cl.Commit() > 0;
            else
                return db.SaveChanges() > 0;
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual bool Add(T model)
        {
            iDal.Add(model);
            if (RecordLog)
                return cl.Commit() > 0;
            else
                return db.SaveChanges() > 0;
        }

        /// <summary>
        /// 增加一组数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual bool Add(List<T> model)
        {
            foreach (T item in model)
            {
                iDal.Add(item);
            }
            if (RecordLog)
                return cl.Commit() > 0;
            else
                return db.SaveChanges() > 0;
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual bool Delete(T model)
        {
            iDal.Delete(model);
            if (RecordLog)
                return cl.Commit() > 0;
            else
                return db.SaveChanges() > 0;
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual bool Delete(List<T> modelList)
        {
            foreach (T model in modelList)
            {
                iDal.Delete(model);
            }
            if (RecordLog)
                return cl.Commit() > 0;
            else
                return db.SaveChanges() > 0;
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="where">删除条件</param>
        /// <returns></returns>
        public virtual bool Delete(Expression<Func<T, bool>> where)
        {
            iDal.Delete(where);
            if (RecordLog)
                return cl.Commit() > 0;
            else
                return db.SaveChanges() > 0;
        }

        /// <summary>
        /// 获取最大或者最小值
        /// </summary>
        /// <param name="where">排序条件</param>
        /// <param name="orderBys">排序规则</param>
        /// <param name="isMax">默认获取最大</param>
        /// <returns></returns>
        public virtual T GetMaxOrMin(Expression<Func<T, bool>> where, Expression<Func<T, object>> orderBys, bool isMax = true)
        {
            return iDal.GetMaxOrMin(where, orderBys, isMax);
        }

        /// <summary>
        /// 执行sql语句
        /// </summary>
        /// <typeparam name="TT"></typeparam>
        /// <param name="procName">语句</param>
        /// <param name="paras">参数</param>
        /// <returns></returns>
        public virtual List<TT> ExcetProc<TT>(string procName, params SqlParameter[] paras)
        {
            return iDal.ExcetProc<TT>(procName, paras);
        }

        /// <summary>
        /// 执行sql语句
        /// </summary>
        /// <typeparam name="TT"></typeparam>
        /// <param name="procName">语句</param>
        /// <param name="paras">参数</param>
        /// <returns></returns>
        public virtual int ExcetProc(string procName, params SqlParameter[] paras)
        {
            return iDal.ExcetProc(procName, paras);
        }

        /// <summary>
        /// EF SQL 语句返回 dataTable
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public DataTable SqlQueryDataTable(string sql, params SqlParameter[] parameters)
        {
            return iDal.SqlQueryDataTable(sql, parameters);
        }
    }
}
