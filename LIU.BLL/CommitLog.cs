﻿using LIU.Common;
using LIU.IDAL.System;
using LIU.Model.System;
using LIU.UnityFactory;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;

namespace LIU.BLL
{
    /// <summary>
    /// 提交日志
    /// </summary>
    public class CommitLog
    {
        private readonly ILogDAL irmDAL = IOCFactory.GetIOCResolve<ILogDAL>();
        private DbContext Db;
        public CommitLog(DbContext db)
        {
            this.Db = db;
        }
        /// <summary>
        /// 提交 并记录到日志
        /// </summary>
        /// <returns></returns>
        public int Commit()
        {
            DbEntityEntry[] dbEntityEntrys = Db.ChangeTracker.Entries().ToArray();
            foreach (DbEntityEntry item in dbEntityEntrys)
            {
                Log model = new Log();
                string keyName = GetKeyProperty(item.Entity.GetType());

                switch (item.State)
                {
                    case EntityState.Added:
                        model.iOperationType = 1;
                        model.sOperationTypeName = "增加";
                        model.sObjectId = string.Format("{0}: {1}", keyName, item.CurrentValues[keyName].ToString());
                        break;
                    case EntityState.Deleted:
                        model.iOperationType = 2;
                        model.sOperationTypeName = "删除";
                        model.sObjectId = string.Format("{0}: {1}", keyName, item.OriginalValues[keyName].ToString());
                        break;
                    case EntityState.Modified:
                        model.iOperationType = 3;
                        model.sOperationTypeName = "修改";
                        model.sObjectId = string.Format("{0}: {1}", keyName, item.CurrentValues[keyName].ToString());
                        StringBuilder chanagedContext = new StringBuilder();
                        List<string> fieldNames = item.OriginalValues.PropertyNames.ToList();
                        foreach (string fieldName in fieldNames)
                        {
                            try
                            {
                                string oldStr = Convert.ToString((item.OriginalValues[fieldName] ?? ""));
                                string newStr = Convert.ToString(item.CurrentValues[fieldName] ?? "");
                                if (!oldStr.Equals(newStr))
                                {

                                    oldStr = String.IsNullOrEmpty(oldStr) ? "[空]" : oldStr;

                                    newStr = String.IsNullOrEmpty(newStr) ? "[空]" : newStr;
                                    string str = string.Format(" {0}: {1}=>{2}; ", fieldName, oldStr, newStr);
                                    chanagedContext.Append(str);
                                }
                            }
                            catch (Exception ex)
                            {
                                LogHelper.Error("提交修改日志出现问题:" + ex.Message + @"\r\n 主键" + model.sObjectId + "  字段" + fieldName + "  实体类" + item.Entity.ToString() + "  数据打印:" + JsonHelper.getJsonString(item.Entity));
                            }
                        }
                        model.sChangeContext = chanagedContext.ToString();
                        break;
                    default:
                        continue;
                }
                model.gkey = Guid.NewGuid();

                model.gOperationKey = GetUserGuid();
                model.dOperationTime = DateTime.Now;
                model.sChangeContext = "【" + item.Entity.ToString() + "】" + model.sChangeContext;
                //string a = HttpContext.Current.User.Identity.Name;
                irmDAL.Add(model);
            }
            return Db.SaveChanges();
        }


        /// <summary>
        /// 获取主键名
        /// </summary>
        /// <param name="entityType"></param>
        /// <returns></returns>
        private string GetKeyProperty(Type entityType)
        {
            //单一主键 多主键需要重新来过            
            foreach (var prop in entityType.GetProperties())
            {
                var attr = prop.GetCustomAttributes(typeof(KeyAttribute), false).FirstOrDefault()
                    as KeyAttribute;
                if (attr != null)
                    return prop.Name;
            }
            return null;
        }

        /// <summary>
        /// 获取单前用户主键
        /// </summary>
        /// <returns></returns>
        private Guid GetUserGuid()
        {
            try
            {
                UserInfo ui = TryParsePrincipal();
                if (ui == null)
                    return Guid.Empty;
                else
                    return ui.gkey;
            }
            catch
            {
                return Guid.Empty;
            }
        }

        /// <summary>
        /// 获取cookie并解析出用户信息
        /// </summary>
        /// <returns></returns>
        public UserInfo TryParsePrincipal()
        {
            HttpRequest request = HttpContext.Current.Request;
            HttpCookie cookie = request.Cookies[FormsAuthentication.FormsCookieName];
            if (cookie == null || string.IsNullOrEmpty(cookie.Value))
            {
                return null;
            }
            //解密coolie值
            FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(cookie.Value);

            UserInfo account = JsonHelper.getObject<UserInfo>(ticket.UserData);
            return account;
        }
    }
}
