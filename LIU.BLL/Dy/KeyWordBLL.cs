﻿using LIU.IBLL.Dy;
using LIU.IDAL.Dy;
using LIU.Model.Dy;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.BLL.Dy
{
    public class KeyWordBLL : BaseBLL<KeyWord>, IKeyWordBLL
    {
        public KeyWordBLL(DbContext db, IKeyWordDAL iDal) : base(db, iDal)
        {
        }
    }
}
