﻿using LIU.IBLL.Dy;
using LIU.IDAL.Dy;
using LIU.Model.Dy;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.BLL.Dy
{
    public class BarrageBLL : BaseBLL<Barrage>, IBarrageBLL
    {
        public BarrageBLL(DbContext db, IBarrageDAL iDal) : base(db, iDal)
        {
        }
    }
}
