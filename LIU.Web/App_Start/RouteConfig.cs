﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace LIU.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Default_Default",
                "Default/{action}/{id}",
                new { controller = "Default", action = "Index", id = UrlParameter.Optional }
           );

            routes.MapRoute(
                 "Home_Default",
                 "Home/{action}/{id}",
                 new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            

            routes.MapRoute(
                 "Login_Default",
                 "Login/{action}",
                 new { controller = "Login", action = "Login" },
                 namespaces: new string[] { "LIU.Web.Controllers" }
            );
            //routes.MapRoute(
            //   "EBook", // 路由名称，这个只要保证在路由集合中唯一即可
            //   "EBook/{controller}/{action}/{id}", //路由规则,匹配以Admin开头的url
            //   new { controller = "", action = "", id = UrlParameter.Optional }, // 
            //   namespaces: new string[] { "LIU.Web.Controllers.EBook" }
            //);

            //routes.MapRoute(
            //   "System", // 路由名称，这个只要保证在路由集合中唯一即可
            //   "System/{controller}/{action}/{id}", //路由规则,匹配以Admin开头的url
            //   new { controller = "", action = "", id = UrlParameter.Optional }, //
            //    namespaces: new string[] { "LIU.Web.Controllers.System" }
            //);

        }
    }
}
