﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using LIU.Common;
using LIU.IBLL.System;
using LIU.Model;
using LIU.Model.System;
using LIU.UnityFactory;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;

namespace LIU.Web
{
    public class ChatHub : Hub
    {
        //private readonly IUserInfoBLL iurBll = IOCFactory.GetIOCResolve<IUserInfoBLL>();
        /// <summary>
        /// 在线人数
        /// </summary>
        private static int OnlineCount = 0;

        public void SendMsg(string message)
        {
            var userInfo = GetUserInfo();
            if (userInfo == null)
            {
                Clients.User(Context.ConnectionId).sendMessage(
                    JsonConvert.SerializeObject(new ChatMsg
                    {
                        type = 0,
                        msg = "发送失败",
                        chatTime = DateTime.Now
                    }));
                return;
            }
            //调用所有客户端的sendMessage方法
            Clients.All.sendMessage(
                JsonConvert.SerializeObject(new ChatMsg
                {
                    type = 1,
                    msg = message,
                    user = userInfo,
                    chatTime = DateTime.Now
                }));

        }

        /// <summary>
        /// 获取用户
        /// </summary>
        /// <returns></returns>
        private UserInfo GetUserInfo()
        {
            try
            {
                Dictionary<string, UserInfo> dic = CacheHelper.Get<Dictionary<string, UserInfo>>("ChatAllDic");
                if (dic.ContainsKey(Context.ConnectionId))
                    return dic[Context.ConnectionId];
                else
                    return null;

            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 发送在线人数
        /// </summary>
        private void SendCount()
        {
            //调用所有客户端的sendMessage方法
            Clients.All.sendMessage(
                JsonConvert.SerializeObject(new ChatMsg
                {
                    type = 2,
                    msg = OnlineCount.ToString(),
                    chatTime = DateTime.Now
                }));
        }

        /// <summary>
        /// 客户端连接
        /// </summary>
        /// <returns></returns>
        public override Task OnConnected()
        {
            Dictionary<string, UserInfo> dic = CacheHelper.Get<Dictionary<string, UserInfo>>("ChatAllDic");
            if (dic == null)
                dic = new Dictionary<string, UserInfo>();
            DB dB = new DB();
            Guid UserId = new Guid(Context.QueryString["UserId"]);
            var user = dB.UserInfo.Where(p => p.gkey == UserId).FirstOrDefault();
            if (user == null)
                return null;
            if (dic.Values.ToList().Where(p => p.gkey == UserId).FirstOrDefault() != null)
                return null;
            dic.Add(Context.ConnectionId, user);
            CacheHelper.Set("ChatAllDic", dic, 3600 * 24);
            OnlineCount++;
            SendCount();
            //Console.WriteLine("哇，有人进来了：{0}", this.Context.ConnectionId);
            return base.OnConnected();
        }

        /// <summary>
        /// 断开连接
        /// </summary>
        /// <param name="stopCalled"></param>
        /// <returns></returns>
        public override Task OnDisconnected(bool stopCalled)
        {
            Dictionary<string, UserInfo> dic = CacheHelper.Get<Dictionary<string, UserInfo>>("ChatAllDic");
            if (dic.ContainsKey(Context.ConnectionId))
            {
                dic.Remove(Context.ConnectionId);
                CacheHelper.Set("ChatAllDic", dic, 3600 * 24);
            }
            OnlineCount--;
            SendCount();
            //Console.WriteLine("靠，有人跑路了：{0}", this.Context.ConnectionId);
            return base.OnDisconnected(stopCalled);
        }

        /// <summary>
        /// 重连
        /// </summary>
        /// <returns></returns>
        public override Task OnReconnected()
        {
            OnConnected();
            return base.OnReconnected();
        }

        public class ChatMsg
        {
            /// <summary>
            /// 消息类型 0，系统消息 1，发送消息 2，在线人数更新
            /// </summary>
            public int type { get; set; }

            /// <summary>
            /// 发送的用户
            /// </summary>
            public UserInfo user { get; set; }

            /// <summary>
            /// 消息
            /// </summary>
            public string msg { get; set; }

            /// <summary>
            /// 消息时间
            /// </summary>
            public DateTime chatTime { get; set; }
        }
    }
}