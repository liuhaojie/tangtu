﻿using LIU.Web.App_Code;
using System.Web;
using System.Web.Mvc;

namespace LIU.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            //filters.Add(new HandleErrorAttribute());
            filters.Add(new AppHandleErrorAttribute());
            //filters.Add(new MyActionFilter());
        }
    }
}
