﻿using LIU.Common;
using LIU.IBLL.System;
using LIU.Model.System;
using LIU.UnityFactory;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace LIU.Web.Controllers
{
    public class LoginController : Controller
    {
        private readonly IUserInfoBLL iUIBll = IOCFactory.GetIOCResolve<IUserInfoBLL>();
        private readonly IVIPHistoryBLL ivipBll = IOCFactory.GetIOCResolve<IVIPHistoryBLL>();
        public ActionResult Login()
        {
            return View();
        }

        /// <summary>
        /// 获取验证码
        /// </summary>
        /// <returns></returns>
        public ActionResult GetCode()
        {
            ValidateCode vc = new ValidateCode(5);
            MemoryStream ms = new MemoryStream();
            vc.Image.Save(ms, global::System.Drawing.Imaging.ImageFormat.Jpeg);
            Session["code"] = vc.Text.ToLower();
            return File(ms.ToArray(), "image/jpeg");
        }

        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="password">密码</param>
        /// <param name="code">验证码</param>
        /// <returns></returns>
        public ActionResult Logining(string userName, string password, string code)
        {
            MsgHelper msg = new MsgHelper();
            try
            {
                if (Session["code"] == null)
                {
                    msg.flag = 0;
                    msg.msg = "验证码过期";
                    return Json(msg);
                }
                if (Session["code"].ToString() != code.ToLower())
                {
                    msg.flag = 0;
                    msg.msg = "验证码错误";
                    return Json(msg);
                }
                UserInfo ui = iUIBll.GetModel(p => p.sLoginName == userName);
                if (ui == null)
                {
                    msg.flag = 0;
                    msg.msg = "用户不存在";
                    return Json(msg);
                }
                ;
                if (ui.sPassWord != MD5Helper.getMD5By32(password))
                {
                    msg.flag = 0;
                    msg.msg = "密码错误";
                    return Json(msg);
                }
                if (ui.iFlag != 1)
                {
                    msg.flag = 0;
                    msg.msg = "账号状态异常";
                    return Json(msg);
                }
                ui.LastDateTime = DateTime.Now;
                VIPHistory model = null;
                if (ui.iLevel > 0 && ui.dLevelTIme < DateTime.Now)//vip到期判断
                {
                    Guid gkey = ui.gkey;
                    model = ivipBll.GetModel(p => p.gUserKey == gkey, p => new { p.dCreateTime }, true);
                    if (model != null)
                    {
                        ui.iLevel = model.iLevel;
                        ui.dLevelTIme = DateTime.Now.AddDays(model.iDay);
                    }
                    else
                    {
                        ui.iLevel = 0;
                        ui.dLevelTIme = null;
                    }
                }
                iUIBll.Update(ui, model);
                SetAuthenticationCoolie(ui);
                HttpContext.Session["UserInfo"] = ui;
                msg.flag = 1;
                //msg.msg = "/Ebook/Novel/Index";
                switch (ui.iState)
                {
                    case 1:
                        if (Request["returnUrl"] == null || string.IsNullOrEmpty(Request["returnUrl"].ToString()))
                            msg.msg = "/Default/Index";
                        else
                            msg.msg = Request["returnUrl"].ToString();
                        break;
                    case 2:
                        if (Request["returnUrl"] == null || string.IsNullOrEmpty(Request["returnUrl"].ToString()))
                            //msg.msg = "/Home/Index";
                            msg.msg = "/Default/Index";
                        else
                            msg.msg = Request["returnUrl"].ToString();
                        break;
                    case 3:
                        msg.msg = "/Default/Index";
                        break;
                }
                return Json(msg);
            }
            catch (Exception ex)
            {
                msg.flag = 0;
                msg.msg = ex.Message;
                return Json(msg);
            }
        }


        /// <summary>
        /// 将用户信息通过ticket加密保存到cookie
        /// </summary>
        /// <param name="userInfo">用户类</param>
        /// <param name="rememberDay">保存几天</param>
        public void SetAuthenticationCoolie(UserInfo userInfo, int rememberDay = 0)
        {
            if (userInfo == null)
                throw new ArgumentNullException("userInfo");

            //序列化UserInfo对象
            string userInfoJson = JsonConvert.SerializeObject(userInfo);
            //创建用户票据
            var ticket = new FormsAuthenticationTicket(1, userInfo.sLoginName, DateTime.Now, DateTime.Now.AddDays(rememberDay), false, userInfoJson);
            //加密
            string encryptAccount = FormsAuthentication.Encrypt(ticket);

            //创建cookie
            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptAccount)
            {
                HttpOnly = true,
                Secure = FormsAuthentication.RequireSSL,
                Domain = FormsAuthentication.CookieDomain,
                Path = FormsAuthentication.FormsCookiePath
            };
            if (rememberDay > 0)
                cookie.Expires = DateTime.Now.AddDays(rememberDay);
            //写入Cookie
            global::System.Web.HttpContext.Current.Response.Cookies[cookie.Name].Expires = DateTime.Now.AddDays(-1);
            global::System.Web.HttpContext.Current.Response.Cookies.Add(cookie);
        }


        /// <summary>
        /// 404自定义界面
        /// </summary>
        /// <returns></returns>
        public ActionResult Error404()
        {
            ViewData.Add("url", global::System.Configuration.ConfigurationManager.AppSettings["tokenurl"]);
            return View();
        }
    }
}