﻿using LIU.Common;
using LIU.IBLL.Bill;
using LIU.Model.Bill;
using LIU.UnityFactory;
using LIU.Web.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LIU.Web.Controllers.Bill
{
    public class DetailController : BaseController
    {
        private readonly IDetailBLL iDBll = IOCFactory.GetIOCResolve<IDetailBLL>();

        /// <summary>
        /// 明细界面
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            ViewData["TransactionType"] = JsonHelper.getJsonString(SysCache.DicInfoList.Where(p => p.sCode == "TransactionType").OrderBy(p => p.iSort).ToList());
            ViewData["TransactionMode"] = JsonHelper.getJsonString(SysCache.DicInfoList.Where(p => p.sCode == "TransactionMode").OrderBy(p => p.iSort).ToList());

            return View();
        }

        /// <summary>
        /// 获取数据
        /// </summary>
        /// <param name="page">第几页</param>
        /// <param name="limit">一页多少行</param>
        /// <param name="income">支出收入 1收入，2支出</param>
        /// <param name="iTransactionType">交易类型</param>
        /// <param name="iTransactionMode">交易方式</param>
        /// <param name="sTransactionObject">交易对象</param>
        /// <param name="dsTransactionTime">交易时间开始</param>
        /// <param name="deTransactionTime">交易时间结束</param>
        /// <returns></returns>
        public ActionResult getList(int page, int limit, int? income, int? iTransactionType, int? iTransactionMode, string sTransactionObject,
            DateTime? dsTransactionTime, DateTime? deTransactionTime)
        {
            int count = 0;
            Guid g = GetSessionUserInfo().gkey;
            List<Detail> list = iDBll.GetList(p => (p.gBelongKey == g) &&
                 (p.iFlag == 1) &&
                 (income == null ? true : p.income == income) &&
                 (iTransactionType == null ? true : p.iTransactionType == iTransactionType) &&
                 (iTransactionMode == null ? true : p.iTransactionMode == iTransactionMode) &&
                 (string.IsNullOrEmpty(sTransactionObject) ? true : p.sTransactionObject.Contains(sTransactionObject)) &&
                 (dsTransactionTime == null ? true : p.dTransactionTime >= dsTransactionTime) &&
                 (deTransactionTime == null ? true : p.dTransactionTime <= deTransactionTime),
                p => new { p.dTransactionTime }, new PageHelper(limit, page), false, ref count);
            LayuiDataHelper layuiDataHelper = new LayuiDataHelper();
            layuiDataHelper.code = 0;
            layuiDataHelper.count = count;
            layuiDataHelper.data = list;
            layuiDataHelper.msg = iDBll.GetHeJi(g,income, iTransactionType, iTransactionMode, sTransactionObject, dsTransactionTime, deTransactionTime);
            return Json(layuiDataHelper);
        }

        /// <summary>
        /// 添加界面界面
        /// </summary>
        /// <returns></returns>
        public ActionResult AddView()
        {
            ViewData["TransactionType"] = JsonHelper.getJsonString(SysCache.DicInfoList.Where(p => p.sCode == "TransactionType").OrderBy(p => p.iSort).ToList());
            ViewData["TransactionMode"] = JsonHelper.getJsonString(SysCache.DicInfoList.Where(p => p.sCode == "TransactionMode").OrderBy(p => p.iSort).ToList());

            return View();
        }

        /// <summary>
        /// 添加明细记录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult AddDetail(Detail model)
        {
            if (model.dAmount == 0m || string.IsNullOrWhiteSpace(model.sTransactionObject))
            {
                return Json(new MsgHelper() { flag = 0, msg = "信息必填" });
            }
            model.gkey = Guid.NewGuid();
            model.gBelongKey = GetSessionUserInfo().gkey;
            model.dAddTime = DateTime.Now;
            model.iFlag = 1;
            model.sNo = DateTime.Now.ToString("yyyyMMddHHmmssFFF") + new Random().Next(1000, 99999);
            if (iDBll.Add(model))
                return Json(new MsgHelper() { flag = 1, msg = "添加成功" });
            else
                return Json(new MsgHelper() { flag = 0, msg = "添加失败" });
        }

        #region 导入

        /// <summary>
        /// 支付宝导入
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public ActionResult AlipayImport(HttpPostedFileBase file)
        {
            string SuffixName = file.FileName.Split('.').Last();
            string fileName = DateTime.Now.ToString("yyyyMMddhhmmssms") + "." + SuffixName;
            string filePath = Server.MapPath(@"~/File/Import");
            if (!Directory.Exists(filePath))
                Directory.CreateDirectory(filePath);
            string path = Path.Combine(filePath, fileName);
            file.SaveAs(path);
            ExcelHelper ex = new ExcelHelper(path);
            DataTable dt = ex.ExportExcelToDataTable();
            LogHelper.Info($"导入支付宝账单数据开始，共{dt.Rows.Count}条");
            List<Detail> list = new List<Detail>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Detail model = new Detail()
                {
                    gkey = Guid.NewGuid(),
                    dAddTime = DateTime.Now,
                    dAmount = Convert.ToDecimal(dt.Rows[i]["金额（元）"]),
                    dTransactionTime = Convert.ToDateTime(dt.Rows[i]["交易创建时间"]),
                    gBelongKey = GetSessionUserInfo().gkey,
                    iFlag = 1,
                    income = getNcome(dt.Rows[i]["收/支"].ToString()),
                    iTransactionMode = Convert.ToInt32(SysCache.DicInfoList.Where(p => p.sCode == "TransactionMode" && p.sName == "支付宝").FirstOrDefault()?.sValue),
                    iTransactionType = 9,
                    sTransactionObject = dt.Rows[i]["交易对方"].ToString(),
                    sExplain = "【支付宝账单导入】" + dt.Rows[i]["备注"].ToString(),
                    sGoods = dt.Rows[i]["商品名称"].ToString(),
                    sNo = dt.Rows[i]["交易号"].ToString()
                };
                list.Add(model);
            }
            FileInfo f = new FileInfo(path);
            f.Delete();
            return Json(Import(list));
        }

        /// <summary>
        /// 支付宝导入
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public ActionResult WeiXinImport(HttpPostedFileBase file)
        {
            string SuffixName = file.FileName.Split('.').Last();
            string fileName = DateTime.Now.ToString("yyyyMMddhhmmssms") + "." + SuffixName;
            string filePath = Server.MapPath(@"~/File/Import");
            if (!Directory.Exists(filePath))
                Directory.CreateDirectory(filePath);
            string path = Path.Combine(filePath, fileName);
            file.SaveAs(path);
            ExcelHelper ex = new ExcelHelper(path);
            DataTable dt = ex.ExportExcelToDataTable();
            LogHelper.Info($"导入微信账单数据开始，共{dt.Rows.Count}条");
            List<Detail> list = new List<Detail>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Detail model = new Detail()
                {
                    gkey = Guid.NewGuid(),
                    dAddTime = DateTime.Now,
                    dAmount = Convert.ToDecimal(dt.Rows[i]["金额(元)"]),
                    dTransactionTime = Convert.ToDateTime(dt.Rows[i]["交易时间"]),
                    gBelongKey = GetSessionUserInfo().gkey,
                    iFlag = 1,
                    income = getNcome(dt.Rows[i]["收/支"].ToString()),
                    iTransactionMode = Convert.ToInt32(SysCache.DicInfoList.Where(p => p.sCode == "TransactionMode" && p.sName == "微信").FirstOrDefault()?.sValue),
                    iTransactionType = 9,
                    sTransactionObject = dt.Rows[i]["交易对方"].ToString() == "/" ? dt.Rows[i]["交易类型"].ToString() : dt.Rows[i]["交易对方"].ToString(),
                    sExplain = "【微信账单导入】" + (dt.Rows[i]["备注"].ToString() == "/" ? "" : dt.Rows[i]["备注"].ToString()),
                    sGoods = dt.Rows[i]["商品"].ToString() == "/" ? dt.Rows[i]["交易类型"].ToString() : dt.Rows[i]["商品"].ToString(),
                    sNo = dt.Rows[i]["交易单号"].ToString()
                };
                list.Add(model);
            }
            FileInfo f = new FileInfo(path);
            f.Delete();
            return Json(Import(list));
        }

        /// <summary>
        /// 验证数据及导入数据
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private MsgHelper Import(List<Detail> list)
        {
            //成功 跳过 失败
            int success = 0, skip = 0, error = 0;
            var snoList = iDBll.GetList(p => p.iFlag == 1).Select(p => new { p.sNo }).Distinct();
            List<Detail> needAdd = new List<Detail>();
            foreach (var item in list)
            {
                if (snoList.Where(p => p.sNo == item.sNo).FirstOrDefault() == null)
                {
                    if (needAdd.Where(p => p.sNo == item.sNo).FirstOrDefault() == null)
                        needAdd.Add(item);
                    else
                    {
                        LogHelper.Info($"交易号【{item.sNo}】在Excel中重复，跳过后面的数据");
                        skip++;
                    }
                }
                else
                {
                    LogHelper.Info($"交易号【{item.sNo}】在数据库中已存在，跳过");
                    skip++;
                }
            }

            iDBll.ImportSqlBulkCopy(needAdd);
            success = needAdd.Count;
            LogHelper.Info($"成功导入{success}条，跳过{skip}条，失败{error}条");
            return new MsgHelper() { flag = 1, msg = $"成功导入{success}条，跳过{skip}条，失败{error}条" };

        }



        /// <summary>
        /// 获取收入支出对应类型
        /// </summary>
        /// <param name="ncome"></param>
        /// <returns></returns>
        private int getNcome(string ncome)
        {
            switch (ncome)
            {
                case "收入":
                    return 1;
                case "支出":
                    return 2;
                default:
                    return 3;
            }
        }
        #endregion
    }
}