﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LIU.Web.Controllers.Bill
{
    public class BillAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Bill";
            }
        }
        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Bill",
                "Bill/{controller}/{action}/{id}",
                new { id = UrlParameter.Optional }
            );
        }
    }
}