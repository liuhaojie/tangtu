﻿using LIU.Common;
using LIU.Web.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LIU.Web.Controllers.Software
{
    /// <summary>
    /// 注册软件类
    /// </summary>
    public class RegisterController : BaseController
    {
       
        /// <summary>
        /// 注册界面
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 返回注册码
        /// </summary>
        /// <param name="uncode"></param>
        /// <returns></returns>
        public ActionResult GetRegisterCode(string uncode)
        {
            MsgHelper msg = new MsgHelper();


            return Json(msg);
        }
    }
}