﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LIU.Web.Controllers.Software
{
    public class SoftwareAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Software";
            }
        }
        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Software_default",
                "Software/{controller}/{action}/{id}",
                new { id = UrlParameter.Optional }
            );
        }
    }
}