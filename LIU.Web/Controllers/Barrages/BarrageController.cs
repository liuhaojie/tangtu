﻿using LIU.Common;
using LIU.IBLL.Dy;
using LIU.Model.Dy;
using LIU.UnityFactory;
using LIU.Web.App_Code;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LIU.Web.Controllers.Barrages
{
    public class BarrageController : BaseController
    {
        private readonly IBarrageBLL iBll = IOCFactory.GetIOCResolve<IBarrageBLL>();
        private readonly IKeyWordBLL iKWBll = IOCFactory.GetIOCResolve<IKeyWordBLL>();

        /// <summary>
        /// 获取弹幕界面
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 获取弹幕列表
        /// </summary>
        /// <param name="page">第几页</param>
        /// <param name="limit">每页多少行</param>
        /// <param name="sRoomId">房间id</param>
        /// <param name="sName">姓名</param>
        /// <param name="sLevel">等级</param>
        /// <param name="sContent">内容</param>
        /// <param name="dStartSpeakTime">开始时间</param>
        /// <param name="dEndSpeakTime">结束时间</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetBarrageList(int page, int limit, string sRoomId, string sName, int? sLevel, string sContent, DateTime? dStartSpeakTime, DateTime? dEndSpeakTime)
        {
            int count = 0;
            List<Barrage> list = iBll.GetList(p =>
                    (string.IsNullOrEmpty(sRoomId.Trim()) ? true : p.sRoomId.Contains(sRoomId.Trim())) &&
                    (string.IsNullOrEmpty(sName.Trim()) ? true : p.sName.Contains(sName.Trim())) &&
                    (string.IsNullOrEmpty(sContent.Trim()) ? true : p.sContent.Contains(sContent.Trim())) &&
                    (sLevel == null ? true : p.sLevel == sLevel) &&
                    (dStartSpeakTime == null ? true : p.dSpeakTime >= dStartSpeakTime) &&
                    (dEndSpeakTime == null ? true : p.dSpeakTime <= dEndSpeakTime),
                p => new { p.dSpeakTime }, new PageHelper(limit, page), true, ref count);
            LayuiDataHelper layuiDataHelper = new LayuiDataHelper();
            layuiDataHelper.code = 0;
            layuiDataHelper.count = count;
            layuiDataHelper.data = list;
            return Json(layuiDataHelper);
        }



        /// <summary>
        /// 获取关键字界面
        /// </summary>
        /// <returns></returns>
        public ActionResult KeyWordView()
        {
            return View();
        }

        /// <summary>
        /// 获取关键字列表
        /// </summary>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <param name="sRoomId"></param>
        /// <param name="dIndexDate">日期</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetKeyWordList(int page, int limit, string sRoomId, DateTime? dIndexDate)
        {
            int count = 0;
            DateTime dt = DateTime.Now.Date;
            List<KeyWord> list = iKWBll.GetList(p =>
            (string.IsNullOrEmpty(sRoomId.Trim()) ? true : p.sRoomId.Contains(sRoomId.Trim())) &&
            (dIndexDate == null ? p.dIndedxDate == dt : dIndexDate == p.dIndedxDate),
                p => new { p.iCount }, new PageHelper(limit, page), false, ref count);
            LayuiDataHelper layuiDataHelper = new LayuiDataHelper();
            layuiDataHelper.code = 0;
            layuiDataHelper.count = count;
            layuiDataHelper.data = list;
            return Json(layuiDataHelper);
        }

        /// <summary>
        /// 报表界面
        /// </summary>
        /// <returns></returns>
        public ActionResult EchartsView()
        {
            return View();
        }

        /// <summary>
        /// 获取图表数据
        /// </summary>
        /// <returns></returns>
        public JsonResult GetEchartsData()
        {
            return Json(iBll.ExcetProc<EchartsData>("Dy_Echarts_By_Day @startTime,@endTime", new SqlParameter[] {
                        new SqlParameter("@startTime",DateTime.Now.AddDays(-30).ToShortDateString()),
                        new SqlParameter("@endTime",DateTime.Now.ToShortDateString())}));
        }
    }

    /// <summary>
    /// 图标数据类
    /// </summary>
    public class EchartsData
    {
        /// <summary>
        /// x
        /// </summary>
        public string text { get; set; }

        /// <summary>
        /// y
        /// </summary>
        public int value { get; set; }
    }

}