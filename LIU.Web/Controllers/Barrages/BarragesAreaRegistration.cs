﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LIU.Web.Controllers.Barrages
{
    public class BarragesAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Barrages";
            }
        }
        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Barrages",
                "Barrages/{controller}/{action}/{id}",
                new { id = UrlParameter.Optional }
            );
        }
    }
}