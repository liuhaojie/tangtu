﻿using LIU.Common;
using LIU.IBLL.System;
using LIU.Model.System;
using LIU.UnityFactory;
using LIU.Web.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LIU.Web.Controllers.Api
{
    public class InfoController : Controller
    {
        /// <summary>
        /// 字典信息
        /// </summary>
        private readonly IDicInfoBLL idiBll = IOCFactory.GetIOCResolve<IDicInfoBLL>();
        /// <summary>
        /// 字典目录
        /// </summary>
        private readonly IDicCatalogBLL idcBll = IOCFactory.GetIOCResolve<IDicCatalogBLL>();

        /// <summary>
        /// 公告
        /// </summary>
        private readonly INoticeBLL inBll = IOCFactory.GetIOCResolve<INoticeBLL>();

        /// <summary>
        /// 获取友情链接
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetFriendUrl()
        {
            DicCatalog dc = idcBll.GetModel(p => p.sCode == "FriendUrl");
            if (dc != null)
            {
                string code = dc.sCode;
                var diList = idiBll.GetList(p => p.sCode == code).Select(p => new
                {
                    p.sName,
                    p.sValue
                });
                if (diList != null && diList.Count() > 0)
                    return Json(new MsgHelper { flag = 1, msg = diList });
            }
            return Json(new MsgHelper { flag = 0, msg = "获取友情链接失败" });
        }


        /// <summary>
        /// 获取最新的公告
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetNewNotice()
        {
            Notice n = inBll.GetModel(p => p.iFlag == 1, p => new { p.dCreateTime }, false);
            if (n == null)

                return Json(new MsgHelper() { flag = 1, msg = "暂无公告" });
            else
                return Json(new MsgHelper { flag = 1, msg = n });
        }


        /// <summary>
        /// 获取留言类型
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetMessageType()
        {
            MsgHelper msg = new MsgHelper();
            List<DicInfo> di = SysCache.DicInfoList.Where(p => p.sCode == "messageCode" && p.iFlag == 1).ToList();
            if (di.Count == 0)
            {
                msg.flag = 0;
                msg.msg = "获取留言类型失败，请联系管理员或者直接留言";
            }
            else
            {
                msg.flag = 1;
                msg.msg = JsonHelper.getJsonString(di);
            }
            return Json(msg);
        }

        /// <summary>
        /// 获取全部公告
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetAllNotice()
        {
            return Json(new MsgHelper()
            {
                flag = 1,
                msg = "成功",
                data = inBll.GetList(p => true).OrderByDescending(p => p.dCreateTime).Select(p => new
                {
                    Date = p.dCreateTime.ToString("yyyy年MM月dd日"),
                    Content = p.sContent
                })
            });
        }
    }
}