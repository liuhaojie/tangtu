﻿using JiebaNet.Analyser;
using JiebaNet.Segmenter;
using LIU.Common;
using LIU.IBLL.Dy;
using LIU.Model.Dy;
using LIU.Model.System;
using LIU.UnityFactory;
using LIU.Web.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LIU.Web.Controllers.Api
{
    public class DyController : Controller
    {
        private readonly IBarrageBLL iBll = IOCFactory.GetIOCResolve<IBarrageBLL>();
        private readonly IKeyWordBLL iKWBll = IOCFactory.GetIOCResolve<IKeyWordBLL>();

        /// <summary>
        /// 接收弹幕
        /// </summary>
        /// <param name="list">弹幕数组</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult SendBarrage()
        {
            List<Barrage> list = new List<Barrage>();
            if (Request["list"] != null)
            {
                list = JsonHelper.getObject<List<Barrage>>(Request["list"].ToString());
            }
            MsgHelper msg = new MsgHelper();
            msg.flag = 1;
            for (int i = 0; i < list.Count; i++)
            {
                Barrage b = list[i];
                b.gkey = Guid.NewGuid();
                b.dSpeakTime = b.dSpeakTime.AddHours(8);
                list[i] = b;
            }
            if (list.Count > 0)
            {
                if (!iBll.Add(list))
                    msg.flag = 0;
                else//添加成功 添加关键字
                {
                    Parameter par = SysCache.ParameterList.Where(p => p.sCode == "ignoreKeyWord").FirstOrDefault();
                    DateTime dt = DateTime.Now.Date;
                    List<string> strLsit = string.IsNullOrWhiteSpace(par.sValue) ? null : par.sValue.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                    string sRoomId = list[0].sRoomId;
                    for (int i = 0; i < list.Count; i++)
                    {

                        if (IsContains(list[i].sContent, strLsit))//判断当前内容是否含有关键字屏蔽内容
                            continue;
                        foreach (var item in GetKey(list[i].sContent))
                        {
                            try
                            {
                                KeyWord kw = iKWBll.GetModel(p => p.dIndedxDate == dt && p.sWord == item && p.sRoomId == sRoomId);
                                if (kw != null)
                                {
                                    kw.iCount++;
                                    kw.dEndTime = DateTime.Now;
                                    iKWBll.Update(kw);
                                }
                                else
                                {
                                    kw = new KeyWord
                                    {
                                        gkey = Guid.NewGuid(),
                                        dStartTime = DateTime.Now,
                                        dEndTime = DateTime.Now,
                                        dIndedxDate = DateTime.Now,
                                        iCount = 1,
                                        sWord = item,
                                        sRoomId = list[i].sRoomId
                                    };
                                    iKWBll.Add(kw);
                                }
                            }
                            catch
                            {
                                continue;
                            }
                        }
                    }
                }
            }
            return Json(msg);
        }


        /// <summary>
        /// 获取语句关键字
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private List<string> GetKey(string str)
        {
            var extractor = new TfidfExtractor();
            return extractor.ExtractTags(str, 10, Constants.NounAndVerbPos).ToList();
        }

        /// <summary>
        /// 判断一个字符串 是否包含一个数组中的摸个元素
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private bool IsContains(string s, List<string> str)
        {
            if (str == null || string.IsNullOrWhiteSpace(s))
                return false;
            foreach (var item in str)
            {
                if (s.Contains(item))
                    return true;
            }
            return false;
        }
    }
}