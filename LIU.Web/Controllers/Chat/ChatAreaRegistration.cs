﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LIU.Web.Controllers.Chat
{
    public class ChatAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Chat";
            }
        }
        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Chat",
                "Chat/{controller}/{action}/{id}",
                new { id = UrlParameter.Optional }
            );
        }
    }
}