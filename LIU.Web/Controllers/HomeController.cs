﻿using LIU.Common;
using LIU.IBLL.System;
using LIU.Model;
using LIU.Model.System;
using LIU.UnityFactory;
using LIU.Web.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LIU.Web.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IUserInfoBLL iBll = IOCFactory.GetIOCResolve<IUserInfoBLL>();
        public ActionResult Index()
        {

            return View(GetSessionUserInfo());
        }

        /// <summary>
        /// 修改密码界面
        /// </summary>
        /// <returns></returns>
        public ActionResult EditPassView()
        {
            return View();
        }

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="oldPass">旧密码</param>
        /// <param name="newPass">新密码</param>
        /// <param name="rePass">确认密码</param>
        /// <returns></returns>
        public ActionResult EditPassWord(string oldPass, string newPass, string rePass)
        {
            MsgHelper msg = new MsgHelper();
            if (newPass != rePass)
            {
                msg.msg = "两次密码不同";
                msg.flag = 0;
                return Json(msg);
            }
            if (!ValidateHelper.RegexPassWord(newPass))
            {
                msg.msg = "密码应为6到16位字符";
                msg.flag = 0;
                return Json(msg);
            }
            Guid g = GetSessionUserInfo().gkey;
            UserInfo ui = iBll.GetModel(p => p.gkey == g);
            if (MD5Helper.getMD5By32(oldPass) != ui.sPassWord)
            {
                msg.msg = "原密码输入错误";
                msg.flag = 0;
                return Json(msg);
            }
            ui.sPassWord = MD5Helper.getMD5By32(newPass);
            if (iBll.Update(ui))
            {
                msg.msg = "密码修改成功";
                msg.flag = 1;
                SetSessionUserInfo(ui);
            }
            else
            {
                msg.msg = "密码修改失败";
                msg.flag = 1;
            }
            return Json(msg);
        }

        /// <summary>
        /// 退出
        /// </summary>
        /// <returns></returns>
        public ActionResult Exit()
        {
            MsgHelper msg = new MsgHelper();
            RemoveSession();
            msg.flag = 1;
            msg.msg = "退出成功";
            return Json(msg);
        }

        /// <summary>
        /// 后台默认页
        /// </summary>
        /// <returns></returns>
        public ActionResult Default()
        {
            return View();
        }

        
    }
}