﻿using LIU.Common;
using LIU.IBLL.EBook;
using LIU.IBLL.System;
using LIU.Model.DTOModel;
using LIU.Model.EBook;
using LIU.Model.System;
using LIU.UnityFactory;
using LIU.Web.App_Code;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace LIU.Web.Controllers
{
    /// <summary>
    /// 前台首页
    /// </summary>
    public class DefaultController : WebBaseController
    {
        private readonly INovelBLL iNBll = IOCFactory.GetIOCResolve<INovelBLL>();
        private readonly IChapterBLL iCBll = IOCFactory.GetIOCResolve<IChapterBLL>();
        private readonly IUserInfoBLL iuiBll = IOCFactory.GetIOCResolve<IUserInfoBLL>();
        private readonly INotifyMessageBLL inmBll = IOCFactory.GetIOCResolve<INotifyMessageBLL>();
        private readonly IValidateBLL ivBll = IOCFactory.GetIOCResolve<IValidateBLL>();
        private readonly IGoodsBLL igBll = IOCFactory.GetIOCResolve<IGoodsBLL>();

        /// <summary>
        /// 初始界面
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {

            //List<Chapter> c_list = iCBll.GetList(p => true, p => new { p.dInsertTime }, new Common.PageHelper(), false);
            List<HomeModel> c_list = null;
            c_list = CacheHelper.Get<List<HomeModel>>("HomeModel");
            if (c_list == null || !c_list.Any())
            {
                c_list = iCBll.GetHomeModel();
                CacheHelper.Set("HomeModel", c_list);
            }
            ViewData["c_list"] = c_list;
            return View();
        }

        /// <summary>
        /// 登录界面
        /// </summary>
        /// <returns></returns>
        public ActionResult Login()
        {
            return View();
        }

        /// <summary>
        /// 注册界面
        /// </summary>
        /// <returns></returns>
        public ActionResult Reg()
        {
            return View();
        }

        /// <summary>
        /// 注册
        /// </summary>
        /// <param name="userName">登录名</param>
        /// <param name="sName">昵称</param>
        /// <param name="pass">密码</param>
        /// <param name="repass">确认密码</param>
        /// <param name="vercode">验证码</param>
        /// <returns></returns>
        public ActionResult Reging(string loginName, string sName, string pass, string repass, string vercode)
        {
            MsgHelper msg = new MsgHelper();
            if (string.IsNullOrWhiteSpace(loginName) || string.IsNullOrWhiteSpace(sName) ||
                 string.IsNullOrWhiteSpace(pass) ||
                 string.IsNullOrWhiteSpace(repass) || string.IsNullOrWhiteSpace(vercode))
            {
                msg.flag = 0;
                msg.msg = "参数不可以为空";
                return Json(msg);
            }
            if (Session["code"] == null)
            {
                msg.flag = 0;
                msg.msg = "验证码过期";
                return Json(msg);
            }
            if (Session["code"].ToString() != vercode.ToLower())
            {
                msg.flag = 0;
                msg.msg = "验证码错误";
                return Json(msg);
            }
            if (!ValidateHelper.RegexMail(loginName))
            {
                msg.flag = 0;
                msg.msg = "邮箱格式错误";
                return Json(msg);
            }
            if (iuiBll.GetModel(p => p.sLoginName == loginName || p.sEmail == loginName) != null)
            {
                msg.flag = 0;
                msg.msg = "邮箱已经存在";
                return Json(msg);
            }
            if (iuiBll.GetModel(p => p.sName == sName) != null)
            {
                msg.flag = 0;
                msg.msg = "昵称已经存在";
                return Json(msg);
            }
            if (pass != repass)
            {
                msg.flag = 0;
                msg.msg = "两次密码输入不一致";
                return Json(msg);
            }
            if (!ValidateHelper.RegexPassWord(pass))
            {
                msg.flag = 0;
                msg.msg = "密码应为6到16位字符";
                return Json(msg);
            }
            UserInfo ui = new UserInfo();
            ui.gkey = Guid.NewGuid();
            ui.sLoginName = loginName;
            ui.sName = sName;
            ui.RegDateTime = DateTime.Now;
            ui.sPassWord = MD5Helper.getMD5By32(pass);
            ui.iFlag = 1;
            ui.iState = 1;
            ui.iSex = 9;
            ui.sEmail = ui.sLoginName;
            ui.iEmailValidate = 0;
            if (iuiBll.Add(ui))
            {
                msg.flag = 1;
                msg.msg = "注册成功";
                NotifyMsgHelper.SendMsg("欢迎注册", "欢迎来到唐突世界，有任何问题可以直接联系管理员!!!", new List<Guid> { ui.gkey });

                Validate vd = new Validate()
                {
                    gkey = Guid.NewGuid(),
                    gUserKey = ui.gkey,
                    dCreateTime = DateTime.Now,
                    dOverdueTime = DateTime.Now.AddMinutes(10),
                    iFlag = 1,
                    iType = 1
                };

                string body = EmailHelper.bodyRegTemp.Replace("#name#", sName).Replace("#url#", "/Default/Validateloginer?key=" + vd.gkey);
                if (EmailHelper.SendWebEmail(ui.sEmail, "【唐突世界】账号激活", body))
                {
                    ivBll.Add(vd);
                    msg.msg = "注册成功,激活邮件已经发送到邮箱,请注意查收";
                }
            }
            else
            {
                msg.flag = 0;
                msg.msg = "注册失败";
            }
            return Json(msg);
        }


        /// <summary>
        /// 判断是否登录 0未登录 1登录
        /// </summary>
        /// <returns></returns>
        public ActionResult GetLogined()
        {
            if (GetSessionUserInfo() == null)
                return Content("0");
            else
                return Content("1");
        }

        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <returns></returns>
        public ActionResult GetUserInfro()
        {
            MsgHelper msg = new MsgHelper();
            UserInfo ui = GetSessionUserInfo();
            if (ui == null)
            {
                msg.flag = 0;
                msg.msg = "没有登录";
            }
            else
            {
                int count = inmBll.GetRowCount(p => p.gReceiveKey == ui.gkey && p.iFlag == 1);//获取有多少条未读信息
                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("sImageUrl", string.IsNullOrEmpty(ui.sImageUrl) ? "/Images/User/default.jpg" : ui.sImageUrl);
                dic.Add("sName", ui.sName);
                dic.Add("gkey", ui.gkey.ToString());
                dic.Add("iState", ui.iState.ToString());
                dic.Add("iLevel", ui.iLevel.ToString());
                dic.Add("dLevelTIme", ui.dLevelTIme.ToString());
                dic.Add("sHerdImageUrl", ui.sHerdImageUrl?.ToString());
                dic.Add("noReadCount", count > 99 ? "99+" : count.ToString());
                dic.Add("iGold", ui.iGold.ToString());
                msg.flag = 1;
                msg.msg = JsonHelper.getJsonString(dic);
            }
            return Json(msg);
        }


        /// <summary>
        /// 获取用户未对信息
        /// </summary>
        /// <returns></returns>
        public ActionResult GetNoReadCount()
        {
            MsgHelper msg = new MsgHelper();
            UserInfo ui = GetSessionUserInfo();
            if (ui == null)
            {
                msg.flag = 0;
                msg.msg = "没有登录";
            }
            else
            {
                int count = inmBll.GetRowCount(p => p.gReceiveKey == ui.gkey && p.iFlag == 1);//获取有多少条未读信息
                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("noReadCount", count > 99 ? "99+" : count.ToString());
                msg.flag = 1;
                msg.msg = JsonHelper.getJsonString(dic);
            }
            return Json(msg);
        }

        /// <summary>
        /// 退出
        /// </summary>
        /// <returns></returns>
        public ActionResult Exit()
        {
            RemoveSession();
            return Redirect("/Default/Index");
        }
        #region 验证邮箱等

        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="gkey"></param>
        /// <param name="sEmail"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SendValidateEmail(Guid gkey, string sEmail)
        {
            MsgHelper msg = new MsgHelper();
            if (!ValidateHelper.RegexMail(sEmail))
            {
                msg.flag = 0;
                msg.msg = "邮箱格式错误";
                return Json(msg);
            }
            if (iuiBll.GetModel(p => (p.sLoginName == sEmail || p.sEmail == sEmail) && p.gkey != gkey) != null)
            {
                msg.flag = 0;
                msg.msg = "邮箱已经存在";
                return Json(msg);
            }
            var model = iuiBll.GetModel(p => p.gkey == gkey);
            if (model == null)
            {
                msg.flag = 0;
                msg.msg = "参数错误";
                return Json(msg);
            }
            Validate vd = ivBll.GetModel(p => p.gUserKey == gkey && p.iType == 2 && p.dOverdueTime > (DateTime.Now) && p.iFlag == 1);
            if (vd != null)
            {
                msg.flag = 0;
                msg.msg = "邮件已发送，请勿重复发送";
                return Json(msg);
            }
            vd = new Validate()
            {
                gkey = Guid.NewGuid(),
                gUserKey = gkey,
                dCreateTime = DateTime.Now,
                dOverdueTime = DateTime.Now.AddMinutes(10),
                iFlag = 1,
                iType = 2
            };
            model.sEmail = sEmail;
            model.iEmailValidate = 0;
            iuiBll.Update(model);
            SetSessionUserInfo(model);
            string body = EmailHelper.bodyEmailTemp.Replace("#name#", model.sName).Replace("#url#", "/Default/ValidateEmail?key=" + vd.gkey);
            if (EmailHelper.SendWebEmail(sEmail, "【唐突世界】验证邮箱", body))
            {
                ivBll.Add(vd);
                msg.flag = 1;
                msg.msg = "验证邮箱已经发送到邮箱,请注意查收";
            }
            else
            {
                msg.flag = 0;
                msg.msg = "邮件发送失败";
            }
            return Json(msg);
        }


        /// <summary>
        /// 验证邮箱
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ValidateEmail(Guid key)
        {
            Validate vd = ivBll.GetModel(p => p.gkey == key && p.iType == 2 && p.iFlag == 1 && p.dOverdueTime > (DateTime.Now));
            if (vd != null)
            {
                vd.iFlag = 0;
                if (ivBll.Update(vd))
                {
                    Guid userKey = vd.gUserKey;
                    var model = iuiBll.GetModel(p => p.gkey == userKey);
                    model.iEmailValidate = 1;
                    iuiBll.Update(model);
                    return result("验证成功");
                }
                else
                    return result("验证失败");
            }
            else
                return result("验证地址失效");
        }

        /// <summary>
        /// 激活账户
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Validateloginer(Guid key)
        {
            Validate vd = ivBll.GetModel(p => p.gkey == key && p.iType == 1 && p.iFlag == 1 && p.dOverdueTime > (DateTime.Now));
            if (vd != null)
            {
                vd.iFlag = 0;
                if (ivBll.Update(vd))
                {
                    Guid userKey = vd.gUserKey;
                    var model = iuiBll.GetModel(p => p.gkey == userKey);
                    model.sEmail = model.sLoginName;
                    model.iEmailValidate = 1;
                    iuiBll.Update(model);
                    return result("激活成功");
                }
                else
                    return result("激活失败");
            }
            else
                return result("激活地址失效");
        }

        /// <summary>
        /// 忘记密码界面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Forget()
        {
            return View();
        }

        /// <summary>
        /// 发送重置密码密码邮件
        /// </summary>
        /// <param name="mail"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SeedPassWordMail(string mail)
        {
            MsgHelper msg = new MsgHelper();
            if (!ValidateHelper.RegexMail(mail))
            {
                msg.flag = 0;
                msg.msg = "邮箱格式错误";
                return Json(msg);
            }
            var model = iuiBll.GetModel(p => p.sEmail == mail && p.iEmailValidate == 1);
            if (model == null)
            {
                msg.flag = 0;
                msg.msg = "邮箱不存在或未验证过，请联系管理员";
                return Json(msg);
            }
            Guid gkey = model.gkey;
            Validate vd = ivBll.GetModel(p => p.gUserKey == gkey && p.iType == 3 && p.dOverdueTime > (DateTime.Now) && p.iFlag == 1);
            if (vd != null)
            {
                msg.flag = 0;
                msg.msg = "邮件已发送，请勿重复发送";
                return Json(msg);
            }
            vd = new Validate()
            {
                gkey = Guid.NewGuid(),
                gUserKey = gkey,
                dCreateTime = DateTime.Now,
                dOverdueTime = DateTime.Now.AddMinutes(10),
                iFlag = 1,
                iType = 3
            };
            string body = EmailHelper.bodyPassTemp.Replace("#name#", model.sName).Replace("#url#", "/Default/ValidatePass?key=" + vd.gkey);
            if (EmailHelper.SendWebEmail(model.sEmail, "【唐突世界】重置密码", body))
            {
                ivBll.Add(vd);
                msg.flag = 1;
                msg.msg = "重置密码邮件已经发送到邮箱,请注意查收";
            }
            else
            {
                msg.flag = 0;
                msg.msg = "邮件发送失败";
            }
            return Json(msg);
        }

        /// <summary>
        /// 重置密码
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ValidatePass(Guid key)
        {
            Validate vd = ivBll.GetModel(p => p.gkey == key && p.iType == 3 && p.iFlag == 1 && p.dOverdueTime > (DateTime.Now));
            if (vd != null)
            {
                return View(vd);
            }
            else
                return result("重置密码地址失效");
        }

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="newPass">新密码</param>
        /// <param name="rePass">确认密码</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult EditPassWord(Guid key, string newPass, string rePass)
        {
            Validate vd = ivBll.GetModel(p => p.gkey == key && p.iType == 3 && p.iFlag == 1 && p.dOverdueTime > (DateTime.Now));
            if (vd != null)
            {
                if (newPass != rePass)
                    return Json(new MsgHelper() { flag = 0, msg = "两次密码不同" });
                if (!ValidateHelper.RegexPassWord(newPass))
                    return Json(new MsgHelper() { flag = 0, msg = "密码应为6到16位字符" });
                Guid g = vd.gUserKey;
                UserInfo ui = iuiBll.GetModel(p => p.gkey == g);
                ui.sPassWord = MD5Helper.getMD5By32(newPass);
                if (iuiBll.Update(ui))
                {
                    vd.iFlag = 0;
                    ivBll.Update(vd);
                    return Json(new MsgHelper() { flag = 1, msg = "密码修改成功" });
                }
                else
                    return Json(new MsgHelper() { flag = 0, msg = "密码修改失败" });
            }
            else
                return Json(new MsgHelper() { flag = 0, msg = "重置密码地址失效" });
        }

        /// <summary>
        /// 放回结果
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        private ContentResult result(string msg)
        {
            return Content(@"
                        <!DOCTYPE html>
                        <script src='../../../layui/layui.all.js'></script>                
                        <script>
                            parent.layer.open({
                                title: '提示',
                                content: '" + msg + @"'
                                ,
                                yes: function () {
                                     top.location.href ='" + global::System.Configuration.ConfigurationManager.AppSettings["WebUrl"] + @"' ; 
                                }
                            });                   
                        </script>");
        }
        #endregion

        #region 商城

        /// <summary>
        /// 商城首页
        /// </summary>
        /// <returns></returns>
        public ActionResult ShopView()
        {
            return View();
        }

        /// <summary>
        /// 获取出售的物品
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetGoods()
        {
            return Json(new MsgHelper() { data = igBll.GetList(p => p.iType == 0), flag = 1, msg = "获取物品成功" });
        }
        #endregion
    }
}