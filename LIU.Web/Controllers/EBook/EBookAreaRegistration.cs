﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LIU.Web.Controllers.EBook
{
    public class EBookAreaRegistration: AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "EBook";
            }
        }
        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "EBook_default",
                "EBook/{controller}/{action}/{id}",
                new { id = UrlParameter.Optional }
            );
        }
    }
}