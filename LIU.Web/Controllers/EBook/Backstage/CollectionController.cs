﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LIU.IBLL.EBook;
using LIU.Model.EBook;
using LIU.UnityFactory;
using LIU.Common;
using LIU.Web.App_Code;
using LIU.CollectionService;

namespace LIU.Web.Controllers.EBook
{
    public class CollectionController : BaseController
    {
        private readonly ICollectionBLL iCBll = IOCFactory.GetIOCResolve<ICollectionBLL>();
        private readonly INovelBLL iNBll = IOCFactory.GetIOCResolve<INovelBLL>();
        private readonly IChapterBLL chapterBLL = IOCFactory.GetIOCResolve<IChapterBLL>();
        // GET: Collection
        public ActionResult Index(string novelkey)
        {
            Guid g = new Guid(novelkey);
            if (iNBll.GetModel(p => p.gkey == g) == null)
            {
                return Content("<script>alert('小说不存在');window.opener=null;window.open('','_self');window.close();</script>");
            }
            Collection collection = iCBll.GetModel(p => p.gNovelKey == g);
            if (collection == null)
            {
                collection = new Collection();
                collection.gkey = Guid.NewGuid();
                collection.gNovelKey = g;
                iCBll.Add(collection);
            }
            return View(collection);
        }

        /// <summary>
        /// 添加配置
        /// </summary>
        /// <param name="collection">配置实体</param>
        /// <returns></returns>
        [ValidateInput(false)]
        public ActionResult addCollection(Collection collection)
        {
            MsgHelper msg = new MsgHelper();
            if (iNBll.GetModel(p => p.gkey == collection.gNovelKey) == null)
            {//判断小说是否存在
                msg.flag = 0;
                msg.msg = "配置失败,小说不存在";
                return Json(msg);
            }
            if (iCBll.Update(collection))
            {//添加
                msg.flag = 1;
                msg.msg = "配置成功";
            }
            else
            {
                msg.flag = 0;
                msg.msg = "配置失败";
            }
            return Json(msg);
        }

        /// <summary>
        /// 测试配置
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        public ActionResult TestCollection(Collection collection)
        {
            CollectionHelper ch = new CollectionHelper();
            ch.StartCollection(collection.gStartAdd, collection.gTextRegex, collection.gNextRegex, collection.gTitleRegex, 1, 3);
            return Content(ch.result);
        }

        /// <summary>
        /// 开始采集
        /// </summary>
        /// <param name="gkey">采集配置主键</param>
        /// <returns></returns>
        public ActionResult StartCollection(Guid gkey)
        {
            //MsgHelper msg = new MsgHelper();
            //Collection collection = iCBll.GetModel(p => p.gkey == (new Guid(gkey)));
            //if (collection == null)
            //{
            //    msg.flag = 0;
            //    msg.msg = "采集失败，请先保存采集在进行采集操作";
            //}
            //else
            //{
            //    int max = chapterBLL.getMaxChapter(collection.gNovelKey);
            //    CollectionHelper ch = new CollectionHelper();
            //    ch.Gkey = collection.gkey;
            //    ch.StartCollection(string.IsNullOrEmpty(collection.gIndexAdd) ? collection.gStartAdd : collection.gIndexAdd,
            //        collection.gTextRegex,
            //        collection.gNextRegex,
            //        collection.gTitleRegex, max < 1 ? 1 : max);
            //    msg.flag = 1;
            //    msg.msg = "采集开始，采集时间可能过长，请通过刷新配置界面查看采集状态";
            //}
            //return Json(msg);
            var novel = iNBll.GetModel(p => p.gkey == gkey);
            if (novel == null)
                return Json(new MsgHelper() { flag = 0, msg = "小说不存在" });
            ICollectionHandle collection = CollectionFactory.CreateICollectionHandle(novel);
            collection.CollectionInfo();
            return Json(new MsgHelper() { flag = 1, msg = "采集开始，采集时间可能过长，请刷新" });
        }
    }
}