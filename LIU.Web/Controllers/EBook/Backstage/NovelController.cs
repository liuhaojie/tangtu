﻿using LIU.Common;
using LIU.IBLL.EBook;
using LIU.Model.EBook;
using LIU.Model.System;
using LIU.UnityFactory;
using LIU.Web.App_Code;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace LIU.Web.Controllers.EBook
{
    public class NovelController : BaseController
    {
        private readonly IChapterBLL iCBll = IOCFactory.GetIOCResolve<IChapterBLL>();
        private readonly INovelBLL iNBll = IOCFactory.GetIOCResolve<INovelBLL>();
        // GET: Novel
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 获取
        /// </summary>
        /// <returns></returns>
        public ActionResult getList(int page, int limit, string sName, string sAuthor, int? iType)
        {
            int count = 0;
            List<Novel> list = iNBll.GetList(p =>
                (string.IsNullOrEmpty(sName.Trim()) ? true : p.sName.Contains(sName.Trim())) &&
                (string.IsNullOrEmpty(sAuthor.Trim()) ? true : p.sAuthor.Contains(sAuthor.Trim())) &&
                ((iType == 0 || iType == null) ? true : p.iType == iType),
                p => new { p.dInsertTime }, new PageHelper(limit, page), false, ref count);
            LayuiDataHelper layuiDataHelper = new LayuiDataHelper();
            layuiDataHelper.code = 0;
            layuiDataHelper.count = count;
            layuiDataHelper.data = list;
            return Json(layuiDataHelper);
        }

        /// <summary>
        /// 添加界面
        /// </summary>
        /// <returns></returns>
        public ActionResult AddView()
        {
            return View();
        }

        /// <summary>
        /// 添加小说
        /// </summary>
        /// <returns></returns>
        public ActionResult AddNovel(Novel novel)
        {
            MsgHelper msg = new MsgHelper();
            if (string.IsNullOrEmpty(novel.sName) || string.IsNullOrEmpty(novel.sAuthor))
            {
                msg.flag = 0;
                msg.msg = "不可以留空";
                return Json(msg);
            }

            if (iNBll.GetModel(p => p.sName == novel.sName) != null)
            {
                msg.flag = 0;
                msg.msg = "小说名已经存在";
            }
            else
            {
                novel.gkey = Guid.NewGuid();
                novel.dInsertTime = DateTime.Now;
                novel.gInsertUserKey = GetSessionUserInfo().gkey;
                if (iNBll.Add(novel))
                {
                    msg.flag = 1;
                    msg.msg = "添加成功";
                }
                else
                {
                    msg.flag = 0;
                    msg.msg = "添加失败";
                }
            }
            return Json(msg);
        }

        /// <summary>
        /// 修改界面
        /// </summary>
        /// <returns></returns>
        public ActionResult EditView(Guid gkey)
        {
            return View(iNBll.GetModel(p => p.gkey == gkey));
        }

        /// <summary>
        /// 修改小说
        /// </summary>
        /// <param name="novel"></param>
        /// <returns></returns>
        public ActionResult EditNover(Novel novel)
        {
            MsgHelper msg = new MsgHelper();
            if (string.IsNullOrEmpty(novel.sName) || string.IsNullOrEmpty(novel.sAuthor))
            {
                msg.flag = 0;
                msg.msg = "不可以留空";
                return Json(msg);
            }
            if (iNBll.GetModel(p => p.sName == novel.sName && p.gkey != novel.gkey) != null)
            {
                msg.flag = 0;
                msg.msg = "小说名重复";
                return Json(msg);
            }
            Novel model = iNBll.GetModel(p => p.gkey == novel.gkey);
            model.iType = novel.iType;
            model.sName = novel.sName;
            model.sAuthor = novel.sAuthor;
            model.gIndexAdd = novel.gIndexAdd;
            model.gStartAdd = novel.gStartAdd;
            model.sWebsiteName = novel.sWebsiteName;
            model.gUpdateUserKey = GetSessionUserInfo().gkey;
            model.dUpdateTime = DateTime.Now;
            if (iNBll.Update(model))
            {
                msg.flag = 1;
                msg.msg = "修改成功";
            }
            else
            {
                msg.flag = 0;
                msg.msg = "修改失败";
            }
            return Json(msg);
        }

        /// <summary>
        /// 删除小说
        /// </summary>
        /// <param name="noverKey">小说主键</param>
        /// <returns></returns>
        public ActionResult DeleteNover(string noverKey)
        {
            MsgHelper msg = new MsgHelper();
            try
            {
                Guid gk = new Guid(noverKey);
                Novel novel = iNBll.GetModel(p => p.gkey == gk);
                if (novel == null)
                {
                    msg.flag = 0;
                    msg.msg = "删除的小说不存在";
                }
                else
                {
                    if (iNBll.UnionDelete(gk, novel))
                    {
                        msg.flag = 1;
                        msg.msg = "删除成功";
                    }
                    else
                    {
                        msg.flag = 0;
                        msg.msg = "删除失败";
                    }
                }
            }
            catch (Exception ex)
            {
                msg.flag = 0;
                msg.msg = ex.Message;
            }
            return Json(msg);
        }

        /// <summary>
        /// 生成静态页面
        /// </summary>
        /// <param name="isAll">是否全部生成</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CreateHtml(bool isAll = false)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + @"\Template\html\BookConent.html";
            if (!global::System.IO.File.Exists(path))
                return Json(new MsgHelper() { flag = 0, msg = "模板文件不存在" });
            FileInfo fi = new FileInfo(path);
            List<Parameter> list = SysCache.ParameterList;
            string str = "";
            using (StreamReader sr = new StreamReader(fi.FullName))
            {
                str = sr.ReadToEnd();
            }

            MatchCollection mcs = getRegex(@"#(\w+)#", str);
            foreach (Match m in mcs)//替换参数
            {
                string title = m.Groups[1].ToString().Trim();
                Parameter pr = list.Where(p => p.sCode == title).FirstOrDefault();
                if (pr == null)
                    str = str.Replace(m.Value, "");
                else
                    str = str.Replace(m.Value, pr.sValue);
            }

            List<Novel> novelList = iNBll.GetList(p => true);
            foreach (var novel in novelList)//循环小说
            {
                Guid gkey = novel.gkey;
                if (!global::System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + $@"\Html\EBook\{gkey}\"))
                    Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + $@"\Html\EBook\{gkey}\");
                List<Chapter> chapterList = iCBll.GetList(p => (isAll ? true : (p.sHtmlUrl == null || p.sHtmlUrl == ""))
                                && (p.gNovelKey == gkey)).OrderBy(p => p.iIndex).ToList();//获取该小说的未生成静态 条数
                string tempStr = "";
                if (chapterList.Count == 0)
                    continue;
                int lastIndex = chapterList.Last().iIndex;//最大章节
                if (chapterList != null && chapterList[0].iIndex != 1)//非第一章 要修改已生成最后一章的数据
                {
                    path = AppDomain.CurrentDomain.BaseDirectory + $@"\Html\EBook\{gkey}\{chapterList[0].iIndex - 1}.html";
                    if (!global::System.IO.File.Exists(path))
                        return Json(new MsgHelper() { flag = 0, msg = "服务器文件出现丢失,请重新生成" });
                    using (StreamReader sr = new StreamReader(path))
                    {
                        tempStr = sr.ReadToEnd();
                    }
                    tempStr = tempStr.Replace($"href=\"/EBook/LookChapter/ChapterList?gkey={gkey}\">下一章</a>", $"href=\"/Html/EBook/{gkey}/{chapterList[0].iIndex}.html\">下一章</a>");
                    tempStr = tempStr.Replace($"href=\"/Html/EBook/{gkey}/{chapterList[0].iIndex}.html\">下一章</a>", $"href=\"/Html/EBook/{gkey}/{chapterList[0].iIndex}.html\">下一章</a>");
                    using (FileStream fs = new FileStream(path, FileMode.Create, FileAccess.Write))
                    {
                        using (StreamWriter sw = new StreamWriter(fs, Encoding.UTF8))
                        {
                            sw.Write(tempStr);
                            sw.Flush();
                            sw.Close();
                            sw.Dispose();
                        }
                        fs.Close();
                        fs.Dispose();
                    }
                }
                foreach (var Model in chapterList)
                {
                    tempStr = str;
                    tempStr = tempStr.Replace("@Model.gkey", Model.gkey.ToString());
                    tempStr = tempStr.Replace("@Model.gNovelKey", Model.gNovelKey.ToString());
                    tempStr = tempStr.Replace("@Model.sTitle", Model.sTitle);
                    tempStr = tempStr.Replace("@Model.sText", Model.sText);
                    tempStr = tempStr.Replace("@Model.dInsertTime", Model.dInsertTime.ToString("yyyy-MM-dd HH:mm:ss"));
                    tempStr = tempStr.Replace("@Model.iIndex", Model.iIndex.ToString());
                    if (Model.iIndex > 1 && Model.iIndex < lastIndex)
                    {
                        tempStr = tempStr.Replace("@Model.prev", $"/Html/EBook/{gkey}/{Model.iIndex - 1}.html");
                        tempStr = tempStr.Replace("@Model.next", $"/Html/EBook/{gkey}/{Model.iIndex + 1}.html");
                    }
                    else if (Model.iIndex == 1 && Model.iIndex == lastIndex)
                    {
                        tempStr = tempStr.Replace("@Model.prev", $"/EBook/LookChapter/ChapterList?gkey={gkey}");
                        tempStr = tempStr.Replace("@Model.next", $"/EBook/LookChapter/ChapterList?gkey={gkey}");
                    }
                    else if (Model.iIndex == 1)
                    {
                        tempStr = tempStr.Replace("@Model.prev", $"/EBook/LookChapter/ChapterList?gkey={gkey}");
                        tempStr = tempStr.Replace("@Model.next", $"/Html/EBook/{gkey}/{Model.iIndex + 1}.html");
                    }
                    else if (Model.iIndex == lastIndex)
                    {
                        tempStr = tempStr.Replace("@Model.prev", $"/Html/EBook/{gkey}/{Model.iIndex - 1}.html");
                        tempStr = tempStr.Replace("@Model.next", $"/EBook/LookChapter/ChapterList?gkey={gkey}");
                    }
                    using (FileStream fs = new FileStream(AppDomain.CurrentDomain.BaseDirectory + $@"\Html\EBook\{gkey}\{Model.iIndex}.html", FileMode.Create, FileAccess.Write))
                    {
                        using (StreamWriter sw = new StreamWriter(fs, Encoding.UTF8))
                        {
                            sw.Write(tempStr);
                            sw.Flush();
                            sw.Close();
                            sw.Dispose();
                        }
                        fs.Close();
                        fs.Dispose();
                    }

                    Model.sHtmlUrl = $"/Html/EBook/{gkey}/{Model.iIndex}.html";
                    iCBll.Update(Model);
                }

            }
            return Json(new MsgHelper() { flag = 1, msg = "生成成功" });
        }

        /// <summary>
        /// 获取正则结果
        /// </summary>
        /// <param name="regText">正则表达式</param>
        /// <param name="content">待匹配内容</param>
        /// <returns></returns>
        private MatchCollection getRegex(string regText, string content)
        {
            Regex reg = new Regex(regText, RegexOptions.IgnoreCase);
            MatchCollection matchs = reg.Matches(content);
            return matchs;// matchs[0].Groups[1].ToString();
        }
    }
}