﻿using LIU.Common;
using LIU.IBLL.EBook;
using LIU.Model.EBook;
using LIU.UnityFactory;
using LIU.Web.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LIU.Web.Controllers.EBook
{
    public class ChapterController : BaseController
    {
        private readonly IChapterBLL iCBll = IOCFactory.GetIOCResolve<IChapterBLL>();
        // GET: Chapter
        public ActionResult Index(string novelkey)
        {
            ViewData["gNovelKey"] = novelkey;
            return View();
        }

        /// <summary>
        /// 获取章节内容
        /// </summary>
        /// <param name="novelKey">小说主键</param>
        /// <param name="index">第几章</param>
        /// <returns></returns>
        public ActionResult GetChapter(string novelKey, string index)
        {
            MsgHelper msg = new MsgHelper();
            try
            {
                Guid gk = new Guid(novelKey);
                int i = Convert.ToInt32(index);
                Chapter model = iCBll.GetModel(p => p.gNovelKey == gk && p.iIndex == i);
                if (model == null)
                {
                    msg.flag = 0;
                    msg.msg = "小说不存在，或者章节不存在";
                }
                else
                {
                    msg.flag = 1;
                    int count = iCBll.GetRowCount(p => p.gNovelKey == gk);                    
                    Dictionary<string, object> dic = new Dictionary<string, object>();
                    dic.Add("pageCount", count);
                    dic.Add("title", model.sTitle);
                    dic.Add("content", model.sText);
                    msg.msg = JsonHelper.getJsonString(dic);
                }
            }
            catch (Exception ex)
            {
                msg.flag = 0;
                msg.msg = ex.Message;
            }
            return Json(msg);
        }

        /// <summary>
        /// 获取小说总章节
        /// </summary>
        /// <param name="novelKey"></param>
        /// <returns></returns>
        public ActionResult GetChapterCount(string novelKey)
        {
            MsgHelper msg = new MsgHelper();
            Guid gk = new Guid(novelKey);
            int count = iCBll.GetRowCount(p => p.gNovelKey == gk);
            if (count == 0)
            {
                msg.flag = 0;
                msg.msg = "小说不存在";
            }
            else
            {
                msg.flag = 1;
                msg.msg = count.ToString();
            }
            return Json(msg);
        }


    }
}