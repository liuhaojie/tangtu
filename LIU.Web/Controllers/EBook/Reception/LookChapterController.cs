﻿using LIU.Common;
using LIU.IBLL.EBook;
using LIU.Model.EBook;
using LIU.UnityFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LIU.Web.Controllers.EBook.Reception
{
    public class LookChapterController : Controller
    {
        private readonly IChapterBLL iCBll = IOCFactory.GetIOCResolve<IChapterBLL>();
        private readonly INovelBLL iNBll = IOCFactory.GetIOCResolve<INovelBLL>();

        /// <summary>
        /// 查看小说具体章节
        /// </summary>
        /// <param name="guid">小说主键</param>
        /// <param name="page">第几章节</param>
        /// <returns></returns>
        public ActionResult Index(Guid gkey, int page = 1)
        {
            Chapter chapter = iCBll.GetModel(p => p.gNovelKey == gkey && p.iIndex == page);
            if (chapter == null)
            {
                return Redirect("/Default/Index");
            }
            else
            {
                chapter.iRead++;
                try
                {//增加阅读次数
                    iCBll.Update(chapter);
                }
                catch { }
                ViewData["count"] = iCBll.getMaxChapter(gkey);
                return View(chapter);
            }
        }

        /// <summary>
        /// 小说列表
        /// </summary>
        /// <returns></returns>
        public ActionResult NovelList(int page = 1, int limit = 10, string keyWord = "")
        {
            int rowsCount = 0;
            List<Novel> list = iNBll.GetList(p => string.IsNullOrEmpty(keyWord) ? true : (
            p.sName.Contains(keyWord) || p.sAuthor.Contains(keyWord)
            ), p => new { p.dInsertTime }, new PageHelper(limit, page), false, ref rowsCount);
            ViewData["page"] = page;
            ViewData["rowsCount"] = rowsCount;
            ViewData["list"] = list;
            ViewData["limit"] = limit;
            ViewData["keyWord"] = keyWord??"";
            return View(list);
        }

        /// <summary>
        /// 章节列表
        /// </summary>
        /// <param name="gkey"></param>
        /// <returns></returns>
        public ActionResult ChapterList(Guid gkey, int page = 1, int limit = 10)
        {
            int rowsCount = 0;
            List<Chapter> list = iCBll.GetList(p => p.gNovelKey == gkey, p => new { p.iIndex }, new PageHelper(limit, page), false, ref rowsCount);
            Novel novel = iNBll.GetModel(p => p.gkey == gkey);
            ViewData["page"] = page;
            ViewData["gkey"] = gkey;
            ViewData["rowsCount"] = rowsCount;
            ViewData["list"] = list;
            ViewData["limit"] = limit;
            return View(novel);
        }

        /// <summary>
        /// 静态页面异步阅读小说
        /// </summary>
        /// <param name="gkey"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Read(Guid gkey)
        {
            try
            {
                Chapter c = iCBll.GetModel(p => p.gkey == gkey);
                if (c != null)
                {
                    c.iRead = c.iRead + 1;
                    iCBll.Update(c);
                    return Json(new MsgHelper() { flag = 1, msg = "成功" });
                }
            }
            catch (Exception ex)
            {
                return Json(new MsgHelper() { flag = 0, msg = "出错" + ex.Message });
            }
            return Json(new MsgHelper() { flag = 0, msg = "有错误" });
        }
    }
}