﻿using LIU.Common;
using LIU.IBLL.System;
using LIU.Model;
using LIU.Model.System;
using LIU.UnityFactory;
using LIU.Web.App_Code;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LIU.Web.Controllers.System
{
    /// <summary>
    /// 前台留言板
    /// </summary>
    public class MsgBoardController : WebBaseController
    {
        /// <summary>
        /// 留言板
        /// </summary>
        private readonly IMessageBoardBLL imbBll = IOCFactory.GetIOCResolve<IMessageBoardBLL>();

        /// <summary>
        /// 用户信息
        /// </summary>
        private readonly IUserInfoBLL iurBll = IOCFactory.GetIOCResolve<IUserInfoBLL>();

        /// <summary>
        /// 留言版首页
        /// </summary>
        /// <param name="limit">条数(默认10条)</param>
        /// <param name="page">页数(默认1)</param>
        /// <returns></returns>
        public ActionResult Index(int page = 1, int limit = 10)
        {
            int count = 0;
            List<MessageBoard> list = imbBll.GetList(p => p.iFlag == 1, p => new { p.dInsert }, new PageHelper(limit, page), false, ref count);
            List<Guid> glist = new List<Guid>();
            foreach (MessageBoard mb in list)
            {
                glist.Add(mb.gInsertKey);
                if (mb.gReplyKey != null)
                    glist.Add(new Guid(mb.gReplyKey.ToString()));
            }
            glist = glist.Distinct().ToList();
            List<UserInfo> uiList = iurBll.GetList(p => glist.Contains(p.gkey));
            List<MessageBoardDTO> dTOList = new List<MessageBoardDTO>();
            foreach (MessageBoard mb in list)
            {
                MessageBoardDTO dTO = new MessageBoardDTO();
                UserInfo insert = uiList.Where(p => p.gkey == mb.gInsertKey).FirstOrDefault();
                UserInfo reply = uiList.Where(p => p.gkey == mb.gReplyKey).FirstOrDefault();
                dTO.gkey = mb.gkey;
                dTO.sMessage = mb.sMessage;
                dTO.sReply = mb.sReply;
                dTO.gInsertKey = mb.gInsertKey;
                dTO.sInsertName = insert == null ? "热心网友" : insert.sName;
                dTO.sInsertImage = insert == null ? SysCache.ParameterList.Where(p => p.sCode == "defultImage" && p.iFlag == 1).FirstOrDefault().sValue : insert.sImageUrl;
                dTO.sInsertHerdImage = insert == null ? "" : insert?.sHerdImageUrl;
                dTO.iInsertLevel = insert == null ? 0 : insert.iLevel;
                dTO.dInsertLevelTime = insert == null ? null : insert.dLevelTIme;
                dTO.dInsert = mb.dInsert;
                dTO.gReplyKey = mb.gReplyKey;
                dTO.sReplyName = reply == null ? "热心网友" : reply.sName;
                dTO.sReplyImage = reply == null ? SysCache.ParameterList.Where(p => p.sCode == "defultImage" && p.iFlag == 1).FirstOrDefault().sValue : reply.sImageUrl;
                dTO.sReplyHerdImage = reply == null ? "" : reply?.sHerdImageUrl;
                dTO.iReplyLevel = reply == null ? 0 : reply.iLevel;
                dTO.dReplyLevelTime = reply == null ? null : reply.dLevelTIme;
                dTO.dReply = mb.dReply;
                dTO.iType = SysCache.DicInfoList.Where(p => p.sCode == "messageCode" && p.iFlag == 1 && p.sValue == mb.iType.ToString()).FirstOrDefault().sName;
                dTOList.Add(dTO);
            }
            ViewData["page"] = page;
            ViewData["rowsCount"] = count;
            ViewData["list"] = dTOList;
            ViewData["limit"] = limit;
            if (GetSessionUserInfo() == null)
                ViewData["UserKey"] = "";
            else
            {
                if (GetSessionUserInfo().gkey == SysCache.Adminkey)
                    ViewData["UserKey"] = "0";
                else
                    ViewData["UserKey"] = GetSessionUserInfo()?.gkey;
            }


            return View();
        }

        /// <summary>
        /// 获取留言类型
        /// </summary>
        /// <returns></returns>
        public ActionResult GetMessageType()
        {
            MsgHelper msg = new MsgHelper();
            List<DicInfo> di = SysCache.DicInfoList.Where(p => p.sCode == "messageCode" && p.iFlag == 1).ToList();
            if (di.Count == 0)
            {
                msg.flag = 0;
                msg.msg = "获取留言类型失败，请联系管理员或者直接留言";
            }
            else
            {
                msg.flag = 1;
                msg.msg = JsonHelper.getJsonString(di);
            }
            return Json(msg);
        }

        /// <summary>
        /// 添加留言
        /// </summary>
        /// <param name="mb"></param>
        /// <returns></returns>
        public ActionResult AddMessage(MessageBoard mb)
        {
            MsgHelper msg = new MsgHelper();
            if (GetSessionUserInfo() == null)
            {
                msg.flag = 0;
                msg.msg = "未登录,不可以留言";
                return Json(msg);
            }
            if (string.IsNullOrWhiteSpace(mb.sMessage))
            {
                msg.flag = 0;
                msg.msg = "留言内容不可以为空";
                return Json(msg);
            }
            if (SysCache.DicInfoList.Where(p => p.sCode == "messageCode" && p.iFlag == 1 && p.sValue == mb.iType.ToString()).FirstOrDefault() == null)
            {
                msg.flag = 0;
                msg.msg = "留言类型不存在";
                return Json(msg);
            }
            Guid gkey = GetSessionUserInfo().gkey;
            DateTime dt = DateTime.Now.Date;
            if (imbBll.GetRowCount(p => p.gInsertKey == gkey && p.dInsert >= dt) >= 3)
            {
                msg.flag = 0;
                msg.msg = "每天留言不可以超过3次";
                return Json(msg);
            }
            mb.gkey = Guid.NewGuid();
            mb.iFlag = 1;
            mb.dInsert = DateTime.Now;
            //if (GetSessionUserInfo() != null)
            //{
            mb.gInsertKey = GetSessionUserInfo().gkey;
            //}
            if (imbBll.Add(mb))
            {
                msg.flag = 1;
                msg.msg = "留言成功";
            }
            else
            {
                msg.flag = 0;
                msg.msg = "留言失败";
            }
            return Json(msg);
        }

    }

    public class MessageBoardDTO
    {

        /// <summary>
        /// 主键
        /// </summary>
        public Guid gkey { get; set; }

        /// <summary>
        /// 留言内容
        /// </summary>
        public string sMessage { get; set; }

        /// <summary>
        /// 回复内容
        /// </summary>
        public string sReply { get; set; }

        /// <summary>
        /// 留言人员主键
        /// </summary>
        public Guid gInsertKey { get; set; }

        /// <summary>
        /// 留言人员名
        /// </summary>
        public string sInsertName { get; set; }

        /// <summary>
        /// 留言人员头像
        /// </summary>
        public string sInsertImage { get; set; }

        /// <summary>
        /// 留言人员头像框
        /// </summary>
        public string sInsertHerdImage { get; set; }

        /// <summary>
        /// 留言人员等级
        /// </summary>
        public int iInsertLevel { get; set; }

        /// <summary>
        /// 留言人员等级到期时间
        /// </summary>
        public DateTime? dInsertLevelTime { get; set; }

        /// <summary>
        /// 留言时间
        /// </summary>
        public DateTime dInsert { get; set; }

        /// <summary>
        /// 回复人员主键
        /// </summary>
        public Guid? gReplyKey { get; set; }

        /// <summary>
        /// 回复人员名
        /// </summary>
        public string sReplyName { get; set; }

        /// <summary>
        /// 回复人员头像
        /// </summary>
        public string sReplyImage { get; set; }

        /// <summary>
        /// 回复人员头像框
        /// </summary>
        public string sReplyHerdImage { get; set; }


        /// <summary>
        /// 回复人员等级
        /// </summary>
        public int iReplyLevel { get; set; }

        /// <summary>
        /// 回复人员等级到期时间
        /// </summary>
        public DateTime? dReplyLevelTime { get; set; }

        /// <summary>
        /// 回复时间
        /// </summary>
        public DateTime? dReply { get; set; }

        /// <summary>
        /// 留言类型 
        /// </summary>
        public string iType { get; set; }

    }
}