﻿using LIU.Common;
using LIU.GoodsService;
using LIU.IBLL.System;
using LIU.Model;
using LIU.Model.DTOModel;
using LIU.Model.System;
using LIU.UnityFactory;
using LIU.Web.App_Code;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace LIU.Web.Controllers.System
{
    public class UserController : WebBaseController
    {


        private readonly IUserInfoBLL iurBll = IOCFactory.GetIOCResolve<IUserInfoBLL>();
        private readonly INotifyMessageBLL inmBll = IOCFactory.GetIOCResolve<INotifyMessageBLL>();
        /// <summary>
        /// 头像框
        /// </summary>
        private readonly IHeadFrameBLL ihfBll = IOCFactory.GetIOCResolve<IHeadFrameBLL>();

        /// <summary>
        /// 用户物品
        /// </summary>
        private readonly IUserGoodsBLL iugBll = IOCFactory.GetIOCResolve<IUserGoodsBLL>();
        /// <summary>
        /// 物品
        /// </summary>
        private readonly IGoodsBLL igBll = IOCFactory.GetIOCResolve<IGoodsBLL>();

        public UserController()
        {
            base.isCheckLogin = true;
        }

        /// <summary>
        /// 基本设置
        /// </summary>
        /// <returns></returns>
        public ActionResult Set()
        {
            return View(GetSessionUserInfo());
        }

        /// <summary>
        /// 上传头像
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UpLoadImg(HttpPostedFileBase file)
        {
            MsgHelper msg = new MsgHelper();
            try
            {
                if (file.ContentLength > 100 * 1024)
                {
                    msg.flag = 0;
                    msg.msg = "上传失败:文件大小超过100KB";
                    return Json(msg);
                }
                string SuffixName = file.FileName.Split('.').Last();
                string fileName = GetSessionUserInfo().gkey.ToString() + "." + SuffixName;
                string filePath = Server.MapPath(@"~/Images/User");
                file.SaveAs(Path.Combine(filePath, fileName));
                UserInfo ui = GetSessionUserInfo();
                ui.sImageUrl = @"/Images/User/" + fileName;
                if (iurBll.Update(ui))
                {
                    msg.flag = 1;
                    msg.msg = "上传成功";
                    SetSessionUserInfo(ui);
                }
                else
                {
                    msg.flag = 0;
                    msg.msg = "上传失败";
                }
            }
            catch (Exception ex)
            {
                msg.flag = 0;
                msg.msg = "上传失败:" + ex.Message;
            }
            return Json(msg);
        }

        /// <summary>
        /// 修改资料
        /// </summary>
        /// <param name="ui"></param>
        /// <returns></returns>
        public ActionResult EditUser(UserInfo ui)
        {
            MsgHelper msg = new MsgHelper();
            UserInfo u = GetSessionUserInfo();
            if (u.sName != ui.sName)
            {
                string name = ui.sName;
                if (iurBll.GetModel(p => p.sName == name) != null)//后面加上更名卡判断
                {
                    msg.flag = 0;
                    msg.msg = "昵称已存在";
                    return Json(msg);
                }
            }
            string HerdImageUrl = ui.sHerdImageUrl;
            HeadFrame hf = ihfBll.GetModel(p => p.sUrl == HerdImageUrl);
            if (hf == null)
            {
                msg.flag = 0;
                msg.msg = "修改失败,头像框地址不存在";
                return Json(msg);
            }
            if (hf.iLevel > u.iLevel || u.dLevelTIme == null || Convert.ToDateTime(u.dLevelTIme) < DateTime.Now)
            {
                msg.flag = 0;
                msg.msg = "修改失败,VIP级别不够或过期";
                return Json(msg);
            }

            u.sName = ui.sName;
            u.iSex = ui.iSex;
            u.sHerdImageUrl = ui.sHerdImageUrl;
            if (iurBll.Update(u))
            {
                msg.flag = 1;
                msg.msg = "修改成功";
                SetSessionUserInfo(u);
            }
            else
            {
                msg.flag = 0;
                msg.msg = "修改失败";
            }
            return Json(msg);
        }

        #region 通知消息
        /// <summary>
        /// 通知消息
        /// </summary>
        /// <returns></returns>
        [ValidateInput(false)]
        public ActionResult NotifyMsg(int page = 1, int limit = 10)
        {
            DB dbContext = IOCFactory.GetIOCResolve<DB>();
            Guid g = GetSessionUserInfo().gkey;
            //dynamic list ;
            var ss = from a in dbContext.NotifyMessage
                     join b in dbContext.UserInfo on a.gSendKey equals b.gkey into Temp
                     from b in Temp.DefaultIfEmpty()
                     where a.iFlag != 0 && a.gReceiveKey == g
                     orderby a.dSendDate descending
                     select new { a.gkey, a.iFlag, a.sSendTitle, a.sSendContent, a.sHref, a.dSendDate, a.iType, sName = b.sName == null ? "系统" : b.sName };
            var jg = ss.Skip((page - 1) * limit).Take(limit).ToList();
            List<ExpandoObject> objList = new List<ExpandoObject>();
            foreach (var item in jg)
            {
                dynamic d = new ExpandoObject();
                d.sSendTitle = item.sSendTitle;
                d.iFlag = item.iFlag;
                d.sSendContent = item.sSendContent;
                d.sHref = item.sHref;
                d.dSendDate = item.dSendDate;
                d.sName = item.sName;
                d.gkey = item.gkey;
                d.iType = item.iType;
                objList.Add(d);
            }
            ViewData["page"] = page;
            ViewData["rowsCount"] = inmBll.GetRowCount(p => p.iFlag != 0 && p.gReceiveKey == g);
            ViewBag.List = objList;//ViewData["list"] =
            ViewData["limit"] = limit;
            return View();
        }

        /// <summary>
        /// 阅读通知消息
        /// </summary>
        /// <param name="g"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        public ActionResult ReadNotifyMsgFlag(Guid g)
        {
            MsgHelper msg = new MsgHelper();
            Guid userKey = GetSessionUserInfo().gkey;
            NotifyMessage nm = inmBll.GetModel(p => p.gkey == g && p.gReceiveKey == userKey);
            if (nm == null)
            {
                msg.flag = 0;
                msg.msg = "阅读信息失败,请注意参数问题";
                return Json(msg);
            }
            object data = null;
            if (nm.iType == 3)//物品
            {
                List<Goods> goods = new List<Goods>();
                List<EmailGoodsDTO> emailGoods = JsonHelper.getObject<List<EmailGoodsDTO>>(nm.sHref);
                var model = emailGoods.Where(p => p.goodsID == 0).FirstOrDefault();
                if (model != null)
                    goods.Add(new Goods
                    {
                        ID = 0,
                        sExplain = $"【{model.count}】金币",
                        sName = $"{model.count}金币",
                        sHref = "/Images/Goods/gold.png",
                        sValue1 = model.count.ToString()
                    });
                List<int> idList = emailGoods.Where(p => p.goodsID != 0).Select(p => p.goodsID).ToList();
                if (idList.Any())
                {
                    List<Goods> goodsList = igBll.GetList(p => idList.Contains(p.ID)).OrderBy(p => p.iSort).ToList();
                    goodsList.ForEach(p =>
                    {
                        p.sValue1 = emailGoods.Where(x => x.goodsID == p.ID).First().count.ToString();
                    });
                    goods.AddRange(goodsList);
                }
                data = new
                {
                    goods,
                    nm.iFlag
                };
            }
            if (nm.iFlag != 1)//已经阅读或者领取过不在更新
            {
                msg.flag = 1;
                msg.msg = JsonHelper.getJsonString(nm);
                msg.data = data;
                return Json(msg);
            }
            nm.iFlag = 2;
            nm.dReadDate = DateTime.Now;
            if (inmBll.Update(nm))
            {
                msg.flag = 1;
                msg.msg = JsonHelper.getJsonString(nm);
                msg.data = data;
            }
            else
            {
                msg.flag = 0;
                msg.msg = "阅读信息失败";
            }
            return Json(msg);
        }

        /// <summary>
        /// 删除通知信息
        /// </summary>
        /// <param name="gkey">主键</param>
        /// <returns></returns>
        public ActionResult DelNotifyMsg(Guid gkey)
        {
            MsgHelper msg = new MsgHelper();
            Guid userKey = GetSessionUserInfo().gkey;
            NotifyMessage nm = inmBll.GetModel(p => p.gkey == gkey && p.gReceiveKey == userKey);
            if (nm == null)
            {
                msg.flag = 0;
                msg.msg = "删除失败,请注意参数问题";
                return Json(msg);
            }
            if (nm.iType == 3 && nm.iFlag != 3)
            {
                msg.flag = 0;
                msg.msg = "包含附件还没有领取";
                return Json(msg);
            }
            nm.iFlag = 0;
            if (inmBll.Update(nm))
            {
                msg.flag = 1;
                msg.msg = "删除成功";

            }
            else
            {
                msg.flag = 0;
                msg.msg = "删除失败";
            }
            return Json(msg);
        }

        /// <summary>
        /// 读取所有通知消息
        /// </summary>
        /// <returns></returns>
        public ActionResult ReadAllNotifyMsg()
        {
            MsgHelper msg = new MsgHelper();
            Guid userKey = GetSessionUserInfo().gkey;
            List<NotifyMessage> list = inmBll.GetList(p => p.gReceiveKey == userKey && p.iFlag == 1);
            foreach (NotifyMessage item in list)
            {
                item.iFlag = 2;
            }
            if (inmBll.Update(list))
            {
                msg.flag = 1;
                msg.msg = "标记完成";

            }
            else
            {
                msg.flag = 0;
                msg.msg = "标记失败";
            }
            return Json(msg);
        }

        /// <summary>
        /// 领取邮件中的物品
        /// </summary>
        /// <param name="gkey"></param>
        /// <returns></returns>
        public ActionResult ReceiveNotifyGoods(Guid? gkey)
        {
            Guid userKey = GetSessionUserInfo().gkey;
            List<NotifyMessage> list = new List<NotifyMessage>();
            if (gkey != null)
            {
                NotifyMessage nm = inmBll.GetModel(p => p.gkey == gkey && p.gReceiveKey == userKey && p.iType == 3 && p.iFlag != 3);
                if (nm == null)
                    return Json(new MsgHelper
                    {
                        flag = 0,
                        msg = "已经领取过了"
                    });
                list.Add(nm);
            }
            else//领取全部
            {
                list = inmBll.GetList(p => p.iType == 3 && p.iFlag != 3);//类型包含物品且未领取
                if (!list.Any())
                    return Json(new MsgHelper
                    {
                        flag = 0,
                        msg = "已经领取过了"
                    });
            }
            if (inmBll.ReceiveGoods(GetSessionUserInfo(), list))
                return Json(new MsgHelper
                {
                    flag = 1,
                    msg = "领取成功"
                });
            else
                return Json(new MsgHelper
                {
                    flag = 0,
                    msg = "领取失败，请稍后重试"
                });
        }
        #endregion


        /// <summary>
        /// 获取头像框列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetHeadFrameList()
        {
            return Json(
                new MsgHelper { data = ihfBll.GetList(p => true).OrderBy(p => p.iLevel).ThenBy(p => p.iSort), flag = 1, msg = "成功" }
                );
        }

        /// <summary>
        /// 用户主页面
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Home(Guid? key)
        {
            if (key == null)
                return View(GetSessionUserInfo());
            else
            {
                return View(iurBll.GetModel(p => p.gkey == key));
            }
        }

        #region 物品
        /// <summary>
        /// 获取用户的物品
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetUserGoods()
        {
            string s = JsonConvert.SerializeObject(iurBll.SqlQueryDataTable("EXEC P_Get_User_Goods @gkey = @key", new global::System.Data.SqlClient.SqlParameter[] {
                new global::System.Data.SqlClient.SqlParameter("@key",GetSessionUserInfo().gkey)}));
            return Content(s);
        }

        /// <summary>
        /// 使用物品
        /// </summary>
        /// <param name="goodsId"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UseGoods(int goodsId, int count)
        {
            Goods goods = igBll.GetModel(p => p.ID == goodsId);
            if (goods == null)
                return Json(new MsgHelper
                {
                    flag = 0,
                    msg = "物品不存在"
                });
            List<UserGoods> list = iugBll.GetList(p => p.GoodsID == goodsId && p.iFlag == 0);
            if (!list.Any() || list.Count < count)
                return Json(new MsgHelper
                {
                    flag = 0,
                    msg = "物品数量不够"
                });
            IGoodsHandle goodsHandle = GoodsFactory.CreateIGoodsHandle(goods);
            list = list.OrderBy(p => p.dCreateTime).Take(count).ToList();
            if (goodsHandle.UseGoods(GetSessionUserInfo(), list))
            {
                UpdateSessionUserInfo();
                return Json(new MsgHelper
                {
                    flag = 1,
                    msg = "使用成功",
                    data = GetSessionUserInfo()
                });
            }
            else
                return Json(new MsgHelper
                {
                    flag = 0,
                    msg = "使用失败,请查物品的使用说明"
                });
        }

        /// <summary>
        /// 出售物品
        /// </summary>
        /// <param name="goodsId"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult OutGoods(int goodsId, int count)
        {
            Goods goods = igBll.GetModel(p => p.ID == goodsId);
            if (goods == null)
                return Json(new MsgHelper
                {
                    flag = 0,
                    msg = "物品不存在"
                });
            List<UserGoods> list = iugBll.GetList(p => p.GoodsID == goodsId && p.iFlag == 0);
            if (!list.Any() || list.Count < count)
                return Json(new MsgHelper
                {
                    flag = 0,
                    msg = "物品数量不够"
                });

            list = list.OrderBy(p => p.dCreateTime).Take(count).ToList();
            if (iugBll.Out(GetSessionUserInfo(), goods, list))
            {
                UpdateSessionUserInfo();
                return Json(new MsgHelper
                {
                    flag = 1,
                    msg = "出售成功",
                    data = GetSessionUserInfo()
                });
            }
            else
                return Json(new MsgHelper
                {
                    flag = 0,
                    msg = "使用失败,请查物品的说明"
                });
        }

        /// <summary>
        /// 购买物品
        /// </summary>
        /// <param name="goodsId"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult BuyGoods(int goodsId, int count)
        {
            Goods goods = igBll.GetModel(p => p.ID == goodsId);
            if (goods == null)
                return Json(new MsgHelper
                {
                    flag = 0,
                    msg = "物品不存在"
                });
            string msg;
            if (iugBll.Buy(GetSessionUserInfo(), goods, out msg, buyCount: count))
            {
                UpdateSessionUserInfo();
                return Json(new MsgHelper
                {
                    flag = 1,
                    msg = "购买成功",
                    data = GetSessionUserInfo()
                });
            }
            else
                return Json(new MsgHelper
                {
                    flag = 0,
                    msg = "购买失败" + msg
                });
        }
        #endregion

    }
}