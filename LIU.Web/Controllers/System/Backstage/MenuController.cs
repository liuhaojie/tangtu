﻿using LIU.Common;
using LIU.IBLL.System;
using LIU.Model.System;
using LIU.UnityFactory;
using LIU.Web.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LIU.Web.Controllers.System
{
    public class MenuController : BaseController
    {
        private readonly IMenuBLL imBll = IOCFactory.GetIOCResolve<IMenuBLL>();
        private readonly IRoleMenuBLL irmBll = IOCFactory.GetIOCResolve<IRoleMenuBLL>();
        // GET: Menu
        public ActionResult Index()
        {
            return View();
        }


        /// <summary>
        /// 获取菜单树节点
        /// </summary>
        /// <returns></returns>
        public ActionResult GetMenuTree()
        {
            MsgHelper msg = new MsgHelper();
            try
            {
                List<MenuTreeHelper> mthList = new List<MenuTreeHelper>();
                MenuTreeHelper mth = new MenuTreeHelper();
                mth.id = Guid.Empty.ToString();
                mth.name = "总菜单";
                mth.spread = true;
                mth.href = "";
                mth.children = imBll.GetMenuTreeList();
                mthList.Add(mth);
                msg.flag = 1;
                msg.msg = mthList;
            }
            catch (Exception ex)
            {
                msg.flag = 0;
                msg.msg = "菜单树加载失败:" + ex.Message;
            }
            return Json(msg);
        }

        /// <summary>
        /// 获取权限菜单树
        /// </summary>
        /// <returns></returns>
        public ActionResult GetRoleMenuTree()
        {
            MsgHelper msg = new MsgHelper();
            try
            {
                string[] roles = GetSessionUserInfo().sRoleKey.Split(',');
                List<Guid> glist = new List<Guid>();
                for (int i = 0; i < roles.Length; i++)
                {
                    glist.Add(new Guid(roles[i]));
                }
                List<RoleMenu> rmList = SysCache.RoleMenuList.Where(p => glist.Contains(p.gRoleKey)).ToList();
                List<Guid> mkeyList = rmList.Select(p => p.gMenuKey).ToList();

                List<Menu> mList = SysCache.MenuList.Where(p => mkeyList.Contains(p.gKey) && p.iType != 3).ToList();
                if (GetSessionUserInfo().gkey == SysCache.Adminkey)//管理员全部菜单
                {
                    mList = SysCache.MenuList.Where(p => p.iType != 3).ToList();
                }
                List<Menu> mparent = mList.Where(p => p.gParentKey == Guid.Empty).ToList();

                List<MenuTreeHelper> MTHList = new List<MenuTreeHelper>();
                for (int i = 0; i < mparent.Count; i++)
                {
                    MenuTreeHelper mth = new MenuTreeHelper();
                    mth.id = mparent[i].gKey.ToString();
                    mth.name = mparent[i].sName;
                    mth.spread = false;
                    mth.href = "";
                    mth.icon = mparent[i].sIcon;
                    mth.shref = mparent[i].sHref;
                    mth.children = GetRoleMenuChildren(mList, mparent[i].gKey);
                    MTHList.Add(mth);
                }
                msg.flag = 1;
                msg.msg = MTHList;
            }
            catch (Exception ex)
            {
                msg.flag = 0;
                msg.msg = "菜单树加载失败:" + ex.Message;
            }
            return Json(msg);
        }



        /// <summary>
        /// 获取权限子菜单
        /// </summary>
        /// <param name="mpList">权限菜单</param>
        /// <param name="gParentKey">父菜单主键</param>
        /// <returns></returns>
        private List<MenuTreeHelper> GetRoleMenuChildren(List<Menu> mpList, Guid gParentKey)
        {
            List<MenuTreeHelper> MTHList = new List<MenuTreeHelper>();
            List<Menu> mList = mpList.Where(p => p.gParentKey == gParentKey).ToList();
            for (int i = 0; i < mList.Count; i++)
            {
                MenuTreeHelper mth = new MenuTreeHelper();
                mth.id = mList[i].gKey.ToString();
                mth.name = mList[i].sName;
                mth.spread = false;
                mth.href = "";
                mth.icon = mList[i].sIcon;
                mth.shref = mList[i].sHref;
                mth.children = GetRoleMenuChildren(mpList, mList[i].gKey);
                MTHList.Add(mth);
            }
            return MTHList;
        }



        /// <summary>
        /// 获取菜单列表
        /// </summary>
        /// <param name="gkey">上级菜单主键</param>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public ActionResult GetMenuList(Guid gkey, int page, int limit)
        {
            LayuiDataHelper layuiDataHelper = new LayuiDataHelper();
            layuiDataHelper.code = 0;
            int count = 0;
            layuiDataHelper.data = imBll.GetList(p => p.gParentKey == gkey, p => new { p.dInsert }, new PageHelper(limit, page), false, ref count);
            layuiDataHelper.count = count;
            return Json(layuiDataHelper);
        }

        /// <summary>
        /// 添加菜单界面
        /// </summary>
        /// <returns></returns>
        public ActionResult AddView(Guid gkey)
        {

            ViewData["gkey"] = gkey;
            return View();
        }

        /// <summary>
        /// 修改界面
        /// </summary>
        /// <returns></returns>
        public ActionResult EditView(Guid gkey)
        {
            Menu m = imBll.GetModel(p => p.gKey == gkey);
            return View(m);
        }

        /// <summary>
        /// 添加菜单
        /// </summary>
        /// <param name="menu"></param>
        /// <returns></returns>
        public ActionResult AddMenu(Menu menu)
        {
            MsgHelper msg = new MsgHelper();

            if (string.IsNullOrEmpty(menu.sName))//判断空
            {
                msg.flag = 0;
                msg.msg = "名称不可以为空";
                return Json(msg);
            }
            if (menu.iType != 1)//判断不是文件夹类型必须字段
            {
                if (string.IsNullOrEmpty(menu.sHref))
                {
                    msg.flag = 0;
                    msg.msg = "地址不可以为空";
                    return Json(msg);
                }
            }
            else
            {
                menu.sHref = "";
            }
            if (menu.iType == 3)//判断按钮类型 必须有id
            {
                if (string.IsNullOrEmpty(menu.sBtnId))
                {
                    msg.flag = 0;
                    msg.msg = "菜单类型为按钮类型,必须要有id";
                    return Json(msg);
                }
            }
            else
            {
                menu.sBtnId = "";
            }
            if (menu.gParentKey == Guid.Empty)
            {
                if (menu.iType == 3)
                {
                    msg.flag = 0;
                    msg.msg = "总菜单下面不可以有【权限按钮】类型";
                    return Json(msg);
                }
            }
            else
            {
                int parentType = imBll.GetModel(p => p.gKey == menu.gParentKey).iType;
                switch (menu.iType)
                {
                    case 1:
                        if (parentType != 1)
                        {
                            msg.flag = 0;
                            msg.msg = "添加【菜单文件夹】类型上级菜单必须为【菜单文件夹】类型";
                            return Json(msg);
                        }
                        break;
                    case 2:
                        if (parentType != 1)
                        {
                            msg.flag = 0;
                            msg.msg = "添加【菜单页面】类型上级菜单必须为【菜单文件夹】类型";
                            return Json(msg);
                        }
                        break;
                    case 3:
                        if (parentType == 1)
                        {
                            msg.flag = 0;
                            msg.msg = "添加【权限按钮】类型上级菜单必须为【菜单页面】类型或【权限按钮】类型";
                            return Json(msg);
                        }
                        break;
                }
            }
            menu.gKey = Guid.NewGuid();
            if (imBll.GetModel(p => p.gParentKey == menu.gParentKey && p.sName == menu.sName) != null)
            {
                msg.flag = 0;
                msg.msg = "同一级下菜单名不可以重复";
                return Json(msg);
            }
            menu.gKey = Guid.NewGuid();
            menu.gInsertKey = GetSessionUserInfo().gkey;
            menu.dInsert = DateTime.Now;
            if (imBll.Add(menu))
            {
                msg.flag = 1;
                msg.msg = "添加成功";
                SysCache.UpdateMenu();
            }
            else
            {
                msg.flag = 0;
                msg.msg = "添加失败";
            }
            return Json(msg);
        }

        /// <summary>
        /// 修改菜单
        /// </summary>
        /// <param name="menu"></param>
        /// <returns></returns>
        public ActionResult EditMenu(Menu menu)
        {
            MsgHelper msg = new MsgHelper();
            if (string.IsNullOrEmpty(menu.sName))
            {
                msg.flag = 0;
                msg.msg = "名称不可以为空";
                return Json(msg);
            }
            if (menu.iType != 1)
            {
                if (string.IsNullOrEmpty(menu.sHref))
                {
                    msg.flag = 0;
                    msg.msg = "地址不可以为空";
                    return Json(msg);
                }
            }
            else
            {
                menu.sHref = "";
            }
            if (menu.iType == 3)
            {
                if (string.IsNullOrEmpty(menu.sBtnId))
                {
                    msg.flag = 0;
                    msg.msg = "菜单类型为按钮类型,必须要有id";
                    return Json(msg);
                }
            }
            else
            {
                menu.sBtnId = "";
            }
            if (menu.gParentKey == Guid.Empty)
            {
                if (menu.iType == 3)
                {
                    msg.flag = 0;
                    msg.msg = "总菜单下面不可以有【权限按钮】类型";
                    return Json(msg);
                }
            }
            int parentType = imBll.GetModel(p => p.gKey == menu.gParentKey).iType;
            switch (menu.iType)
            {
                case 1:
                    if (parentType != 1)
                    {
                        msg.flag = 0;
                        msg.msg = "添加【菜单文件夹】类型上级菜单必须为【菜单文件夹】类型";
                        return Json(msg);
                    }
                    break;
                case 2:
                    if (parentType != 1)
                    {
                        msg.flag = 0;
                        msg.msg = "添加【菜单页面】类型上级菜单必须为【菜单文件夹】类型";
                        return Json(msg);
                    }
                    break;
                case 3:
                    if (parentType == 1)
                    {
                        msg.flag = 0;
                        msg.msg = "添加【权限按钮】类型上级菜单必须为【菜单页面】类型或【权限按钮】类型";
                        return Json(msg);
                    }
                    break;
            }

            if (imBll.GetModel(p => p.sName == menu.sName && p.gParentKey == menu.gParentKey && p.gKey != menu.gKey) != null)
            {
                msg.flag = 0;
                msg.msg = "同一级下菜单名不可以重复";
                return Json(msg);
            }
            if (imBll.Update(menu))
            {
                msg.flag = 1;
                msg.msg = "修改成功";
                SysCache.UpdateMenu();
            }
            else
            {
                msg.flag = 0;
                msg.msg = "修改失败";
            }
            return Json(msg);
        }

        /// <summary>
        /// 删除菜单
        /// </summary>
        /// <param name="gkeys">菜单主键以;分割</param>
        /// <returns></returns>
        public ActionResult DelMenu(string gkeys)
        {
            MsgHelper msg = new MsgHelper();

            string[] arr = gkeys.Split(';');
            List<Guid> gList = new List<Guid>();
            for (int i = 0; i < arr.Length; i++)
            {
                gList.Add(new Guid(arr[i]));
            }
            //判断时候有子菜单
            for (int i = 0; i < gList.Count; i++)
            {
                Guid g = gList[i];
                //判断是否有子菜单且子菜单主键不在删除列表中
                if (imBll.GetModel(p => p.gParentKey == g && !gList.Contains(p.gKey)) != null)
                {
                    msg.flag = 0;
                    msg.msg = "删除的菜单含有子元素，请先删除子元素";
                    return Json(msg);
                }
            }

            //做级联删除 删除菜单对应的删除 角色权限的菜单
            if (imBll.CascadeDel(gList))
            {
                msg.flag = 1;
                msg.msg = "删除成功";
                SysCache.UpdateMenu();
                SysCache.UpdateRoleMenu();
            }
            else
            {
                msg.flag = 0;
                msg.msg = "删除失败";
            }
            return Json(msg);
        }
    }
}