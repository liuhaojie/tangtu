﻿using LIU.CollectionService;
using LIU.Common;
using LIU.IBLL.EBook;
using LIU.IBLL.System;
using LIU.Model.EBook;
using LIU.Model.System;
using LIU.UnityFactory;
using LIU.Web.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LIU.Web.Controllers.System
{
    /// <summary>
    /// 定时器
    /// </summary>
    public class TasksController : Controller
    {
        /// <summary>
        /// 删除系统通知消息
        /// </summary>
        /// <param name="code">参数代码</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DelSysNotifyMsg(string code)
        {
            if (code != "5A8A8879BF1B237228845E73193D4384")
                return Content("false");
            INotifyMessageBLL inmBll = IOCFactory.GetIOCResolve<INotifyMessageBLL>();
            //获取系统消息阅读后自动删除时间 0表示不删除            
            string day = GetParameterValue("SysNotifyMsgDelDay");
            if (day != "" || Convert.ToInt32(day) > 0)
            {
                var list = inmBll.GetList(p => p.iFlag == 2 && p.iType == 1);
                List<NotifyMessage> nmlist = new List<NotifyMessage>();
                foreach (var item in list)
                {
                    //判断阅读时间加上删除天数 是否 小于当前时间
                    if (Convert.ToDateTime(item.dReadDate).AddDays(Convert.ToInt32(day)) < DateTime.Now)
                    {
                        item.iFlag = 0;
                        nmlist.Add(item);
                    }
                }
                if (nmlist.Count > 0)
                    inmBll.Update(nmlist);
                LogHelper.Info("删除系统通知消息" + nmlist.Count + "条");
            }
            return Content("");
        }

        /// <summary>
        /// 爬取小说
        /// </summary>
        /// <param name="code">参数代码</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CrawlNovel(string code)
        {
            if (code != "156069CF8716EAA21138659071DEFEE3")
                return Content("false");
            ICollectionBLL iCBll = IOCFactory.GetIOCResolve<ICollectionBLL>();
            INovelBLL iNBll = IOCFactory.GetIOCResolve<INovelBLL>();
            IChapterBLL chapterBLL = IOCFactory.GetIOCResolve<IChapterBLL>();
            List<Novel> novelList = iNBll.GetList(p => p.iType != 1);
            foreach (var item in novelList)
            {
                LogHelper.Info($"采集小说【{item.sName}】开始");
                ICollectionHandle collection = CollectionFactory.CreateICollectionHandle(item);
                collection.CollectionInfo(false);
                //Collection model = iCBll.GetModel(p => p.gNovelKey == g);
                //if (model != null)
                //{
                //    int max = chapterBLL.getMaxChapter(model.gNovelKey);
                //    CollectionHelper ch = new CollectionHelper();
                //    ch.Gkey = model.gkey;
                //    ch.StartCollection(string.IsNullOrEmpty(model.gIndexAdd) ? model.gStartAdd : model.gIndexAdd,
                //    model.gTextRegex,
                //    model.gNextRegex,
                //    model.gTitleRegex, max < 1 ? 1 : max, 0, false);
                //}
                LogHelper.Info($"采集小说【{item.sName}】结束");                
            }
            EBook.NovelController e = new EBook.NovelController();
            e.CreateHtml();
            return Content("");
        }

        /// <summary>
        /// 检查VIP到期
        /// </summary>
        /// <param name="code">参数代码</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CheckVIP(string code)
        {
            if (code != "FEADA4F99C8CD1A265182032ED2FC1F4")
                return Content("false");
            IUserInfoBLL iurBll = IOCFactory.GetIOCResolve<IUserInfoBLL>();
            IHeadFrameBLL ihfBll = IOCFactory.GetIOCResolve<IHeadFrameBLL>();
            IVIPHistoryBLL ivipBll = IOCFactory.GetIOCResolve<IVIPHistoryBLL>();
            DateTime dt = DateTime.Now.Date;
            List<UserInfo> uiList = iurBll.GetList(p => p.dLevelTIme <= dt);
            foreach (var item in uiList)
            {
                string sherdImg = item.sHerdImageUrl;
                if (!string.IsNullOrWhiteSpace(sherdImg))//判断头像框等级是否符合过期后的信息               
                    if (ihfBll.GetModel(p => p.sUrl == sherdImg)?.iLevel != 0)
                        item.sHerdImageUrl = null;
                //判断是否有冻结的vip
                Guid gkey = item.gkey;
                VIPHistory model = ivipBll.GetModel(p => p.gUserKey == gkey, p => p.dCreateTime, true);
                if (model != null)
                {
                    item.iLevel = model.iLevel;
                    item.dLevelTIme = DateTime.Now.AddDays(model.iDay);
                }
                else
                {
                    item.iLevel = 0;
                    item.dLevelTIme = null;
                }
                if (iurBll.Update(item, model))
                {
                    NotifyMsgHelper.SendMsg("VIP到期提醒", $@"
                &nbsp;&nbsp;&nbsp;&nbsp;您的VIP已经到期了,期待我们下次的见面！！！", new List<Guid>() { item.gkey });
                }
            }
            return Content("");
        }



        /// <summary>
        /// 获取系统参数值
        /// </summary>
        /// <param name="code">参数代码</param>
        /// <returns></returns>
        public static string GetParameterValue(string code)
        {
            Parameter par = SysCache.ParameterList.Where(p => p.sCode == code).FirstOrDefault();
            if (par == null)
                return "";
            else
                return par.sValue;
        }
    }
}