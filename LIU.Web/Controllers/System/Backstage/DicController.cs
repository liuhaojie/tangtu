﻿using LIU.Common;
using LIU.IBLL.System;
using LIU.Model.System;
using LIU.UnityFactory;
using LIU.Web.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LIU.Web.Controllers.System
{
    public class DicController : BaseController
    {
        /// <summary>
        /// 字典信息
        /// </summary>
        private readonly IDicInfoBLL idiBll = IOCFactory.GetIOCResolve<IDicInfoBLL>();
        /// <summary>
        /// 字典目录
        /// </summary>
        private readonly IDicCatalogBLL idcBll = IOCFactory.GetIOCResolve<IDicCatalogBLL>();

        #region 字典目录

        /// <summary>
        /// 字典目录界面
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 获取字典目录列表
        /// </summary>
        /// <param name="page">当前页</param>
        /// <param name="limit">每页多少行</param>
        /// <returns></returns>
        public ActionResult GetDicCatalogList(int page, int limit, string sName, string sCode)
        {
            LayuiDataHelper layuiDataHelper = new LayuiDataHelper();
            layuiDataHelper.code = 0;
            int count = 0;
            layuiDataHelper.data = idcBll.GetList(p => (string.IsNullOrEmpty(sName) ? true : p.sName.Contains(sName)) &&
              (string.IsNullOrEmpty(sCode) ? true : p.sCode.Contains(sCode))
            , p => new { p.iSort }, new PageHelper(limit, page), true, ref count);
            layuiDataHelper.count = count;
            return Json(layuiDataHelper);
        }

        /// <summary>
        /// 添加字典目录界面
        /// </summary>
        /// <returns></returns>
        public ActionResult DicCatalogAddView()
        {
            return View();
        }

        /// <summary>
        /// 添加字典目录
        /// </summary>
        /// <param name="dicCatalog"></param>
        /// <returns></returns>
        public ActionResult AddDicCatalog(DicCatalog dicCatalog)
        {
            MsgHelper msg = new MsgHelper();
            //验证字段
            if (string.IsNullOrWhiteSpace(dicCatalog.sCode) ||
                string.IsNullOrWhiteSpace(dicCatalog.sName))
            {
                msg.msg = "参数出错";
                msg.flag = 0;
                return Json(msg);
            }
            if (idcBll.GetModel(p => ((p.sName == dicCatalog.sName) || (p.sCode == dicCatalog.sCode)) && p.gkey != dicCatalog.gkey) != null)
            {
                msg.msg = "目录代码或名称重复";
                msg.flag = 0;
                return Json(msg);
            }



            DicCatalog dc = idcBll.GetMaxOrMin(p => true, p => new { p.iSort });//获取最大排序
            int max = 0;
            if (dc != null)
                max = dc.iSort;
            dicCatalog.iSort = ++max;
            dicCatalog.gkey = Guid.NewGuid();
            dicCatalog.iFlag = 1;
            if (idcBll.Add(dicCatalog))
            {
                msg.msg = "添加成功";
                msg.flag = 1;
            }
            else
            {
                msg.msg = "添加失败";
                msg.flag = 0;
            }
            return Json(msg);
        }

        /// <summary>
        /// 修改字典目录界面
        /// </summary>
        /// <param name="gkey">字典目录主键</param>
        /// <returns></returns>
        public ActionResult DicCatalogEditView(Guid gkey)
        {
            DicCatalog dicCatalog = idcBll.GetModel(p => p.gkey == gkey);
            return View(dicCatalog);
        }

        /// <summary>
        /// 修改字典目录
        /// </summary>
        /// <param name="dicCatalog"></param>
        /// <returns></returns>
        public ActionResult EditDicCatalog(DicCatalog dicCatalog)
        {
            MsgHelper msg = new MsgHelper();
            DicCatalog dc = idcBll.GetModel(p => p.gkey == dicCatalog.gkey);
            //验证字段
            if (string.IsNullOrWhiteSpace(dicCatalog.sCode) ||
                string.IsNullOrWhiteSpace(dicCatalog.sName))
            {
                msg.msg = "参数出错";
                msg.flag = 0;
                return Json(msg);
            }
            if (idcBll.GetModel(p => ((p.sCode == dicCatalog.sCode) || (p.sName == dicCatalog.sName)) && p.gkey != dicCatalog.gkey) != null)
            {
                msg.msg = "目录代码或名称重复";
                msg.flag = 0;
                return Json(msg);
            }
            int oldsort = dc.iSort;
            dc.sCode = dicCatalog.sCode;
            dc.sName = dicCatalog.sName;
            dc.iSort = dicCatalog.iSort;
            dc.sRemark = dicCatalog.sRemark;
            if (idcBll.updateOfSort(dc, oldsort))
            {
                msg.msg = "修改成功";
                msg.flag = 1;
            }
            else
            {
                msg.msg = "修改失败";
                msg.flag = 0;
            }
            return Json(msg);
        }

        /// <summary>
        /// 删除字典目录
        /// </summary>
        /// <param name="gkey">主键</param>
        /// <returns></returns>
        public ActionResult DelDicCatalog(Guid gkey)
        {
            MsgHelper msg = new MsgHelper();
            if (idcBll.DeleteDicAll(gkey))
            {
                msg.msg = "删除成功";
                msg.flag = 1;
            }
            else
            {
                msg.msg = "删除失败";
                msg.flag = 0;
            }
            return Json(msg);
        }

        /// <summary>
        /// 移动字典目录
        /// </summary>
        /// <param name="gkey">主键</param>
        /// <param name="action">up上移，down下移</param>
        /// <returns></returns>
        public ActionResult DicCatalogMove(Guid gkey, string action)
        {
            MsgHelper msg = new MsgHelper();
            switch (action)
            {
                case "up":
                    DicCatalog catalog = idcBll.GetModel(p => p.gkey == gkey);
                    if (catalog == null)
                    {
                        msg.flag = 0;
                        msg.msg = "主键错误";
                        return Json(msg);
                    }
                    if (catalog.iSort <= 1)
                    {
                        msg.flag = 0;
                        msg.msg = "当前字典目录已经是最上层，无法进行上移操作";
                        return Json(msg);
                    }
                    int oldSort = catalog.iSort;
                    catalog.iSort = --catalog.iSort;
                    if (idcBll.updateOfSort(catalog, oldSort))
                    {
                        msg.flag = 1;
                        msg.msg = "上移成功";
                    }
                    else
                    {
                        msg.flag = 0;
                        msg.msg = "上移失败";
                    }
                    break;
                case "down":
                    DicCatalog dicCatalog = idcBll.GetModel(p => p.gkey == gkey);
                    if (dicCatalog == null)
                    {
                        msg.flag = 0;
                        msg.msg = "主键错误";
                        return Json(msg);
                    }
                    if (dicCatalog.iSort == idcBll.GetMaxOrMin(p => true, p => new { p.iSort }).iSort)
                    {
                        msg.flag = 0;
                        msg.msg = "当前字典目录已经是最下层，无法进行下移操作\r\n如果需要，请进行修改操作";
                        return Json(msg);
                    }
                    int oldSorts = dicCatalog.iSort;
                    dicCatalog.iSort = ++dicCatalog.iSort;
                    if (idcBll.updateOfSort(dicCatalog, oldSorts))
                    {
                        msg.flag = 1;
                        msg.msg = "下移成功";
                    }
                    else
                    {
                        msg.flag = 0;
                        msg.msg = "下移失败";
                    }
                    break;
                default:
                    msg.flag = 0;
                    msg.msg = "参数错误";
                    break;
            }
            return Json(msg);
        }
        #endregion

        #region 字典详情
        /// <summary>
        /// 字典详情界面
        /// </summary>
        /// <returns></returns>
        public ActionResult DicInfoView(Guid gkey)
        {
            ViewData["gkey"] = gkey;
            return View();
        }

        /// <summary>
        /// 获取字典详情列表
        /// </summary>
        /// <param name="gkey">字典目录主键</param>
        /// <param name="page">当前页</param>
        /// <param name="limit">每页多少行</param>
        /// <returns></returns>
        public ActionResult GetDicInfoList(Guid gkey, int page, int limit)
        {
            LayuiDataHelper layuiDataHelper = new LayuiDataHelper();
            layuiDataHelper.code = 0;
            int count = 0;
            layuiDataHelper.data = idiBll.GetList(p => p.gDicCatalogKey == gkey, p => new { p.iSort }, new PageHelper(limit, page), true, ref count);
            layuiDataHelper.count = count;
            return Json(layuiDataHelper);
        }

        /// <summary>
        /// 添加字典详情界面
        /// </summary>
        /// <returns></returns>
        public ActionResult DicInfoAddView(Guid gkey)
        {
            ViewData["gkey"] = gkey;
            return View();
        }

        /// <summary>
        /// 添加字典详情
        /// </summary>
        /// <param name="dicInfo"></param>
        /// <returns></returns>
        public ActionResult AddDicInfo(DicInfo dicInfo)
        {
            MsgHelper msg = new MsgHelper();
            DicCatalog dicCatalog = idcBll.GetModel(p => p.gkey == dicInfo.gDicCatalogKey);
            if (dicCatalog == null)
            {
                msg.flag = 0;
                msg.msg = "字典目录主键出错";
                return Json(msg);
            }
            if (string.IsNullOrWhiteSpace(dicInfo.sName) || string.IsNullOrWhiteSpace(dicInfo.sValue))
            {
                msg.flag = 0;
                msg.msg = "参数错误";
                return Json(msg);
            }
            if (idiBll.GetModel(p => ((p.sValue == dicInfo.sValue) || (p.sName == dicInfo.sName)) && p.gDicCatalogKey == dicInfo.gDicCatalogKey && p.gkey != dicInfo.gkey) != null)
            {
                msg.msg = "字典名称或值重复";
                msg.flag = 0;
                return Json(msg);
            }

            DicInfo dic = idiBll.GetMaxOrMin(p => p.gDicCatalogKey == dicInfo.gDicCatalogKey, p => new { p.iSort });
            int max = 0;
            if (dic != null)
                max = dic.iSort;
            dicInfo.gkey = Guid.NewGuid();
            dicInfo.iFlag = 1;
            dicInfo.iSort = ++max;
            dicInfo.sCode = dicCatalog.sCode;
            dicInfo.gDicCatalogKey = dicCatalog.gkey;
            if (idiBll.Add(dicInfo))
            {
                msg.flag = 1;
                msg.msg = "添加成功";
            }
            else
            {
                msg.flag = 0;
                msg.msg = "添加失败";
            }
            SysCache.UpdateDicInfo();
            return Json(msg);
        }

        /// <summary>
        /// 修改字典详情界面
        /// </summary>
        /// <param name="gkey">字典详情主键</param>
        /// <returns></returns>
        public ActionResult DicInfoEditView(Guid gkey)
        {
            DicInfo dicinfo = idiBll.GetModel(p => p.gkey == gkey);
            return View(dicinfo);
        }

        /// <summary>
        /// 修改字典详情
        /// </summary>
        /// <param name="dicInfo"></param>
        /// <returns></returns>
        public ActionResult EditDicInfo(DicInfo dicInfo)
        {
            MsgHelper msg = new MsgHelper();
            DicInfo di = idiBll.GetModel(p => p.gkey == dicInfo.gkey);
            //验证字段
            if (string.IsNullOrWhiteSpace(dicInfo.sValue) ||
                string.IsNullOrWhiteSpace(dicInfo.sName))
            {
                msg.msg = "参数出错";
                msg.flag = 0;
                return Json(msg);
            }
            //判断重复
            if (idiBll.GetModel(p => ((p.sValue == dicInfo.sValue) || (p.sName == dicInfo.sName)) && p.gDicCatalogKey == dicInfo.gDicCatalogKey && p.gkey != dicInfo.gkey) != null)
            {
                msg.msg = "值或名称重复";
                msg.flag = 0;
                return Json(msg);
            }
            int oldsort = di.iSort;
            di.sValue = dicInfo.sValue;
            di.sName = dicInfo.sName;
            di.iSort = dicInfo.iSort;
            di.sRemark = dicInfo.sRemark;
            if (idiBll.updateOfSort(di, oldsort))
            {
                msg.msg = "修改成功";
                msg.flag = 1;
            }
            else
            {
                msg.msg = "修改失败";
                msg.flag = 0;
            }
            SysCache.UpdateDicInfo();
            return Json(msg);
        }


        /// <summary>
        /// 删除字典详情
        /// </summary>
        /// <param name="gkey"></param>
        /// <returns></returns>
        public ActionResult DelDicInfo(Guid gkey)
        {
            MsgHelper msg = new MsgHelper();
            if (idiBll.Delete(p => p.gkey == gkey))
            {
                msg.flag = 1;
                msg.msg = "删除成功";
            }
            else
            {
                msg.flag = 1;
                msg.msg = "删除失败";
            }
            SysCache.UpdateDicInfo();
            return Json(msg);
        }

        /// <summary>
        /// 移动字典目录
        /// </summary>
        /// <param name="gkey">主键</param>
        /// <param name="action">up上移，down下移</param>
        /// <returns></returns>
        public ActionResult DicInfoMove(Guid gkey, string action)
        {
            MsgHelper msg = new MsgHelper();
            switch (action)
            {
                case "up":
                    DicInfo dicInfo = idiBll.GetModel(p => p.gkey == gkey);
                    if (dicInfo == null)
                    {
                        msg.flag = 0;
                        msg.msg = "主键错误";
                        return Json(msg);
                    }
                    if (dicInfo.iSort <= 1)
                    {
                        msg.flag = 0;
                        msg.msg = "当前字典已经是最上层，无法进行上移操作";
                        return Json(msg);
                    }
                    int oldSort = dicInfo.iSort;
                    dicInfo.iSort = --dicInfo.iSort;
                    if (idiBll.updateOfSort(dicInfo, oldSort))
                    {
                        msg.flag = 1;
                        msg.msg = "上移成功";
                    }
                    else
                    {
                        msg.flag = 0;
                        msg.msg = "上移失败";
                    }
                    break;
                case "down":
                    DicInfo DicInfo = idiBll.GetModel(p => p.gkey == gkey);
                    if (DicInfo == null)
                    {
                        msg.flag = 0;
                        msg.msg = "主键错误";
                        return Json(msg);
                    }
                    if (DicInfo.iSort == idiBll.GetMaxOrMin(p => p.gDicCatalogKey == DicInfo.gDicCatalogKey, p => new { p.iSort }).iSort)
                    {
                        msg.flag = 0;
                        msg.msg = "当前字典已经是最下层，无法进行下移操作\r\n如果需要，请进行修改操作";
                        return Json(msg);
                    }
                    int oldSorts = DicInfo.iSort;
                    DicInfo.iSort = ++DicInfo.iSort;
                    if (idiBll.updateOfSort(DicInfo, oldSorts))
                    {
                        msg.flag = 1;
                        msg.msg = "下移成功";
                    }
                    else
                    {
                        msg.flag = 0;
                        msg.msg = "下移失败";
                    }
                    break;
                default:
                    msg.flag = 0;
                    msg.msg = "参数错误";
                    break;
            }
            return Json(msg);
        }
        #endregion

       
    }
}