﻿using LIU.Common;
using LIU.IBLL.System;
using LIU.Model.System;
using LIU.UnityFactory;
using LIU.Web.App_Code;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LIU.Web.Controllers.System
{
    public class UserInfoController : BaseController
    {
        private readonly IUserInfoBLL iurBll = IOCFactory.GetIOCResolve<IUserInfoBLL>();
        private readonly IRoleBLL irBll = IOCFactory.GetIOCResolve<IRoleBLL>();
        private readonly IGoldFlowBLL igfBll = IOCFactory.GetIOCResolve<IGoldFlowBLL>();

        // GET: UserInfo
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 获取列表值
        /// </summary>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public ActionResult GetList(int page, int limit)
        {
            LayuiDataHelper layuiDataHelper = new LayuiDataHelper();
            layuiDataHelper.code = 0;
            int count = 0;
            layuiDataHelper.data = iurBll.GetList(p => true, p => new { p.RegDateTime }, new PageHelper(limit, page), false, ref count);
            layuiDataHelper.count = count;
            return Json(layuiDataHelper);
        }

        /// <summary>
        /// 获取下来用户
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public ActionResult GetUser(string name)
        {
            int count = 0;
            return Json(new MsgHelper()
            {
                flag = 1,
                data = iurBll.GetList(p => (string.IsNullOrEmpty(name) ? true : p.sName.Contains(name)) && (p.iFlag == 1), p => new { p.RegDateTime }, new PageHelper(), false, ref count).Select(p => new { p.gkey, p.sName })
            });
        }

        /// <summary>
        /// 改变用户状态
        /// </summary>
        /// <param name="gkey"></param>
        /// <returns></returns>
        public ActionResult ChangeUserType(Guid gkey)
        {
            MsgHelper msg = new MsgHelper();
            UserInfo ui = iurBll.GetModel(p => p.gkey == gkey);
            if (ui.iFlag == 2)
            {
                ui.iFlag = 1;
                msg.msg = "解封";
            }
            else
            {
                ui.iFlag = 2;
                msg.msg = "封停";
            }
            if (iurBll.Update(ui))
            {
                msg.flag = 1;
                msg.msg += "成功";
            }
            else
            {
                msg.flag = 0;
                msg.msg += "失败";
            }
            return Json(msg);
        }

        /// <summary>
        /// 配置权限界面
        /// </summary>
        /// <param name="gkey"></param>
        /// <returns></returns>
        public ActionResult ConfigRoleView(Guid gkey)
        {
            List<Role> rList = irBll.GetList(p => true);
            ViewData["Role"] = JsonHelper.getJsonString(rList);
            UserInfo ui = iurBll.GetModel(p => p.gkey == gkey);
            ViewData["userRole"] = ui.sRoleKey;
            ViewData["gkey"] = gkey;
            return View();
        }


        /// <summary>
        /// 配置权限
        /// </summary>
        /// <param name="gkeys">角色权限主键</param>
        /// <param name="gkey">用户主键</param>
        /// <returns></returns>
        public ActionResult ConfigRole(string gkeys, Guid gkey)
        {
            MsgHelper msg = new MsgHelper();
            string[] arr = gkeys.Split(',');
            List<Guid> gList = new List<Guid>();
            if (gkeys != "")
            {
                for (int i = 0; i < arr.Length; i++)
                {
                    gList.Add(new Guid(arr[i]));
                }
            }
            if (irBll.GetRowCount(p => gList.Contains(p.gkey)) == gList.Count)
            {
                UserInfo ui = iurBll.GetModel(p => p.gkey == gkey);
                ui.sRoleKey = gkeys;
                if (iurBll.Update(ui))
                {
                    msg.flag = 1;
                    msg.msg = "配置成功";
                }
                else
                {
                    msg.flag = 0;
                    msg.msg = "配置失败";
                }
            }
            else
            {
                msg.flag = 0;
                msg.msg = "参数错误";
            }
            return Json(msg);
        }


        /// <summary>
        /// 改变用户类型
        /// </summary>
        /// <param name="gkey"></param>
        /// <returns></returns>
        public ActionResult ChangeState(Guid gkey)
        {
            MsgHelper msg = new MsgHelper();
            UserInfo ui = iurBll.GetModel(p => p.gkey == gkey);
            if (ui.iState == 2)
            {
                ui.iState = 1;
                msg.msg = "设置前台用户";
            }
            else
            {
                ui.iState = 2;
                msg.msg = "设置后台用户";
            }
            if (iurBll.Update(ui))
            {
                msg.flag = 1;
                msg.msg += "成功";
            }
            else
            {
                msg.flag = 0;
                msg.msg += "失败";
            }
            return Json(msg);

        }

        /// <summary>
        /// 赠送VIP
        /// </summary>
        /// <param name="vipLevel">vip等级</param>
        /// <param name="dCount">vip天数</param>
        /// <param name="gkey">用户主键</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GiveVIP(Guid gkey, int vipLevel, int dCount)
        {
            UserInfo ui = iurBll.GetModel(p => p.gkey == gkey);
            if (vipLevel < 0 || vipLevel > 7 || dCount > 366 || dCount < 1)
                return Json(new MsgHelper { flag = 0, msg = "参数错误" });
            if (vipLevel > ui.iLevel)//赠送vip大于当前vip，则直接修改Vip等级，否则沿用之前Vip等级
                ui.iLevel = vipLevel;
            if (ui.dLevelTIme == null)//如果到期时间为空，则在当前时间下增加天数，否则为续费操作
                ui.dLevelTIme = DateTime.Now.AddDays(dCount);
            else
                ui.dLevelTIme = Convert.ToDateTime(ui.dLevelTIme).AddDays(dCount);
            if (iurBll.Update(ui))
            {
                NotifyMsgHelper.SendMsg("恭喜获得VIP", $@"
                &nbsp;&nbsp;&nbsp;&nbsp;系统赠送您<span class='iconfonts icon-vip{ui.iLevel}' style='color:#FFD700;font-size: 30px;line-height: 30px;position: relative;top: 5px;'></span><span style='color:red'>【VIP{ui.iLevel}】【{dCount}天】</span>
                &nbsp;&nbsp;&nbsp;&nbsp;<br>当前VIP到期日期为:<span style='color:blue'>{Convert.ToDateTime(ui.dLevelTIme).Date}</span>
                &nbsp;&nbsp;&nbsp;&nbsp;<br>请尽情享受VIP的快乐之旅！！！", new List<Guid>() { ui.gkey });
                return Json(new MsgHelper { flag = 1, msg = "赠送成功" });
            }
            else
                return Json(new MsgHelper { flag = 0, msg = "赠送失败" });
        }

        /// <summary>
        /// 赠送金币
        /// </summary>
        /// <param name="gkey">为空就是全部赠送</param>
        /// <param name="golds"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GiveGold(Guid? gkeys, int golds)
        {
            if (golds < 1 || golds > 1000000)
                return Json(new MsgHelper { flag = 0, msg = "参数错误" });
            //List<UserInfo> ui = new List<UserInfo>();
            //List<GoldFlow> gfList = new List<GoldFlow>();
            if (gkeys == null)
            {
                //ui = iurBll.GetList(p => true);
                //ui.ForEach(p =>
                //{
                //    GoldFlow gf = new GoldFlow();
                //    gf.iGold = golds;
                //    gf.iOldGold = p.iGold;
                //    gf.gUserKey = p.gkey;
                //    gf.dTime = DateTime.Now;
                //    gf.iType = 2;
                //    gf.iFlag = 1;
                //    p.iGold = p.iGold + golds;
                //    gf.iNewGold = p.iGold;
                //    gfList.Add(gf);
                //});
                if (iurBll.ExcetProc("Sys_Give_Gold @gold", new SqlParameter[] {
                        new SqlParameter("@gold",golds) }) > 0)
                    return Json(new MsgHelper { flag = 1, msg = "赠送成功" });
                else
                    return Json(new MsgHelper { flag = 0, msg = "赠送失败" });
            }
            else
            {
                UserInfo u = iurBll.GetModel(p => p.gkey == gkeys);
                GoldFlow gf = new GoldFlow();
                gf.iGold = golds;
                gf.iOldGold = u.iGold;
                gf.gUserKey = u.gkey;
                gf.dTime = DateTime.Now;
                gf.iType = 2;
                gf.iFlag = 1;
                u.iGold = u.iGold + golds;
                gf.iNewGold = u.iGold;
                //ui.Add(u);
                //gfList.Add(gf);
                if (iurBll.Update(u))
                {
                    igfBll.Add(gf);
                    NotifyMsgHelper.SendMsg("恭喜获得金币", $@"
                &nbsp;&nbsp;&nbsp;&nbsp;系统赠送您<span class='iconfonts icon-jinbi' style='color:#FFD700;font-size: 30px;line-height: 30px;position: relative;top: 5px;'></span><span style='color:red'>【金币:{golds}】</span>", new List<Guid> { u.gkey });
                    return Json(new MsgHelper { flag = 1, msg = "赠送成功" });
                }
                else
                    return Json(new MsgHelper { flag = 0, msg = "赠送失败" });
            }
        }
    }
}