﻿using LIU.Common;
using LIU.IBLL.System;
using LIU.Model.System;
using LIU.UnityFactory;
using LIU.Web.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LIU.Web.Controllers.System
{
    public class RoleController : BaseController
    {
        private readonly IRoleBLL irBll = IOCFactory.GetIOCResolve<IRoleBLL>();
        private readonly IMenuBLL imBll = IOCFactory.GetIOCResolve<IMenuBLL>();
        private readonly IRoleMenuBLL irmBll = IOCFactory.GetIOCResolve<IRoleMenuBLL>();
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 获取列表值
        /// </summary>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public ActionResult GetList(int page, int limit)
        {
            LayuiDataHelper layuiDataHelper = new LayuiDataHelper();
            layuiDataHelper.code = 0;
            int count = 0;
            layuiDataHelper.data = irBll.GetList(p => true, p => new { p.dInsert }, new PageHelper(limit, page), false, ref count);
            layuiDataHelper.count = count;
            return Json(layuiDataHelper);
        }

        /// <summary>
        /// 添加视图
        /// </summary>
        /// <returns></returns>
        public ActionResult AddView()
        {
            return View();
        }

        /// <summary>
        /// 修改视图
        /// </summary>
        /// <param name="gkey"></param>
        /// <returns></returns>
        public ActionResult EditView(Guid gkey)
        {
            Role r = irBll.GetModel(p => p.gkey == gkey);
            return View(r);
        }

        /// <summary>
        /// 配置权限界面
        /// </summary>
        /// <param name="gkey"></param>
        /// <returns></returns>
        public ActionResult ConfigView(Guid gkey)
        {
            ViewData["gkey"] = gkey;
            return View();
        }

        /// <summary>
        /// 获取权限树
        /// </summary>
        /// <param name="gkey">权限主键</param>
        /// <returns></returns>
        public ActionResult GetTree(Guid gkey)
        {
            List<Menu> mList = imBll.GetList(p => true);//菜单集合
            List<RoleMenu> rmList = irmBll.GetList(p => p.gRoleKey == gkey);//角色权限菜单集合
            List<XTree> xtreeList = new List<XTree>();
            var fu = mList.Where(p => p.gParentKey == Guid.Empty);//获取最顶层
            foreach (var item in fu)
            {
                XTree xt = new XTree();
                xt.title = item.sName;
                xt.value = item.gKey.ToString();
                xt.@checked = rmList.Where(p => p.gMenuKey == item.gKey).FirstOrDefault() != null;
                xt.data = getChildren(mList, rmList, item.gKey);
                if (item.iType == 2)
                {
                    XTree xtz = new XTree();
                    xtz.title = "查看";
                    xtz.value = item.gKey.ToString();
                    xtz.@checked = rmList.Where(p => p.gMenuKey == item.gKey).FirstOrDefault() != null;
                    xtz.data = new List<XTree>();
                    xt.data.Insert(0, xtz);
                }
                xtreeList.Add(xt);
            }
            return Json(xtreeList, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 获取xtree子菜单
        /// </summary>
        /// <param name="mList"></param>
        /// <param name="rmList"></param>
        /// <param name="parentKey"></param>
        /// <returns></returns>
        private List<XTree> getChildren(List<Menu> mList, List<RoleMenu> rmList, Guid parentKey)
        {
            List<XTree> xtreeList = new List<XTree>();
            var fu = mList.Where(p => p.gParentKey == parentKey);
            foreach (var item in fu)
            {
                XTree xt = new XTree();
                xt.title = item.sName;
                xt.value = item.gKey.ToString();
                xt.@checked = rmList.Where(p => p.gMenuKey == item.gKey).FirstOrDefault() != null;
                xt.data = getChildren(mList, rmList, item.gKey);
                if (item.iType == 2)
                {
                    XTree xtz = new XTree();
                    xtz.title = "查看";
                    xtz.value = item.gKey.ToString();
                    xtz.@checked = rmList.Where(p => p.gMenuKey == item.gKey).FirstOrDefault() != null;
                    xtz.data = new List<XTree>();
                    xt.data.Insert(0,xtz);
                }
                xtreeList.Add(xt);
            }
            return xtreeList;
        }

        /// <summary>
        /// 添加角色
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public ActionResult AddRole(Role role)
        {
            MsgHelper msg = new MsgHelper();
            if (string.IsNullOrEmpty(role.sExplain) || string.IsNullOrEmpty(role.sName))
            {
                msg.flag = 0;
                msg.msg = "不可以留空";
                return Json(msg);
            }
            if (irBll.GetModel(p => p.sName == role.sName) != null)
            {
                msg.flag = 0;
                msg.msg = "角色名重复";
                return Json(msg);
            }
            role.gkey = Guid.NewGuid();
            role.dInsert = DateTime.Now;
            role.gInsertKey = GetSessionUserInfo().gkey;
            if (irBll.Add(role))
            {
                msg.flag = 1;
                msg.msg = "添加成功";
            }
            else
            {
                msg.flag = 0;
                msg.msg = "添加失败";
            }
            return Json(msg);
        }

        /// <summary>
        /// 修改角色
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public ActionResult EditRole(Role role)
        {
            MsgHelper msg = new MsgHelper();
            if (string.IsNullOrEmpty(role.sExplain) || string.IsNullOrEmpty(role.sName))
            {
                msg.flag = 0;
                msg.msg = "不可以留空";
                return Json(msg);
            }
            if (irBll.GetModel(p => p.sName == role.sName && p.gkey != role.gkey) != null)
            {
                msg.flag = 0;
                msg.msg = "角色名重复";
                return Json(msg);
            }
            role.dUpdate = DateTime.Now;
            role.gUpdateKey = GetSessionUserInfo().gkey;
            if (irBll.Update(role))
            {
                msg.flag = 1;
                msg.msg = "修改成功";
            }
            else
            {
                msg.flag = 0;
                msg.msg = "修改失败";
            }
            return Json(msg);
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="gkeys">角色主键 以;分割</param>
        /// <returns></returns>
        public ActionResult DelRole(string gkeys)
        {
            MsgHelper msg = new MsgHelper();
            string[] arr = gkeys.Split(';');
            List<Guid> gList = new List<Guid>();
            for (int i = 0; i < arr.Length; i++)
            {
                gList.Add(new Guid(arr[i]));
            }

            //做级联删除 删除角色 对应的删除 角色权限的菜单
            if (irBll.CascadeDel(gList))
            {
                msg.flag = 1;
                msg.msg = "删除成功";
                SysCache.UpdateRoleMenu();
            }
            else
            {
                msg.flag = 0;
                msg.msg = "删除失败";
            }
            return Json(msg);
        }

        /// <summary>
        /// 配置角色
        /// </summary>
        /// <param name="gRoleKey">角色主键</param>
        /// <param name="arrMenuKey">菜单主键集合 ;分割</param>
        /// <returns></returns>
        public ActionResult ConfigRole(Guid gRoleKey, string arrMenuKey)
        {
            MsgHelper msg = new MsgHelper();
            string[] arr = arrMenuKey.Split(';');
            List<Guid> gList = new List<Guid>();
            for (int i = 0; i < arr.Length; i++)
            {
                gList.Add(new Guid(arr[i]));
            }
            //获取所有菜单父菜单到顶
            List<Guid> guidList = new List<Guid>();
            for (int i = 0; i < gList.Count; i++)
            {
                guidList.Add(gList[i]);
                GetParent(ref guidList, SysCache.MenuList, SysCache.MenuList.Where(p => p.gKey == gList[i]).FirstOrDefault().gParentKey);
            }

            //去除重复
            HashSet<Guid> hashSet = new HashSet<Guid>(guidList);
            
            //irmBll.Delete(p => p.gRoleKey == gRoleKey);

            List<RoleMenu> rmList = new List<RoleMenu>();

            foreach (var item in hashSet)
            {
                RoleMenu rm = new RoleMenu();
                rm.gRoleKey = gRoleKey;
                rm.gMenuKey = item;
                rm.dInsert = DateTime.Now;
                rm.gInsertKey = GetSessionUserInfo().gkey;
                rmList.Add(rm);
            }
            if (irmBll.DelAndAdd(gRoleKey,rmList))
            {
                msg.flag = 1;
                msg.msg = "配置权限成功";
                SysCache.UpdateRoleMenu();
            }
            else
            {
                msg.flag = 0;
                msg.msg = "配置权限失败";
            }
            return Json(msg);
        }

        /// <summary>
        /// 父级菜单
        /// </summary>
        /// <param name="gList">父级菜单集合</param>
        /// <param name="mList">菜单集合</param>
        /// <param name="g">当前菜单的父级菜单key</param>
        private void GetParent(ref List<Guid> gList, List<Menu> mList, Guid g)
        {
            Menu m = mList.Where(p => p.gKey == g).FirstOrDefault();
            if (m != null)
            {
                gList.Add(m.gKey);
                GetParent(ref gList, mList, m.gParentKey);
            }
        }
    }

    public class XTree
    {
        /// <summary>
        /// 菜单名
        /// </summary>
        public string title { get; set; }
        /// <summary>
        /// 值
        /// </summary>
        public string value { get; set; }

        /// <summary>
        /// 是否选中
        /// </summary>
        public bool @checked { get; set; }

        /// <summary>
        /// 子节点
        /// </summary>
        public List<XTree> data { get; set; }
    }
}