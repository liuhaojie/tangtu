﻿using LIU.Common;
using LIU.IBLL.System;
using LIU.Model.DTOModel;
using LIU.Model.System;
using LIU.UnityFactory;
using LIU.Web.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace LIU.Web.Controllers.System
{
    public class SysController : BaseController
    {
        /// <summary>
        /// 系统参数 
        /// </summary>
        private readonly IParameterBLL ipBll = IOCFactory.GetIOCResolve<IParameterBLL>();

        /// <summary>
        /// 留言板
        /// </summary>
        private readonly IMessageBoardBLL imbBll = IOCFactory.GetIOCResolve<IMessageBoardBLL>();

        /// <summary>
        /// 通知消息
        /// </summary>
        private readonly INotifyMessageBLL inmBll = IOCFactory.GetIOCResolve<INotifyMessageBLL>();

        /// <summary>
        /// 公告
        /// </summary>
        private readonly INoticeBLL inBll = IOCFactory.GetIOCResolve<INoticeBLL>();

        /// <summary>
        /// 头像框
        /// </summary>
        private readonly IHeadFrameBLL ihfBll = IOCFactory.GetIOCResolve<IHeadFrameBLL>();

        /// <summary>
        /// 物品
        /// </summary>
        private readonly IGoodsBLL igBll = IOCFactory.GetIOCResolve<IGoodsBLL>();

        #region 系统参数
        /// <summary>
        /// 系统参数界面 系统参数添加后不可以修改名称和代码
        /// </summary>
        /// <returns></returns>
        public ActionResult ParameterIndex()
        {
            return View();
        }


        /// <summary>
        /// 获取系统参数列表
        /// </summary>
        /// <param name="page">当前页</param>
        /// <param name="limit">每页多少行</param>
        /// <returns></returns>
        public ActionResult GetParameterList(int page, int limit, string sName, string sCode)
        {
            LayuiDataHelper layuiDataHelper = new LayuiDataHelper();
            layuiDataHelper.code = 0;
            int count = 0;
            layuiDataHelper.data = ipBll.GetList(p =>
                (string.IsNullOrEmpty(sName) ? true : p.sName.Contains(sName)) &&
              (string.IsNullOrEmpty(sCode) ? true : p.sCode.Contains(sCode))
            , p => new { p.sName }, new PageHelper(limit, page), true, ref count);
            layuiDataHelper.count = count;
            return Json(layuiDataHelper);
        }

        /// <summary>
        /// 添加系统参数界面
        /// </summary>
        /// <returns></returns>
        public ActionResult ParameterAddView()
        {
            return View();
        }

        /// <summary>
        /// 添加系统参数
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        public ActionResult AddParameter(Parameter parameter)
        {
            MsgHelper msg = new MsgHelper();
            //验证字段
            if (string.IsNullOrWhiteSpace(parameter.sCode) ||
                string.IsNullOrWhiteSpace(parameter.sValue) ||
                string.IsNullOrWhiteSpace(parameter.sName))
            {
                msg.msg = "参数出错";
                msg.flag = 0;
                return Json(msg);
            }
            if (ipBll.GetModel(p => (p.sName == parameter.sName) || (p.sCode == parameter.sCode)) != null)
            {
                msg.msg = "系统参数代码或名称重复";
                msg.flag = 0;
                return Json(msg);
            }
            parameter.gkey = Guid.NewGuid();
            parameter.iFlag = 1;
            if (ipBll.Add(parameter))
            {
                msg.msg = "添加成功";
                msg.flag = 1;
            }
            else
            {
                msg.msg = "添加失败";
                msg.flag = 0;
            }
            SysCache.UpdateParameter();
            return Json(msg);
        }

        /// <summary>
        /// 修改系统参数界面
        /// </summary>
        /// <param name="gkey">系统参数主键</param>
        /// <returns></returns>
        public ActionResult ParameterEditView(Guid gkey)
        {
            Parameter parameter = ipBll.GetModel(p => p.gkey == gkey);
            return View(parameter);
        }

        /// <summary>
        /// 修改系统参数 
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        public ActionResult EditParameter(Parameter parameter)
        {
            MsgHelper msg = new MsgHelper();
            Parameter pa = ipBll.GetModel(p => p.gkey == parameter.gkey);

            if (pa == null)
            {
                msg.msg = "主键出错";
                msg.flag = 0;
                return Json(msg);
            }
            //验证字段
            if (string.IsNullOrWhiteSpace(parameter.sValue))
            {
                msg.msg = "参数出错";
                msg.flag = 0;
                return Json(msg);
            }

            pa.sValue = parameter.sValue;
            pa.sRemark = parameter.sRemark;
            if (ipBll.Update(pa))
            {
                msg.msg = "修改成功";
                msg.flag = 1;
            }
            else
            {
                msg.msg = "修改失败";
                msg.flag = 0;
            }
            SysCache.UpdateParameter();
            return Json(msg);
        }

        /// <summary>
        /// 删除系统参数
        /// </summary>
        /// <param name="gkey">主键</param>
        /// <returns></returns>
        public ActionResult DelParameter(Guid gkey)
        {
            MsgHelper msg = new MsgHelper();
            if (ipBll.Delete(p => p.gkey == gkey))
            {
                msg.msg = "删除成功";
                msg.flag = 1;
            }
            else
            {
                msg.msg = "删除失败";
                msg.flag = 0;
            }
            SysCache.UpdateParameter();
            return Json(msg);
        }


        /// <summary>
        /// 生成布局页
        /// </summary>
        /// <returns></returns>
        public ActionResult CreateLayout()
        {
            MsgHelper msg = new MsgHelper();
            try
            {
                CreateShared.createShared();
                msg.msg = "生成成功";
                msg.flag = 1;
            }
            catch (Exception ex)
            {
                msg.msg = "生成失败:" + ex.Message;
                msg.flag = 0;
            }
            return Json(msg);
        }
        #endregion

        #region 留言板
        /// <summary>
        /// 留言板后台界面
        /// </summary>
        /// <returns></returns>
        public ActionResult MessageBoardIndex()
        {
            return View();
        }

        /// <summary>
        /// 获取留言内容列表
        /// </summary>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public ActionResult GetMessageList(int page, int limit)
        {
            LayuiDataHelper layuiDataHelper = new LayuiDataHelper();
            layuiDataHelper.code = 0;
            int count = 0;
            layuiDataHelper.data = imbBll.GetList(p => p.iFlag == 1, p => new { p.dInsert }, new PageHelper(limit, page), false, ref count);
            layuiDataHelper.count = count;
            return Json(layuiDataHelper);
        }

        /// <summary>
        /// 回复留言内容
        /// </summary>
        /// <param name="gkey"></param>
        /// <returns></returns>
        public ActionResult ReplyMessageView(Guid gkey)
        {
            MessageBoard mb = imbBll.GetModel(p => p.gkey == gkey);
            return View(mb);
        }

        /// <summary>
        /// 回复留言
        /// </summary>
        /// <param name="mb"></param>
        /// <returns></returns>
        public ActionResult ReplyMessage(MessageBoard mb)
        {
            MsgHelper msg = new MsgHelper();
            MessageBoard messageBoard = imbBll.GetModel(p => p.gkey == mb.gkey);
            if (messageBoard == null)
            {
                msg.msg = "主键出错";
                msg.flag = 0;
                return Json(msg);
            }
            if (!string.IsNullOrWhiteSpace(messageBoard.sReply))
            {
                msg.msg = "改留言已经回复过，不可以再次回复";
                msg.flag = 0;
                return Json(msg);
            }
            if (string.IsNullOrWhiteSpace(mb.sReply))
            {
                msg.msg = "回复内容不可以为空";
                msg.flag = 0;
                return Json(msg);
            }
            messageBoard.sReply = mb.sReply;
            messageBoard.dReply = DateTime.Now;
            messageBoard.gReplyKey = GetSessionUserInfo().gkey;
            if (imbBll.Update(messageBoard))
            {
                msg.msg = "回复成功";
                msg.flag = 1;
            }
            else
            {
                msg.msg = "回复失败";
                msg.flag = 0;
            }
            return Json(msg);
        }

        /// <summary>
        /// 删除留言
        /// </summary>
        /// <param name="gkey"></param>
        /// <returns></returns>
        public ActionResult DelMessage(Guid gkey)
        {
            MsgHelper msg = new MsgHelper();
            MessageBoard mb = null;
            Guid gUserKey = GetSessionUserInfo().gkey;
            if (gUserKey == SysCache.Adminkey)//管理员可以删除全部
                mb = imbBll.GetModel(p => p.gkey == gkey);
            else//非管理员只能删除自己的
                mb = imbBll.GetModel(p => p.gkey == gkey && p.gInsertKey == gUserKey);
            if (mb == null)
            {
                msg.msg = "参数错误";
                msg.flag = 1;
                return Json(msg);
            }
            mb.iFlag = 0;
            if (imbBll.Update(mb))
            {
                msg.msg = "删除成功";
                msg.flag = 1;
            }
            else
            {
                msg.msg = "删除失败";
                msg.flag = 0;
            }
            return Json(msg);
        }

        /// <summary>
        /// 获取未回复留言数
        /// </summary>
        /// <returns></returns>
        public ActionResult GetNoReplyCount()
        {
            MsgHelper msg = new MsgHelper();
            if (IsHavePowerOfUrl("/System/Sys/MessageBoardIndex"))
            {
                msg.msg = imbBll.GetRowCount(p => string.IsNullOrEmpty(p.sReply) && p.iFlag == 1);
                msg.flag = 1;
            }
            else
            {
                msg.flag = 99;
                msg.msg = 0;
            }
            return Json(msg);
        }

        /// <summary>
        /// 获取未查看通知消息数
        /// </summary>
        /// <returns></returns>
        public ActionResult GetNoReadMsgCount()
        {
            MsgHelper msg = new MsgHelper();
            Guid g = GetSessionUserInfo().gkey;
            msg.flag = 1;
            msg.msg = inmBll.GetRowCount(p => p.iFlag == 1 && p.gReceiveKey == g);
            return Json(msg);
        }
        #endregion


        #region 系统通知消息

        /// <summary>
        /// 发送通知消息界面
        /// </summary>
        /// <returns></returns>
        public ActionResult SendNotifyMsgView()
        {
            ViewData["NotifyMsgCode"] = GetDicValue("NotifyMsgCode");//获取消息类型
            return View();
        }


        /// <summary>
        /// 发送通知消息
        /// </summary>
        /// <param name="iType">消息类型 3，带附件消息 与goods联动</param>
        /// <param name="iUserType">用户类型 0全部用户，1部分用户 与user联动</param>
        /// <param name="title">标题</param>
        /// <param name="content">内容</param>
        /// <returns></returns>
        [ValidateInput(false)]
        public ActionResult SendNotifyMsg(int iType, int iUserType, string title, string content, List<UserInfo> user, List<EmailGoodsDTO> goods)
        {
            MsgHelper msg = new MsgHelper();
            if (iUserType == 1 && user.Count == 0)
                return Json(new MsgHelper()
                {
                    flag = 0,
                    msg = "用户不可以为空"
                });
            if (iType == 3 && goods.Count == 0)
                return Json(new MsgHelper()
                {
                    flag = 0,
                    msg = "物品不可以为空"
                });
            switch (iType)
            {
                case 1://系统通知
                    if (iUserType == 1)
                    {
                        if (NotifyMsgHelper.SendMsg(title, content, user.Select(p => p.gkey).ToList()))
                        {
                            msg.flag = 1;
                            msg.msg = "发送成功";
                        }
                        else
                        {
                            msg.flag = 0;
                            msg.msg = "发送失败";
                        }

                    }
                    else
                    {
                        if (NotifyMsgHelper.SendMsg(title, content))
                        {
                            msg.flag = 1;
                            msg.msg = "发送成功";
                        }
                        else
                        {
                            msg.flag = 0;
                            msg.msg = "发送失败";
                        }
                    }
                    break;
                case 2://非系统通知
                    if (iUserType == 1)
                    {
                        if (NotifyMsgHelper.SendMsg(GetSessionUserInfo(), title, content, user.Select(p => p.gkey).ToList()))
                        {
                            msg.flag = 1;
                            msg.msg = "发送成功";
                        }
                        else
                        {
                            msg.flag = 0;
                            msg.msg = "发送失败";
                        }
                    }
                    else
                    {
                        if (NotifyMsgHelper.SendMsg(GetSessionUserInfo(), title, content))
                        {
                            msg.flag = 1;
                            msg.msg = "发送成功";
                        }
                        else
                        {
                            msg.flag = 0;
                            msg.msg = "发送失败";
                        }
                    }
                    break;
                case 3://带附件
                    if (iUserType == 1)
                    {
                        if (NotifyMsgHelper.SendMsg(GetSessionUserInfo(), title, content, user.Select(p => p.gkey).ToList(), iType: 3, sHref: JsonHelper.getJsonString(goods)))
                        {
                            msg.flag = 1;
                            msg.msg = "发送成功";
                        }
                        else
                        {
                            msg.flag = 0;
                            msg.msg = "发送失败";
                        }
                    }
                    else
                    {
                        if (NotifyMsgHelper.SendMsg(GetSessionUserInfo(), title, content, iType: 3, sHref: JsonHelper.getJsonString(goods)))
                        {
                            msg.flag = 1;
                            msg.msg = "发送成功";
                        }
                        else
                        {
                            msg.flag = 0;
                            msg.msg = "发送失败";
                        }
                    }
                    break;
                default:
                    msg.flag = 0;
                    msg.msg = "未知的消息类型";
                    break;
            }
            return Json(msg);
        }


        /// <summary>
        /// 上传图片
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UploadImage(HttpPostedFileBase file)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            try
            {
                string SuffixName = file.FileName.Split('.').Last();
                string fileName = DateTime.Now.ToString("yyyyMMddhhmmssms") + "." + SuffixName;
                string filePath = Server.MapPath(@"~/Images/layedit");
                file.SaveAs(Path.Combine(filePath, fileName));
                dic.Add("code", "0");//0表示成功
                dic.Add("msg", "上传成功");
                Dictionary<string, object> dicsrc = new Dictionary<string, object>();
                dicsrc.Add("src", "/Images/layedit/" + fileName);
                dic.Add("data", dicsrc);
            }
            catch (Exception ex)
            {
                dic.Add("code", "1");//0表示成功
                dic.Add("msg", "上传失败:" + ex.Message);
            }
            return Json(dic);
        }
        #endregion

        #region 定时器
        /// <summary>
        /// 定时器界面
        /// </summary>
        /// <returns></returns>
        public ActionResult TimerIndex()
        {
            return View();
        }


        /// <summary>
        /// 定时器列表
        /// </summary>
        /// <returns></returns>
        public ActionResult GetTimerList()
        {
            LayuiDataHelper layuiDataHelper = new LayuiDataHelper();
            layuiDataHelper.code = 0;

            Dictionary<AutoTaskAttribute, Timer> timers = AutoTaskAttribute.timers;
            int count = timers.Keys.Count;
            //List<ExpandoObject> objList = new List<ExpandoObject>();
            StringBuilder sb = new StringBuilder();
            sb.Append("[");
            foreach (AutoTaskAttribute item in timers.Keys)
            {
                //dynamic d = new ExpandoObject();
                //d.IntervalSeconds = item.IntervalSeconds;
                //d.StartTime = item.StartTime;
                //d.EnterMethod = item.EnterMethod;
                //d.TypeId = item.TypeId;
                //d.Explain = item.Explain;
                long duetime = 0; //计算下次执行时间
                if (item.IntervalSeconds > 0)
                {
                    var datetime = DateTime.Parse(item.StartTime);
                    if (DateTime.Now <= datetime)
                    {
                        duetime = (long)(datetime - DateTime.Now).TotalSeconds * 1000;
                    }
                    else
                    {
                        duetime = item.IntervalSeconds * 1000 - ((long)(DateTime.Now - datetime).TotalMilliseconds) % (item.IntervalSeconds * 1000);
                    }
                }
                //d.NextTime = DateTime.Now.AddMilliseconds(duetime);

                //objList.Add(d);
                sb.Append("{ \"IntervalSeconds\":\"" + item.IntervalSeconds.ToString()
                    + "\",\"StartTime\":\"" + item.StartTime.ToString()
                    + "\",\"EnterMethod\":\"" + item.EnterMethod.ToString()
                    + "\",\"TypeId\":\"" + item.TypeId.ToString()
                    + "\",\"Explain\":\"" + item.Explain.ToString()
                    + "\",\"NextTime\":\"" + DateTime.Now.AddMilliseconds(duetime).ToString("yyyy-MM-dd HH:mm:ss") + "\"},");
            }
            sb.Remove(sb.Length - 1, 1);
            sb.Append("]");
            layuiDataHelper.data = JsonHelper.getObject<List<TimerDTO>>(sb.ToString());
            layuiDataHelper.count = count;
            return Json(layuiDataHelper);
        }

        public class TimerDTO
        {
            public string IntervalSeconds;
            public string StartTime;
            public string EnterMethod;
            public string TypeId;
            public string Explain;
            public string NextTime;
        }
        #endregion

        #region 公告

        /// <summary>
        /// 公告界面
        /// </summary>
        /// <returns></returns>
        public ActionResult NoticeView()
        {
            return View();
        }

        /// <summary>
        /// 添加公告界面
        /// </summary>
        /// <returns></returns>
        public ActionResult AddNoticeView()
        {
            return View();
        }



        /// <summary>
        /// 添加公告
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddNotice(Notice model)
        {
            if (model == null || string.IsNullOrWhiteSpace(model.sContent))
                return Json(new MsgHelper { flag = 0, msg = "数据错误" });
            model.gkey = Guid.NewGuid();
            model.iFlag = 1;
            model.dCreateTime = DateTime.Now;
            model.gCreateId = GetSessionUserInfo().gkey;
            if (inBll.Add(model))
                return Json(new MsgHelper { flag = 1, msg = "添加成功" });
            else
                return Json(new MsgHelper { flag = 0, msg = "添加失败" });
        }

        /// <summary>
        /// 获取公告列表
        /// </summary>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetNoticeList(int page, int limit)
        {
            LayuiDataHelper layuiDataHelper = new LayuiDataHelper();
            layuiDataHelper.code = 0;
            int count = 0;
            layuiDataHelper.data = inBll.GetList(p => p.iFlag == 1, p => new { p.dCreateTime }, new PageHelper(limit, page), false, ref count);
            layuiDataHelper.count = count;
            return Json(layuiDataHelper);
        }
        #endregion

        #region 头像框

        /// <summary>
        /// 头像框界面
        /// </summary>
        /// <returns></returns>
        public ActionResult HeadFrameView()
        {
            return View();
        }

        /// <summary>
        /// 获取头像框列表
        /// </summary>
        /// <returns></returns>
        public ActionResult GetHeadFrameList(int page, int limit)
        {
            LayuiDataHelper layuiDataHelper = new LayuiDataHelper();
            layuiDataHelper.code = 0;
            int count = 0;
            layuiDataHelper.data = ihfBll.GetList(p => true, p => new { p.iSort, p.dAddTime }, new PageHelper(limit, page), false, ref count);
            layuiDataHelper.count = count;
            return Json(layuiDataHelper);
        }


        /// <summary>
        /// 上传头像框
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UpLoadHeadFrame(HttpPostedFileBase file)
        {
            MsgHelper msg = new MsgHelper();
            try
            {
                if (file.ContentLength > 60 * 1024)
                {
                    msg.flag = 0;
                    msg.msg = "上传失败:文件大小超过60KB";
                    return Json(msg);
                }
                string SuffixName = file.FileName.Split('.').Last();
                string fileName = Guid.NewGuid().ToString() + "." + SuffixName;
                string filePath = Server.MapPath(@"~/Images/HeadFrame");
                file.SaveAs(Path.Combine(filePath, fileName));

                HeadFrame hf = new HeadFrame()
                {
                    gkey = Guid.NewGuid(),
                    dAddTime = DateTime.Now,
                    iLevel = 0,
                    iSort = 0,
                    sName = "未命名",
                    sUrl = @"/Images/HeadFrame/" + fileName
                };
                if (ihfBll.Add(hf))
                {
                    msg.flag = 1;
                    msg.msg = "上传成功";
                }
                else
                {
                    msg.flag = 0;
                    msg.msg = "上传失败";
                }
            }
            catch (Exception ex)
            {
                msg.flag = 0;
                msg.msg = "上传失败:" + ex.Message;
            }
            return Json(msg);
        }


        /// <summary>
        /// 批量设置头像框等级
        /// </summary>
        /// <param name="gkeys"></param>
        /// <param name="level"></param>
        /// <returns></returns>
        public ActionResult SetHeadFrameLevel(List<Guid> gkeys, int level)
        {
            if (level > 9 || level < 0)
                return Json(new MsgHelper { flag = 0, msg = "参数错误" });
            List<HeadFrame> modelList = new List<HeadFrame>();
            foreach (var item in ihfBll.GetList(p => gkeys.Contains(p.gkey)))
            {
                item.iLevel = level;
                modelList.Add(item);
            }
            if (ihfBll.Update(modelList))
                return Json(new MsgHelper { flag = 1, msg = "设置成功" });
            else
                return Json(new MsgHelper { flag = 0, msg = "设置失败" });
        }
        #endregion

        #region 物品
        /// <summary>
        /// 物品界面
        /// </summary>
        /// <returns></returns>
        public ActionResult GoodsView()
        {
            return View();
        }

        /// <summary>
        /// 获取物品列表
        /// </summary>
        /// <returns></returns>
        public ActionResult GetGoodsList(int page, int limit, string sName, string sClassify, string sRestrictionsType)
        {
            LayuiDataHelper layuiDataHelper = new LayuiDataHelper();
            layuiDataHelper.code = 0;
            int count = 0;
            layuiDataHelper.data = igBll.GetList(p =>
             (string.IsNullOrEmpty(sName.Trim()) ? true : p.sName.Contains(sName.Trim())) &&
                (string.IsNullOrEmpty(sClassify.Trim()) ? true : p.sClassify.Contains(sClassify.Trim())) &&
                (string.IsNullOrEmpty(sRestrictionsType.Trim()) ? true : p.sRestrictionsType.ToLower() == sRestrictionsType.Trim().ToLower())
            , p => new { p.dCreateTime, p.iSort }, new PageHelper(limit, page), true, ref count);
            layuiDataHelper.count = count;
            return Json(layuiDataHelper);
        }

        /// <summary>
        /// 获取下拉物品
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetGoods()
        {
            var list = igBll.GetList(p => true).Select(p => new { p.ID, p.sName }).ToList();
            list.Insert(0, new { ID = 0, sName = "金币" });
            return Json(new MsgHelper
            {
                flag = 1,
                data = list
            });

        }

        /// <summary>
        /// 上传物品照片
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UpLoadGoodsImage(HttpPostedFileBase file, int ID)
        {
            MsgHelper msg = new MsgHelper();
            try
            {
                if (file.ContentLength > 60 * 1024)
                {
                    msg.flag = 0;
                    msg.msg = "上传失败:文件大小超过60KB";
                    return Json(msg);
                }
                Goods model = igBll.GetModel(p => p.ID == ID);
                if (model == null)
                {
                    msg.flag = 0;
                    msg.msg = "物品不存在";
                    return Json(msg);
                }
                string SuffixName = file.FileName.Split('.').Last();
                string fileName = ID + "." + SuffixName;
                string filePath = Server.MapPath(@"~/Images/Goods");
                if (!Directory.Exists(filePath))
                    Directory.CreateDirectory(filePath);
                file.SaveAs(Path.Combine(filePath, fileName));
                model.sHref = @"/Images/Goods/" + fileName;
                if (igBll.Update(model))
                {
                    msg.flag = 1;
                    msg.msg = "上传成功";
                }
                else
                {
                    msg.flag = 0;
                    msg.msg = "上传失败";
                }
            }
            catch (Exception ex)
            {
                msg.flag = 0;
                msg.msg = "上传失败:" + ex.Message;
            }
            return Json(msg);
        }


        /// <summary>
        /// 导入
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public ActionResult GoodsImport(HttpPostedFileBase file)
        {
            string SuffixName = file.FileName.Split('.').Last();
            string fileName = DateTime.Now.ToString("yyyyMMddhhmmssms") + "." + SuffixName;
            string filePath = Server.MapPath(@"~/File/Import");
            if (!Directory.Exists(filePath))
                Directory.CreateDirectory(filePath);
            string path = Path.Combine(filePath, fileName);
            file.SaveAs(path);
            ExcelHelper ex = new ExcelHelper(path);
            DataTable dt = ex.ExportExcelToDataTable();
            LogHelper.Info($"导入物品开始，共{dt.Rows.Count}条");
            List<Goods> list = new List<Goods>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Goods model = new Goods()
                {
                    sName = dt.Rows[i]["物品名称"].ToString(),
                    sExplain = dt.Rows[i]["物品说明"].ToString(),
                    sClassify = dt.Rows[i]["分类"].ToString(),
                    iSort = Convert.ToInt32(dt.Rows[i]["排序"]),
                    sValue1 = dt.Rows[i]["预留参数1"].ToString(),
                    sValue2 = dt.Rows[i]["预留参数2"].ToString(),
                    iOldPrice = Convert.ToInt32(dt.Rows[i]["原价"]),
                    iNewPrice = Convert.ToInt32(dt.Rows[i]["现价"]),
                    sRestrictionsType = dt.Rows[i]["限购类型"].ToString(),
                    iRestrictions = Convert.ToInt32(dt.Rows[i]["限购数量"]),
                    sHref = dt.Rows[i]["图片地址"].ToString(),
                    iType = Convert.ToInt32(dt.Rows[i]["类型参数"]),
                    dCreateTime = DateTime.Now
                };
                list.Add(model);
            }
            FileInfo f = new FileInfo(path);
            f.Delete();
            return Json(Import(list));
        }

        /// <summary>
        /// 验证数据及导入数据
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private MsgHelper Import(List<Goods> list)
        {
            //成功 跳过 失败
            int success = 0, skip = 0, error = 0;
            var snoList = igBll.GetList(p => true).Select(p => new { p.sName }).Distinct();
            List<Goods> needAdd = new List<Goods>();
            foreach (var item in list)
            {
                if (string.IsNullOrWhiteSpace(item.sName) ||
                    string.IsNullOrWhiteSpace(item.sExplain) ||
                    string.IsNullOrWhiteSpace(item.sClassify) ||
                    string.IsNullOrWhiteSpace(item.sRestrictionsType))
                {
                    LogHelper.Info($"物品【{item.sName}】数据不能为空");
                    skip++;
                    continue;
                }
                if (snoList.Where(p => p.sName == item.sName).FirstOrDefault() == null)
                {
                    if (needAdd.Where(p => p.sName == item.sName).FirstOrDefault() == null)
                        needAdd.Add(item);
                    else
                    {
                        LogHelper.Info($"物品【{item.sName}】在Excel中重复，跳过后面的数据");
                        skip++;
                    }
                }
                else
                {
                    LogHelper.Info($"物品【{item.sName}】在数据库中已存在，跳过");
                    skip++;
                }
            }

            igBll.ImportSqlBulkCopy(needAdd);
            success = needAdd.Count;
            LogHelper.Info($"成功导入{success}条，跳过{skip}条，失败{error}条");
            return new MsgHelper() { flag = 1, msg = $"成功导入{success}条，跳过{skip}条，失败{error}条" };
        }
        #endregion
    }
}