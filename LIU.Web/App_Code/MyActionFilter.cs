﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LIU.Web.App_Code
{
    /// <summary>
    /// 全局拦截器 (暂时弃用)
    /// </summary>
    public class MyActionFilter : ActionFilterAttribute
    {
        /// <summary>
        /// 前
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            //判断是否登录
            //判断是否有权限
        }

        /// <summary>
        /// 后
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
            //插入需要隐藏的按钮id
            filterContext.Controller.ViewData["hideId"] = "del;del1";
        }
    }
}