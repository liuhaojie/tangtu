﻿using LIU.Common;
using LIU.IBLL.System;
using LIU.Model.System;
using LIU.UnityFactory;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace LIU.Web.App_Code
{
    public class WebBaseController : Controller
    {
        /// <summary>
        /// 是否验证登录
        /// </summary>
        protected bool isCheckLogin = false;

        #region 验证登录与权限


        /// <summary>
        /// 重写 在调用操作方法前调用
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            if (isCheckLogin)
            {
                UserInfo ui = GetSessionUserInfo();
                //判断是否登录
                if (ui == null)
                {
                    string controllerName = (filterContext.RouteData.Values["controller"]).ToString().ToLower();
                    string actionName = (filterContext.RouteData.Values["action"]).ToString().ToLower();
                    if (controllerName == "user" && actionName == "home")//判读是否为几个特殊的界面不进行登录验证 /user/Home
                        return;
                    if (filterContext.RequestContext.HttpContext.Request.IsAjaxRequest())
                    {
                        filterContext.HttpContext.Response.StatusCode = 520;
                        MsgHelper msg = new MsgHelper();
                        msg.flag = 98;
                        msg.msg = "/Default/Login?returnUrl=" + Request.Url.PathAndQuery;
                        filterContext.Result = new JsonResult() { Data = msg };
                    }
                    else
                    {
                        filterContext.Result = new RedirectResult("/Default/Login?returnUrl=" + Request.Url.PathAndQuery);
                    }
                    return;
                }
            }
        }

        /// <summary>
        /// 获取sessionUserInfo
        /// </summary>
        /// <returns></returns>
        public UserInfo GetSessionUserInfo()
        {
            try
            {
                if (HttpContext.Session["UserInfo"] == null)
                {
                    HttpContext.Session["UserInfo"] = TryParsePrincipal();
                }
                return HttpContext.Session["UserInfo"] as UserInfo;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 设置sessionUserInfo
        /// </summary>
        /// <returns></returns>
        public void SetSessionUserInfo(UserInfo ui)
        {
            RemoveSession();
            HttpContext.Session["UserInfo"] = ui;
            SetAuthenticationCoolie(ui);
        }

        /// <summary>
        /// 更新sessionUserInfo
        /// </summary>
        public void UpdateSessionUserInfo()
        {
            IUserInfoBLL iBll = IOCFactory.GetIOCResolve<IUserInfoBLL>();
            UserInfo ui = GetSessionUserInfo();
            Guid gkey = ui.gkey;
            ui = iBll.GetModel(p => p.gkey == gkey);
            SetSessionUserInfo(ui);
        }

        /// <summary>
        /// 清空session
        /// </summary>
        public void RemoveSession()
        {
            System.Web.HttpContext.Current.Response.Cookies[FormsAuthentication.FormsCookieName].Expires = DateTime.Now.AddDays(-1);
            HttpContext.Session.Remove("UserInfo");

        }

        /// <summary>
        /// 将用户信息通过ticket加密保存到cookie
        /// </summary>
        /// <param name="userInfo">用户类</param>
        /// <param name="rememberDay">保存几天</param>
        public void SetAuthenticationCoolie(UserInfo userInfo, int rememberDay = 0)
        {
            if (userInfo == null)
                throw new ArgumentNullException("userInfo");

            //序列化UserInfo对象
            string userInfoJson = JsonConvert.SerializeObject(userInfo);
            //创建用户票据
            var ticket = new FormsAuthenticationTicket(1, userInfo.sLoginName, DateTime.Now, DateTime.Now.AddDays(rememberDay), false, userInfoJson);
            //加密
            string encryptAccount = FormsAuthentication.Encrypt(ticket);

            //创建cookie
            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptAccount)
            {
                HttpOnly = true,
                Secure = FormsAuthentication.RequireSSL,
                Domain = FormsAuthentication.CookieDomain,
                Path = FormsAuthentication.FormsCookiePath
            };

            if (rememberDay > 0)
                cookie.Expires = DateTime.Now.AddDays(rememberDay);

            //写入Cookie
            System.Web.HttpContext.Current.Response.Cookies[cookie.Name].Expires = DateTime.Now.AddDays(-1);

            System.Web.HttpContext.Current.Response.Cookies.Add(cookie);
        }

        /// <summary>
        /// 获取cookie并解析出用户信息
        /// </summary>
        /// <returns></returns>
        public UserInfo TryParsePrincipal()
        {
            HttpRequest request = System.Web.HttpContext.Current.Request;
            HttpCookie cookie = request.Cookies[FormsAuthentication.FormsCookieName];
            if (cookie == null || string.IsNullOrEmpty(cookie.Value))
            {
                return null;
            }
            //解密coolie值
            FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(cookie.Value);

            UserInfo account = JsonConvert.DeserializeObject<UserInfo>(ticket.UserData);
            return account;
        }



        #endregion

        /// <summary>
        /// 获取系统参数值
        /// </summary>
        /// <param name="code">参数代码</param>
        /// <returns></returns>
        public string GetParameterValue(string code)
        {
            Parameter par = SysCache.ParameterList.Where(p => p.sCode == code).FirstOrDefault();
            if (par == null)
                return "";
            else
                return par.sValue;
        }

        #region 重写Json


        protected new JsonResult Json(object data, string contentType)
        {
            return new ToJsonResult
            {
                Data = data,
                ContentEncoding = Encoding.UTF8,
                ContentType = contentType,
                JsonRequestBehavior = JsonRequestBehavior.DenyGet,
                FormateStr = "yyyy-MM-dd HH:mm:ss"
            };
        }

        protected override JsonResult Json(object data, string contentType, Encoding contentEncoding)
        {
            return new ToJsonResult
            {
                Data = data,
                ContentEncoding = contentEncoding,
                ContentType = contentType,
                JsonRequestBehavior = JsonRequestBehavior.DenyGet,
                FormateStr = "yyyy-MM-dd HH:mm:ss"
            };
        }

        protected new JsonResult Json(object data, JsonRequestBehavior behavior)
        {
            return new ToJsonResult
            {
                Data = data,
                ContentEncoding = Encoding.UTF8,
                ContentType = "",
                JsonRequestBehavior = behavior,
                FormateStr = "yyyy-MM-dd HH:mm:ss"
            };
        }

        protected new JsonResult Json(object data, string contentType, JsonRequestBehavior behavior)
        {
            return new ToJsonResult
            {
                Data = data,
                ContentEncoding = Encoding.UTF8,
                ContentType = contentType,
                JsonRequestBehavior = behavior,
                FormateStr = "yyyy-MM-dd HH:mm:ss"
            };
        }

        protected override JsonResult Json(object data, string contentType, Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new ToJsonResult
            {
                Data = data,
                ContentEncoding = Encoding.UTF8,
                ContentType = contentType,
                JsonRequestBehavior = behavior,
                FormateStr = "yyyy-MM-dd HH:mm:ss"
            };
        }
        protected new JsonResult Json(object data)
        {
            return new ToJsonResult
            {
                Data = data,
                ContentEncoding = Encoding.UTF8,
                ContentType = "",
                JsonRequestBehavior = JsonRequestBehavior.DenyGet,
                FormateStr = "yyyy-MM-dd HH:mm:ss"
            };
        }

        #endregion
    }
}