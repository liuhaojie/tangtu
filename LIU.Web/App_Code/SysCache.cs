﻿using LIU.IBLL.System;
using LIU.Model.System;
using LIU.UnityFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LIU.Web.App_Code
{
    /// <summary>
    /// 缓存
    /// </summary>
    public static class SysCache
    {
        private static List<Menu> _menuList;
        private static List<RoleMenu> _roleMenuList;
        private static List<DicInfo> _dicInfoList;
        private static List<Parameter> _parameterList;
        private static string strDate = DateTime.Now.ToString("yyyy-MM-dd");
        private static Guid _adminkey;

        /// <summary>
        /// 更新系统缓存
        /// </summary>
        public static void Update()
        {
            UpdateMenu();
            UpdateRoleMenu();
            UpdateDicInfo();
            UpdateParameter();
            UpdateAdminkey();
        }

        #region 缓存系统菜单
        /// <summary>
        /// 获取缓存中的系统菜单
        /// </summary>
        public static List<Menu> MenuList
        {
            get
            {
                if (strDate != DateTime.Now.ToString("yyyy-MM-dd"))
                    UpdateMenu();
                if (_menuList == null)
                    UpdateMenu();
                return _menuList;
            }
        }

        /// <summary>
        /// 更新系统菜单缓存
        /// </summary>
        public static void UpdateMenu()
        {
            IMenuBLL iBll = IOCFactory.GetIOCResolve<IMenuBLL>();
            _menuList = iBll.GetList(p => true);
        }
        #endregion

        #region 缓存角色权限
        /// <summary>
        /// 获取缓存中的角色权限
        /// </summary>
        public static List<RoleMenu> RoleMenuList
        {
            get
            {
                if (strDate != DateTime.Now.ToString("yyyy-MM-dd"))
                    UpdateRoleMenu();
                if (_roleMenuList == null)
                    UpdateRoleMenu();
                return _roleMenuList;
            }
        }

        /// <summary>
        /// 更新角色权限缓存
        /// </summary>
        public static void UpdateRoleMenu()
        {
            IRoleMenuBLL iBll = IOCFactory.GetIOCResolve<IRoleMenuBLL>();
            _roleMenuList = iBll.GetList(p => true);
        }
        #endregion

        #region 缓存字典
        /// <summary>
        /// 获取缓存中的字典
        /// </summary>
        public static List<DicInfo> DicInfoList
        {
            get
            {
                if (strDate != DateTime.Now.ToString("yyyy-MM-dd"))
                    UpdateDicInfo();
                if (_dicInfoList == null)
                    UpdateDicInfo();
                return _dicInfoList;
            }
        }

        /// <summary>
        /// 更新字典缓存
        /// </summary>
        public static void UpdateDicInfo()
        {
            IDicInfoBLL iBll = IOCFactory.GetIOCResolve<IDicInfoBLL>();
            _dicInfoList = iBll.GetList(p => true);
        }
        #endregion

        #region 缓存系统参数
        /// <summary>
        /// 获取缓存中的系统参数
        /// </summary>
        public static List<Parameter> ParameterList
        {
            get
            {
                if (strDate != DateTime.Now.ToString("yyyy-MM-dd"))
                    UpdateParameter();
                if (_parameterList == null)
                    UpdateParameter();
                return _parameterList;
            }
        }

        /// <summary>
        /// 更新系统参数
        /// </summary>
        public static void UpdateParameter()
        {
            IParameterBLL iBll = IOCFactory.GetIOCResolve<IParameterBLL>();
            _parameterList = iBll.GetList(p => true);
        }
        #endregion

        #region 缓存系统参数
        /// <summary>
        /// 获取缓存中的系统参数
        /// </summary>
        public static Guid Adminkey
        {
            get
            {
                if (strDate != DateTime.Now.ToString("yyyy-MM-dd"))
                    UpdateAdminkey();
                if (_adminkey == null|| _adminkey==Guid.Empty)
                    UpdateAdminkey();
                return _adminkey;
            }
        }

        /// <summary>
        /// 更新系统参数
        /// </summary>
        public static void UpdateAdminkey()
        {
            _adminkey = new Guid(System.Configuration.ConfigurationManager.AppSettings["adminKey"]);
        }
        #endregion

    }
}