﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using LIU.Model.System;
using System.Text;

namespace LIU.Web.App_Code
{
    /// <summary>
    /// 创建布局页
    /// </summary>
    public class CreateShared
    {
        /// <summary>
        /// 创建布局页
        /// </summary>
        public static void createShared()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + @"\Template";
            DirectoryInfo directoryInfo = new DirectoryInfo(path);
            FileInfo[] files = directoryInfo.GetFiles();
            List<Parameter> list = SysCache.ParameterList;
            foreach (FileInfo item in files)//获取布局页模板
            {
                if (!item.Name.ToLower().EndsWith("js"))//不是js 就跳过
                    continue;
                string str = "";
                using (StreamReader sr = new StreamReader(item.FullName))
                {
                    str = sr.ReadToEnd();
                }

                MatchCollection mcs = getRegex(@"#(\w+)#", str);
                foreach (Match m in mcs)//替换参数
                {
                    string title = m.Groups[1].ToString().Trim();
                    Parameter pr = list.Where(p => p.sCode == title).FirstOrDefault();
                    if (pr == null)
                        str = str.Replace(m.Value, "");
                    else
                        str = str.Replace(m.Value, pr.sValue.Replace("\"", "\\\""));
                }
                //布局页参数全部用js替换，这样能保证全局的修改，包括静态界面
                //using (FileStream fs = new FileStream(AppDomain.CurrentDomain.BaseDirectory + @"\Views\Shared\" + item.Name, FileMode.Create, FileAccess.Write))
                //{
                //    using (StreamWriter sw = new StreamWriter(fs, Encoding.UTF8))
                //    {
                //        sw.Write(str);
                //        sw.Flush();
                //        sw.Close();
                //        sw.Dispose();
                //    }
                //    fs.Close();
                //    fs.Dispose();
                //}
                if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + @"\Scripts\WebModular"))
                    Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + @"\Scripts\WebModular");
                using (FileStream fs = new FileStream(AppDomain.CurrentDomain.BaseDirectory + @"\Scripts\WebModular\" + item.Name, FileMode.Create, FileAccess.Write))
                {
                    using (StreamWriter sw = new StreamWriter(fs, Encoding.UTF8))
                    {
                        sw.Write(str);
                        sw.Flush();
                        sw.Close();
                        sw.Dispose();
                    }
                    fs.Close();
                    fs.Dispose();
                }
            }
        }


        /// <summary>
        /// 获取正则结果
        /// </summary>
        /// <param name="regText">正则表达式</param>
        /// <param name="content">待匹配内容</param>
        /// <returns></returns>
        private static MatchCollection getRegex(string regText, string content)
        {
            Regex reg = new Regex(regText, RegexOptions.IgnoreCase);
            MatchCollection matchs = reg.Matches(content);
            return matchs;// matchs[0].Groups[1].ToString();
        }
    }
}