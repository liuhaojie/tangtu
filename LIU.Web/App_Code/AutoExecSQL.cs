﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;
using System.Data.Entity;
using LIU.Common;
using LIU.UnityFactory;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.SqlClient;

namespace LIU.Web.App_Code
{

    /// <summary>
    /// 自动 执行脚本 增量脚本执行
    /// </summary>
    public class AutoExecSQL
    {
        private static DbContext dbContext = IOCFactory.GetIOCResolve<DbContext>();

        public static void ExecHandle()
        {
            CheckTable();
            string path = AppDomain.CurrentDomain.BaseDirectory + @"App_Data\";
            DirectoryInfo directoryInfo = new DirectoryInfo(path);
            FileInfo[] fileInfos = directoryInfo.GetFiles("*.sql", SearchOption.AllDirectories);
            LogHelper.Info($"开始执行脚本文件：总共【{fileInfos.Length}】文件");
            foreach (var item in fileInfos)
            {
                try
                {
                    if (!CheckSQL(item.Name)||item.Name== "000_初始化脚本.sql")
                        continue;
                    var commands = ParseCommands(item.FullName);
                    foreach (var com in commands)
                    {
                        dbContext.Database.ExecuteSqlCommand(com.Trim());
                    }
                    AddSQLLog(new SQLLog
                    {
                        FileName = item.Name,
                        Success = true,
                        Result = "执行成功"
                    });
                }
                catch (Exception ex)
                {
                    AddSQLLog(new SQLLog
                    {
                        FileName = item.Name,
                        Success = false,
                        Result = $"执行出错:{ex.Message}"
                    });
                    LogHelper.Error($"文件{item.Name}执行出错:{ex.Message}");
                    continue;
                }
            }
            LogHelper.Info($"结束执行脚本文件");
        }

        /// <summary>
        /// 检查table是否存在，不存在就自动创建
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private static void CheckTable()
        {
            string sql = @"IF NOT EXISTS(SELECT * FROM sysobjects WHERE name='sys_SQLLog' AND type='u')
                            BEGIN
                                CREATE TABLE sys_SQLLog
	                            (
		                            ID INT NOT NULL IDENTITY(1,1),
		                            FileName NVARCHAR(1024) NOT NULL,
		                            Success BIT NOT NULL,
		                            Result NVARCHAR(MAX),
		                            PRIMARY KEY (ID)
	                            )
                            END
                            ";
            dbContext.Database.ExecuteSqlCommand(sql);
        }

        /// <summary>
        /// 检查脚本文件是否可以执行
        /// </summary>
        /// <param name="fileName">文件名称</param>
        /// <returns>true 可以执行</returns>
        private static bool CheckSQL(string fileName)
        {
            var list = dbContext.Database.SqlQuery<SQLLog>("SELECT * FROM sys_SQLLog WHERE FileName=@fileName", new SqlParameter("@fileName", fileName)).ToList();
            //判断 是否存在同名执行的记录且执行结果为成功 则不可以执行脚本
            if (list.Any() && list[0].Success)
                return false;
            else
                return true;
        }

        /// <summary>
        /// 解析命令
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private static string[] ParseCommands(string filePath)
        {
            List<string> statements = new List<string>();
            using (var stream = File.OpenRead(filePath))
            {
                using (var reader = new StreamReader(stream))
                {
                    string statement;
                    while ((statement = ReadNextStatementFromStream(reader)) != null)
                        statements.Add(statement);

                    return statements.ToArray();
                }
            }
        }

        /// <summary>
        /// 从流中读取一个命令语句
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        private static string ReadNextStatementFromStream(StreamReader reader)
        {
            var builder = new StringBuilder();
            while (true)
            {
                var lineOfText = reader.ReadLine();
                if (lineOfText == null)
                {
                    if (builder.Length > 0)
                        return builder.ToString();

                    return null;
                }

                lineOfText = lineOfText.Trim();

                if (lineOfText.ToUpper() == "GO")
                    break;

                if (string.IsNullOrWhiteSpace(lineOfText) || lineOfText.StartsWith("--") || lineOfText.StartsWith("use", StringComparison.OrdinalIgnoreCase))
                    continue;

                builder.Append(lineOfText + Environment.NewLine);
            }

            return builder.ToString();
        }

        /// <summary>
        /// 添加执行日志
        /// </summary>
        /// <param name="model"></param>
        private static void AddSQLLog(SQLLog model)
        {
            string sql = $"INSERT sys_SQLLog (FileName,Success,Result) VALUES (@FileName,@Success,@Result)";
            dbContext.Database.ExecuteSqlCommand(sql,
                new SqlParameter("@FileName", model.FileName),
                new SqlParameter("@Success", model.Success),
                new SqlParameter("@Result", model.Result));
        }
    }

    /// <summary>
    /// 脚本执行日志
    /// </summary>
    [Table("sys_SQLLog")]
    public class SQLLog
    {
        /// <summary>
        /// 主键 自增
        /// </summary>
        [Key]
        public int ID { get; set; }

        /// <summary>
        /// 文件名称
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 是否成功
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// 执行结果
        /// </summary>
        public string Result { get; set; }
    }
}