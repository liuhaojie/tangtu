﻿using LIU.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LIU.Web.App_Code
{
    /// <summary>
    /// 捕获全局错误
    /// </summary>
    public class AppHandleErrorAttribute : HandleErrorAttribute
    {
        /// <summary>
        /// 捕获错误
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);
            Exception Error = filterContext.Exception;
            filterContext.ExceptionHandled = true;//必要
            string ermsg = "系统错误：" + Error.Message.Replace("\n", " ").Replace("\r", " ");
            try
            {
                DbEntityValidationException dbError = Error as DbEntityValidationException;
                DbEntityValidationResult dbevr = dbError.EntityValidationErrors.FirstOrDefault();
                List<DbValidationError> dbValidationError = dbevr.ValidationErrors.ToList();
                for (int i = 0; i < dbValidationError.Count(); i++)
                {
                    ermsg += dbValidationError[i].PropertyName + ":" + dbValidationError[i].ErrorMessage + ";";
                }
            }
            catch
            { }
            ermsg = ermsg.Replace(@"\", @"\\");
            //错误文本记录
            LogHelper.Error(ermsg + @"\r\n" + Error.TargetSite + @"\r\n" + Error.StackTrace);
            //判断是否是ajax请求
            if (filterContext.RequestContext.HttpContext.Request.IsAjaxRequest())
            {
                filterContext.HttpContext.Response.StatusCode = 520;
                MsgHelper msg = new MsgHelper();
                msg.flag = 97;
                msg.msg = ermsg;
                filterContext.Result = new JsonResult() { Data = msg };
            }
            else
            {
                filterContext.Result = new ContentResult() { Content = @"
                <!DOCTYPE html>
                <script src='../../../layui/layui.all.js'></script>                
                <script>
                    parent.layer.open({
                        title: '提示',
                        content: '" + ermsg + @"'
                        //,
                        //yes: function () {
                         //   window.opener=null;window.close();
                        //}
                    });                   
                </script>" };
                //filterContext.Result = new RedirectResult("/login/login");
            }
        }
    }
}