﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using LIU.Common;
using LIU.Model.System;
using Newtonsoft.Json;

namespace LIU.Web.App_Code
{
    public class BaseController : Controller
    {
        #region 验证登录与权限

        /// <summary>
        /// 获取sessionUserInfo
        /// </summary>
        /// <returns></returns>
        public UserInfo GetSessionUserInfo()
        {
            try
            {
                if (HttpContext.Session["UserInfo"] == null)
                {
                    HttpContext.Session["UserInfo"] = TryParsePrincipal();
                }
                return HttpContext.Session["UserInfo"] as UserInfo;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 设置sessionUserInfo
        /// </summary>
        /// <returns></returns>
        public void SetSessionUserInfo(UserInfo ui)
        {
            HttpContext.Session["UserInfo"] = ui;
        }


        /// <summary>
        /// 清空session
        /// </summary>
        public void RemoveSession()
        {
            System.Web.HttpContext.Current.Response.Cookies[FormsAuthentication.FormsCookieName].Expires = DateTime.Now.AddDays(-1);
            HttpContext.Session.Remove("UserInfo");

        }

        /// <summary>
        /// 重写 在调用操作方法前调用
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            UserInfo ui = GetSessionUserInfo();
            //判断是否登录
            if (ui == null)
            {

                if (filterContext.RequestContext.HttpContext.Request.IsAjaxRequest())
                {
                    filterContext.HttpContext.Response.StatusCode = 520;
                    MsgHelper msg = new MsgHelper();
                    msg.flag = 98;
                    msg.msg = "/Login/Login?returnUrl=" + Request.Url.PathAndQuery;
                    filterContext.Result = new JsonResult() { Data = msg };
                }
                else
                {
                    filterContext.Result = new RedirectResult("/Login/Login?returnUrl=" + Request.Url.PathAndQuery);
                }
                return;
            }
            if (ui.gkey == SysCache.Adminkey)//管理员
                return;
            else if (ui.iState == 1)
            {
                //没有权限访问
                if (filterContext.RequestContext.HttpContext.Request.IsAjaxRequest())
                {
                    filterContext.HttpContext.Response.StatusCode = 520;
                    MsgHelper msg = new MsgHelper();
                    msg.flag = 99;
                    msg.msg = "没有权限,请联系管理员";
                    filterContext.Result = new JsonResult() { Data = msg };
                }
                else
                {
                    filterContext.Result = new ContentResult() { Content = @"
                <!DOCTYPE html>
                <script src='../../../layui/layui.all.js'></script>                
                <script>
                    parent.layer.open({
                        title: '提示',
                        content: '没有权限,请联系管理员'
                    });                   
                </script>" };
                    //    filterContext.Result = Content("<script>alert('没有权限,请联系管理员');window.opener=null;window.close();</script>");
                }
                return;
            }
            //判断是否有权限访问

            string controllerName = (filterContext.RouteData.Values["controller"]).ToString().ToLower();
            string actionName = (filterContext.RouteData.Values["action"]).ToString().ToLower();
            string path = Request.Url.AbsolutePath.ToLower();
            string pathLast = path.Split('/').Last();//判断是否没有输入全，由路由规则自动补全路径
            if (path == "")
            {
                path = "/" + controllerName + "/" + actionName;
            }
            else if (pathLast == "")
            {
                path = path + controllerName + "/" + actionName;
            }
            else if (pathLast == controllerName)
            {
                path = path + "/" + actionName;
            }
            Menu m = SysCache.MenuList.Where(p => p.sHref.ToLower() == path).FirstOrDefault();
            if (m != null)//判断请求页面或功能是否在菜单表中 在-》判断是否有权限 不在-》跳过
            {
                var list = SysCache.RoleMenuList.Where(p => ui.sRoleKey.ToLower().Contains(p.gRoleKey.ToString()));
                foreach (var item in list)
                {
                    if (item.gMenuKey.ToString() == m.gKey.ToString())//角色权限 菜单主键与请求菜单主键相同 表示有权限访问
                    {
                        return;
                    }
                }
                //没有权限访问
                if (filterContext.RequestContext.HttpContext.Request.IsAjaxRequest())
                {
                    filterContext.HttpContext.Response.StatusCode = 520;
                    MsgHelper msg = new MsgHelper();
                    msg.flag = 99;
                    msg.msg = "没有权限,请联系管理员";
                    filterContext.Result = new JsonResult() { Data = msg };
                }
                else
                {
                    filterContext.Result = new ContentResult() { Content = @"
                <!DOCTYPE html>
                <script src='../../../layui/layui.all.js'></script>                
                <script>
                    parent.layer.open({
                        title: '提示',
                        content: '没有权限,请联系管理员'
                    });                   
                </script>" };
                    //    filterContext.Result = Content("<script>alert('没有权限,请联系管理员');window.opener=null;window.close();</script>");
                }
                return;
            }
        }

        /// <summary>
        /// 重写 在调用操作方法后调用 隐藏没有权限的按钮
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
            if (GetSessionUserInfo().gkey == SysCache.Adminkey)//管理员
                return;
            string result = "";
            //获取请求地址
            string controllerName = (filterContext.RouteData.Values["controller"]).ToString().ToLower();
            string actionName = (filterContext.RouteData.Values["action"]).ToString().ToLower();
            string path = Request.Url.AbsolutePath.ToLower();
            string pathLast = path.Split('/').Last();//判断是否没有输入全，由路由规则自动补全路径
            if (path == "")
            {
                path = "/" + controllerName + "/" + actionName;
            }
            else if (pathLast == "")
            {
                path = path + controllerName + "/" + actionName;
            }
            else if (pathLast == controllerName)
            {
                path = path + "/" + actionName;
            }
            Menu m = SysCache.MenuList.Where(p => p.sHref.ToLower() == path).FirstOrDefault();//获取地址是否在菜单中
            if (m != null)//判断请求地址是否在菜单列表中
            {
                //获取请求地址下的按钮集合
                List<Menu> list = SysCache.MenuList.Where(p => p.gParentKey == m.gKey && p.iType == 3).ToList();
                if (list.Count > 0)//表示请求地址下有需要权限的按钮存在
                {
                    //获取角色权限菜单
                    List<RoleMenu> Rolelist = SysCache.RoleMenuList.Where(p => GetSessionUserInfo().sRoleKey.ToLower().Contains(p.gRoleKey.ToString())).ToList();
                    if (Rolelist.Count > 0)//表示角色有权限菜单
                    {
                        List<Guid> glist = Rolelist.Select(p => p.gMenuKey).ToList();
                        //获取按钮集合没有权限的菜单
                        var idList = list.Where(p => !glist.Contains(p.gKey)).Select(p => p.sBtnId);
                        result = string.Join(";", idList);
                    }
                    else
                    {
                        result = string.Join(";", list.Select(p => p.sBtnId));
                    }
                }
            }
            //插入需要隐藏的按钮id
            filterContext.Controller.ViewData["hideId"] = result;// "del;del1";

        }


        /// <summary>
        /// 将用户信息通过ticket加密保存到cookie
        /// </summary>
        /// <param name="userInfo">用户类</param>
        /// <param name="rememberDay">保存几天</param>
        public void SetAuthenticationCoolie(UserInfo userInfo, int rememberDay = 0)
        {
            if (userInfo == null)
                throw new ArgumentNullException("userInfo");

            //序列化UserInfo对象
            string userInfoJson = JsonConvert.SerializeObject(userInfo);
            //创建用户票据
            var ticket = new FormsAuthenticationTicket(1, userInfo.sLoginName, DateTime.Now, DateTime.Now.AddDays(rememberDay), false, userInfoJson);
            //加密
            string encryptAccount = FormsAuthentication.Encrypt(ticket);

            //创建cookie
            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptAccount)
            {
                HttpOnly = true,
                Secure = FormsAuthentication.RequireSSL,
                Domain = FormsAuthentication.CookieDomain,
                Path = FormsAuthentication.FormsCookiePath
            };

            if (rememberDay > 0)
                cookie.Expires = DateTime.Now.AddDays(rememberDay);

            //写入Cookie
            System.Web.HttpContext.Current.Response.Cookies[cookie.Name].Expires = DateTime.Now.AddDays(-1);

            System.Web.HttpContext.Current.Response.Cookies.Add(cookie);
        }

        /// <summary>
        /// 获取cookie并解析出用户信息
        /// </summary>
        /// <returns></returns>
        public UserInfo TryParsePrincipal()
        {
            HttpRequest request = System.Web.HttpContext.Current.Request;
            HttpCookie cookie = request.Cookies[FormsAuthentication.FormsCookieName];
            if (cookie == null || string.IsNullOrEmpty(cookie.Value))
            {
                return null;
            }
            //解密coolie值
            FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(cookie.Value);

            UserInfo account = JsonConvert.DeserializeObject<UserInfo>(ticket.UserData);
            return account;
        }

        /// <summary>
        /// 判断指定路径当前登录用户是否有权限访问
        /// </summary>
        /// <param name="url">指定路径</param>
        /// <returns>true 有权限</returns>
        public bool IsHavePowerOfUrl(string url)
        {
            bool result = false;
            //获取指定路径主键
            Menu m = SysCache.MenuList.Where(p => p.sHref.ToLower() == url.ToLower()).FirstOrDefault();
            if (m == null)
                result = false;
            else
            {
                string[] roles = GetSessionUserInfo().sRoleKey.ToLower().Split(',');
                //获取当前用户的权限菜单
                List<RoleMenu> rmList = SysCache.RoleMenuList.Where(p => roles.Contains(p.gRoleKey.ToString().ToLower())).ToList();
                //判断指定路径是否在当前用户的权限
                if (rmList.Where(p => p.gMenuKey == m.gKey).FirstOrDefault() == null)
                    result = false;
                else
                    result = true;
            }
            return result;
        }
        #endregion

        #region 重写Json


        protected new JsonResult Json(object data, string contentType)
        {
            return new ToJsonResult
            {
                Data = data,
                ContentEncoding = Encoding.UTF8,
                ContentType = contentType,
                JsonRequestBehavior = JsonRequestBehavior.DenyGet,
                FormateStr = "yyyy-MM-dd HH:mm:ss"
            };
        }

        protected override JsonResult Json(object data, string contentType, Encoding contentEncoding)
        {
            return new ToJsonResult
            {
                Data = data,
                ContentEncoding = contentEncoding,
                ContentType = contentType,
                JsonRequestBehavior = JsonRequestBehavior.DenyGet,
                FormateStr = "yyyy-MM-dd HH:mm:ss"
            };
        }

        protected new JsonResult Json(object data, JsonRequestBehavior behavior)
        {
            return new ToJsonResult
            {
                Data = data,
                ContentEncoding = Encoding.UTF8,
                ContentType = "",
                JsonRequestBehavior = behavior,
                FormateStr = "yyyy-MM-dd HH:mm:ss"
            };
        }

        protected new JsonResult Json(object data, string contentType, JsonRequestBehavior behavior)
        {
            return new ToJsonResult
            {
                Data = data,
                ContentEncoding = Encoding.UTF8,
                ContentType = contentType,
                JsonRequestBehavior = behavior,
                FormateStr = "yyyy-MM-dd HH:mm:ss"
            };
        }

        protected override JsonResult Json(object data, string contentType, Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new ToJsonResult
            {
                Data = data,
                ContentEncoding = Encoding.UTF8,
                ContentType = contentType,
                JsonRequestBehavior = behavior,
                FormateStr = "yyyy-MM-dd HH:mm:ss"
            };
        }
        protected new JsonResult Json(object data)
        {
            return new ToJsonResult
            {
                Data = data,
                ContentEncoding = Encoding.UTF8,
                ContentType = "",
                JsonRequestBehavior = JsonRequestBehavior.DenyGet,
                FormateStr = "yyyy-MM-dd HH:mm:ss"
            };
        }

        #endregion

        #region 字典参数

        /// <summary>
        /// 获取系统参数值
        /// </summary>
        /// <param name="code">参数代码</param>
        /// <returns></returns>
        public string GetParameterValue(string code)
        {
            Parameter par = SysCache.ParameterList.Where(p => p.sCode == code).FirstOrDefault();
            if (par == null)
                return "";
            else
                return par.sValue;
        }

        /// <summary>
        /// 获取系统字典
        /// </summary>
        /// <param name="code">参数代码</param>
        /// <returns></returns>
        public List<DicInfo> GetDicValue(string code)
        {
            return SysCache.DicInfoList.Where(p => p.sCode == code).OrderBy(p => p.iSort).ToList();
        }

        #endregion
    }
}