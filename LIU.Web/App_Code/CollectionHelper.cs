﻿using LIU.Common;
using LIU.IBLL.EBook;
using LIU.Model.EBook;
using LIU.UnityFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;

namespace LIU.Web.App_Code
{ /// <summary>
  /// 小说采集
  /// </summary>
    public class CollectionHelper
    {
        private readonly ICollectionBLL iCollBll = IOCFactory.GetIOCResolve<ICollectionBLL>();
        private readonly IChapterBLL iChapBll = IOCFactory.GetIOCResolve<IChapterBLL>();
        private string url;//文章地址
        private string RegContext;//内容正则
        private string RegNext;//下一章地址正则
        private string RegTitle;//标题正则
        private int chapterIndex;//当前第几章
        private string httpHerd;//地址头
        private int sIndex;//开始第几章
        private int eIndex;//结束第几章
        public string result;//结果
        private string indexUrl;//单前访问地址

        private Guid _gkey = Guid.Empty;//配置主键

        /// <summary>
        /// 配置主键存在就写入数据库并使用多线程
        /// </summary>
        public Guid Gkey
        {
            get
            {
                return _gkey;
            }

            set
            {
                _gkey = value;
            }
        }




        /// <summary>
        /// 采集
        /// </summary>
        /// <param name="url">文章地址</param>
        /// <param name="RegContext">内容正则</param>
        /// <param name="RegNext">下一章地址正则</param>
        /// <param name="RegTitle">标题正则</param>
        /// <param name="sIndex">开始第几章（默认第一章）</param>
        /// <param name="eIndex">结束第几章（默认一直匹配到最新章节）</param>
        /// <param name="isThread">是否开启多线程</param>
        public void StartCollection(string url,
            string RegContext, string RegNext, string RegTitle, int sIndex = 1, int eIndex = 0, bool isThread = true)
        {
            this.url = url;
            this.httpHerd = url.Substring(0, url.LastIndexOf('/') + 1);
            this.RegContext = RegContext;
            this.RegNext = RegNext;
            this.RegTitle = RegTitle;
            this.sIndex = sIndex;
            this.eIndex = eIndex;
            this.chapterIndex = sIndex;
            if (_gkey == Guid.Empty)
            {
                getValue();
            }
            else
            {
                if (isThread)
                {
                    Thread thread = new Thread(getValue);
                    thread.Start();
                }
                else
                    getValue();
            }
        }

        /// <summary>
        /// 获取值
        /// </summary>
        private void getValue()
        {
            try
            {
                while (!string.IsNullOrEmpty(url) && (chapterIndex <= eIndex || eIndex == 0))
                {
                    result = "Url:" + url + ";";
                    indexUrl = url;
                    string html = HttpHelper.HttpGet(url, "");
                    Chapter eb = new Chapter();//章节
                    url = "";//表示没有下一章了，如果下面没有对其赋值则退出循环
                    if (!string.IsNullOrEmpty(RegContext))//判断正则是否存在 文章内容
                    {
                        string content = getRegex(RegContext, html);
                        if (string.IsNullOrEmpty(content))//判断是否有匹配的内容
                        {
                            result += string.Format(@"第【{0}】章内容获取失败;", chapterIndex);
                        }
                        else
                        {
                            eb.sText = content;
                            result += string.Format(@"第【{0}】章内容获取成功;", chapterIndex);
                        }
                    }
                    else
                    {
                        result += "内容正则不存在,无法获取内容;";
                    }

                    if (!string.IsNullOrEmpty(RegTitle))//判断正则是否存在 文章标题
                    {
                        string content = getRegex(RegTitle, html);
                        if (string.IsNullOrEmpty(content))//判断是否有匹配的内容
                        {
                            result += string.Format(@"第【{0}】章标题获取失败;", chapterIndex);
                        }
                        else
                        {
                            eb.sTitle = content;
                            result += string.Format(@"第【{0}】章标题获取成功;", chapterIndex);
                        }
                    }
                    else
                    {
                        result += "标题正则不存在,无法获取标题;";
                    }

                    if (!string.IsNullOrEmpty(RegNext))//判断正则是否存在 下一章地址
                    {
                        string content = getRegex(RegNext, html);
                        if (string.IsNullOrEmpty(content))//判断是否有匹配的内容
                        {
                            result += "获取完成";
                        }
                        else
                        {
                            result += @"下一章地址获取成功";
                            if (content.Substring(0, 4) != "http")
                                url = httpHerd + content;
                            else
                                url = content;
                        }
                    }
                    else
                    {
                        result += "下一章正则不存在,无法获取下一章地址";
                    }

                    if (_gkey != Guid.Empty)
                    {
                        Collection collection = iCollBll.GetModel(p => p.gkey == _gkey);
                        collection.gErrorMag = result;
                        collection.gIndexAdd = indexUrl;
                        iCollBll.Update(collection);

                        if (sIndex == chapterIndex)
                        {
                            Chapter ch = iChapBll.GetModel(p => p.gNovelKey == collection.gNovelKey && p.iIndex == sIndex);
                            if (ch != null)
                            {
                                iChapBll.Delete(ch);
                            }
                        }

                        eb.gkey = Guid.NewGuid();
                        eb.gNovelKey = collection.gNovelKey;
                        eb.iIndex = chapterIndex;
                        eb.dInsertTime = DateTime.Now;
                        eb.gInsertUserKey = Guid.Empty;
                        iChapBll.Add(eb);
                    }
                    chapterIndex++;
                }
            }
            catch (Exception ex)
            {
                if (_gkey != Guid.Empty)
                {
                    Collection collection = iCollBll.GetModel(p => p.gkey == _gkey);
                    collection.gErrorMag = "错误信息：" + ex.Message + "\r\n" + result;
                    iCollBll.Update(collection);
                }
                else
                    result += ex.Message;
            }
        }

        /// <summary>
        /// 获取正则结果
        /// </summary>
        /// <param name="regText">正则表达式</param>
        /// <param name="content">待匹配内容</param>
        /// <returns></returns>
        private string getRegex(string regText, string content)
        {
            Regex reg = new Regex(regText, RegexOptions.IgnoreCase);
            MatchCollection matchs = reg.Matches(content);
            return matchs[0].Groups[1].ToString();
        }
    }
}