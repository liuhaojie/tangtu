﻿using LIU.Common;
using LIU.IBLL.System;
using LIU.Model.System;
using LIU.UnityFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LIU.Web.App_Code
{
    /// <summary>
    /// 通知消息帮助类
    /// </summary>
    public class NotifyMsgHelper
    {
        /// <summary>
        /// 通知消息bll
        /// </summary>
        private static INotifyMessageBLL inmBll = IOCFactory.GetIOCResolve<INotifyMessageBLL>();

        /// <summary>
        /// 人员bll
        /// </summary>
        private static IUserInfoBLL iuiBll = IOCFactory.GetIOCResolve<IUserInfoBLL>();

        /// <summary>
        /// 非系统通知消息
        /// </summary>
        /// <param name="ui">当前用户</param>
        /// <param name="title">通知标题</param>
        /// <param name="content">通知内容</param>
        /// <param name="glist">接收用户主键</param>
        public static bool SendMsg(UserInfo ui, string title, string content, List<Guid> glist, int iType = 2, string sHref = "")
        {
            Guid g = Guid.NewGuid();
            List<NotifyMessage> nmList = new List<NotifyMessage>();
            for (int i = 0; i < glist.Count; i++)
            {
                NotifyMessage nm = new NotifyMessage();
                nm.gkey = Guid.NewGuid();
                nm.gMsgKey = g;
                nm.gSendKey = ui.gkey;
                nm.sSendTitle = title;
                nm.sSendContent = content;
                nm.dSendDate = DateTime.Now;
                nm.gReceiveKey = glist[i];
                nm.sHref = sHref;
                nm.iFlag = 1;
                nm.iType = iType;
                nmList.Add(nm);
            }
            if (inmBll.Add(nmList))
                return true;
            else
                return false;
        }

        /// <summary>
        /// 非系统通知消息(全部人员)
        /// </summary>
        /// <param name="ui">当前用户</param>
        /// <param name="title">通知标题</param>
        /// <param name="content">通知内容</param>
        public static bool SendMsg(UserInfo ui, string title, string content, int iType = 2,string sHref = "")
        {
            Guid g = Guid.NewGuid();
            List<NotifyMessage> nmList = new List<NotifyMessage>();
            List<UserInfo> list = iuiBll.GetList(p => true);
            foreach (UserInfo item in list)
            {
                NotifyMessage nm = new NotifyMessage();
                nm.gkey = Guid.NewGuid();
                nm.gMsgKey = g;
                nm.gSendKey = ui.gkey;
                nm.sSendTitle = title;
                nm.sSendContent = content;
                nm.dSendDate = DateTime.Now;
                nm.gReceiveKey = item.gkey;
                nm.sHref = sHref;
                nm.iFlag = 1;
                nm.iType = iType;
                nmList.Add(nm);
            }
            if (inmBll.Add(nmList))
                return true;
            else
                return false;
        }

        /// <summary>
        /// 系统通知消息
        /// </summary>
        /// <param name="title">通知标题</param>
        /// <param name="content">通知内容</param>
        /// <param name="glist">接收用户主键</param>
        public static bool SendMsg(string title, string content, List<Guid> glist, string sHref = "")
        {
            Guid g = Guid.NewGuid();
            List<NotifyMessage> nmList = new List<NotifyMessage>();
            for (int i = 0; i < glist.Count; i++)
            {
                NotifyMessage nm = new NotifyMessage();
                nm.gkey = Guid.NewGuid();
                nm.gMsgKey = g;
                nm.gSendKey = Guid.Empty;
                nm.sSendTitle = "【系统通知】:" + title;
                nm.sSendContent = content;
                nm.dSendDate = DateTime.Now;
                nm.gReceiveKey = glist[i];
                nm.sHref = sHref;
                nm.iFlag = 1;
                nm.iType = 1;
                nmList.Add(nm);
            }
            if (inmBll.Add(nmList))
                return true;
            else
                return false;
        }

        /// <summary>
        /// 系统通知消息(全部人员)
        /// </summary>
        /// <param name="title">通知标题</param>
        /// <param name="content">通知内容</param>
        public static bool SendMsg(string title, string content, string sHref = "")
        {
            Guid g = Guid.NewGuid();
            List<NotifyMessage> nmList = new List<NotifyMessage>();
            List<UserInfo> list = iuiBll.GetList(p => true);
            foreach (UserInfo item in list)
            {
                NotifyMessage nm = new NotifyMessage();
                nm.gkey = Guid.NewGuid();
                nm.gMsgKey = g;
                nm.gSendKey = Guid.Empty;
                nm.sSendTitle = "【系统通知】:" + title;
                nm.sSendContent = content;
                nm.dSendDate = DateTime.Now;
                nm.gReceiveKey = item.gkey;
                nm.sHref = sHref;
                nm.iFlag = 1;
                nm.iType = 1;
                nmList.Add(nm);
            }
            if (inmBll.Add(nmList))
                return true;
            else
                return false;
        }
    }
}