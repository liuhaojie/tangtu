﻿using LIU.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace LIU.Web.App_Code
{
    public class EmailHelper
    {


        /// <summary>
        /// 激活账号
        /// </summary>
        public static string bodyRegTemp = @"
                <p>#name#<br></p>
                <p>您收到这封邮件，是由于在 【唐突世界】 进行了新用户注册，或用户修改 Email，或修重置密码
                使用了这个邮箱地址。如果您并没有访问过 【唐突世界】 ，或没有进行上述操作，请忽
                略这封邮件。您不需要退订或进行其他进一步的操作。</p>
                <br>
                ----------------------------------------------------------------------<br>
                <strong>帐号激活</strong>
                <br>
                <p>您只需点击下面的链接即可激活您的帐号（有效期10分钟）：<br>

                <a href='http://tangtu.top#url#' target='_blank' rel='noopener' style='color:red;'> http://tangtu.top#url# </a>
                <br>(如果上面不是链接形式，请将该地址手工粘贴到浏览器地址栏再访问)</p>
                ----------------------------------------------------------------------<br>
                <p>感谢您的访问，祝您使用愉快！</p>
                <p>
                此致<br>

                唐突世界 管理团队.<br>
                <a href ='http://tangtu.top' rel='noopener' target='_blank'>http://tangtu.top</a></p>";

        /// <summary>
        /// 校验邮箱
        /// </summary>
        public static string bodyEmailTemp = @"
                <p>#name#<br></p>
                <p>您收到这封邮件，是由于在 【唐突世界】 进行了新用户注册，或用户修改 Email，或重置密码
                使用了这个邮箱地址。如果您并没有访问过 【唐突世界】 ，或没有进行上述操作，请忽
                略这封邮件。您不需要退订或进行其他进一步的操作。</p>
                <br>
                ----------------------------------------------------------------------<br>
                <strong>校验邮箱</strong>
                <br>
                <p>您只需点击下面的链接即可校验您的邮箱（有效期10分钟）：<br>

                <a href='http://tangtu.top#url#' target='_blank' rel='noopener' style='color:red;'> http://tangtu.top#url# </a>
                <br>(如果上面不是链接形式，请将该地址手工粘贴到浏览器地址栏再访问)</p>
                ----------------------------------------------------------------------<br>
                <p>感谢您的访问，祝您使用愉快！</p>
                <p>
                此致<br>

                唐突世界 管理团队.<br>
                <a href ='http://tangtu.top' rel='noopener' target='_blank'>http://tangtu.top</a></p>";

        /// <summary>
        /// 修改密码
        /// </summary>
        public static string bodyPassTemp = @"
                <p>#name#<br></p>
                <p>您收到这封邮件，是由于在 【唐突世界】 进行了新用户注册，或用户修改 Email，或重置密码
                使用了这个邮箱地址。如果您并没有访问过 【唐突世界】 ，或没有进行上述操作，请忽
                略这封邮件。您不需要退订或进行其他进一步的操作。</p>
                <br>
                ----------------------------------------------------------------------<br>
                <strong>重置密码</strong>
                <br>
                <p>您只需点击下面的链接即可重置您的密码（有效期10分钟）：<br>

                <a href='http://tangtu.top#url#' target='_blank' rel='noopener' style='color:red;'> http://tangtu.top#url# </a>
                <br>(如果上面不是链接形式，请将该地址手工粘贴到浏览器地址栏再访问)</p>
                ----------------------------------------------------------------------<br>
                <p>感谢您的访问，祝您使用愉快！</p>
                <p>
                此致<br>

                唐突世界 管理团队.<br>
                <a href ='http://tangtu.top' rel='noopener' target='_blank'>http://tangtu.top</a></p>";


        /// <summary>
        /// 通过System.Web.Mail.MailMessage去发送，可以不被阿里云限制25端口的使用
        /// 暂时一般都用465端口
        /// </summary>
        /// <param name="smtpserver">SMTP服务,譬如：smtp.126.com</param>
        /// <param name="userName">发件箱</param>
        /// <param name="pwd">密码</param>
        /// <param name="nickName">昵称</param>
        /// <param name="strfrom">发件箱</param>
        /// <param name="strto">收件箱</param>
        /// <param name="MessageSubject">主题</param>
        /// <param name="MessageBody">内容</param>
        /// <param name="SUpFile">附件</param>
        /// <param name="port">端口</param>
        /// <param name="enablessl">SSL加密</param>
        /// <returns></returns>
        [Obsolete("过时的发生类，不建议移植其他")]
        public static bool SendWebEmail(string strto, string MessageSubject, string MessageBody,
            string smtpserver = "smtp.163.com", string userName = "tangtu_top@163.com",
            string pwd = "LIU123456", string nickName = "唐突世界", string strfrom = "tangtu_top@163.com",
            string SUpFile = "", int port = 465, int enablessl = 1)
        {
            System.Web.Mail.MailMessage mmsg = new System.Web.Mail.MailMessage();
            //邮件主题
            mmsg.Subject = MessageSubject;
            mmsg.BodyFormat = System.Web.Mail.MailFormat.Html;
            //邮件正文
            mmsg.Body = MessageBody;
            //正文编码
            mmsg.BodyEncoding = Encoding.UTF8;
            //优先级
            mmsg.Priority = System.Web.Mail.MailPriority.High;

            System.Web.Mail.MailAttachment data = null;
            if (SUpFile != "")
            {
                SUpFile = HttpContext.Current.Server.MapPath(SUpFile);//获得附件在本地地址
                System.Web.Mail.MailAttachment attachment = new System.Web.Mail.MailAttachment(SUpFile); //create the attachment
                mmsg.Attachments.Add(attachment); //add the attachment
            }
            //发件者邮箱地址
            mmsg.From = string.Format("\"{0}\"<{1}>", nickName, strfrom);

            //收件人收箱地址
            mmsg.To = strto;
            mmsg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");
            //用户名
            mmsg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", userName);
            //密码 不是邮箱登陆密码 而是邮箱设置POP3/SMTP 时生成的第三方客户端授权码
            mmsg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", pwd);
            //端口
            mmsg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserverport", port);
            //使用SSL
            mmsg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpusessl", (enablessl == 1 ? "true" : "false"));
            //Smtp服务器
            System.Web.Mail.SmtpMail.SmtpServer = smtpserver;
            try
            {
                System.Web.Mail.SmtpMail.Send(mmsg);
            }
            catch (Exception ex)
            {
                LogHelper.Error(ex.Message + "\r\n" + ex.StackTrace);
                //throw ex;
                return false;
            }
            return true;
        }
    }
}