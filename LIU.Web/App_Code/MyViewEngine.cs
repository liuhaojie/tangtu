﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LIU.Web.App_Code
{
    /// <summary>
    /// 自定义寻找View页面规则
    /// </summary>
    public class MyViewEngine : RazorViewEngine
    {
        public MyViewEngine()
        {
            ViewLocationFormats = new[]
            {
                "~/Views/EBook/{1}/{0}.cshtml",//我们的规则
                "~/Views/System/{1}/{0}.cshtml",
                "~/Views/Software/{1}/{0}.cshtml",
                "~/Views/Barrages/{1}/{0}.cshtml",//我们的规则
                "~/Views/Bill/{1}/{0}.cshtml",//我们的规则
                "~/Views/Chat/{1}/{0}.cshtml",//我们的规则
                "~/Views/{1}/{0}.cshtml",
                "~/Views/Shared/{0}.cshtml"
            };
        }
        public override ViewEngineResult FindView(ControllerContext controllerContext, string viewName, string masterName, bool useCache)
        {
            return base.FindView(controllerContext, viewName, masterName, useCache);
        }
    }
}