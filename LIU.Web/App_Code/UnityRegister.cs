﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Unity;
using LIU.Model;
using System.Data.Entity;
using LIU.UnityFactory;
using LIU.IBLL.System;
using LIU.BLL.System;
using LIU.IDAL.System;
using LIU.DAL.System;
using LIU.IBLL.EBook;
using LIU.BLL.EBook;
using LIU.IDAL.EBook;
using LIU.DAL.EBook;
using LIU.IBLL.Dy;
using LIU.BLL.Dy;
using LIU.IDAL.Dy;
using LIU.DAL.Dy;
using LIU.IBLL.Bill;
using LIU.BLL.Bill;
using LIU.IDAL.Bill;
using LIU.DAL.Bill;
using LIU.Common;
using Unity.Lifetime;

namespace LIU.Web.App_Code
{
    /// <summary>
    /// unity注册
    /// </summary>
    public class UnityRegister
    {

        /// <summary>
        /// 配置控制反转
        /// </summary>
        /// <param name="state"></param>
        public static void Register(HttpApplicationState state)
        {
            IUnityContainer unityContainer = new UnityContainer();

            //注册上下文
            unityContainer.RegisterType<DbContext, DB>(new HttpContextLifetimeManager(typeof(DbContext)));

            ITypeFinder defaultTypeFinder = new DefaultTypeFinder();
            var list = defaultTypeFinder.GetTypes();
            foreach (var item in list)
            {
                unityContainer.RegisterType(item, defaultTypeFinder.GetType(item), new HttpContextLifetimeManager(item));
            }

            #region 自动注册替换下的历史数据


            //#region 系统类
            ////用户表
            //unityContainer.RegisterType<IUserInfoBLL, UserInfoBLL>(new HttpContextLifetimeManager<IUserInfoBLL>());
            //unityContainer.RegisterType<IUserInfoDAL, UserInfoDAL>(new HttpContextLifetimeManager<IUserInfoDAL>());

            ////菜单
            //unityContainer.RegisterType<IMenuBLL, MenuBLL>(new HttpContextLifetimeManager<IMenuBLL>());
            //unityContainer.RegisterType<IMenuDAL, MenuDAL>(new HttpContextLifetimeManager<IMenuDAL>());

            ////角色
            //unityContainer.RegisterType<IRoleBLL, RoleBLL>(new HttpContextLifetimeManager<IRoleBLL>());
            //unityContainer.RegisterType<IRoleDAL, RoleDAL>(new HttpContextLifetimeManager<IRoleDAL>());

            ////角色权限
            //unityContainer.RegisterType<IRoleMenuBLL, RoleMenuBLL>(new HttpContextLifetimeManager<IRoleMenuBLL>());
            //unityContainer.RegisterType<IRoleMenuDAL, RoleMenuDAL>(new HttpContextLifetimeManager<IRoleMenuDAL>());

            ////字典目录
            //unityContainer.RegisterType<IDicCatalogBLL, DicCatalogBLL>(new HttpContextLifetimeManager<IDicCatalogBLL>());
            //unityContainer.RegisterType<IDicCatalogDAL, DicCatalogDAL>(new HttpContextLifetimeManager<IDicCatalogDAL>());

            ////字典详情
            //unityContainer.RegisterType<IDicInfoBLL, DicInfoBLL>(new HttpContextLifetimeManager<IDicInfoBLL>());
            //unityContainer.RegisterType<IDicInfoDAL, DicInfoDAL>(new HttpContextLifetimeManager<IDicInfoDAL>());

            ////系统参数
            //unityContainer.RegisterType<IParameterBLL, ParameterBLL>(new HttpContextLifetimeManager<IParameterBLL>());
            //unityContainer.RegisterType<IParameterDAL, ParameterDAL>(new HttpContextLifetimeManager<IParameterDAL>());

            ////留言板
            //unityContainer.RegisterType<IMessageBoardBLL, MessageBoardBLL>(new HttpContextLifetimeManager<IMessageBoardBLL>());
            //unityContainer.RegisterType<IMessageBoardDAL, MessageBoardDAL>(new HttpContextLifetimeManager<IMessageBoardDAL>());

            ////通知or消息
            //unityContainer.RegisterType<INotifyMessageBLL, NotifyMessageBLL>(new HttpContextLifetimeManager<INotifyMessageBLL>());
            //unityContainer.RegisterType<INotifyMessageDAL, NotifyMessageDAL>(new HttpContextLifetimeManager<INotifyMessageDAL>());

            ////系统日志
            //unityContainer.RegisterType<ILogBLL, LogBLL>(new HttpContextLifetimeManager<ILogBLL>());
            //unityContainer.RegisterType<ILogDAL, LogDAL>(new HttpContextLifetimeManager<ILogDAL>());

            ////公告
            //unityContainer.RegisterType<INoticeBLL, NoticeBLL>(new HttpContextLifetimeManager<INoticeBLL>());
            //unityContainer.RegisterType<INoticeDAL, NoticeDAL>(new HttpContextLifetimeManager<INoticeDAL>());

            ////头像框
            //unityContainer.RegisterType<IHeadFrameBLL, HeadFrameBLL>(new HttpContextLifetimeManager<IHeadFrameBLL>());
            //unityContainer.RegisterType<IHeadFrameDAL, HeadFrameDAL>(new HttpContextLifetimeManager<IHeadFrameDAL>());

            ////金币流水表
            //unityContainer.RegisterType<IGoldFlowBLL, GoldFlowBLL>(new HttpContextLifetimeManager<IGoldFlowBLL>());
            //unityContainer.RegisterType<IGoldFlowDAL, GoldFlowDAL>(new HttpContextLifetimeManager<IGoldFlowDAL>());

            ////验证表
            //unityContainer.RegisterType<IValidateBLL, ValidateBLL>(new HttpContextLifetimeManager<IValidateBLL>());
            //unityContainer.RegisterType<IValidateDAL, ValidateDAL>(new HttpContextLifetimeManager<IValidateDAL>());

            ////物品表
            //unityContainer.RegisterType<IGoodsBLL, GoodsBLL>(new HttpContextLifetimeManager<IGoodsBLL>());
            //unityContainer.RegisterType<IGoodsDAL, GoodsDAL>(new HttpContextLifetimeManager<IGoodsDAL>());

            ////用户物品表
            //unityContainer.RegisterType<IUserGoodsBLL, UserGoodsBLL>(new HttpContextLifetimeManager<IUserGoodsBLL>());
            //unityContainer.RegisterType<IUserGoodsDAL, UserGoodsDAL>(new HttpContextLifetimeManager<IUserGoodsDAL>());

            ////vip冻结表
            //unityContainer.RegisterType<IVIPHistoryBLL, VIPHistoryBLL>(new HttpContextLifetimeManager<IVIPHistoryBLL>());
            //unityContainer.RegisterType<IVIPHistoryDAL, VIPHistoryDAL>(new HttpContextLifetimeManager<IVIPHistoryDAL>());
            //#endregion

            //#region 小说模块
            ////小说表
            //unityContainer.RegisterType<INovelBLL, NovelBLL>(new HttpContextLifetimeManager<INovelBLL>());
            //unityContainer.RegisterType<INovelDAL, NovelDAL>(new HttpContextLifetimeManager<INovelDAL>());

            ////章节表
            //unityContainer.RegisterType<IChapterBLL, ChapterBLL>(new HttpContextLifetimeManager<IChapterBLL>());
            //unityContainer.RegisterType<IChapterDAL, ChapterDAL>(new HttpContextLifetimeManager<IChapterDAL>());

            ////采集配置表
            //unityContainer.RegisterType<ICollectionBLL, CollectionBLL>(new HttpContextLifetimeManager<ICollectionBLL>());
            //unityContainer.RegisterType<ICollectionDAL, CollectionDAL>(new HttpContextLifetimeManager<ICollectionDAL>());
            //#endregion

            //#region 斗鱼

            ////弹幕
            //unityContainer.RegisterType<IBarrageBLL, BarrageBLL>(new HttpContextLifetimeManager<IBarrageBLL>());
            //unityContainer.RegisterType<IBarrageDAL, BarrageDAL>(new HttpContextLifetimeManager<IBarrageDAL>());

            ////关键字
            //unityContainer.RegisterType<IKeyWordBLL, KeyWordBLL>(new HttpContextLifetimeManager<IKeyWordBLL>());
            //unityContainer.RegisterType<IKeyWordDAL, KeyWordDAL>(new HttpContextLifetimeManager<IKeyWordDAL>());

            //#endregion

            //#region 账单管理
            ////账单明细
            //unityContainer.RegisterType<IDetailBLL, DetailBLL>(new HttpContextLifetimeManager<IDetailBLL>());
            //unityContainer.RegisterType<IDetailDAL, DetailDAL>(new HttpContextLifetimeManager<IDetailDAL>());

            //#endregion
            #endregion
            state.Add("UnityContainer", unityContainer);
        }
    }
}