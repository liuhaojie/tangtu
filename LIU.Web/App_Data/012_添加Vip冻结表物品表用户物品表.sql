IF NOT EXISTS(SELECT * FROM sysobjects WHERE name='sys_goods' AND type='u')
begin
CREATE TABLE [dbo].[sys_goods](
	[ID] [int] IDENTITY(10000,1) NOT NULL,
	[sName] [varchar](64) NOT NULL,
	[sExplain] [varchar](512) NOT NULL,
	[sHref] [varchar](512) NOT NULL,
	[dCreateTime] [datetime] NOT NULL,
	[sClassify] [varchar](50) NULL,
	[iSort] [int] NOT NULL,
	[sValue1] [varchar](128) NULL,
	[sValue2] [varchar](128) NULL,
	[iOldPrice] [int] NOT NULL,
	[iNewPrice] [int] NOT NULL,
	[sRestrictionsType] [varchar](32) NOT NULL,
	[iRestrictions] [int] NOT NULL,
	[iType] [int] NOT NULL,
 CONSTRAINT [PK_sys_goods] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

IF NOT EXISTS(SELECT * FROM sysobjects WHERE name='sys_user_goods' AND type='u')
begin
CREATE TABLE [dbo].[sys_user_goods](
	[gkey] [uniqueidentifier] NOT NULL,
	[GoodsID] [int] NOT NULL,
	[gUserKey] [uniqueidentifier] NOT NULL,
	[iFlag] [int] NOT NULL,
	[sGoodsFrom] [varchar](500) NULL,
	[dOverdueTime] [datetime] NULL,
	[dCreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_sys_user_goods] PRIMARY KEY CLUSTERED 
(
	[gkey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

IF NOT EXISTS(SELECT * FROM sysobjects WHERE name='sys_vipHistory' AND type='u')
begin
CREATE TABLE [dbo].[sys_vipHistory](
	[gkey] [uniqueidentifier] NOT NULL,
	[gUserKey] [uniqueidentifier] NOT NULL,
	[iLevel] [int] NOT NULL,
	[iDay] [int] NOT NULL,
	[dCreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_sys_vipHistory] PRIMARY KEY CLUSTERED 
(
	[gkey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
end

GO
IF NOT EXISTS(SELECT * FROM sysobjects WHERE name='DF_sys_goods_iSort' AND type='d')
begin
ALTER TABLE [dbo].[sys_goods] ADD  CONSTRAINT [DF_sys_goods_iSort]  DEFAULT ((0)) FOR [iSort]
end
GO
IF NOT EXISTS(SELECT * FROM sysobjects WHERE name='DF_sys_goods_iType' AND type='d')
begin
ALTER TABLE [dbo].[sys_goods] ADD  CONSTRAINT [DF_sys_goods_iType]  DEFAULT ((0)) FOR [iType]
end
GO
IF NOT EXISTS(SELECT * FROM sysobjects WHERE name='DF_sys_user_goods_iFlag' AND type='d')
begin
ALTER TABLE [dbo].[sys_user_goods] ADD  CONSTRAINT [DF_sys_user_goods_iFlag]  DEFAULT ((0)) FOR [iFlag]
end
GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键 自增 从10000开始' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_goods', @level2type=N'COLUMN',@level2name=N'ID'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'物品名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_goods', @level2type=N'COLUMN',@level2name=N'sName'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'物品说明' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_goods', @level2type=N'COLUMN',@level2name=N'sExplain'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'物品图片地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_goods', @level2type=N'COLUMN',@level2name=N'sHref'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'添加时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_goods', @level2type=N'COLUMN',@level2name=N'dCreateTime'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'分类' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_goods', @level2type=N'COLUMN',@level2name=N'sClassify'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'排序' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_goods', @level2type=N'COLUMN',@level2name=N'iSort'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留参数1 在vip道具中 sValue1做为vip等级  sValue2作为vip天数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_goods', @level2type=N'COLUMN',@level2name=N'sValue1'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'预留参数2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_goods', @level2type=N'COLUMN',@level2name=N'sValue2'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'原价' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_goods', @level2type=N'COLUMN',@level2name=N'iOldPrice'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'现价' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_goods', @level2type=N'COLUMN',@level2name=N'iNewPrice'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'限购类型 all:不限购,only:终身，day:天，week:周，month:月，year:年' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_goods', @level2type=N'COLUMN',@level2name=N'sRestrictionsType'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'限购 0表示限' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_goods', @level2type=N'COLUMN',@level2name=N'iRestrictions'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0表示可以购买 1表示不可以购买' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_goods', @level2type=N'COLUMN',@level2name=N'iType'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user_goods', @level2type=N'COLUMN',@level2name=N'gkey'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'物品表主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user_goods', @level2type=N'COLUMN',@level2name=N'GoodsID'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user_goods', @level2type=N'COLUMN',@level2name=N'gUserKey'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'使用表示 0未使用，1已使用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user_goods', @level2type=N'COLUMN',@level2name=N'iFlag'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'物品来源' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user_goods', @level2type=N'COLUMN',@level2name=N'sGoodsFrom'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'物品过期时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user_goods', @level2type=N'COLUMN',@level2name=N'dOverdueTime'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'添加时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user_goods', @level2type=N'COLUMN',@level2name=N'dCreateTime'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_vipHistory', @level2type=N'COLUMN',@level2name=N'gUserKey'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'vip等级' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_vipHistory', @level2type=N'COLUMN',@level2name=N'iLevel'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'vip剩余天数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_vipHistory', @level2type=N'COLUMN',@level2name=N'iDay'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'添加时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_vipHistory', @level2type=N'COLUMN',@level2name=N'dCreateTime'
--GO
