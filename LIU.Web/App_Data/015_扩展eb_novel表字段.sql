--采集的网站名称
IF NOT EXISTS (SELECT 1 FROM sys.columns WHERE name='sWebsiteName' AND object_id=OBJECT_ID('eb_novel'))
BEGIN
    ALTER TABLE dbo.eb_novel ADD sWebsiteName VARCHAR(256) NULL
END

--采集的网站地址
IF NOT EXISTS (SELECT 1 FROM sys.columns WHERE name='sWebsiteUrl' AND object_id=OBJECT_ID('eb_novel'))
BEGIN
    ALTER TABLE dbo.eb_novel ADD sWebsiteUrl VARCHAR(512) NULL
END

--首章地址
IF NOT EXISTS (SELECT 1 FROM sys.columns WHERE name='gStartAdd' AND object_id=OBJECT_ID('eb_novel'))
BEGIN
    ALTER TABLE dbo.eb_novel ADD gStartAdd VARCHAR(1024) NULL
END

--下一章地址（下一章地址存在就从下一章地址开始采集）
IF NOT EXISTS (SELECT 1 FROM sys.columns WHERE name='gIndexAdd' AND object_id=OBJECT_ID('eb_novel'))
BEGIN
    ALTER TABLE dbo.eb_novel ADD gIndexAdd VARCHAR(1024) NULL
END

--采集信息出错信息
IF NOT EXISTS (SELECT 1 FROM sys.columns WHERE name='gErrorMag' AND object_id=OBJECT_ID('eb_novel'))
BEGIN
    ALTER TABLE dbo.eb_novel ADD gErrorMag VARCHAR(1024) NULL
END

