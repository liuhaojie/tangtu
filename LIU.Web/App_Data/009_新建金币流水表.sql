--新建金币流水表
IF NOT EXISTS(SELECT * FROM sysobjects WHERE name='sys_goldFlow' AND type='u')
begin
CREATE TABLE [dbo].[sys_goldFlow](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[iOldGold] [int] NOT NULL,
	[iNewGold] [int] NOT NULL,
	[gUserKey] [uniqueidentifier] NOT NULL,
	[dTime] [datetime] NOT NULL,
	[sMemo] [varchar](500) NULL,
	[iType] [int] NOT NULL,
	[iGold] [int] NOT NULL,
	[iFlag] [int] NOT NULL,
	[gObjectKey] [uniqueidentifier] NULL,
 CONSTRAINT [PK_sys_goldFlow] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO
--SET ANSI_PADDING OFF
--GO
IF NOT EXISTS(SELECT * FROM sysobjects WHERE name='DF_sys_goldFlow_iType' AND type='d')
begin
ALTER TABLE [dbo].[sys_goldFlow] ADD  CONSTRAINT [DF_sys_goldFlow_iType]  DEFAULT ((9)) FOR [iType]
end
GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'变化前的金币' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_goldFlow', @level2type=N'COLUMN',@level2name=N'iOldGold'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'变化后的金币' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_goldFlow', @level2type=N'COLUMN',@level2name=N'iNewGold'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_goldFlow', @level2type=N'COLUMN',@level2name=N'gUserKey'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发生的时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_goldFlow', @level2type=N'COLUMN',@level2name=N'dTime'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'说明' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_goldFlow', @level2type=N'COLUMN',@level2name=N'sMemo'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'类型 1签到，2赠送，3消费，4获得，5,取消，9其他 默认' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_goldFlow', @level2type=N'COLUMN',@level2name=N'iType'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'变化的金币数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_goldFlow', @level2type=N'COLUMN',@level2name=N'iGold'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N' 1 增加 2减少' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_goldFlow', @level2type=N'COLUMN',@level2name=N'iFlag'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'产生变化的对象，空默认系统' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_goldFlow', @level2type=N'COLUMN',@level2name=N'gObjectKey'
--GO
