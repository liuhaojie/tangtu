--获取用户物品
if Exists(select name from sysobjects where NAME = 'P_Get_User_Goods' and type='P')
BEGIN
	DROP PROCEDURE P_Get_User_Goods
END
GO
CREATE PROC P_Get_User_Goods(
 @gkey UNIQUEIDENTIFIER
)
AS 
BEGIN
	SELECT * FROM 
	(SELECT GoodsID,COUNT(*) AS iCount FROM dbo.sys_user_goods
	WHERE gUserKey=@gkey AND iFlag=0 GROUP BY GoodsID) AS a
	LEFT JOIN dbo.sys_goods AS b ON a.GoodsID=b.ID ORDER BY b.sClassify ASC,B.iSort ASC
END


