
--创建验证表
IF NOT EXISTS(SELECT * FROM sysobjects WHERE name='sys_validate' AND type='u')
begin
CREATE TABLE [dbo].[sys_validate](
	[gkey] [uniqueidentifier] NOT NULL,
	[gUserKey] [uniqueidentifier] NOT NULL,
	[dCreateTime] [datetime] NOT NULL,
	[dOverdueTime] [datetime] NOT NULL,
	[iType] [int] NOT NULL,
	[iFlag] [int] NOT NULL,
 CONSTRAINT [PK_sys_validate] PRIMARY KEY CLUSTERED 
(
	[gkey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_validate', @level2type=N'COLUMN',@level2name=N'gUserKey'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_validate', @level2type=N'COLUMN',@level2name=N'dCreateTime'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'过期时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_validate', @level2type=N'COLUMN',@level2name=N'dOverdueTime'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N' 1.激活 2.邮箱验证 3.密码修改' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_validate', @level2type=N'COLUMN',@level2name=N'iType'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0.已经使用 1.未使用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_validate', @level2type=N'COLUMN',@level2name=N'iFlag'
--GO
