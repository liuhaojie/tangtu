
--交易类型
IF NOT EXISTS(SELECT * FROM dbo.sys_dicCatalog WHERE gkey='5736801D-F0C0-46C9-AA98-9547F1B7FAA0')
BEGIN
INSERT dbo.sys_dicCatalog
(
    gkey,
    sName,
    sCode,
    sRemark,
    iSort,
    iFlag
)
VALUES
(   '5736801D-F0C0-46C9-AA98-9547F1B7FAA0', -- gkey - uniqueidentifier
    '交易类型',   -- sName - varchar(50)
    'TransactionType',   -- sCode - varchar(50)
    '账单的交易类型',   -- sRemark - varchar(300)
    5,    -- iSort - int
    1     -- iFlag - int
    )
END



IF NOT EXISTS(SELECT * FROM dbo.sys_dicInfo WHERE gkey='565E8143-E43B-473E-A073-F743FE5AD88A')
BEGIN
INSERT dbo.sys_dicInfo
(
    gkey,
    gDicCatalogKey,
    sCode,
    sName,
    sValue,
    sRemark,
    iSort,
    iFlag
)
VALUES
(   '565E8143-E43B-473E-A073-F743FE5AD88A', -- gkey - uniqueidentifier
    '5736801D-F0C0-46C9-AA98-9547F1B7FAA0', -- gDicCatalogKey - uniqueidentifier
    'TransactionType',   -- sCode - varchar(50)
    '购物',   -- sName - varchar(100)
    '1',   -- sValue - varchar(100)
    '',   -- sRemark - varchar(300)
    1,    -- iSort - int
    1     -- iFlag - int
    )
	END
  
IF NOT EXISTS(SELECT * FROM dbo.sys_dicInfo WHERE gkey='EBE674A0-40D6-4EC2-BF9B-D277FCBEE38A')
BEGIN
INSERT dbo.sys_dicInfo
(
    gkey,
    gDicCatalogKey,
    sCode,
    sName,
    sValue,
    sRemark,
    iSort,
    iFlag
)
VALUES
(   'EBE674A0-40D6-4EC2-BF9B-D277FCBEE38A', -- gkey - uniqueidentifier
    '5736801D-F0C0-46C9-AA98-9547F1B7FAA0', -- gDicCatalogKey - uniqueidentifier
    'TransactionType',   -- sCode - varchar(50)
    '借款',   -- sName - varchar(100)
    '2',   -- sValue - varchar(100)
    '',   -- sRemark - varchar(300)
    2,    -- iSort - int
    1     -- iFlag - int
    )
END

IF NOT EXISTS(SELECT * FROM dbo.sys_dicInfo WHERE gkey='6BC8D45C-18B6-4CD2-80D3-5898B26002BB')
BEGIN
INSERT dbo.sys_dicInfo
(
    gkey,
    gDicCatalogKey,
    sCode,
    sName,
    sValue,
    sRemark,
    iSort,
    iFlag
)
VALUES
(   '6BC8D45C-18B6-4CD2-80D3-5898B26002BB', -- gkey - uniqueidentifier
    '5736801D-F0C0-46C9-AA98-9547F1B7FAA0', -- gDicCatalogKey - uniqueidentifier
    'TransactionType',   -- sCode - varchar(50)
    '还款',   -- sName - varchar(100)
    '3',   -- sValue - varchar(100)
    '',   -- sRemark - varchar(300)
    3,    -- iSort - int
    1     -- iFlag - int
    )
END


IF NOT EXISTS(SELECT * FROM dbo.sys_dicInfo WHERE gkey='6D8AE156-63BE-4C2A-ADDC-DA14DEFA4972')
BEGIN
INSERT dbo.sys_dicInfo
(
    gkey,
    gDicCatalogKey,
    sCode,
    sName,
    sValue,
    sRemark,
    iSort,
    iFlag
)
VALUES
(   '6D8AE156-63BE-4C2A-ADDC-DA14DEFA4972', -- gkey - uniqueidentifier
    '5736801D-F0C0-46C9-AA98-9547F1B7FAA0', -- gDicCatalogKey - uniqueidentifier
    'TransactionType',   -- sCode - varchar(50)
    '结婚',   -- sName - varchar(100)
    '4',   -- sValue - varchar(100)
    '',   -- sRemark - varchar(300)
    4,    -- iSort - int
    1     -- iFlag - int
    )
END


IF NOT EXISTS(SELECT * FROM dbo.sys_dicInfo WHERE gkey='17DACAEA-4667-4342-9F66-7CC0B5D3176D')
BEGIN
INSERT dbo.sys_dicInfo
(
    gkey,
    gDicCatalogKey,
    sCode,
    sName,
    sValue,
    sRemark,
    iSort,
    iFlag
)
VALUES
(   '17DACAEA-4667-4342-9F66-7CC0B5D3176D', -- gkey - uniqueidentifier
    '5736801D-F0C0-46C9-AA98-9547F1B7FAA0', -- gDicCatalogKey - uniqueidentifier
    'TransactionType',   -- sCode - varchar(50)
    '其他',   -- sName - varchar(100)
    '9',   -- sValue - varchar(100)
    '',   -- sRemark - varchar(300)
    9,    -- iSort - int
    1     -- iFlag - int
    )
END
--交易方式
IF NOT EXISTS(SELECT * FROM dbo.sys_dicCatalog WHERE gkey='D6827527-B5AA-4081-B1C3-99DE1D8118BA')
BEGIN
INSERT dbo.sys_dicCatalog
(
    gkey,
    sName,
    sCode,
    sRemark,
    iSort,
    iFlag
)
VALUES
(   'D6827527-B5AA-4081-B1C3-99DE1D8118BA', -- gkey - uniqueidentifier
    '交易方式',   -- sName - varchar(50)
    'TransactionMode',   -- sCode - varchar(50)
    '账单的交易方式',   -- sRemark - varchar(300)
    6,    -- iSort - int
    1     -- iFlag - int
    )
END

IF NOT EXISTS(SELECT * FROM dbo.sys_dicInfo WHERE gkey='63BD931C-1426-40F5-A15B-927CBD0CB7CD')
BEGIN
INSERT dbo.sys_dicInfo
(
    gkey,
    gDicCatalogKey,
    sCode,
    sName,
    sValue,
    sRemark,
    iSort,
    iFlag
)
VALUES
(   '63BD931C-1426-40F5-A15B-927CBD0CB7CD', -- gkey - uniqueidentifier
    'D6827527-B5AA-4081-B1C3-99DE1D8118BA', -- gDicCatalogKey - uniqueidentifier
    'TransactionMode',   -- sCode - varchar(50)
    '支付宝',   -- sName - varchar(100)
    '1',   -- sValue - varchar(100)
    '',   -- sRemark - varchar(300)
    1,    -- iSort - int
    1     -- iFlag - int
    )
	END

IF NOT EXISTS(SELECT * FROM dbo.sys_dicInfo WHERE gkey='2FCA7DE3-C6B2-4719-AAFA-47592291D3A9')
BEGIN
INSERT dbo.sys_dicInfo
(
    gkey,
    gDicCatalogKey,
    sCode,
    sName,
    sValue,
    sRemark,
    iSort,
    iFlag
)
VALUES
(   '2FCA7DE3-C6B2-4719-AAFA-47592291D3A9', -- gkey - uniqueidentifier
    'D6827527-B5AA-4081-B1C3-99DE1D8118BA', -- gDicCatalogKey - uniqueidentifier
    'TransactionMode',   -- sCode - varchar(50)
    '微信',   -- sName - varchar(100)
    '2',   -- sValue - varchar(100)
    '',   -- sRemark - varchar(300)
    2,    -- iSort - int
    1     -- iFlag - int
    )
	END

IF NOT EXISTS(SELECT * FROM dbo.sys_dicInfo WHERE gkey='F877F57B-8FB4-4600-A727-38049ACA263B')
BEGIN
INSERT dbo.sys_dicInfo
(
    gkey,
    gDicCatalogKey,
    sCode,
    sName,
    sValue,
    sRemark,
    iSort,
    iFlag
)
VALUES
(   'F877F57B-8FB4-4600-A727-38049ACA263B', -- gkey - uniqueidentifier
    'D6827527-B5AA-4081-B1C3-99DE1D8118BA', -- gDicCatalogKey - uniqueidentifier
    'TransactionMode',   -- sCode - varchar(50)
    '现金',   -- sName - varchar(100)
    '3',   -- sValue - varchar(100)
    '',   -- sRemark - varchar(300)
    3,    -- iSort - int
    1     -- iFlag - int
    )
	END
	
IF NOT EXISTS(SELECT * FROM dbo.sys_dicInfo WHERE gkey='82AF38E6-18E8-4370-B803-605167E0A52D')
BEGIN
INSERT dbo.sys_dicInfo
(
    gkey,
    gDicCatalogKey,
    sCode,
    sName,
    sValue,
    sRemark,
    iSort,
    iFlag
)
VALUES
(   '82AF38E6-18E8-4370-B803-605167E0A52D', -- gkey - uniqueidentifier
    'D6827527-B5AA-4081-B1C3-99DE1D8118BA', -- gDicCatalogKey - uniqueidentifier
    'TransactionMode',   -- sCode - varchar(50)
    '其他',   -- sName - varchar(100)
    '9',   -- sValue - varchar(100)
    '',   -- sRemark - varchar(300)
    9,    -- iSort - int
    1     -- iFlag - int
    )
	END
