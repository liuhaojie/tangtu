IF NOT EXISTS(SELECT * FROM sysobjects WHERE name='bl_detail' AND type='u')
begin
CREATE TABLE [dbo].[bl_detail](
	[gkey] [uniqueidentifier] NOT NULL,
	[gBelongKey] [uniqueidentifier] NOT NULL,
	[iFlag] [int] NOT NULL,
	[dAmount] [decimal](10, 2) NOT NULL,
	[income] [int] NOT NULL,
	[iTransactionType] [int] NOT NULL,
	[iTransactionMode] [int] NOT NULL,
	[sTransactionObject] [nvarchar](100) NOT NULL,
	[sExplain] [nvarchar](300) NULL,
	[dTransactionTime] [datetime] NOT NULL,
	[dAddTime] [datetime] NOT NULL,
 CONSTRAINT [PK_bl_detail] PRIMARY KEY CLUSTERED 
(
	[gkey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'条账单归属人员' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bl_detail', @level2type=N'COLUMN',@level2name=N'gBelongKey'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标志 0删除，1存在' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bl_detail', @level2type=N'COLUMN',@level2name=N'iFlag'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bl_detail', @level2type=N'COLUMN',@level2name=N'dAmount'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'支出收入 1收入，2支出' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bl_detail', @level2type=N'COLUMN',@level2name=N'income'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易类型 通过字典获取' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bl_detail', @level2type=N'COLUMN',@level2name=N'iTransactionType'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易方式 通过字典获取' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bl_detail', @level2type=N'COLUMN',@level2name=N'iTransactionMode'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易对象' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bl_detail', @level2type=N'COLUMN',@level2name=N'sTransactionObject'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'说明' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bl_detail', @level2type=N'COLUMN',@level2name=N'sExplain'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bl_detail', @level2type=N'COLUMN',@level2name=N'dTransactionTime'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'添加时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bl_detail', @level2type=N'COLUMN',@level2name=N'dAddTime'
--GO
