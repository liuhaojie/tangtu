
--添加头像框地址
IF NOT EXISTS ( SELECT 1 FROM SYSOBJECTS T1  INNER JOIN SYSCOLUMNS T2 ON T1.ID=T2.ID  
		WHERE T1.NAME='sys_userInfo' AND T2.NAME='sHerdImageUrl'  
 )  
 BEGIN
    ALTER TABLE dbo.sys_userInfo ADD sHerdImageUrl VARCHAR(300) NULL
 END
 GO
 
--添加Vip级别
IF NOT EXISTS ( SELECT 1 FROM SYSOBJECTS T1  INNER JOIN SYSCOLUMNS T2 ON T1.ID=T2.ID  
		WHERE T1.NAME='sys_userInfo' AND T2.NAME='iLevel'  
 )  
 BEGIN
    ALTER TABLE dbo.sys_userInfo ADD iLevel INT DEFAULT(0) NOT NULL
 END

 --添加vip到期时间
IF NOT EXISTS ( SELECT 1 FROM SYSOBJECTS T1  INNER JOIN SYSCOLUMNS T2 ON T1.ID=T2.ID  
		WHERE T1.NAME='sys_userInfo' AND T2.NAME='dLevelTIme'  
 )  
 BEGIN
    ALTER TABLE dbo.sys_userInfo ADD dLevelTIme DATETIME NULL
 END
 GO

 
--添加金币
IF NOT EXISTS ( SELECT 1 FROM SYSOBJECTS T1  INNER JOIN SYSCOLUMNS T2 ON T1.ID=T2.ID  
		WHERE T1.NAME='sys_userInfo' AND T2.NAME='iGold'  
 )  
 BEGIN
    ALTER TABLE dbo.sys_userInfo ADD iGold int DEFAULT(0) NOT NULL
 END

 --添加邮箱地址
IF NOT EXISTS ( SELECT 1 FROM SYSOBJECTS T1  INNER JOIN SYSCOLUMNS T2 ON T1.ID=T2.ID  
		WHERE T1.NAME='sys_userInfo' AND T2.NAME='sEmail'  
 )  
 BEGIN
    ALTER TABLE dbo.sys_userInfo ADD sEmail varchar(256) NULL
 END

  --添加邮箱验证结果
IF NOT EXISTS ( SELECT 1 FROM SYSOBJECTS T1  INNER JOIN SYSCOLUMNS T2 ON T1.ID=T2.ID  
		WHERE T1.NAME='sys_userInfo' AND T2.NAME='iEmailValidate'  
 )  
 BEGIN
    ALTER TABLE dbo.sys_userInfo ADD iEmailValidate int DEFAULT(0) not NULL
 END

 GO
-- EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'头像框地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_userInfo', @level2type=N'COLUMN',@level2name=N'sHerdImageUrl'
--GO
-- EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Vip级别' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_userInfo', @level2type=N'COLUMN',@level2name=N'iLevel'
--GO
-- EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'vip到期时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_userInfo', @level2type=N'COLUMN',@level2name=N'dLevelTIme'
--GO
--GO
-- EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'金币' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_userInfo', @level2type=N'COLUMN',@level2name=N'iGold'
--GO
-- EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'邮箱地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_userInfo', @level2type=N'COLUMN',@level2name=N'sEmail'
--GO
-- EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'邮箱验证结果 0未验证通过 1验证通过' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_userInfo', @level2type=N'COLUMN',@level2name=N'iEmailValidate'
--GO