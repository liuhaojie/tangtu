--创建表
IF NOT EXISTS(SELECT * FROM sysobjects WHERE name='sys_headFrame' AND type='u')
begin
CREATE TABLE [dbo].[sys_headFrame](
	[gkey] [uniqueidentifier] NOT NULL,
	[iLevel] [int] NOT NULL,
	[sUrl] [varchar](500) NOT NULL,
	[sName] [varchar](200) NOT NULL,
	[iSort] [int] NOT NULL,
	[dAddTime] [datetime] NOT NULL,
 CONSTRAINT [PK_sys_headFrame] PRIMARY KEY CLUSTERED 
(
	[gkey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO
--SET ANSI_PADDING OFF
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_headFrame', @level2type=N'COLUMN',@level2name=N'gkey'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'级别' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_headFrame', @level2type=N'COLUMN',@level2name=N'iLevel'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'图片地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_headFrame', @level2type=N'COLUMN',@level2name=N'sUrl'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'图片名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_headFrame', @level2type=N'COLUMN',@level2name=N'sName'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'排序号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_headFrame', @level2type=N'COLUMN',@level2name=N'iSort'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_headFrame', @level2type=N'COLUMN',@level2name=N'dAddTime'
--GO
