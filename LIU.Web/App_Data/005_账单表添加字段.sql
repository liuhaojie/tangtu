
--添加交易编号
IF NOT EXISTS ( SELECT 1 FROM SYSOBJECTS T1  INNER JOIN SYSCOLUMNS T2 ON T1.ID=T2.ID  
		WHERE T1.NAME='bl_detail' AND T2.NAME='sNo'  
 )  
 BEGIN
    ALTER TABLE dbo.bl_detail ADD sNo VARCHAR(256) NULL
 END
 GO
 
--添加交易商品
IF NOT EXISTS ( SELECT 1 FROM SYSOBJECTS T1  INNER JOIN SYSCOLUMNS T2 ON T1.ID=T2.ID  
		WHERE T1.NAME='bl_detail' AND T2.NAME='sGoods'  
 )  
 BEGIN
    ALTER TABLE dbo.bl_detail ADD sGoods VARCHAR(1024) NULL
 END
 GO
-- EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bl_detail', @level2type=N'COLUMN',@level2name=N'sNo'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'商品名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bl_detail', @level2type=N'COLUMN',@level2name=N'sGoods'
--GO