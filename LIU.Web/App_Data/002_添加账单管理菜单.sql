--添加 账单管理 主菜单
IF NOT EXISTS(SELECT * FROM dbo.sys_menu WHERE gKey='1EC2B948-B45D-400E-877E-8E2E152208E3')
BEGIN
INSERT dbo.sys_menu
(
    gKey,
    sIcon,
    sHref,
    sBtnId,
    gParentKey,
    iType,
    gInsertKey,
    dInsert,
    gUpdateKey,
    dUpdate,
    sName
)
VALUES
(   '1EC2B948-B45D-400E-877E-8E2E152208E3',      -- gKey - uniqueidentifier
    NULL,        -- sIcon - varchar(50)
    '',        -- sHref - varchar(200)
    '',        -- sBtnId - varchar(50)
    '00000000-0000-0000-0000-000000000000',      -- gParentKey - uniqueidentifier
    1,         -- iType - int
    'A4408581-85C9-4117-B3D9-DFFFDD72FCCF',      -- gInsertKey - uniqueidentifier
    GETDATE(), -- dInsert - datetime
    NULL,      -- gUpdateKey - uniqueidentifier
    NULL, -- dUpdate - datetime
    '账单管理'         -- sName - varchar(100)
    )
END
GO

--添加 账单明细 界面菜单
IF NOT EXISTS(SELECT * FROM dbo.sys_menu WHERE gKey='E76F81EB-695A-408C-85F7-24FFA1F319A0')
BEGIN
INSERT dbo.sys_menu
(
    gKey,
    sIcon,
    sHref,
    sBtnId,
    gParentKey,
    iType,
    gInsertKey,
    dInsert,
    gUpdateKey,
    dUpdate,
    sName
)
VALUES
(   'E76F81EB-695A-408C-85F7-24FFA1F319A0',      -- gKey - uniqueidentifier
    NULL,        -- sIcon - varchar(50)
    '/Bill/Detail/Index',        -- sHref - varchar(200)
    '',        -- sBtnId - varchar(50)
    '1EC2B948-B45D-400E-877E-8E2E152208E3',      -- gParentKey - uniqueidentifier
    2,         -- iType - int
    'A4408581-85C9-4117-B3D9-DFFFDD72FCCF',      -- gInsertKey - uniqueidentifier
    GETDATE(), -- dInsert - datetime
    NULL,      -- gUpdateKey - uniqueidentifier
    NULL, -- dUpdate - datetime
    '账单明细'         -- sName - varchar(100)
)
END

