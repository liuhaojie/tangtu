--添加 物品管理 菜单
IF NOT EXISTS(SELECT * FROM dbo.sys_menu WHERE gKey='CD182E21-54FC-4470-8F5C-23046B12A0CC')
BEGIN
INSERT dbo.sys_menu
(
    gKey,
    sIcon,
    sHref,
    sBtnId,
    gParentKey,
    iType,
    gInsertKey,
    dInsert,
    gUpdateKey,
    dUpdate,
    sName
)
VALUES
(   'CD182E21-54FC-4470-8F5C-23046B12A0CC',      -- gKey - uniqueidentifier
    NULL,        -- sIcon - varchar(50)
    '/System/Sys/GoodsView',        -- sHref - varchar(200)
    '',        -- sBtnId - varchar(50)
    '15F4A6D0-F464-4451-A1DA-1616DE2657EA',      -- gParentKey - uniqueidentifier
    2,         -- iType - int
    'A4408581-85C9-4117-B3D9-DFFFDD72FCCF',      -- gInsertKey - uniqueidentifier
    GETDATE(), -- dInsert - datetime
    NULL,      -- gUpdateKey - uniqueidentifier
    NULL, -- dUpdate - datetime
    '物品管理'         -- sName - varchar(100)
    )
END
GO
