--批量赠送金币
if Exists(select name from sysobjects where NAME = 'Sys_Give_Gold' and type='P')
BEGIN
	DROP PROCEDURE Sys_Give_Gold
END
GO

CREATE PROC Sys_Give_Gold(
 @gold int
)
AS 
BEGIN
	INSERT dbo.sys_goldFlow
	(
	    iOldGold,
	    iNewGold,
	    gUserKey,
	    dTime,
	    sMemo,
	    iType,
	    iGold,
	    iFlag,
	    gObjectKey
	)SELECT iGold,iGold+@gold,gkey,GETDATE(),'',2,@gold,1,NULL FROM dbo.sys_userInfo

	--赠送所有用户金币
	UPDATE dbo.sys_userInfo SET iGold=iGold+@gold
	
	DECLARE @msgkey UNIQUEIDENTIFIER
	SET @msgkey=NEWID()
	--发生通知
	INSERT dbo.sys_notifyMessage
	(
	    gkey,
	    gMsgKey,
	    gSendKey,
	    sSendContent,
	    dSendDate,
	    gReceiveKey,
	    dReadDate,
	    iFlag,
	    sSendTitle,
	    sHref,
	    iType
	) SELECT NEWID(),@msgkey,'00000000-0000-0000-0000-000000000000','&nbsp;&nbsp;&nbsp;&nbsp;系统赠送您<span class="iconfonts icon-jinbi" style="color:#FFD700;font-size: 18px;line-height: 18px;position: relative;"></span><span style="color:red">【金币:'+CAST(@gold AS VARCHAR(30))+'】</span>',
	 GETDATE(),gkey,NULL,1,'【系统通知】:恭喜获得金币','',1 FROM dbo.sys_userInfo
	 
END
