USE [master]
GO
/****** Object:  Database [DB]    Script Date: 2018/12/7 15:12:56 ******/
CREATE DATABASE [DB]
GO
USE [DB]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 2018/12/7 15:12:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[dy_Barrage]    Script Date: 2018/12/7 15:12:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dy_Barrage](
	[gkey] [uniqueidentifier] NOT NULL,
	[sRoomId] [varchar](32) NULL,
	[sName] [varchar](52) NULL,
	[sContent] [varchar](1024) NULL,
	[dSpeakTime] [datetime] NULL,
	[sLevel] [int] NULL,
 CONSTRAINT [PK_dy_Barrage] PRIMARY KEY CLUSTERED 
(
	[gkey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[dy_KeyWord]    Script Date: 2018/12/7 15:12:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dy_KeyWord](
	[gkey] [uniqueidentifier] NOT NULL,
	[sWord] [varchar](100) NOT NULL,
	[iCount] [int] NOT NULL,
	[dStartTime] [datetime] NOT NULL,
	[dEndTime] [datetime] NOT NULL,
	[dIndedxDate] [date] NOT NULL,
	[sRoomId] [varchar](32) NOT NULL,
 CONSTRAINT [PK_dy_KeyWord] PRIMARY KEY CLUSTERED 
(
	[gkey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[eb_chapter]    Script Date: 2018/12/7 15:12:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[eb_chapter](
	[gkey] [uniqueidentifier] NOT NULL,
	[gNovelKey] [uniqueidentifier] NOT NULL,
	[iIndex] [int] NOT NULL,
	[sTitle] [nvarchar](256) NULL,
	[sText] [nvarchar](max) NULL,
	[dInsertTime] [datetime] NOT NULL,
	[gInsertUserKey] [uniqueidentifier] NOT NULL,
	[iRead] [int] NOT NULL CONSTRAINT [DF_eb_chapter_iRead]  DEFAULT ((0)),
	[sHtmlUrl] [varchar](256) NULL,
 CONSTRAINT [PK_dbo.eb_chapter] PRIMARY KEY CLUSTERED 
(
	[gkey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[eb_collection]    Script Date: 2018/12/7 15:12:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[eb_collection](
	[gkey] [uniqueidentifier] NOT NULL,
	[gNovelKey] [uniqueidentifier] NOT NULL,
	[gTextRegex] [nvarchar](512) NULL,
	[gTitleRegex] [nvarchar](512) NULL,
	[gNextRegex] [nvarchar](512) NULL,
	[gStartAdd] [nvarchar](512) NULL,
	[gIndexAdd] [nvarchar](512) NULL,
	[gErrorMag] [nvarchar](1024) NULL,
 CONSTRAINT [PK_dbo.eb_collection] PRIMARY KEY CLUSTERED 
(
	[gkey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[eb_novel]    Script Date: 2018/12/7 15:12:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[eb_novel](
	[gkey] [uniqueidentifier] NOT NULL,
	[sName] [nvarchar](128) NULL,
	[sAuthor] [nvarchar](128) NULL,
	[dUpdateTime] [datetime] NULL,
	[gUpdateUserKey] [uniqueidentifier] NOT NULL,
	[dInsertTime] [datetime] NULL,
	[gInsertUserKey] [uniqueidentifier] NOT NULL,
	[iType] [int] NOT NULL,
 CONSTRAINT [PK_dbo.eb_novel] PRIMARY KEY CLUSTERED 
(
	[gkey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sys_dicCatalog]    Script Date: 2018/12/7 15:12:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[sys_dicCatalog](
	[gkey] [uniqueidentifier] NOT NULL,
	[sName] [varchar](50) NOT NULL,
	[sCode] [varchar](50) NOT NULL,
	[sRemark] [varchar](300) NULL,
	[iSort] [int] NOT NULL,
	[iFlag] [int] NOT NULL,
 CONSTRAINT [PK_sys_dicCatalog] PRIMARY KEY CLUSTERED 
(
	[gkey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[sys_dicInfo]    Script Date: 2018/12/7 15:12:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[sys_dicInfo](
	[gkey] [uniqueidentifier] NOT NULL,
	[gDicCatalogKey] [uniqueidentifier] NOT NULL,
	[sCode] [varchar](50) NOT NULL,
	[sName] [varchar](100) NOT NULL,
	[sValue] [varchar](100) NOT NULL,
	[sRemark] [varchar](300) NULL,
	[iSort] [int] NOT NULL,
	[iFlag] [int] NOT NULL,
 CONSTRAINT [PK_sys_dicInfo] PRIMARY KEY CLUSTERED 
(
	[gkey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[sys_log]    Script Date: 2018/12/7 15:12:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sys_log](
	[gkey] [uniqueidentifier] NOT NULL,
	[sObjectId] [varchar](100) NOT NULL,
	[iOperationType] [int] NOT NULL,
	[sOperationTypeName] [varchar](10) NOT NULL,
	[sChangeContext] [varchar](max) NULL,
	[gOperationKey] [uniqueidentifier] NOT NULL,
	[dOperationTime] [datetime] NOT NULL,
 CONSTRAINT [PK_sys_log] PRIMARY KEY CLUSTERED 
(
	[gkey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[sys_menu]    Script Date: 2018/12/7 15:12:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sys_menu](
	[gKey] [uniqueidentifier] NOT NULL,
	[sIcon] [varchar](50) NULL,
	[sHref] [varchar](200) NULL,
	[sBtnId] [varchar](50) NULL,
	[gParentKey] [uniqueidentifier] NOT NULL,
	[iType] [int] NULL,
	[gInsertKey] [uniqueidentifier] NULL,
	[dInsert] [datetime] NULL,
	[gUpdateKey] [uniqueidentifier] NULL,
	[dUpdate] [datetime] NULL,
	[sName] [varchar](100) NULL,
 CONSTRAINT [PK_sys_menu] PRIMARY KEY CLUSTERED 
(
	[gKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[sys_messageBoard]    Script Date: 2018/12/7 15:12:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[sys_messageBoard](
	[gkey] [uniqueidentifier] NOT NULL,
	[sMessage] [varchar](1000) NOT NULL,
	[sReply] [varchar](1000) NULL,
	[gInsertKey] [uniqueidentifier] NOT NULL,
	[dInsert] [datetime] NOT NULL,
	[gReplyKey] [uniqueidentifier] NULL,
	[dReply] [datetime] NULL,
	[iType] [int] NOT NULL,
	[iFlag] [int] NOT NULL CONSTRAINT [DF_sys_messageBoard_iFlag]  DEFAULT ((1)),
 CONSTRAINT [PK_sys_messageBoard] PRIMARY KEY CLUSTERED 
(
	[gkey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[sys_Notice]    Script Date: 2018/12/7 15:12:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_Notice](
	[gkey] [uniqueidentifier] NOT NULL,
	[sContent] [nvarchar](1024) NOT NULL,
	[dCreateTime] [datetime] NOT NULL,
	[gCreateId] [uniqueidentifier] NOT NULL,
	[iFlag] [int] NOT NULL,
 CONSTRAINT [PK_sys_Notice] PRIMARY KEY CLUSTERED 
(
	[gkey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sys_notifyMessage]    Script Date: 2018/12/7 15:12:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sys_notifyMessage](
	[gkey] [uniqueidentifier] NOT NULL,
	[gMsgKey] [uniqueidentifier] NOT NULL,
	[gSendKey] [uniqueidentifier] NOT NULL,
	[sSendContent] [varchar](max) NOT NULL,
	[dSendDate] [datetime] NOT NULL,
	[gReceiveKey] [uniqueidentifier] NOT NULL,
	[dReadDate] [datetime] NULL,
	[iFlag] [int] NOT NULL,
	[sSendTitle] [varchar](100) NOT NULL,
	[sHref] [varchar](300) NOT NULL,
	[iType] [int] NOT NULL,
 CONSTRAINT [PK_sys_notifyMessage] PRIMARY KEY CLUSTERED 
(
	[gkey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[sys_parameter]    Script Date: 2018/12/7 15:12:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sys_parameter](
	[gkey] [uniqueidentifier] NOT NULL,
	[sName] [varchar](100) NOT NULL,
	[sCode] [varchar](100) NOT NULL,
	[sValue] [varchar](1000) NULL,
	[sRemark] [varchar](500) NULL,
	[iFlag] [int] NOT NULL,
 CONSTRAINT [PK_sys_parameter] PRIMARY KEY CLUSTERED 
(
	[gkey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[sys_role]    Script Date: 2018/12/7 15:12:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sys_role](
	[gkey] [uniqueidentifier] NOT NULL,
	[sName] [varchar](50) NULL,
	[sExplain] [varchar](200) NULL,
	[gInsertKey] [uniqueidentifier] NULL,
	[dInsert] [datetime] NULL,
	[gUpdateKey] [uniqueidentifier] NULL,
	[dUpdate] [datetime] NULL,
 CONSTRAINT [PK_sys_role] PRIMARY KEY CLUSTERED 
(
	[gkey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[sys_role_menu]    Script Date: 2018/12/7 15:12:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_role_menu](
	[gRoleKey] [uniqueidentifier] NOT NULL,
	[gMenuKey] [uniqueidentifier] NOT NULL,
	[gInsertKey] [uniqueidentifier] NULL,
	[dInsert] [datetime] NULL,
	[gUpdateKey] [uniqueidentifier] NULL,
	[dUpdate] [datetime] NULL,
 CONSTRAINT [PK_sys_role_menu] PRIMARY KEY CLUSTERED 
(
	[gRoleKey] ASC,
	[gMenuKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sys_userInfo]    Script Date: 2018/12/7 15:12:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sys_userInfo](
	[gkey] [uniqueidentifier] NOT NULL,
	[sLoginName] [nvarchar](30) NULL,
	[sPassWord] [nvarchar](32) NULL,
	[sName] [nvarchar](30) NULL,
	[iSex] [int] NULL,
	[iFlag] [int] NOT NULL,
	[LastDateTime] [datetime] NULL,
	[RegDateTime] [datetime] NULL,
	[sRoleKey] [varchar](500) NULL,
	[iState] [int] NOT NULL CONSTRAINT [DF_sys_userInfo_iState]  DEFAULT ((1)),
	[sImageUrl] [varchar](300) NULL,
 CONSTRAINT [PK_dbo.sys_userInfo] PRIMARY KEY CLUSTERED 
(
	[gkey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Index [IX_dy_Barrage]    Script Date: 2018/12/7 15:12:56 ******/
CREATE NONCLUSTERED INDEX [IX_dy_Barrage] ON [dbo].[dy_Barrage]
(
	[dSpeakTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'房间id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dy_Barrage', @level2type=N'COLUMN',@level2name=N'sRoomId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发言人名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dy_Barrage', @level2type=N'COLUMN',@level2name=N'sName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发言内容' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dy_Barrage', @level2type=N'COLUMN',@level2name=N'sContent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发言时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dy_Barrage', @level2type=N'COLUMN',@level2name=N'dSpeakTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发言人等级' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dy_Barrage', @level2type=N'COLUMN',@level2name=N'sLevel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'关键字' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dy_KeyWord', @level2type=N'COLUMN',@level2name=N'sWord'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'总数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dy_KeyWord', @level2type=N'COLUMN',@level2name=N'iCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'第一条时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dy_KeyWord', @level2type=N'COLUMN',@level2name=N'dStartTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最后一跳时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dy_KeyWord', @level2type=N'COLUMN',@level2name=N'dEndTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'属于那一天  yyyy-MM-dd 格式 对应数据库 date字段' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dy_KeyWord', @level2type=N'COLUMN',@level2name=N'dIndedxDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'房间id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dy_KeyWord', @level2type=N'COLUMN',@level2name=N'sRoomId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dicCatalog', @level2type=N'COLUMN',@level2name=N'gkey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'目录名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dicCatalog', @level2type=N'COLUMN',@level2name=N'sName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'目录代码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dicCatalog', @level2type=N'COLUMN',@level2name=N'sCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'目录备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dicCatalog', @level2type=N'COLUMN',@level2name=N'sRemark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'排序' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dicCatalog', @level2type=N'COLUMN',@level2name=N'iSort'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标志 0不启用 1启用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dicCatalog', @level2type=N'COLUMN',@level2name=N'iFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dicInfo', @level2type=N'COLUMN',@level2name=N'gkey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'字典目录表主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dicInfo', @level2type=N'COLUMN',@level2name=N'gDicCatalogKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'字典代码(对应字典目录代码)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dicInfo', @level2type=N'COLUMN',@level2name=N'sCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'字典名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dicInfo', @level2type=N'COLUMN',@level2name=N'sName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'字典值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dicInfo', @level2type=N'COLUMN',@level2name=N'sValue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'字典备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dicInfo', @level2type=N'COLUMN',@level2name=N'sRemark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'排序' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dicInfo', @level2type=N'COLUMN',@level2name=N'iSort'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标志 0不启用 1启用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dicInfo', @level2type=N'COLUMN',@level2name=N'iFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'留言内容' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_messageBoard', @level2type=N'COLUMN',@level2name=N'sMessage'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'回复内容' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_messageBoard', @level2type=N'COLUMN',@level2name=N'sReply'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'留言人员主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_messageBoard', @level2type=N'COLUMN',@level2name=N'gInsertKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'留言时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_messageBoard', @level2type=N'COLUMN',@level2name=N'dInsert'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'回复人员主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_messageBoard', @level2type=N'COLUMN',@level2name=N'gReplyKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'回复时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_messageBoard', @level2type=N'COLUMN',@level2name=N'dReply'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'留言类型 根据字典表获取具体类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_messageBoard', @level2type=N'COLUMN',@level2name=N'iType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标志 0删除 1启用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_messageBoard', @level2type=N'COLUMN',@level2name=N'iFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_Notice', @level2type=N'COLUMN',@level2name=N'gkey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'内容' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_Notice', @level2type=N'COLUMN',@level2name=N'sContent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_Notice', @level2type=N'COLUMN',@level2name=N'dCreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_Notice', @level2type=N'COLUMN',@level2name=N'gCreateId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0:禁用 1:启用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_Notice', @level2type=N'COLUMN',@level2name=N'iFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_notifyMessage', @level2type=N'COLUMN',@level2name=N'gkey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息键 用于区分同次发送的消息 分组作用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_notifyMessage', @level2type=N'COLUMN',@level2name=N'gMsgKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发送人主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_notifyMessage', @level2type=N'COLUMN',@level2name=N'gSendKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发送内容' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_notifyMessage', @level2type=N'COLUMN',@level2name=N'sSendContent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发送时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_notifyMessage', @level2type=N'COLUMN',@level2name=N'dSendDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'接收人主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_notifyMessage', @level2type=N'COLUMN',@level2name=N'gReceiveKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'阅读时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_notifyMessage', @level2type=N'COLUMN',@level2name=N'dReadDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息状态 0:删除 1未读 2已读' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_notifyMessage', @level2type=N'COLUMN',@level2name=N'iFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发送标题' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_notifyMessage', @level2type=N'COLUMN',@level2name=N'sSendTitle'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'地址链接' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_notifyMessage', @level2type=N'COLUMN',@level2name=N'sHref'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息类型 1:系统通知 2:非系统通知' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_notifyMessage', @level2type=N'COLUMN',@level2name=N'iType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_parameter', @level2type=N'COLUMN',@level2name=N'gkey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'参数名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_parameter', @level2type=N'COLUMN',@level2name=N'sName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'参数代码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_parameter', @level2type=N'COLUMN',@level2name=N'sCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'参数值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_parameter', @level2type=N'COLUMN',@level2name=N'sValue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'参数说明' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_parameter', @level2type=N'COLUMN',@level2name=N'sRemark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标志 0不启用 1启用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_parameter', @level2type=N'COLUMN',@level2name=N'iFlag'
GO

USE [DB]
GO
INSERT [dbo].[__MigrationHistory] ([MigrationId], [ContextKey], [Model], [ProductVersion]) VALUES (N'201803080227195_InitialCreate', N'LIU.Model.DB', 0x1F8B0800000000000400ED5ADB6EDB38107D5F60FF41D0D32ED05AB9BC7403BB45E22445D0DC1025DDC78296C68A108A52452AB0BFAD0FFB49FB0B3BBA8BBAD8A2656713B4C843A221E770349A33E470F2EF8F7FC69F161ED59E21E4AECF26FAFE684FD78059BEED3267A24762FEFE83FEE9E3EFBF8DCF6C6FA17DCDE71DC6F35093F189FE2844706418DC7A048FF091E75AA1CFFDB91859BE6710DB370EF6F6FE32F6F70D40081DB1346D7C1731E17A903CE0E3D46716042222F4CAB781F24C8E236682AA5D130F78402C98E897170FA36496AE1D5397A00126D0B9AE11C67C41049A77F4C0C114A1CF1C334001A1F7CB0070DE9C500E99D947E5F4BE6FB07710BF81512AE65056C485EF2902EE1F662E31EAEA1B39562F5C864E3B43E78A65FCD689E326FAF4910402425DABAF7534A5613CAFE2D6D1D989EF3F8D3295775A31F0AEF8FC1825F1CF3B6D1A5111853061108990E08CDB68465DEB0B2CEFFD27601316515AB50C6DC3314980A2DBD00F2014CB3B9867F63A4FB0D43543D634EAAA85A2A495BECEE7C8B575ED1AD727330AC5B73756035CFBCF40BF0C44712F980D8B1CE28289C303650C7EEF0A0A39060633D251D7AEC8E21298231E273AFEA96BE7EE02EC5C92E13E3017D98B4A228C7A2C030BB1F355EC0BC6F1F11E099FAF754A04A4CFAA9F28C54286876ADF696C94AC58CD159F52B052222AD0A5D0FAC5187594380CEFC02959B3B358741262BDD05AD72FF75AA620A138B6EDDDAF9464B71759E92C0CFDF08A385B5EA9772648625B2509240A3F15FF79FC6BF7DBD471241EFD70F71BD54360171B537DA35A13ACA9AAFABEA4B85DEE68776C39C5C4BA2A8798DEB48AADBB6073BF1FB3CC2517E08D72A59F8B5D97BEE3B235143BDCDB06C36E09E77FFBE1AAAC1E47C0F0855EE26D5CB371FE5EA3704ECB6D66B313FB25E1E274C3E48167847EAAAB398655B4202E8330833D3D69A11812296319CF9C299B96429920EAA563B9725A718F8A11630D42E540DD04A90CAEC1C9B6E30644265FA35D669D064039D4E6EBC2ABE5A58491DE4AE4B71746C7F5C5F88A04010678E53A239368667A97317D6FAA57FB5E8A6158BCA5E82FAC2D56127E481CA88DC65FC58673374C8296CC481CE053DB6B4CC318EAF06BBE801C26F55C5A3A3B9F1FFF9D51A648F21DDAA5C3CEF11D3C6022791D286CE85C37D1352D4249D89A9431EC228F7527F7950865CD23C194E2FE58F95D41152897F547C96F0BAA28B94C0525B90C904112517F0CE9EC52459206147C5D3BD0480EAF8D3551C7462D7CEAD16934C2B3B6C5D7A3BD1F17CA9CB6211D3A01FA306285F2DB2045F53A4002ABC855D02A15BF0C57195079D30EEBAE37B2AE2CDC25B052AC4496BC34AFF124172B6095C5B784558A5F0DDDD2FD7F33A6B5EAF6205987DEEEF8959D9BA5E49C8A1430F2325A42C9850A49BE5A264B49BE3AA0106BB5DA59F2536DECAD6E459D0780B4CC96F6FF54F46AF8551C8F37A358977A0F9675ABEE9068959A5BE24945AE8056D6D51258297ED9149056C652B4996A1B56562A4B10A9A83F865C2B57A1E491FE8852055D0594065E98528D0AB23EA558BDA8246B15E338ABDED677C51BE55C3A45D7D051CFAE1D9772D94D5A3C61647EA753EAE2FB9613AE0873E7C0457A53A663B5F9A1D6617F3DDD6E83739BF66B79FF2FB77D1173BF47805E45ABE66E7C4732B4AF361851EE4ABBF1971FD49366CF24B41E49F88747167F56B194FBCE83905A2ECBE3F382D85E6F59D1F39BF4997FC568571F78506CB4F47A87E135FAB9C3E0EA3DDB6168F5BEEC30B47AEF55014DB1BFFA36835FEA630CCB8572737358366C3630DBB261AF08686D680EF65BCF7CDD33E287A7EB358DCFF5FBA47ADBF38D067CA31799476AA37FA718FFF5D6633BEE8132EE0E6CAD36166BA1D14BBDDA66DCE408D6D664DC943E2D4DC77E506A4DC8669F6970BF312D6DD0DC998F96A766C2EC9BB5ED7E64D73A5B6B59762CC0B6D7D06C5B812FF9B76868CBB359988E8DEA3F758F4F81BB4E0911FF8B374BBD5682E673624BF2E8C417AC5A944FA905EF150882D14A8E434C67C412386C01E7C97F137C2534C22967DE0C70B7B98944108963CEC19B5129898E8DD5EB277D5DD9E6F14D103FF16DBC029AE9C684BB6127914BEDC2EEF3963DA60322E6D667407952499B027F83B32C90AE7DD6132873DF2904C0E22DE21EBC802218BF612679864D6CC318BC048758CBFC7EA11B64FD8790DD3E3E758913128F6718A53E3E620CDBDEE2E37FC621E1ECDB300000, N'6.1.3-40302')
GO
INSERT [dbo].[eb_collection] ([gkey], [gNovelKey], [gTextRegex], [gTitleRegex], [gNextRegex], [gStartAdd], [gIndexAdd], [gErrorMag]) 
VALUES (N'909b8640-68a1-43b0-8f0a-01890a1d49ba', N'6fc9a5ff-99e4-4e5d-a44b-8e306bd78278', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[eb_collection] ([gkey], [gNovelKey], [gTextRegex], [gTitleRegex], [gNextRegex], [gStartAdd], [gIndexAdd], [gErrorMag]) 
VALUES (N'3ac1a4aa-ca36-4c47-bf13-2b783b082504', N'493c0ccc-97e6-46ef-947a-48f1ebe558c6', N'<div class="yd_text2">([\d\D]*?)</div>', N'<h1>([\D\d]*?)</h1>', N'返回目录</a><a href="([\d\D]*?)" target="_top">下一章 →</a>', N'https://www.88dush.com/xiaoshuo/0/545/11451196.html',null,NULL)
GO
INSERT [dbo].[eb_collection] ([gkey], [gNovelKey], [gTextRegex], [gTitleRegex], [gNextRegex], [gStartAdd], [gIndexAdd], [gErrorMag]) 
VALUES (N'cfce5353-ab74-489c-af0f-746111d589fc', N'79808e5a-6a40-44e2-8ab8-b807ac99045b', N'<div class="yd_text2">([\d\D]*?)</div>', N'<h1>([\D\d]*?)</h1>', N'返回目录</a><a href="([\d\D]*?)" target="_top">下一章 →</a>', N'https://www.88dus.com/xiaoshuo/38/38639/8804554.html', NULL,null)
GO
INSERT [dbo].[eb_collection] ([gkey], [gNovelKey], [gTextRegex], [gTitleRegex], [gNextRegex], [gStartAdd], [gIndexAdd], [gErrorMag]) 
VALUES (N'7ee099d5-2cfd-43ae-adc2-94d0188eaac6', N'068db14b-2efb-40b4-b209-51357ddc9f58', N'<div class="yd_text2">([\d\D]*?)</div>', N'<h1>([\D\d]*?)</h1>', N'返回目录</a><a href="([\d\D]*?)" target="_top">下一章 →</a>', N'https://www.88dush.com/xiaoshuo/92/92416/30193274.html',NULL, NULL)
GO
INSERT [dbo].[eb_novel] ([gkey], [sName], [sAuthor], [dUpdateTime], [gUpdateUserKey], [dInsertTime], [gInsertUserKey], [iType]) VALUES (N'493c0ccc-97e6-46ef-947a-48f1ebe558c6', N'斗破苍穹', N'天蚕土豆', NULL, N'00000000-0000-0000-0000-000000000000', CAST(N'2018-11-08 14:51:48.547' AS DateTime), N'cad6ca3b-e537-4f84-b959-e1f61ced9d79', 1)
GO
INSERT [dbo].[eb_novel] ([gkey], [sName], [sAuthor], [dUpdateTime], [gUpdateUserKey], [dInsertTime], [gInsertUserKey], [iType]) VALUES (N'068db14b-2efb-40b4-b209-51357ddc9f58', N'牧神记', N'宅猪', NULL, N'00000000-0000-0000-0000-000000000000', CAST(N'2018-12-04 12:34:05.180' AS DateTime), N'a4408581-85c9-4117-b3d9-dfffdd72fccf', 2)
GO
INSERT [dbo].[eb_novel] ([gkey], [sName], [sAuthor], [dUpdateTime], [gUpdateUserKey], [dInsertTime], [gInsertUserKey], [iType]) VALUES (N'6fc9a5ff-99e4-4e5d-a44b-8e306bd78278', N'迷域行者（漫画）', N'七月初七&廿一', CAST(N'2018-10-30 17:31:41.577' AS DateTime), N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-08-30 09:32:41.177' AS DateTime), N'a4408581-85c9-4117-b3d9-dfffdd72fccf', 1)
GO
INSERT [dbo].[eb_novel] ([gkey], [sName], [sAuthor], [dUpdateTime], [gUpdateUserKey], [dInsertTime], [gInsertUserKey], [iType]) VALUES (N'79808e5a-6a40-44e2-8ab8-b807ac99045b', N'从前有座灵剑山', N'国王陛下', NULL, N'00000000-0000-0000-0000-000000000000', CAST(N'2018-05-04 16:59:42.007' AS DateTime), N'a4408581-85c9-4117-b3d9-dfffdd72fccf', 1)
GO
INSERT [dbo].[sys_dicCatalog] ([gkey], [sName], [sCode], [sRemark], [iSort], [iFlag]) VALUES (N'fdda77cf-e1d6-43a0-9124-709a5bf5e1b3', N'留言类型', N'messageCode', N'留言板类型', 2, 1)
GO
INSERT [dbo].[sys_dicCatalog] ([gkey], [sName], [sCode], [sRemark], [iSort], [iFlag]) VALUES (N'b06945fe-77d0-4d5b-acdf-71ec90ff7232', N'性别', N'sexCode', N'用户性别', 1, 1)
GO
INSERT [dbo].[sys_dicCatalog] ([gkey], [sName], [sCode], [sRemark], [iSort], [iFlag]) VALUES (N'965d2221-7f0e-435a-86e4-eb46cd22860e', N'友情链接', N'FriendUrl', N'友情链接模块，友情链接管理', 4, 1)
GO
INSERT [dbo].[sys_dicCatalog] ([gkey], [sName], [sCode], [sRemark], [iSort], [iFlag]) VALUES (N'b0d95fc3-d762-40e0-9821-ff422fee955b', N'系统通知类型', N'NotifyMsgCode', N'系统通知/消息类型', 3, 1)
GO
INSERT [dbo].[sys_dicInfo] ([gkey], [gDicCatalogKey], [sCode], [sName], [sValue], [sRemark], [iSort], [iFlag]) VALUES (N'351d3619-fe10-410a-bd05-06a74e294c70', N'965d2221-7f0e-435a-86e4-eb46cd22860e', N'FriendUrl', N'百度', N'https://www.baidu.com/', N'百度', 1, 1)
GO
INSERT [dbo].[sys_dicInfo] ([gkey], [gDicCatalogKey], [sCode], [sName], [sValue], [sRemark], [iSort], [iFlag]) VALUES (N'77ba5b3d-8d9e-4f01-8c96-2165626fd117', N'b0d95fc3-d762-40e0-9821-ff422fee955b', N'NotifyMsgCode', N'系统通知', N'1', NULL, 1, 1)
GO
INSERT [dbo].[sys_dicInfo] ([gkey], [gDicCatalogKey], [sCode], [sName], [sValue], [sRemark], [iSort], [iFlag]) VALUES (N'8d652803-e899-4f99-91f0-2ccb3628286a', N'b0d95fc3-d762-40e0-9821-ff422fee955b', N'NotifyMsgCode', N'非系统通知', N'2', NULL, 2, 1)
GO
INSERT [dbo].[sys_dicInfo] ([gkey], [gDicCatalogKey], [sCode], [sName], [sValue], [sRemark], [iSort], [iFlag]) VALUES (N'50e63772-2c1d-413d-b1cf-2f54f71e6b4d', N'fdda77cf-e1d6-43a0-9124-709a5bf5e1b3', N'messageCode', N'小说', N'1', NULL, 1, 1)
GO
INSERT [dbo].[sys_dicInfo] ([gkey], [gDicCatalogKey], [sCode], [sName], [sValue], [sRemark], [iSort], [iFlag]) VALUES (N'5e542c0f-46c1-4835-85b1-7ef49a13a567', N'b06945fe-77d0-4d5b-acdf-71ec90ff7232', N'sexCode', N'未知', N'9', N'停停停', 9, 1)
GO
INSERT [dbo].[sys_dicInfo] ([gkey], [gDicCatalogKey], [sCode], [sName], [sValue], [sRemark], [iSort], [iFlag]) VALUES (N'd9e427fe-bf22-4af8-b809-96807f5910ec', N'b06945fe-77d0-4d5b-acdf-71ec90ff7232', N'sexCode', N'男', N'1', NULL, 1, 1)
GO
INSERT [dbo].[sys_dicInfo] ([gkey], [gDicCatalogKey], [sCode], [sName], [sValue], [sRemark], [iSort], [iFlag]) VALUES (N'37d03a30-b210-4252-8b99-f6ab45716b19', N'fdda77cf-e1d6-43a0-9124-709a5bf5e1b3', N'messageCode', N'网站建议', N'9', NULL, 2, 1)
GO
INSERT [dbo].[sys_dicInfo] ([gkey], [gDicCatalogKey], [sCode], [sName], [sValue], [sRemark], [iSort], [iFlag]) VALUES (N'62df9e12-cc5e-4584-8944-feff7e388073', N'b06945fe-77d0-4d5b-acdf-71ec90ff7232', N'sexCode', N'女', N'2', NULL, 2, 1)
GO
INSERT [dbo].[sys_menu] ([gKey], [sIcon], [sHref], [sBtnId], [gParentKey], [iType], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate], [sName]) VALUES (N'7cdc825e-c8ac-4a08-802c-03be2f50b6d1', NULL, N'/Ebook/Novel/Index', N'', N'958b0517-3bd3-49b6-bf31-1eea7f156423', 2, N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-03-20 00:00:00.000' AS DateTime), NULL, NULL, N'小说列表')
GO
INSERT [dbo].[sys_menu] ([gKey], [sIcon], [sHref], [sBtnId], [gParentKey], [iType], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate], [sName]) VALUES (N'5ef63130-6b3f-48cf-af98-04a7ba8de2e5', NULL, N'/System/Role/Index', N'', N'15f4a6d0-f464-4451-a1da-1616de2657ea', 2, N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-03-21 00:00:00.000' AS DateTime), NULL, NULL, N'角色权限')
GO
INSERT [dbo].[sys_menu] ([gKey], [sIcon], [sHref], [sBtnId], [gParentKey], [iType], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate], [sName]) VALUES (N'299851fd-4e84-4b80-ad62-0ea04e723c38', NULL, N'/System/Menu/EditMenu', N'btnEdit', N'05589d85-3925-4b8b-a0d9-1c4d7ac752e0', 3, N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-03-21 00:00:00.000' AS DateTime), NULL, NULL, N'修改')
GO
INSERT [dbo].[sys_menu] ([gKey], [sIcon], [sHref], [sBtnId], [gParentKey], [iType], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate], [sName]) VALUES (N'57d93a69-389d-47e9-8cc4-12bf3bbcf504', NULL, N'/EBool/Novel/DeleteNover', N'BtnDel', N'7cdc825e-c8ac-4a08-802c-03be2f50b6d1', 3, N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-03-21 00:00:00.000' AS DateTime), NULL, NULL, N'删除')
GO
INSERT [dbo].[sys_menu] ([gKey], [sIcon], [sHref], [sBtnId], [gParentKey], [iType], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate], [sName]) VALUES (N'15f4a6d0-f464-4451-a1da-1616de2657ea', NULL, N'', N'', N'00000000-0000-0000-0000-000000000000', 1, N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-03-20 00:00:00.000' AS DateTime), NULL, NULL, N'系统设置')
GO
INSERT [dbo].[sys_menu] ([gKey], [sIcon], [sHref], [sBtnId], [gParentKey], [iType], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate], [sName]) VALUES (N'49baced3-0540-4a4e-a9c2-19cc79565c51', NULL, N'/Barrages/Barrage/Index', N'', N'3a40c281-ed9b-4e63-ac88-a9b52fcef8b4', 2, N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-05 00:00:00.000' AS DateTime), NULL, NULL, N'弹幕查看')
GO
INSERT [dbo].[sys_menu] ([gKey], [sIcon], [sHref], [sBtnId], [gParentKey], [iType], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate], [sName]) VALUES (N'05589d85-3925-4b8b-a0d9-1c4d7ac752e0', NULL, N'/System/Menu/Index', N'', N'15f4a6d0-f464-4451-a1da-1616de2657ea', 2, NULL, NULL, NULL, NULL, N'菜单配置')
GO
INSERT [dbo].[sys_menu] ([gKey], [sIcon], [sHref], [sBtnId], [gParentKey], [iType], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate], [sName]) VALUES (N'9cb01caf-912e-484a-8874-1c546cc64e8e', NULL, N'/EBook/Collection/addCollection', N'BtnConfig', N'7cdc825e-c8ac-4a08-802c-03be2f50b6d1', 3, N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-03-21 00:00:00.000' AS DateTime), NULL, NULL, N'配置采集规则')
GO
INSERT [dbo].[sys_menu] ([gKey], [sIcon], [sHref], [sBtnId], [gParentKey], [iType], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate], [sName]) VALUES (N'958b0517-3bd3-49b6-bf31-1eea7f156423', NULL, N'', N'', N'00000000-0000-0000-0000-000000000000', 1, N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-03-20 00:00:00.000' AS DateTime), NULL, NULL, N'小说模块')
GO
INSERT [dbo].[sys_menu] ([gKey], [sIcon], [sHref], [sBtnId], [gParentKey], [iType], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate], [sName]) VALUES (N'4f32a8b1-264d-47c2-8112-237b8a9faa48', NULL, N'/System/Novel/EditNover', N'BtnEdit', N'7cdc825e-c8ac-4a08-802c-03be2f50b6d1', 3, N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-03-21 00:00:00.000' AS DateTime), NULL, NULL, N'修改')
GO
INSERT [dbo].[sys_menu] ([gKey], [sIcon], [sHref], [sBtnId], [gParentKey], [iType], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate], [sName]) VALUES (N'2eea0f19-8ca1-4ee7-9d09-2aeafe750ace', NULL, N'/System/UserInfo/ChangeUserType', N'btnAccount', N'3b75f1e3-2a5e-4a00-89ed-82db13a072d1', 3, N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-03-26 00:00:00.000' AS DateTime), NULL, NULL, N'封停/解封')
GO
INSERT [dbo].[sys_menu] ([gKey], [sIcon], [sHref], [sBtnId], [gParentKey], [iType], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate], [sName]) VALUES (N'4acdde35-196b-4555-b7f2-2d4ed95a3b46', NULL, N'/System/Sys/ParameterIndex', N'', N'15f4a6d0-f464-4451-a1da-1616de2657ea', 2, N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-05-16 00:00:00.000' AS DateTime), NULL, NULL, N'系统参数')
GO
INSERT [dbo].[sys_menu] ([gKey], [sIcon], [sHref], [sBtnId], [gParentKey], [iType], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate], [sName]) VALUES (N'da49f22f-368f-4983-b732-494cc52a40f6', NULL, N'/EBook/Novel/AddNovel', N'BtnAdd', N'7cdc825e-c8ac-4a08-802c-03be2f50b6d1', 3, N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-03-21 00:00:00.000' AS DateTime), NULL, NULL, N'添加')
GO
INSERT [dbo].[sys_menu] ([gKey], [sIcon], [sHref], [sBtnId], [gParentKey], [iType], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate], [sName]) VALUES (N'c5c5d6a9-a110-4708-a8af-50a4abc83240', NULL, N'/System/menu/AddMenu', N'btnAdd', N'05589d85-3925-4b8b-a0d9-1c4d7ac752e0', 3, NULL, NULL, NULL, NULL, N'添加')
GO
INSERT [dbo].[sys_menu] ([gKey], [sIcon], [sHref], [sBtnId], [gParentKey], [iType], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate], [sName]) VALUES (N'538ddea9-0c3c-4467-9861-51cb83ef9362', NULL, N'/Barrages/Barrage/KeyWordView', N'', N'3a40c281-ed9b-4e63-ac88-a9b52fcef8b4', 2, NULL, NULL, NULL, NULL, N'每日关键字')
GO
INSERT [dbo].[sys_menu] ([gKey], [sIcon], [sHref], [sBtnId], [gParentKey], [iType], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate], [sName]) VALUES (N'e3e79c4f-227a-4cca-a8a1-5bcb2c4735ef', NULL, N'/System/Role/ConfigRole', N'btnConfig', N'5ef63130-6b3f-48cf-af98-04a7ba8de2e5', 3, NULL, NULL, NULL, NULL, N'配置权限')
GO
INSERT [dbo].[sys_menu] ([gKey], [sIcon], [sHref], [sBtnId], [gParentKey], [iType], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate], [sName]) VALUES (N'fadd37cf-0d17-4f0d-9b94-79bd3f6e309d', NULL, N'/System/Role/AddRole', N'btnAdd', N'5ef63130-6b3f-48cf-af98-04a7ba8de2e5', 3, N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-03-21 00:00:00.000' AS DateTime), NULL, NULL, N'添加')
GO
INSERT [dbo].[sys_menu] ([gKey], [sIcon], [sHref], [sBtnId], [gParentKey], [iType], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate], [sName]) VALUES (N'3b75f1e3-2a5e-4a00-89ed-82db13a072d1', NULL, N'/System/UserInfo/Index', N'', N'15f4a6d0-f464-4451-a1da-1616de2657ea', 2, N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-03-26 00:00:00.000' AS DateTime), NULL, NULL, N'用户管理')
GO
INSERT [dbo].[sys_menu] ([gKey], [sIcon], [sHref], [sBtnId], [gParentKey], [iType], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate], [sName]) VALUES (N'818f3237-d2ce-4979-b366-9387ef629c16', NULL, N'/System/Sys/SendNotifyMsgView', N'', N'15f4a6d0-f464-4451-a1da-1616de2657ea', 2, N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-06-13 00:00:00.000' AS DateTime), NULL, NULL, N'发送通知')
GO
INSERT [dbo].[sys_menu] ([gKey], [sIcon], [sHref], [sBtnId], [gParentKey], [iType], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate], [sName]) VALUES (N'c24989ce-97ae-4f00-af1d-9ccf5486170d', NULL, N'/System/Sys/NoticeView', N'', N'15f4a6d0-f464-4451-a1da-1616de2657ea', 2, N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-16 00:00:00.000' AS DateTime), NULL, NULL, N'公告管理')
GO
INSERT [dbo].[sys_menu] ([gKey], [sIcon], [sHref], [sBtnId], [gParentKey], [iType], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate], [sName]) VALUES (N'c6f34594-e8f4-47c3-b5f8-a7374ad3707e', NULL, N'/System/Role/DelRole', N'btnDel', N'5ef63130-6b3f-48cf-af98-04a7ba8de2e5', 3, N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-03-21 00:00:00.000' AS DateTime), NULL, NULL, N'删除')
GO
INSERT [dbo].[sys_menu] ([gKey], [sIcon], [sHref], [sBtnId], [gParentKey], [iType], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate], [sName]) VALUES (N'3a40c281-ed9b-4e63-ac88-a9b52fcef8b4', NULL, N'', N'', N'00000000-0000-0000-0000-000000000000', 1, N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-05 00:00:00.000' AS DateTime), NULL, NULL, N'弹幕管理')
GO
INSERT [dbo].[sys_menu] ([gKey], [sIcon], [sHref], [sBtnId], [gParentKey], [iType], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate], [sName]) VALUES (N'27a18466-3607-4a51-99ed-b52efd1f8e01', NULL, N'/System/UserInfo/ConfigRole', N'btnConfig', N'3b75f1e3-2a5e-4a00-89ed-82db13a072d1', 3, N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-03-28 00:00:00.000' AS DateTime), NULL, NULL, N'配置权限')
GO
INSERT [dbo].[sys_menu] ([gKey], [sIcon], [sHref], [sBtnId], [gParentKey], [iType], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate], [sName]) VALUES (N'5ac6e67c-e189-4784-96ca-be4ff35ebe38', NULL, N'/System/Sys/TimerIndex', N'', N'15f4a6d0-f464-4451-a1da-1616de2657ea', 2, NULL, NULL, NULL, NULL, N'定时器列表')
GO
INSERT [dbo].[sys_menu] ([gKey], [sIcon], [sHref], [sBtnId], [gParentKey], [iType], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate], [sName]) VALUES (N'582ea173-75fd-4b7f-8cd9-dc8bda9a694e', NULL, N'/System/Menu/DelMenu', N'btnDel', N'05589d85-3925-4b8b-a0d9-1c4d7ac752e0', 3, N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-03-21 00:00:00.000' AS DateTime), NULL, NULL, N'删除')
GO
INSERT [dbo].[sys_menu] ([gKey], [sIcon], [sHref], [sBtnId], [gParentKey], [iType], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate], [sName]) VALUES (N'c7c041f2-4afb-459a-ae1d-e21971cc83e1', NULL, N'/System/Dic/Index', N'', N'15f4a6d0-f464-4451-a1da-1616de2657ea', 2, N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-05-14 00:00:00.000' AS DateTime), NULL, NULL, N'数据字典')
GO
INSERT [dbo].[sys_menu] ([gKey], [sIcon], [sHref], [sBtnId], [gParentKey], [iType], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate], [sName]) VALUES (N'e0ee02d1-91bc-4572-a925-e654110a1376', NULL, N'/System/Role/EditRole', N'btnEdit', N'5ef63130-6b3f-48cf-af98-04a7ba8de2e5', 3, N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-03-21 00:00:00.000' AS DateTime), NULL, NULL, N'修改')
GO
INSERT [dbo].[sys_menu] ([gKey], [sIcon], [sHref], [sBtnId], [gParentKey], [iType], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate], [sName]) VALUES (N'4390e935-c8aa-48a7-ae5a-f766f2d51ee2', NULL, N'/System/Sys/MessageBoardIndex', N'', N'15f4a6d0-f464-4451-a1da-1616de2657ea', 2, N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-05-18 00:00:00.000' AS DateTime), NULL, NULL, N'留言列表')
GO
INSERT [dbo].[sys_messageBoard] ([gkey], [sMessage], [sReply], [gInsertKey], [dInsert], [gReplyKey], [dReply], [iType], [iFlag]) VALUES (N'2446f703-e84b-4114-a8ef-1d509fb92d76', N'测试', NULL, N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-06-05 15:14:13.290' AS DateTime), NULL, NULL, 9, 1)
GO
INSERT [dbo].[sys_messageBoard] ([gkey], [sMessage], [sReply], [gInsertKey], [dInsert], [gReplyKey], [dReply], [iType], [iFlag]) VALUES (N'433c8cb9-b44f-4cf6-9ad9-1ffd4116c40e', N'恕我直言', NULL, N'00000000-0000-0000-0000-000000000000', CAST(N'2018-11-16 19:38:33.257' AS DateTime), NULL, NULL, 1, 1)
GO
INSERT [dbo].[sys_messageBoard] ([gkey], [sMessage], [sReply], [gInsertKey], [dInsert], [gReplyKey], [dReply], [iType], [iFlag]) VALUES (N'd32d3426-460b-4dfc-8d29-2948f81bafc0', N'来了来了', NULL, N'cad6ca3b-e537-4f84-b959-e1f61ced9d79', CAST(N'2018-11-05 17:34:04.517' AS DateTime), NULL, NULL, 9, 1)
GO
INSERT [dbo].[sys_messageBoard] ([gkey], [sMessage], [sReply], [gInsertKey], [dInsert], [gReplyKey], [dReply], [iType], [iFlag]) VALUES (N'c8a46e78-43d6-46c3-8b9e-374ad4550204', N'测试', NULL, N'00000000-0000-0000-0000-000000000000', CAST(N'2018-05-17 15:28:46.610' AS DateTime), NULL, NULL, 1, 0)
GO
INSERT [dbo].[sys_messageBoard] ([gkey], [sMessage], [sReply], [gInsertKey], [dInsert], [gReplyKey], [dReply], [iType], [iFlag]) VALUES (N'e974a90a-5fcc-4ede-ba93-49d5b17b7bca', N'123123', NULL, N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-05-18 16:11:54.323' AS DateTime), NULL, NULL, 1, 0)
GO

INSERT [dbo].[sys_messageBoard] ([gkey], [sMessage], [sReply], [gInsertKey], [dInsert], [gReplyKey], [dReply], [iType], [iFlag]) VALUES (N'dce32736-4e2e-4432-be3f-78fa88b49a90', N'优化网站', NULL, N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-05-18 11:56:14.100' AS DateTime), NULL, NULL, 9, 0)
GO
INSERT [dbo].[sys_messageBoard] ([gkey], [sMessage], [sReply], [gInsertKey], [dInsert], [gReplyKey], [dReply], [iType], [iFlag]) VALUES (N'a1701183-25a9-4083-9ef1-b5db73cd682e', N'风格不错，小说较少，希望多多添加', N'okok,没问题', N'00000000-0000-0000-0000-000000000000', CAST(N'2018-11-08 14:25:09.203' AS DateTime), N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-09 11:55:30.187' AS DateTime), 9, 1)
GO
INSERT [dbo].[sys_messageBoard] ([gkey], [sMessage], [sReply], [gInsertKey], [dInsert], [gReplyKey], [dReply], [iType], [iFlag]) VALUES (N'0e07b02f-df53-4a20-82b2-b8fe86eb4bc8', N'测试asddddda212菜单且我的大叔的萨达是的的重新注册瑟瑟发抖三费fadsf56a4f6as4df5sdfasda4fsdf', N'啊实打实的为其全文的', N'00000000-0000-0000-0000-000000000000', CAST(N'2018-05-17 16:04:00.430' AS DateTime), N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-05-18 17:11:37.437' AS DateTime), 1, 0)
GO
INSERT [dbo].[sys_messageBoard] ([gkey], [sMessage], [sReply], [gInsertKey], [dInsert], [gReplyKey], [dReply], [iType], [iFlag]) VALUES (N'ec0f8ff8-cfc1-494f-b971-d246bf29b55b', N'https://blog.csdn.net/chengqiuming/article/details/70139323', NULL, N'00000000-0000-0000-0000-000000000000', CAST(N'2018-07-17 14:50:23.157' AS DateTime), NULL, NULL, 9, 0)
GO
INSERT [dbo].[sys_messageBoard] ([gkey], [sMessage], [sReply], [gInsertKey], [dInsert], [gReplyKey], [dReply], [iType], [iFlag]) VALUES (N'1f242be9-4b45-41e5-b114-d44e5c981e37', N'欢迎大家提出修改意见', NULL, N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-06-01 14:36:20.537' AS DateTime), NULL, NULL, 9, 1)
GO
INSERT [dbo].[sys_messageBoard] ([gkey], [sMessage], [sReply], [gInsertKey], [dInsert], [gReplyKey], [dReply], [iType], [iFlag]) VALUES (N'17a4188c-a576-488f-ae7d-ede3a175c41f', N'我要是最帅的', NULL, N'e895a672-0a2a-4785-babe-b052d10b7006', CAST(N'2018-05-19 19:26:54.317' AS DateTime), NULL, NULL, 9, 0)
GO
INSERT [dbo].[sys_messageBoard] ([gkey], [sMessage], [sReply], [gInsertKey], [dInsert], [gReplyKey], [dReply], [iType], [iFlag]) VALUES (N'368cb065-6dd0-4d44-84fc-ef62933e1530', N'test2', NULL, N'a97f301b-2c8c-4209-a02a-62396cb74cec', CAST(N'2018-11-05 17:22:30.077' AS DateTime), NULL, NULL, 1, 1)
GO
INSERT [dbo].[sys_messageBoard] ([gkey], [sMessage], [sReply], [gInsertKey], [dInsert], [gReplyKey], [dReply], [iType], [iFlag]) VALUES (N'735657db-069f-4af1-a2c2-f4896aa28089', N'哇卡哇卡', NULL, N'e44446fd-efad-4c28-9341-deb84acc03b1', CAST(N'2018-06-13 16:21:25.587' AS DateTime), NULL, NULL, 1, 1)
GO
INSERT [dbo].[sys_Notice] ([gkey], [sContent], [dCreateTime], [gCreateId], [iFlag]) VALUES (N'87f3660f-d67d-4c80-9dff-54f1cc2a8618', N'<p>&nbsp; &nbsp; &nbsp;添加背景特效，小说添加自动获取最新章节定时任务</p><p><br></p><p style="text-align: right;"><br></p><p style="text-align: right;"><b><i>lhj0502@vip.qq.com&nbsp; &nbsp;&nbsp;</i></b></p><p style="text-align: right;"><b><i>2018-12-05&nbsp; &nbsp; &nbsp;</i></b></p><p style="text-align: right;"><b>.net基础交流群：</b><i>864337791&nbsp; &nbsp;</i></p>', CAST(N'2018-12-05 14:43:14.047' AS DateTime), N'a4408581-85c9-4117-b3d9-dfffdd72fccf', 1)
GO
INSERT [dbo].[sys_Notice] ([gkey], [sContent], [dCreateTime], [gCreateId], [iFlag]) VALUES (N'f738839b-de81-476c-903c-f738b0f09c68', N'<p><br></p><p>&nbsp; &nbsp; 系统小说页面全部启用静态化，页面请求优化，减少请求次数，提高响应速度！！！&nbsp;</p><p><br></p><p style="text-align: right;"><br></p><p style="text-align: right;"><b><i>lhj0502@vip.qq.com&nbsp; &nbsp;&nbsp;</i></b></p><p style="text-align: right;"><b><i>2018-11-16&nbsp; &nbsp; &nbsp;</i></b></p><p style="text-align: right;"><b>.net基础交流群：</b><i style="font-weight: bold;">864337791&nbsp;&nbsp;&nbsp;</i></p>', CAST(N'2018-11-16 17:14:50.430' AS DateTime), N'a4408581-85c9-4117-b3d9-dfffdd72fccf', 1)
GO
INSERT [dbo].[sys_notifyMessage] ([gkey], [gMsgKey], [gSendKey], [sSendContent], [dSendDate], [gReceiveKey], [dReadDate], [iFlag], [sSendTitle], [sHref], [iType]) VALUES (N'b2ebec65-508b-4a82-93e3-0035f719ed4a', N'3f404d0a-26c6-4556-a1b0-19cc9791f02f', N'00000000-0000-0000-0000-000000000000', N'<p><img src="/Images/layedit/201811050524362436.gif" alt="undefined">&nbsp; &nbsp;SDDE-398</p><div><br></div>', CAST(N'2018-11-05 17:25:33.547' AS DateTime), N'c5f9f003-aff6-4a81-891b-9db3d7fba1c7', NULL, 1, N'【系统通知】:神秘代码', N'', 1)
GO
INSERT [dbo].[sys_notifyMessage] ([gkey], [gMsgKey], [gSendKey], [sSendContent], [dSendDate], [gReceiveKey], [dReadDate], [iFlag], [sSendTitle], [sHref], [iType]) VALUES (N'aa694a9b-0ef1-4da8-89c8-0191b94ab3e3', N'e3c1da52-0d15-4d0f-bae1-92748ea91506', N'00000000-0000-0000-0000-000000000000', N'<p>上线漫画《迷域行者》 一部非常好的漫画，推理，烧脑，精彩</p>', CAST(N'2018-09-01 15:30:02.797' AS DateTime), N'e44446fd-efad-4c28-9341-deb84acc03b1', NULL, 1, N'【系统通知】:上线漫画《迷域行者》', N'', 1)
GO
INSERT [dbo].[sys_notifyMessage] ([gkey], [gMsgKey], [gSendKey], [sSendContent], [dSendDate], [gReceiveKey], [dReadDate], [iFlag], [sSendTitle], [sHref], [iType]) VALUES (N'd6666326-b264-4e81-a675-0898006fea58', N'ec18fab4-1457-47fa-b9b7-27b03aa5ae5a', N'00000000-0000-0000-0000-000000000000', N'<p align="center"><img width="136" height="37" style="width: 204px; height: 57px;" alt="undefined" src="/Images/layedit/20180613040751751.png"><div align="left">&nbsp;</div></p><p align="left">&nbsp;&nbsp;&nbsp; 本次更新了 上传头像，修改资料，消息系统等。。欢迎大家踊跃使用！！！</p><p align="left">&nbsp;</p>', CAST(N'2018-06-13 16:09:33.570' AS DateTime), N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-06-22 17:29:50.143' AS DateTime), 0, N'【系统通知】:新版本版本上线', N'', 1)
GO
INSERT [dbo].[sys_notifyMessage] ([gkey], [gMsgKey], [gSendKey], [sSendContent], [dSendDate], [gReceiveKey], [dReadDate], [iFlag], [sSendTitle], [sHref], [iType]) VALUES (N'86f0d3a0-1345-4a30-a16e-0be69b2b1e80', N'ec18fab4-1457-47fa-b9b7-27b03aa5ae5a', N'00000000-0000-0000-0000-000000000000', N'<p align="center"><img width="136" height="37" style="width: 204px; height: 57px;" alt="undefined" src="/Images/layedit/20180613040751751.png"><div align="left">&nbsp;</div></p><p align="left">&nbsp;&nbsp;&nbsp; 本次更新了 上传头像，修改资料，消息系统等。。欢迎大家踊跃使用！！！</p><p align="left">&nbsp;</p>', CAST(N'2018-06-13 16:09:33.570' AS DateTime), N'3a7742a0-94ed-4c47-817c-5ce4a7da17a1', NULL, 1, N'【系统通知】:新版本版本上线', N'', 1)
GO
INSERT [dbo].[sys_notifyMessage] ([gkey], [gMsgKey], [gSendKey], [sSendContent], [dSendDate], [gReceiveKey], [dReadDate], [iFlag], [sSendTitle], [sHref], [iType]) VALUES (N'97507dee-d603-46c7-8301-0f95c8c65cda', N'f5989823-6449-4c23-93b3-e9c45677d1ca', N'00000000-0000-0000-0000-000000000000', N'欢迎来到唐突世界，有任何问题可以直接联系管理员!!!', CAST(N'2018-06-13 16:11:27.947' AS DateTime), N'd37d1f3b-e4d6-4287-a976-109dc38c279c', NULL, 1, N'【系统通知】:欢迎注册', N'', 1)
GO
INSERT [dbo].[sys_notifyMessage] ([gkey], [gMsgKey], [gSendKey], [sSendContent], [dSendDate], [gReceiveKey], [dReadDate], [iFlag], [sSendTitle], [sHref], [iType]) VALUES (N'9ca6fb25-a233-465b-b505-176680e068ec', N'3f404d0a-26c6-4556-a1b0-19cc9791f02f', N'00000000-0000-0000-0000-000000000000', N'<p><img src="/Images/layedit/201811050524362436.gif" alt="undefined">&nbsp; &nbsp;SDDE-398</p><div><br></div>', CAST(N'2018-11-05 17:25:33.547' AS DateTime), N'e44446fd-efad-4c28-9341-deb84acc03b1', NULL, 1, N'【系统通知】:神秘代码', N'', 1)
GO
INSERT [dbo].[sys_notifyMessage] ([gkey], [gMsgKey], [gSendKey], [sSendContent], [dSendDate], [gReceiveKey], [dReadDate], [iFlag], [sSendTitle], [sHref], [iType]) VALUES (N'21e1aaf7-36a3-41dd-96f9-186dba292171', N'ab7e45cc-6249-434e-a539-5f7061be1382', N'00000000-0000-0000-0000-000000000000', N'欢迎来到唐突世界，有任何问题可以直接联系管理员!!!', CAST(N'2018-08-09 15:38:08.113' AS DateTime), N'c5f9f003-aff6-4a81-891b-9db3d7fba1c7', NULL, 1, N'【系统通知】:欢迎注册', N'', 1)
GO
INSERT [dbo].[sys_notifyMessage] ([gkey], [gMsgKey], [gSendKey], [sSendContent], [dSendDate], [gReceiveKey], [dReadDate], [iFlag], [sSendTitle], [sHref], [iType]) VALUES (N'3bc4eec0-e499-498b-89ca-209c37b3a4a8', N'3f404d0a-26c6-4556-a1b0-19cc9791f02f', N'00000000-0000-0000-0000-000000000000', N'<p><img src="/Images/layedit/201811050524362436.gif" alt="undefined">&nbsp; &nbsp;SDDE-398</p><div><br></div>', CAST(N'2018-11-05 17:25:33.547' AS DateTime), N'97233a76-2d79-43a5-894c-db6e5c0603df', NULL, 1, N'【系统通知】:神秘代码', N'', 1)
GO
INSERT [dbo].[sys_notifyMessage] ([gkey], [gMsgKey], [gSendKey], [sSendContent], [dSendDate], [gReceiveKey], [dReadDate], [iFlag], [sSendTitle], [sHref], [iType]) VALUES (N'd49b89d8-16cb-4def-ae76-299b0503aaa5', N'e3c1da52-0d15-4d0f-bae1-92748ea91506', N'00000000-0000-0000-0000-000000000000', N'<p>上线漫画《迷域行者》 一部非常好的漫画，推理，烧脑，精彩</p>', CAST(N'2018-09-01 15:30:02.797' AS DateTime), N'3a7742a0-94ed-4c47-817c-5ce4a7da17a1', NULL, 1, N'【系统通知】:上线漫画《迷域行者》', N'', 1)
GO
INSERT [dbo].[sys_notifyMessage] ([gkey], [gMsgKey], [gSendKey], [sSendContent], [dSendDate], [gReceiveKey], [dReadDate], [iFlag], [sSendTitle], [sHref], [iType]) VALUES (N'47d094c0-cc80-41ff-afbb-2c0be898f118', N'3f404d0a-26c6-4556-a1b0-19cc9791f02f', N'00000000-0000-0000-0000-000000000000', N'<p><img src="/Images/layedit/201811050524362436.gif" alt="undefined">&nbsp; &nbsp;SDDE-398</p><div><br></div>', CAST(N'2018-11-05 17:25:33.547' AS DateTime), N'a97f301b-2c8c-4209-a02a-62396cb74cec', CAST(N'2018-11-05 17:45:24.250' AS DateTime), 0, N'【系统通知】:神秘代码', N'', 1)
GO
INSERT [dbo].[sys_notifyMessage] ([gkey], [gMsgKey], [gSendKey], [sSendContent], [dSendDate], [gReceiveKey], [dReadDate], [iFlag], [sSendTitle], [sHref], [iType]) VALUES (N'3f6a33eb-91a3-4316-b91e-2d91f1d779b2', N'ec18fab4-1457-47fa-b9b7-27b03aa5ae5a', N'00000000-0000-0000-0000-000000000000', N'<p align="center"><img width="136" height="37" style="width: 204px; height: 57px;" alt="undefined" src="/Images/layedit/20180613040751751.png"><div align="left">&nbsp;</div></p><p align="left">&nbsp;&nbsp;&nbsp; 本次更新了 上传头像，修改资料，消息系统等。。欢迎大家踊跃使用！！！</p><p align="left">&nbsp;</p>', CAST(N'2018-06-13 16:09:33.570' AS DateTime), N'e44446fd-efad-4c28-9341-deb84acc03b1', CAST(N'2018-06-13 16:18:02.987' AS DateTime), 0, N'【系统通知】:新版本版本上线', N'', 1)
GO
INSERT [dbo].[sys_notifyMessage] ([gkey], [gMsgKey], [gSendKey], [sSendContent], [dSendDate], [gReceiveKey], [dReadDate], [iFlag], [sSendTitle], [sHref], [iType]) VALUES (N'942bccaf-88db-4af2-a7fe-3bf1884da4ff', N'3f404d0a-26c6-4556-a1b0-19cc9791f02f', N'00000000-0000-0000-0000-000000000000', N'<p><img src="/Images/layedit/201811050524362436.gif" alt="undefined">&nbsp; &nbsp;SDDE-398</p><div><br></div>', CAST(N'2018-11-05 17:25:33.547' AS DateTime), N'e895a672-0a2a-4785-babe-b052d10b7006', NULL, 1, N'【系统通知】:神秘代码', N'', 1)
GO
INSERT [dbo].[sys_notifyMessage] ([gkey], [gMsgKey], [gSendKey], [sSendContent], [dSendDate], [gReceiveKey], [dReadDate], [iFlag], [sSendTitle], [sHref], [iType]) VALUES (N'4b6605d5-6d95-4f69-b646-3d8149567a18', N'e3c1da52-0d15-4d0f-bae1-92748ea91506', N'00000000-0000-0000-0000-000000000000', N'<p>上线漫画《迷域行者》 一部非常好的漫画，推理，烧脑，精彩</p>', CAST(N'2018-09-01 15:30:02.797' AS DateTime), N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-10-22 22:49:58.100' AS DateTime), 0, N'【系统通知】:上线漫画《迷域行者》', N'', 1)
GO
INSERT [dbo].[sys_notifyMessage] ([gkey], [gMsgKey], [gSendKey], [sSendContent], [dSendDate], [gReceiveKey], [dReadDate], [iFlag], [sSendTitle], [sHref], [iType]) VALUES (N'64b739e2-8bee-43b2-abfe-419dbae82c76', N'e3c1da52-0d15-4d0f-bae1-92748ea91506', N'00000000-0000-0000-0000-000000000000', N'<p>上线漫画《迷域行者》 一部非常好的漫画，推理，烧脑，精彩</p>', CAST(N'2018-09-01 15:30:02.797' AS DateTime), N'97233a76-2d79-43a5-894c-db6e5c0603df', NULL, 1, N'【系统通知】:上线漫画《迷域行者》', N'', 1)
GO
INSERT [dbo].[sys_notifyMessage] ([gkey], [gMsgKey], [gSendKey], [sSendContent], [dSendDate], [gReceiveKey], [dReadDate], [iFlag], [sSendTitle], [sHref], [iType]) VALUES (N'c5a446df-ad8c-4fe7-991f-5109029f8727', N'3f404d0a-26c6-4556-a1b0-19cc9791f02f', N'00000000-0000-0000-0000-000000000000', N'<p><img src="/Images/layedit/201811050524362436.gif" alt="undefined">&nbsp; &nbsp;SDDE-398</p><div><br></div>', CAST(N'2018-11-05 17:25:33.547' AS DateTime), N'96014d19-eb76-4c61-aa9d-417f58df78ea', NULL, 1, N'【系统通知】:神秘代码', N'', 1)
GO
INSERT [dbo].[sys_notifyMessage] ([gkey], [gMsgKey], [gSendKey], [sSendContent], [dSendDate], [gReceiveKey], [dReadDate], [iFlag], [sSendTitle], [sHref], [iType]) VALUES (N'084a2e27-d627-4bd4-b13b-5879ae14463d', N'e3c1da52-0d15-4d0f-bae1-92748ea91506', N'00000000-0000-0000-0000-000000000000', N'<p>上线漫画《迷域行者》 一部非常好的漫画，推理，烧脑，精彩</p>', CAST(N'2018-09-01 15:30:02.797' AS DateTime), N'e895a672-0a2a-4785-babe-b052d10b7006', NULL, 1, N'【系统通知】:上线漫画《迷域行者》', N'', 1)
GO
INSERT [dbo].[sys_notifyMessage] ([gkey], [gMsgKey], [gSendKey], [sSendContent], [dSendDate], [gReceiveKey], [dReadDate], [iFlag], [sSendTitle], [sHref], [iType]) VALUES (N'012b18da-f17c-4e31-ba12-5a821478c4b7', N'd74ead80-a589-4f43-866f-430766f58972', N'00000000-0000-0000-0000-000000000000', N'欢迎来到唐突世界，有任何问题可以直接联系管理员!!!', CAST(N'2018-11-05 17:33:23.703' AS DateTime), N'cad6ca3b-e537-4f84-b959-e1f61ced9d79', CAST(N'2018-11-13 16:58:35.163' AS DateTime), 0, N'【系统通知】:欢迎注册', N'', 1)
GO
INSERT [dbo].[sys_notifyMessage] ([gkey], [gMsgKey], [gSendKey], [sSendContent], [dSendDate], [gReceiveKey], [dReadDate], [iFlag], [sSendTitle], [sHref], [iType]) VALUES (N'db51020d-0439-4488-830a-78018fe8d1c7', N'5a9d51d4-8938-4b8a-a3ad-0f1dd1b62c45', N'00000000-0000-0000-0000-000000000000', N'欢迎来到唐突世界，有任何问题可以直接联系管理员!!!', CAST(N'2018-11-05 17:21:13.890' AS DateTime), N'0bed9438-c13c-4b5e-81d1-25fd9daa5ea1', CAST(N'2018-11-05 17:24:21.577' AS DateTime), 0, N'【系统通知】:欢迎注册', N'', 1)
GO
INSERT [dbo].[sys_notifyMessage] ([gkey], [gMsgKey], [gSendKey], [sSendContent], [dSendDate], [gReceiveKey], [dReadDate], [iFlag], [sSendTitle], [sHref], [iType]) VALUES (N'266caaf5-7741-4d88-9bec-7cc20a2be2f3', N'e3c1da52-0d15-4d0f-bae1-92748ea91506', N'00000000-0000-0000-0000-000000000000', N'<p>上线漫画《迷域行者》 一部非常好的漫画，推理，烧脑，精彩</p>', CAST(N'2018-09-01 15:30:02.797' AS DateTime), N'd37d1f3b-e4d6-4287-a976-109dc38c279c', NULL, 1, N'【系统通知】:上线漫画《迷域行者》', N'', 1)
GO
INSERT [dbo].[sys_notifyMessage] ([gkey], [gMsgKey], [gSendKey], [sSendContent], [dSendDate], [gReceiveKey], [dReadDate], [iFlag], [sSendTitle], [sHref], [iType]) VALUES (N'2643fce0-1209-4cd5-96d0-801249a566a6', N'e3c1da52-0d15-4d0f-bae1-92748ea91506', N'00000000-0000-0000-0000-000000000000', N'<p>上线漫画《迷域行者》 一部非常好的漫画，推理，烧脑，精彩</p>', CAST(N'2018-09-01 15:30:02.797' AS DateTime), N'96014d19-eb76-4c61-aa9d-417f58df78ea', NULL, 1, N'【系统通知】:上线漫画《迷域行者》', N'', 1)
GO
INSERT [dbo].[sys_notifyMessage] ([gkey], [gMsgKey], [gSendKey], [sSendContent], [dSendDate], [gReceiveKey], [dReadDate], [iFlag], [sSendTitle], [sHref], [iType]) VALUES (N'6b0eb937-687c-4832-adc7-840e365a092d', N'33cfe61f-1329-435f-b157-ccb1875e2265', N'00000000-0000-0000-0000-000000000000', N'欢迎来到唐突世界，有任何问题可以直接联系管理员!!!', CAST(N'2018-11-07 17:33:43.813' AS DateTime), N'5f85a7a4-708e-45ec-8c32-8c6f803235b5', NULL, 1, N'【系统通知】:欢迎注册', N'', 1)
GO
INSERT [dbo].[sys_notifyMessage] ([gkey], [gMsgKey], [gSendKey], [sSendContent], [dSendDate], [gReceiveKey], [dReadDate], [iFlag], [sSendTitle], [sHref], [iType]) VALUES (N'56b70af7-8aaa-4a7b-973b-8c92800c14cf', N'3f404d0a-26c6-4556-a1b0-19cc9791f02f', N'00000000-0000-0000-0000-000000000000', N'<p><img src="/Images/layedit/201811050524362436.gif" alt="undefined">&nbsp; &nbsp;SDDE-398</p><div><br></div>', CAST(N'2018-11-05 17:25:33.547' AS DateTime), N'e6b35da6-5f99-4c75-9a1b-53faf6c5b1be', NULL, 1, N'【系统通知】:神秘代码', N'', 1)
GO
INSERT [dbo].[sys_notifyMessage] ([gkey], [gMsgKey], [gSendKey], [sSendContent], [dSendDate], [gReceiveKey], [dReadDate], [iFlag], [sSendTitle], [sHref], [iType]) VALUES (N'b8e0bffd-7667-4cbb-b0ef-a2c85cfd950a', N'ec18fab4-1457-47fa-b9b7-27b03aa5ae5a', N'00000000-0000-0000-0000-000000000000', N'<p align="center"><img width="136" height="37" style="width: 204px; height: 57px;" alt="undefined" src="/Images/layedit/20180613040751751.png"><div align="left">&nbsp;</div></p><p align="left">&nbsp;&nbsp;&nbsp; 本次更新了 上传头像，修改资料，消息系统等。。欢迎大家踊跃使用！！！</p><p align="left">&nbsp;</p>', CAST(N'2018-06-13 16:09:33.570' AS DateTime), N'e895a672-0a2a-4785-babe-b052d10b7006', NULL, 1, N'【系统通知】:新版本版本上线', N'', 1)
GO
INSERT [dbo].[sys_notifyMessage] ([gkey], [gMsgKey], [gSendKey], [sSendContent], [dSendDate], [gReceiveKey], [dReadDate], [iFlag], [sSendTitle], [sHref], [iType]) VALUES (N'b684ae11-be92-4a92-8b0a-b023f1bff65b', N'3f404d0a-26c6-4556-a1b0-19cc9791f02f', N'00000000-0000-0000-0000-000000000000', N'<p><img src="/Images/layedit/201811050524362436.gif" alt="undefined">&nbsp; &nbsp;SDDE-398</p><div><br></div>', CAST(N'2018-11-05 17:25:33.547' AS DateTime), N'0bed9438-c13c-4b5e-81d1-25fd9daa5ea1', NULL, 1, N'【系统通知】:神秘代码', N'', 1)
GO
INSERT [dbo].[sys_notifyMessage] ([gkey], [gMsgKey], [gSendKey], [sSendContent], [dSendDate], [gReceiveKey], [dReadDate], [iFlag], [sSendTitle], [sHref], [iType]) VALUES (N'125dd063-dfc5-4975-b6fe-b3f13b205641', N'bde6f54d-d086-4280-8246-7b8f8f78ae8d', N'00000000-0000-0000-0000-000000000000', N'欢迎来到唐突世界，有任何问题可以直接联系管理员!!!', CAST(N'2018-10-26 23:52:59.980' AS DateTime), N'e6b35da6-5f99-4c75-9a1b-53faf6c5b1be', NULL, 1, N'【系统通知】:欢迎注册', N'', 1)
GO
INSERT [dbo].[sys_notifyMessage] ([gkey], [gMsgKey], [gSendKey], [sSendContent], [dSendDate], [gReceiveKey], [dReadDate], [iFlag], [sSendTitle], [sHref], [iType]) VALUES (N'461c706d-5e52-4d76-a2c7-be48c0b22776', N'3f404d0a-26c6-4556-a1b0-19cc9791f02f', N'00000000-0000-0000-0000-000000000000', N'<p><img src="/Images/layedit/201811050524362436.gif" alt="undefined">&nbsp; &nbsp;SDDE-398</p><div><br></div>', CAST(N'2018-11-05 17:25:33.547' AS DateTime), N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-05 17:54:26.687' AS DateTime), 0, N'【系统通知】:神秘代码', N'', 1)
GO
INSERT [dbo].[sys_notifyMessage] ([gkey], [gMsgKey], [gSendKey], [sSendContent], [dSendDate], [gReceiveKey], [dReadDate], [iFlag], [sSendTitle], [sHref], [iType]) VALUES (N'815d7813-79e4-4829-a268-c4a7b5ad16d8', N'e3c1da52-0d15-4d0f-bae1-92748ea91506', N'00000000-0000-0000-0000-000000000000', N'<p>上线漫画《迷域行者》 一部非常好的漫画，推理，烧脑，精彩</p>', CAST(N'2018-09-01 15:30:02.797' AS DateTime), N'c5f9f003-aff6-4a81-891b-9db3d7fba1c7', NULL, 1, N'【系统通知】:上线漫画《迷域行者》', N'', 1)
GO
INSERT [dbo].[sys_notifyMessage] ([gkey], [gMsgKey], [gSendKey], [sSendContent], [dSendDate], [gReceiveKey], [dReadDate], [iFlag], [sSendTitle], [sHref], [iType]) VALUES (N'8e958806-c298-4518-a8a0-ce3224354089', N'3f404d0a-26c6-4556-a1b0-19cc9791f02f', N'00000000-0000-0000-0000-000000000000', N'<p><img src="/Images/layedit/201811050524362436.gif" alt="undefined">&nbsp; &nbsp;SDDE-398</p><div><br></div>', CAST(N'2018-11-05 17:25:33.547' AS DateTime), N'3a7742a0-94ed-4c47-817c-5ce4a7da17a1', NULL, 1, N'【系统通知】:神秘代码', N'', 1)
GO
INSERT [dbo].[sys_notifyMessage] ([gkey], [gMsgKey], [gSendKey], [sSendContent], [dSendDate], [gReceiveKey], [dReadDate], [iFlag], [sSendTitle], [sHref], [iType]) VALUES (N'e86a1166-877a-414f-a2ed-cff933ff2b8a', N'97630a32-92e1-4f9b-9e81-10706277c5fe', N'00000000-0000-0000-0000-000000000000', N'欢迎来到唐突世界，有任何问题可以直接联系管理员!!!', CAST(N'2018-11-05 17:21:02.547' AS DateTime), N'a97f301b-2c8c-4209-a02a-62396cb74cec', CAST(N'2018-11-05 17:45:38.313' AS DateTime), 0, N'【系统通知】:欢迎注册', N'', 1)
GO
INSERT [dbo].[sys_notifyMessage] ([gkey], [gMsgKey], [gSendKey], [sSendContent], [dSendDate], [gReceiveKey], [dReadDate], [iFlag], [sSendTitle], [sHref], [iType]) VALUES (N'909b2151-b3f9-48ab-97e9-d51588e0e389', N'8a2e0475-24c9-4a8f-8fa2-1a38cb4ff583', N'00000000-0000-0000-0000-000000000000', N'欢迎来到唐突世界，有任何问题可以直接联系管理员!!!', CAST(N'2018-11-05 17:27:07.860' AS DateTime), N'f598e36b-ebb0-423b-b776-29334876145e', CAST(N'2018-11-05 17:31:55.250' AS DateTime), 0, N'【系统通知】:欢迎注册', N'', 1)
GO
INSERT [dbo].[sys_notifyMessage] ([gkey], [gMsgKey], [gSendKey], [sSendContent], [dSendDate], [gReceiveKey], [dReadDate], [iFlag], [sSendTitle], [sHref], [iType]) VALUES (N'38f6fa67-1073-4ecc-b041-d550d1f0034e', N'3f404d0a-26c6-4556-a1b0-19cc9791f02f', N'00000000-0000-0000-0000-000000000000', N'<p><img src="/Images/layedit/201811050524362436.gif" alt="undefined">&nbsp; &nbsp;SDDE-398</p><div><br></div>', CAST(N'2018-11-05 17:25:33.547' AS DateTime), N'd37d1f3b-e4d6-4287-a976-109dc38c279c', CAST(N'2018-11-05 18:54:26.093' AS DateTime), 0, N'【系统通知】:神秘代码', N'', 1)
GO
INSERT [dbo].[sys_notifyMessage] ([gkey], [gMsgKey], [gSendKey], [sSendContent], [dSendDate], [gReceiveKey], [dReadDate], [iFlag], [sSendTitle], [sHref], [iType]) VALUES (N'a4408581-85c9-4117-b3d9-dfffdd72fcc4', N'a4408581-85c9-4117-b3d9-dfffdd72fcc5', N'a4408581-85c9-4117-b3d9-dfffdd72fcc6', N'<b>啊飒飒的</b>', CAST(N'2018-06-09 00:00:00.000' AS DateTime), N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-06-22 16:37:27.683' AS DateTime), 0, N'系统测试', N'', 1)
GO
INSERT [dbo].[sys_notifyMessage] ([gkey], [gMsgKey], [gSendKey], [sSendContent], [dSendDate], [gReceiveKey], [dReadDate], [iFlag], [sSendTitle], [sHref], [iType]) VALUES (N'da171926-9317-4752-985a-e0d186d0728d', N'f81e264b-cd3e-4d6c-83c7-2a930df9c7f8', N'8f4d174f-ad1c-4a06-b122-33d349825c85', N'你真的太帅了', CAST(N'2018-06-08 08:51:15.000' AS DateTime), N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-06-22 15:16:52.230' AS DateTime), 0, N'系统通知:欢迎注册', N'', 1)
GO
INSERT [dbo].[sys_notifyMessage] ([gkey], [gMsgKey], [gSendKey], [sSendContent], [dSendDate], [gReceiveKey], [dReadDate], [iFlag], [sSendTitle], [sHref], [iType]) VALUES (N'0261232b-5058-42f5-a617-e37fc0e5ad4c', N'0ee1e5f4-caea-453f-99ee-f45db9fe164b', N'00000000-0000-0000-0000-000000000000', N'欢迎来到唐突世界，有任何问题可以直接联系管理员!!!', CAST(N'2018-11-22 17:29:45.007' AS DateTime), N'17cb45cf-7aa8-404b-b176-b30344295b5c', NULL, 1, N'【系统通知】:欢迎注册', N'', 1)
GO
INSERT [dbo].[sys_notifyMessage] ([gkey], [gMsgKey], [gSendKey], [sSendContent], [dSendDate], [gReceiveKey], [dReadDate], [iFlag], [sSendTitle], [sHref], [iType]) VALUES (N'2ab874b1-7291-4fde-be84-f04ab2d16b50', N'ec18fab4-1457-47fa-b9b7-27b03aa5ae5a', N'00000000-0000-0000-0000-000000000000', N'<p align="center"><img width="136" height="37" style="width: 204px; height: 57px;" alt="undefined" src="/Images/layedit/20180613040751751.png"><div align="left">&nbsp;</div></p><p align="left">&nbsp;&nbsp;&nbsp; 本次更新了 上传头像，修改资料，消息系统等。。欢迎大家踊跃使用！！！</p><p align="left">&nbsp;</p>', CAST(N'2018-06-13 16:09:33.570' AS DateTime), N'96014d19-eb76-4c61-aa9d-417f58df78ea', NULL, 1, N'【系统通知】:新版本版本上线', N'', 1)
GO
INSERT [dbo].[sys_notifyMessage] ([gkey], [gMsgKey], [gSendKey], [sSendContent], [dSendDate], [gReceiveKey], [dReadDate], [iFlag], [sSendTitle], [sHref], [iType]) VALUES (N'9815620d-8df7-43ae-9e1a-f52743604bcb', N'ec18fab4-1457-47fa-b9b7-27b03aa5ae5a', N'00000000-0000-0000-0000-000000000000', N'<p align="center"><img width="136" height="37" style="width: 204px; height: 57px;" alt="undefined" src="/Images/layedit/20180613040751751.png"><div align="left">&nbsp;</div></p><p align="left">&nbsp;&nbsp;&nbsp; 本次更新了 上传头像，修改资料，消息系统等。。欢迎大家踊跃使用！！！</p><p align="left">&nbsp;</p>', CAST(N'2018-06-13 16:09:33.570' AS DateTime), N'97233a76-2d79-43a5-894c-db6e5c0603df', NULL, 1, N'【系统通知】:新版本版本上线', N'', 1)
GO
INSERT [dbo].[sys_parameter] ([gkey], [sName], [sCode], [sValue], [sRemark], [iFlag]) VALUES (N'0d81341f-24b1-4f74-a7f4-07b0c5c632d1', N'关键字', N'keywords', N'唐突世界,谁有我低调', N'网站关键字', 1)
GO
INSERT [dbo].[sys_parameter] ([gkey], [sName], [sCode], [sValue], [sRemark], [iFlag]) VALUES (N'4e6453fe-9a06-4265-9899-1b4d7e0bc212', N'删除系统通知消息', N'SysNotifyMsgDelDay', N'7', N'系统自动删除已阅读系统通知消息的间隔天数  值：0表示不删除  单位天', 1)
GO
INSERT [dbo].[sys_parameter] ([gkey], [sName], [sCode], [sValue], [sRemark], [iFlag]) VALUES (N'6ce12132-3cf7-4528-9f6b-38914ee10873', N'斗鱼弹幕关键字剔除', N'ignoreKeyWord', N'#;#1;#2;签到;抢分', N'含有对应关键字的语句不进行关键字筛查  以 ; 分割', 1)
GO
INSERT [dbo].[sys_parameter] ([gkey], [sName], [sCode], [sValue], [sRemark], [iFlag]) VALUES (N'5e506a24-b525-4880-a05f-6dc16e3dee11', N'快速留言板', N'fastMessageBoard', N'true', N'页面快速留言板 true启用 false 不启用', 1)
GO
INSERT [dbo].[sys_parameter] ([gkey], [sName], [sCode], [sValue], [sRemark], [iFlag]) VALUES (N'4c822f71-f765-41d0-9a3a-7154a8492124', N'网站底部', N'footer', N'2018 &copy; 谁有我低调 出品<a href="http://www.miitbeian.gov.cn" rel="nofollow" target="_blank">皖ICP备17009084号-1</a> 如有侵权，邮箱联系，2个工作日内删除', N'前台页面底部html', 1)
GO
INSERT [dbo].[sys_parameter] ([gkey], [sName], [sCode], [sValue], [sRemark], [iFlag]) VALUES (N'babd7e74-1029-4ab8-8b4e-8d67dd5a93b0', N'网站logo', N'logoImage', N'~/Images/logo.png', N'网站logo图地址', 1)
GO
INSERT [dbo].[sys_parameter] ([gkey], [sName], [sCode], [sValue], [sRemark], [iFlag]) VALUES (N'226f35e7-4d81-4461-9190-8d75b7fc153b', N'默认头像照片', N'defultImage', N'/Images/User/default.jpg', N'默认的用户照片', 1)
GO
INSERT [dbo].[sys_parameter] ([gkey], [sName], [sCode], [sValue], [sRemark], [iFlag]) VALUES (N'968853bf-872a-4aa5-a89e-afb0b3f3015e', N'公告内容', N'noticeMsg', N'<div>头像上传，个人设置，系统消息上线 欢迎使用<br><br></div><div style="text-align: right;"><span style="color:red">lhj0502@vip.qq.com</span><br><span> 2018-06-13</span></div>', N'显示公告的内容  换行<br>
<div>
头像上传，个人设置，系统消息上线 欢迎使用<br><br>
现在注册联系管理员赠送管理权限<br>
</div>
<div style="text-align: right;">
<span style="color:red">lhj0502@vip.qq.com</span><br>
<span> 2018-06-13</span>
</div>


', 1)
GO
INSERT [dbo].[sys_parameter] ([gkey], [sName], [sCode], [sValue], [sRemark], [iFlag]) VALUES (N'206fb74f-b81a-44cf-82c8-c961ad9a3025', N'友情链接模块', N'friendUrl', N'true', N'友情链接模块 false表示不启用 true表示启用', 1)
GO
INSERT [dbo].[sys_parameter] ([gkey], [sName], [sCode], [sValue], [sRemark], [iFlag]) VALUES (N'49e8a03f-43d6-49b0-8c33-d93387ad149c', N'公告显示', N'noticeShow', N'true', N'公告模块显示 true显示 false不显示', 1)
GO
INSERT [dbo].[sys_parameter] ([gkey], [sName], [sCode], [sValue], [sRemark], [iFlag]) VALUES (N'98d288aa-c064-408c-97f6-e4afa2feefeb', N'标题', N'title', N'唐突世界', N'浏览器标题栏', 1)
GO
INSERT [dbo].[sys_parameter] ([gkey], [sName], [sCode], [sValue], [sRemark], [iFlag]) VALUES (N'7355bdfd-cf15-475b-b3c2-f6b8c6fc4254', N'网站描述', N'description', N'唐突世界致力于发展综合性平台', N'网站描述 herd标签中', 1)
GO
INSERT [dbo].[sys_role] ([gkey], [sName], [sExplain], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'504db0ae-210c-498a-b6a3-141d7c7ffd1a', N'小说模块管理员', N'小说模块管理员，拥有小说模块所有功能', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-03-23 15:05:37.373' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role] ([gkey], [sName], [sExplain], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'e1349881-e823-44c9-ad82-29f0ecac4f97', N'管理员', N'普通管理员', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-22 18:52:23.977' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role] ([gkey], [sName], [sExplain], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'ed44cdf6-35be-430e-9454-331fbf0dfa08', N'超级管理员', N'拥有全站所有权限', NULL, NULL, N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-03-28 14:22:29.147' AS DateTime))
GO
INSERT [dbo].[sys_role] ([gkey], [sName], [sExplain], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'6ab27348-c589-4bc6-9854-6580dda725ec', N'基础角色', N'拥有查看权限', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-03-28 14:21:59.560' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role] ([gkey], [sName], [sExplain], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'cef7de3a-fe98-46c9-9ede-7d5304df23d1', N'弹幕管理员', N'查看弹幕的人员', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-07 17:21:32.327' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'504db0ae-210c-498a-b6a3-141d7c7ffd1a', N'7cdc825e-c8ac-4a08-802c-03be2f50b6d1', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-03-23 15:18:42.990' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'504db0ae-210c-498a-b6a3-141d7c7ffd1a', N'57d93a69-389d-47e9-8cc4-12bf3bbcf504', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-03-23 15:18:42.990' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'504db0ae-210c-498a-b6a3-141d7c7ffd1a', N'9cb01caf-912e-484a-8874-1c546cc64e8e', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-03-23 15:18:42.990' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'504db0ae-210c-498a-b6a3-141d7c7ffd1a', N'958b0517-3bd3-49b6-bf31-1eea7f156423', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-03-23 15:18:42.990' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'504db0ae-210c-498a-b6a3-141d7c7ffd1a', N'4f32a8b1-264d-47c2-8112-237b8a9faa48', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-03-23 15:18:42.990' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'504db0ae-210c-498a-b6a3-141d7c7ffd1a', N'da49f22f-368f-4983-b732-494cc52a40f6', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-03-23 15:18:42.990' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'e1349881-e823-44c9-ad82-29f0ecac4f97', N'7cdc825e-c8ac-4a08-802c-03be2f50b6d1', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-22 18:53:19.320' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'e1349881-e823-44c9-ad82-29f0ecac4f97', N'5ef63130-6b3f-48cf-af98-04a7ba8de2e5', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-22 18:53:19.320' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'e1349881-e823-44c9-ad82-29f0ecac4f97', N'15f4a6d0-f464-4451-a1da-1616de2657ea', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-22 18:53:19.320' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'e1349881-e823-44c9-ad82-29f0ecac4f97', N'49baced3-0540-4a4e-a9c2-19cc79565c51', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-22 18:53:19.320' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'e1349881-e823-44c9-ad82-29f0ecac4f97', N'05589d85-3925-4b8b-a0d9-1c4d7ac752e0', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-22 18:53:19.320' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'e1349881-e823-44c9-ad82-29f0ecac4f97', N'9cb01caf-912e-484a-8874-1c546cc64e8e', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-22 18:53:19.320' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'e1349881-e823-44c9-ad82-29f0ecac4f97', N'958b0517-3bd3-49b6-bf31-1eea7f156423', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-22 18:53:19.320' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'e1349881-e823-44c9-ad82-29f0ecac4f97', N'4f32a8b1-264d-47c2-8112-237b8a9faa48', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-22 18:53:19.320' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'e1349881-e823-44c9-ad82-29f0ecac4f97', N'4acdde35-196b-4555-b7f2-2d4ed95a3b46', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-22 18:53:19.320' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'e1349881-e823-44c9-ad82-29f0ecac4f97', N'da49f22f-368f-4983-b732-494cc52a40f6', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-22 18:53:19.320' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'e1349881-e823-44c9-ad82-29f0ecac4f97', N'538ddea9-0c3c-4467-9861-51cb83ef9362', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-22 18:53:19.320' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'e1349881-e823-44c9-ad82-29f0ecac4f97', N'3b75f1e3-2a5e-4a00-89ed-82db13a072d1', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-22 18:53:19.320' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'e1349881-e823-44c9-ad82-29f0ecac4f97', N'818f3237-d2ce-4979-b366-9387ef629c16', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-22 18:53:19.320' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'e1349881-e823-44c9-ad82-29f0ecac4f97', N'c24989ce-97ae-4f00-af1d-9ccf5486170d', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-22 18:53:19.320' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'e1349881-e823-44c9-ad82-29f0ecac4f97', N'3a40c281-ed9b-4e63-ac88-a9b52fcef8b4', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-22 18:53:19.320' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'e1349881-e823-44c9-ad82-29f0ecac4f97', N'5ac6e67c-e189-4784-96ca-be4ff35ebe38', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-22 18:53:19.320' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'e1349881-e823-44c9-ad82-29f0ecac4f97', N'c7c041f2-4afb-459a-ae1d-e21971cc83e1', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-22 18:53:19.320' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'e1349881-e823-44c9-ad82-29f0ecac4f97', N'4390e935-c8aa-48a7-ae5a-f766f2d51ee2', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-22 18:53:19.320' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'ed44cdf6-35be-430e-9454-331fbf0dfa08', N'7cdc825e-c8ac-4a08-802c-03be2f50b6d1', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-16 17:09:32.007' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'ed44cdf6-35be-430e-9454-331fbf0dfa08', N'5ef63130-6b3f-48cf-af98-04a7ba8de2e5', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-16 17:09:32.007' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'ed44cdf6-35be-430e-9454-331fbf0dfa08', N'299851fd-4e84-4b80-ad62-0ea04e723c38', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-16 17:09:32.007' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'ed44cdf6-35be-430e-9454-331fbf0dfa08', N'57d93a69-389d-47e9-8cc4-12bf3bbcf504', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-16 17:09:32.007' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'ed44cdf6-35be-430e-9454-331fbf0dfa08', N'15f4a6d0-f464-4451-a1da-1616de2657ea', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-16 17:09:32.007' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'ed44cdf6-35be-430e-9454-331fbf0dfa08', N'49baced3-0540-4a4e-a9c2-19cc79565c51', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-16 17:09:32.007' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'ed44cdf6-35be-430e-9454-331fbf0dfa08', N'05589d85-3925-4b8b-a0d9-1c4d7ac752e0', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-16 17:09:32.007' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'ed44cdf6-35be-430e-9454-331fbf0dfa08', N'9cb01caf-912e-484a-8874-1c546cc64e8e', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-16 17:09:32.007' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'ed44cdf6-35be-430e-9454-331fbf0dfa08', N'958b0517-3bd3-49b6-bf31-1eea7f156423', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-16 17:09:32.007' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'ed44cdf6-35be-430e-9454-331fbf0dfa08', N'4f32a8b1-264d-47c2-8112-237b8a9faa48', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-16 17:09:32.007' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'ed44cdf6-35be-430e-9454-331fbf0dfa08', N'2eea0f19-8ca1-4ee7-9d09-2aeafe750ace', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-16 17:09:32.007' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'ed44cdf6-35be-430e-9454-331fbf0dfa08', N'4acdde35-196b-4555-b7f2-2d4ed95a3b46', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-16 17:09:32.007' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'ed44cdf6-35be-430e-9454-331fbf0dfa08', N'da49f22f-368f-4983-b732-494cc52a40f6', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-16 17:09:32.007' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'ed44cdf6-35be-430e-9454-331fbf0dfa08', N'c5c5d6a9-a110-4708-a8af-50a4abc83240', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-16 17:09:32.007' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'ed44cdf6-35be-430e-9454-331fbf0dfa08', N'538ddea9-0c3c-4467-9861-51cb83ef9362', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-16 17:09:32.007' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'ed44cdf6-35be-430e-9454-331fbf0dfa08', N'e3e79c4f-227a-4cca-a8a1-5bcb2c4735ef', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-16 17:09:32.007' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'ed44cdf6-35be-430e-9454-331fbf0dfa08', N'fadd37cf-0d17-4f0d-9b94-79bd3f6e309d', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-16 17:09:32.007' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'ed44cdf6-35be-430e-9454-331fbf0dfa08', N'3b75f1e3-2a5e-4a00-89ed-82db13a072d1', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-16 17:09:32.007' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'ed44cdf6-35be-430e-9454-331fbf0dfa08', N'818f3237-d2ce-4979-b366-9387ef629c16', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-16 17:09:32.007' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'ed44cdf6-35be-430e-9454-331fbf0dfa08', N'c24989ce-97ae-4f00-af1d-9ccf5486170d', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-16 17:09:32.007' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'ed44cdf6-35be-430e-9454-331fbf0dfa08', N'c6f34594-e8f4-47c3-b5f8-a7374ad3707e', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-16 17:09:32.007' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'ed44cdf6-35be-430e-9454-331fbf0dfa08', N'3a40c281-ed9b-4e63-ac88-a9b52fcef8b4', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-16 17:09:32.007' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'ed44cdf6-35be-430e-9454-331fbf0dfa08', N'27a18466-3607-4a51-99ed-b52efd1f8e01', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-16 17:09:32.007' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'ed44cdf6-35be-430e-9454-331fbf0dfa08', N'5ac6e67c-e189-4784-96ca-be4ff35ebe38', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-16 17:09:32.007' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'ed44cdf6-35be-430e-9454-331fbf0dfa08', N'582ea173-75fd-4b7f-8cd9-dc8bda9a694e', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-16 17:09:32.007' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'ed44cdf6-35be-430e-9454-331fbf0dfa08', N'c7c041f2-4afb-459a-ae1d-e21971cc83e1', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-16 17:09:32.007' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'ed44cdf6-35be-430e-9454-331fbf0dfa08', N'e0ee02d1-91bc-4572-a925-e654110a1376', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-16 17:09:32.007' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'ed44cdf6-35be-430e-9454-331fbf0dfa08', N'4390e935-c8aa-48a7-ae5a-f766f2d51ee2', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-16 17:09:32.007' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'6ab27348-c589-4bc6-9854-6580dda725ec', N'7cdc825e-c8ac-4a08-802c-03be2f50b6d1', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-03-28 14:22:06.047' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'6ab27348-c589-4bc6-9854-6580dda725ec', N'958b0517-3bd3-49b6-bf31-1eea7f156423', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-03-28 14:22:06.047' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'cef7de3a-fe98-46c9-9ede-7d5304df23d1', N'49baced3-0540-4a4e-a9c2-19cc79565c51', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-07 17:38:08.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'cef7de3a-fe98-46c9-9ede-7d5304df23d1', N'538ddea9-0c3c-4467-9861-51cb83ef9362', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-07 17:38:08.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_role_menu] ([gRoleKey], [gMenuKey], [gInsertKey], [dInsert], [gUpdateKey], [dUpdate]) VALUES (N'cef7de3a-fe98-46c9-9ede-7d5304df23d1', N'3a40c281-ed9b-4e63-ac88-a9b52fcef8b4', N'a4408581-85c9-4117-b3d9-dfffdd72fccf', CAST(N'2018-11-07 17:38:08.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[sys_userInfo] ([gkey], [sLoginName], [sPassWord], [sName], [iSex], [iFlag], [LastDateTime], [RegDateTime], [sRoleKey], [iState], [sImageUrl]) VALUES (N'd37d1f3b-e4d6-4287-a976-109dc38c279c', N'test', N'098F6BCD4621D373CADE4E832627B4F6', N'测试', 9, 1, CAST(N'2018-11-05 18:52:23.923' AS DateTime), CAST(N'2018-06-13 16:11:27.947' AS DateTime), N'ED44CDF6-35BE-430E-9454-331FBF0DFA08', 2, N'/Images/User/d37d1f3b-e4d6-4287-a976-109dc38c279c.gif')
GO
INSERT [dbo].[sys_userInfo] ([gkey], [sLoginName], [sPassWord], [sName], [iSex], [iFlag], [LastDateTime], [RegDateTime], [sRoleKey], [iState], [sImageUrl]) VALUES (N'0bed9438-c13c-4b5e-81d1-25fd9daa5ea1', N'2472517852@qq.com', N'5AA287D375E17BEA340155B2F2637CE9', N'cwt', 9, 1, CAST(N'2018-11-05 17:21:33.297' AS DateTime), CAST(N'2018-11-05 17:21:13.890' AS DateTime), NULL, 1, N'/Images/User/default.jpg')
GO
INSERT [dbo].[sys_userInfo] ([gkey], [sLoginName], [sPassWord], [sName], [iSex], [iFlag], [LastDateTime], [RegDateTime], [sRoleKey], [iState], [sImageUrl]) VALUES (N'f598e36b-ebb0-423b-b776-29334876145e', N'1060533242@qq.com', N'25F9E794323B453885F5181F1B624D0B', N'萨克斯', 9, 1, CAST(N'2018-11-05 17:27:29.377' AS DateTime), CAST(N'2018-11-05 17:27:07.860' AS DateTime), NULL, 1, N'/Images/User/default.jpg')
GO
INSERT [dbo].[sys_userInfo] ([gkey], [sLoginName], [sPassWord], [sName], [iSex], [iFlag], [LastDateTime], [RegDateTime], [sRoleKey], [iState], [sImageUrl]) VALUES (N'96014d19-eb76-4c61-aa9d-417f58df78ea', N'user1', N'E10ADC3949BA59ABBE56E057F20F883E', N'人员1', 1, 1, CAST(N'2018-05-09 10:01:37.853' AS DateTime), CAST(N'2018-03-23 08:39:56.000' AS DateTime), N'6ab27348-c589-4bc6-9854-6580dda725ec', 2, N'/Images/User/default.jpg')
GO
INSERT [dbo].[sys_userInfo] ([gkey], [sLoginName], [sPassWord], [sName], [iSex], [iFlag], [LastDateTime], [RegDateTime], [sRoleKey], [iState], [sImageUrl]) VALUES (N'e6b35da6-5f99-4c75-9a1b-53faf6c5b1be', N'1834149048@qq.com', N'05ED5305A199FF60A79DD4D1D7DC6718', N'。。。', 9, 1, CAST(N'2018-10-26 23:53:44.357' AS DateTime), CAST(N'2018-10-26 23:52:59.967' AS DateTime), NULL, 1, N'/Images/User/default.jpg')
GO
INSERT [dbo].[sys_userInfo] ([gkey], [sLoginName], [sPassWord], [sName], [iSex], [iFlag], [LastDateTime], [RegDateTime], [sRoleKey], [iState], [sImageUrl]) VALUES (N'3a7742a0-94ed-4c47-817c-5ce4a7da17a1', N'37499030@qq.com', N'E10ADC3949BA59ABBE56E057F20F883E', N'谁有我低调6', 9, 1, CAST(N'2018-06-22 15:18:34.297' AS DateTime), CAST(N'2018-05-10 10:52:46.343' AS DateTime), NULL, 1, N'/Images/User/3a7742a0-94ed-4c47-817c-5ce4a7da17a1.gif')
GO
INSERT [dbo].[sys_userInfo] ([gkey], [sLoginName], [sPassWord], [sName], [iSex], [iFlag], [LastDateTime], [RegDateTime], [sRoleKey], [iState], [sImageUrl]) VALUES (N'a97f301b-2c8c-4209-a02a-62396cb74cec', N'donglin322@126.com', N'1497EB4D8A9FEBD0C85E42CD25EFBCF1', N'dolLove', 9, 1, CAST(N'2018-11-05 18:14:03.283' AS DateTime), CAST(N'2018-11-05 17:21:02.547' AS DateTime), NULL, 1, N'/Images/User/default.jpg')
GO
INSERT [dbo].[sys_userInfo] ([gkey], [sLoginName], [sPassWord], [sName], [iSex], [iFlag], [LastDateTime], [RegDateTime], [sRoleKey], [iState], [sImageUrl]) VALUES (N'5f85a7a4-708e-45ec-8c32-8c6f803235b5', N'74751', N'DCFE56045D0694FAF16B3EFB1B561E84', N'弹幕', 9, 1, CAST(N'2018-11-07 17:42:51.327' AS DateTime), CAST(N'2018-11-07 17:33:43.797' AS DateTime), N'cef7de3a-fe98-46c9-9ede-7d5304df23d1', 2, N'/Images/User/5f85a7a4-708e-45ec-8c32-8c6f803235b5.gif')
GO
INSERT [dbo].[sys_userInfo] ([gkey], [sLoginName], [sPassWord], [sName], [iSex], [iFlag], [LastDateTime], [RegDateTime], [sRoleKey], [iState], [sImageUrl]) VALUES (N'c5f9f003-aff6-4a81-891b-9db3d7fba1c7', N'test@qq.com', N'FE81269ABF74F70EF7B2E87FBC2BA6DD', N'test', 9, 1, NULL, CAST(N'2018-08-09 15:38:07.880' AS DateTime), N'6ab27348-c589-4bc6-9854-6580dda725ec', 1, N'/Images/User/default.jpg')
GO
INSERT [dbo].[sys_userInfo] ([gkey], [sLoginName], [sPassWord], [sName], [iSex], [iFlag], [LastDateTime], [RegDateTime], [sRoleKey], [iState], [sImageUrl]) VALUES (N'e895a672-0a2a-4785-babe-b052d10b7006', N'374599030@qq.com', N'E10ADC3949BA59ABBE56E057F20F883E', N'谁有我低调', 9, 1, CAST(N'2018-07-09 13:33:00.537' AS DateTime), CAST(N'2018-05-10 10:51:05.477' AS DateTime), NULL, 1, N'/Images/User/default.jpg')
GO
INSERT [dbo].[sys_userInfo] ([gkey], [sLoginName], [sPassWord], [sName], [iSex], [iFlag], [LastDateTime], [RegDateTime], [sRoleKey], [iState], [sImageUrl]) VALUES (N'17cb45cf-7aa8-404b-b176-b30344295b5c', N'15086894869@163.com', N'E10ADC3949BA59ABBE56E057F20F883E', N'左眯眼', 1, 1, CAST(N'2018-11-22 17:33:22.197' AS DateTime), CAST(N'2018-11-22 17:29:45.007' AS DateTime), N'e1349881-e823-44c9-ad82-29f0ecac4f97', 2, N'/Images/User/17cb45cf-7aa8-404b-b176-b30344295b5c.jpg')
GO
INSERT [dbo].[sys_userInfo] ([gkey], [sLoginName], [sPassWord], [sName], [iSex], [iFlag], [LastDateTime], [RegDateTime], [sRoleKey], [iState], [sImageUrl]) VALUES (N'97233a76-2d79-43a5-894c-db6e5c0603df', N'bookadmin', N'E10ADC3949BA59ABBE56E057F20F883E', N'小说管理员', 1, 1, CAST(N'2018-07-09 13:34:18.727' AS DateTime), CAST(N'2018-03-23 08:39:56.000' AS DateTime), N'504DB0AE-210C-498A-B6A3-141D7C7FFD1A', 2, N'/Images/User/default.jpg')
GO
INSERT [dbo].[sys_userInfo] ([gkey], [sLoginName], [sPassWord], [sName], [iSex], [iFlag], [LastDateTime], [RegDateTime], [sRoleKey], [iState], [sImageUrl]) VALUES (N'e44446fd-efad-4c28-9341-deb84acc03b1', N'731565038@qq.com', N'8D735673415253DC67DB9E6DC6F153D0', N'Jerry', 1, 1, CAST(N'2018-06-13 16:14:07.700' AS DateTime), CAST(N'2018-05-24 16:22:38.797' AS DateTime), N'ed44cdf6-35be-430e-9454-331fbf0dfa08', 2, N'/Images/User/e44446fd-efad-4c28-9341-deb84acc03b1.gif')
GO
INSERT [dbo].[sys_userInfo] ([gkey], [sLoginName], [sPassWord], [sName], [iSex], [iFlag], [LastDateTime], [RegDateTime], [sRoleKey], [iState], [sImageUrl]) VALUES (N'a4408581-85c9-4117-b3d9-dfffdd72fccf', N'admin', N'E10ADC3949BA59ABBE56E057F20F883E', N'管理员', 1, 1, CAST(N'2018-12-06 14:18:19.737' AS DateTime), CAST(N'2018-03-15 06:49:49.000' AS DateTime), N'ED44CDF6-35BE-430E-9454-331FBF0DFA08', 2, N'/Images/User/a4408581-85c9-4117-b3d9-dfffdd72fccf.gif')
GO
INSERT [dbo].[sys_userInfo] ([gkey], [sLoginName], [sPassWord], [sName], [iSex], [iFlag], [LastDateTime], [RegDateTime], [sRoleKey], [iState], [sImageUrl]) VALUES (N'cad6ca3b-e537-4f84-b959-e1f61ced9d79', N'543495249@qq.com', N'7252B90780ABFB10764CDA839CD64DC4', N'帅鸽', 9, 1, CAST(N'2018-11-21 14:27:08.337' AS DateTime), CAST(N'2018-11-05 17:33:23.703' AS DateTime), N'ed44cdf6-35be-430e-9454-331fbf0dfa08', 2, N'/Images/User/default.jpg')
GO

