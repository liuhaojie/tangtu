--获取报表数据_斗鱼每日弹幕数量
if Exists(select name from sysobjects where NAME = 'Dy_Echarts_By_Day' and type='P')
BEGIN
	DROP PROCEDURE Dy_Echarts_By_Day
END
GO

CREATE PROC Dy_Echarts_By_Day(
 @startTime VARCHAR(36),
 @endTime VARCHAR(36)
)
AS 
BEGIN
	SELECT CONVERT(varchar(100) , dSpeakTime, 23) AS text,COUNT(1) AS value FROM dbo.dy_Barrage 
	WHERE dSpeakTime>=@startTime AND dSpeakTime<=@endTime
	GROUP BY  CONVERT(varchar(100),  dSpeakTime, 23) ORDER BY text
END
