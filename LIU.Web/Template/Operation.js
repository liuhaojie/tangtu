﻿layui.use(['form', 'util'], function () {
    var util = layui.util, form = layui.form;

    util.fixbar({
        bgcolor: '#009688'
    });

    //监听提交
    form.on('submit(config)', function (data) {
        var dt = Cache.GetUserInfo();
        if (dt) {
            $.ajax({
                url: "/System/MsgBoard/AddMessage",
                type: "post",
                dataType: "json",
                data: data.field,
                success: function (data) {
                    layer.open({
                        title: '提示',
                        content: data.msg,
                        yes: function (index) {
                            if (data.flag == 1) {
                                if ($("#msgPanel").css("display") != "none")
                                    $(":reset").click();
                                else
                                    top.location.href = "/System/MsgBoard/Index";
                            }
                            layer.close(index);
                        }
                    });
                }
            });
        }
        return false;
    });

});
//获取用户登录信息
$(document).ready(function () {
    var dt = Cache.GetUserInfo();
    if (dt) {
        $("#unlogin").hide();
        $("#logined").show();
        if (dt.noReadCount == "0")
            $("#name").text(dt.sName);
        else {
            $("#name").html(dt.sName + '<i class="layui-badge-dot " style="position:relative;top:-8px;"></i>');
            $("#iNotifyMsg").parents(".layui-nav-child").css("width", "150")
            $("#iNotifyMsg").html('<i class="iconfont icon-tongzhi" style="top: 4px;"></i>我的消息<span class="layui-badge">' + dt.noReadCount + '</span>');
        }
        $("#picImage").attr("src", dt.sImageUrl);
        if (dt.dLevelTIme && new Date(Date.parse(dt.dLevelTIme.replace(/-/g, "/"))) > new Date()) {
            $("#name").after('<span class="iconfonts icon-vip' + dt.iLevel + '" style="color:#FFD700;font-size: 30px;line-height: 30px;position: relative;top: 5px;"></span>')
        }
        if (dt.sHerdImageUrl)
            $("#picImage").after('<img src="' + dt.sHerdImageUrl + '"  style="width:60px;height:60px;margin-left:-48px;">');
        if (dt.iState == "2")//后台用户
            $("#BackManage").show();
        layui.use('element', function () {
            var element = layui.element;
            element.render();
        });
    }
    else {//未登录不可以评价
        $("#btnMsgBoardSub").addClass("layui-btn-disabled");
        $("#btnMsgBoardSub").text("未登录");
    }

    if (true) {
        GetMsgType();
        $("#msgPanel").show();
    }
    if (true) {
        $("#noticeShow").show();
        getNotice();
    }
    if (true) {
        $("#friendUrl").show();
        getFriendUrl();
    }
    //预留方法
    Cache.ready();
})

//获取留言类型
function GetMsgType() {
    var dt = Cache.GetMsgType();
    if (dt) {
        for (var i = 0; i < dt.length; i++) {
            $("#iType").append(' <option value="' + dt[i].sValue + '">' + dt[i].sName + '</option>')
        }
        layui.use('form', function () {
            var form = layui.form;
            form.render("select", "msgFrom"); //刷新select选择框渲染
        })
    }
}

//获取公告
function getNotice() {
    var dt = Cache.getNotice();
    if (dt) {
        $("#noticeMsg").html(dt);
    }
}

//获取友情链接
function getFriendUrl() {
    var dt = Cache.GetFriendUrl();
    if (dt) {
        var html = "";
        for (var i = 0; i < dt.length; i++) {
            html += " <dd><a href='" + dt[i].sValue + "' target='_blank'  class='fly-link'>" + dt[i].sName + "</a>"

        }
        $("#friendUrlList").html(html);
    }

}

//百度统计
var _hmt = _hmt || [];
(function () {
    var hm = document.createElement("script");
    hm.src = "https://hm.baidu.com/hm.js?f1d1d0d6d7c0d78dcfb38156932a7847";
    var s = document.getElementsByTagName("script")[0];
    s.parentNode.insertBefore(hm, s);
})();