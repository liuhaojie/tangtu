﻿using LIU.Common;
using LIU.Web.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LIU.Web.App_Task
{
    [AutoTask(EnterMethod = "StartTask", IntervalSeconds = 60 * 60 * 24, StartTime = "2018-07-01 02:00:00", Explain = "定时获取小说最新章节")]
    public class CrawlNovelTask
    {
        public static void StartTask()
        {
            LogHelper.Info("定时【获取小说最新章节】开始");
            try
            {
                string url = System.Configuration.ConfigurationManager.AppSettings["WebUrl"] + "/System/Tasks/CrawlNovel";
                HttpHelper.HttpPost(url, @"{'code': '156069CF8716EAA21138659071DEFEE3'}", true);
            }
            catch (Exception ex)
            {
                LogHelper.Error("定时【获取小说最新章节】任务出错:" + ex.Message + "\r\n" + ex.StackTrace);
            }
            LogHelper.Info("定时【获取小说最新章节】结束");
        }



    }
}