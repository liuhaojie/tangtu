﻿using LIU.BLL.System;
using LIU.Common;
using LIU.DAL.System;
using LIU.IBLL.System;
using LIU.IDAL.System;
using LIU.Model;
using LIU.Model.System;
using LIU.UnityFactory;
using LIU.Web.App_Code;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;

namespace LIU.Web.App_Task
{
    /// <summary>
    /// 每天定时任务
    /// </summary>
    [AutoTask(EnterMethod = "StartTask", IntervalSeconds = 60 * 60 * 24, StartTime = "2018-07-01 01:00:00", Explain = "定时删除系统通知消息")]
    public class DailyTasks
    {

        public static void StartTask()
        {
            LogHelper.Info("定时任务开始");
            try
            {
                string url = System.Configuration.ConfigurationManager.AppSettings["DelSysNotifyMsgurl"];
                HttpHelper.HttpPost(url, @"{'code': '5A8A8879BF1B237228845E73193D4384'}", true);
            }
            catch (Exception ex)
            {
                LogHelper.Error("定时任务出错:" + ex.Message + "\r\n" + ex.StackTrace);
            }
            LogHelper.Info("定时任务结束");
        }

        #region 弃用 --ioc创建会失败，原因不是http请求


        /// <summary>
        /// 删除系统通知消息
        /// </summary>
        public static void DelSysNotifyMsg()
        {
            INotifyMessageBLL inmBll = IOCFactory.GetIOCResolve<INotifyMessageBLL>();
            //获取系统消息阅读后自动删除时间 0表示不删除            
            string day = GetParameterValue("SysNotifyMsgDelDay");
            if (day != "" || Convert.ToInt32(day) > 0)
            {
                var list = inmBll.GetList(p => p.iFlag == 2 && p.iType == 1);
                List<NotifyMessage> nmlist = new List<NotifyMessage>();
                foreach (var item in list)
                {
                    //判断阅读时间加上删除天数 是否 大于当前时间
                    if (Convert.ToDateTime(item.dReadDate).AddDays(Convert.ToInt32(day)) > DateTime.Now)
                    {
                        item.iFlag = 0;
                        nmlist.Add(item);
                    }
                }
                if (nmlist.Count > 0)
                    inmBll.Update(nmlist);
                LogHelper.Info("删除系统通知消息" + nmlist.Count + "条");
            }
        }

        /// <summary>
        /// 获取系统参数值
        /// </summary>
        /// <param name="code">参数代码</param>
        /// <returns></returns>
        public static string GetParameterValue(string code)
        {
            Parameter par = SysCache.ParameterList.Where(p => p.sCode == code).FirstOrDefault();
            if (par == null)
                return "";
            else
                return par.sValue;
        }
        #endregion
    }
}