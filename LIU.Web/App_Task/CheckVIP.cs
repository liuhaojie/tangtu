﻿using LIU.Common;
using LIU.Web.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LIU.Web.App_Task
{
    /// <summary>
    /// 每天定时任务
    /// </summary>
    [AutoTask(EnterMethod = "StartTask", IntervalSeconds = 60 * 60 * 24, StartTime = "2019-03-01 01:00:00", Explain = "定时检查VIP到期")]
    public class CheckVIP
    {
        public static void StartTask()
        {
            LogHelper.Info("定时检查VIP开始");
            try
            {
                string url = System.Configuration.ConfigurationManager.AppSettings["WebUrl"] + "/System/Tasks/CheckVIP";
                HttpHelper.HttpPost(url, @"{'code': 'FEADA4F99C8CD1A265182032ED2FC1F4'}", true);
            }
            catch (Exception ex)
            {
                LogHelper.Error("定时检查VIP出错:" + ex.Message + "\r\n" + ex.StackTrace);
            }
            LogHelper.Info("定时检查VIP结束");
        }
    }
}