﻿using LIU.Common;
using LIU.Web.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LIU.Web.App_Task
{
    /// <summary>
    /// 钉钉机器人 报时
    /// </summary>
   // [AutoTask(EnterMethod = "StartTask", IntervalSeconds = 3600, StartTime = "2018-05-02 11:00:00")]
    public class DDTime
    {
        public static void StartTask()
        {
            string content = "现在是北京时间:" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "  ";
            switch (DateTime.Now.ToString("HH:mm"))
            {
                case "09:00":
                    content += "元气满满的一天又开始啦!";
                    break;
                case "10:00":
                    content += "开始解决最困难的问题吧!";
                    break;
                case "11:00":
                    content += "肚子有点饿，等待下班中!";
                    break;
                case "12:00":
                    content += "我要做到第一个冲向食堂的人!";
                    break;
                case "13:00":
                    content += "犯困的下午已经开始啦!";
                    break;
                case "14:00":
                    content += "困~~~~~~~~~~~~~~~!";
                    break;
                case "15:00":
                    content += "困~~~~~~~~~~~~~~~+1!";
                    break;
                case "16:04":
                    content += "打起精神,埋头造轮子!";
                    break;
                case "17:00":
                    content += "临近下班的时候心就已经不安稳了!";
                    break;
                case "18:00":
                    content += "下班回家,我们明天再战!";
                    break;
                default:
                    content = "";
                    break;
            }
            if (content == "")
                return;

            LogHelper.Info("钉钉机器人 报时 开始");
            try
            {
                string url = "https://oapi.dingtalk.com/robot/send?access_token=6c7efef4d2d5fb3dd7bcb707bb0a6abfb18a4f6f6b6b937608138c25c9ed1896";
                string postData = @"{
                            'msgtype': 'text',
                            'text': {
                                    'content': '" + content + @"  '
                            },
                            'at':{
                                'isAtAll': true
                            }
                            }";
                HttpHelper.HttpPost(url, postData, true);
            }
            catch (Exception ex)
            {
                LogHelper.Error("钉钉机器人 报时" + ex.StackTrace);
            }
            LogHelper.Info("钉钉机器人 报时 结束");
        }
    }
}