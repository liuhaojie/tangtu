﻿//ajax启动时灰层罩
$(document).ajaxStart(function () {
    layui.use('layer', function () {
        var layer = layui.layer;
        layer.load(2);
    })

})

//ajax结束时取消灰层罩
$(document).ajaxStop(function () {
    layui.use('layer', function () {
        var layer = layui.layer;
        layer.closeAll('loading');
    })
})
//97表示系统内部错误 98表示没有登录 99表示没有权限 
$.ajaxSetup({
    statusCode: {
        520: function (data) {
            var data = $.parseJSON(data.responseText);
            if (data.flag == "98") {
                top.location.href = data.msg;
            }
            else {
                if (getSpecialCode(data.msg)) {
                    alert(data.msg);
                } else {
                    layui.use('layer', function () {
                        var layer = layui.layer;
                        parent.layer.open({
                            title: '提示',
                            content: data.msg,
                            icon: 2
                        });
                    })
                }

            }
        }
    }
})

//获取特殊字符
function getSpecialCode(str) {
    var patt1 = new RegExp("<[A-z]");
    return patt1.test(str);
}