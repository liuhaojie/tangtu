﻿window.Cache = window.Cache || {}

//获取公告
Cache.getNotice = function () {
    Cache.Notice = sessionStorage.getItem("Notice");

    if (Cache.Notice)
        return Cache.Notice;
    else
        $.ajax({
            url: "/Api/Info/GetNewNotice",
            type: "post",
            async: false,//同步
            beforeSend: function () { },
            complete: function () { },
            success: function (data) {
                if (data.flag == 1) {
                    Cache.Notice = data.msg.sContent;
                }
                else
                    Cache.Notice = null;
            },
            error: function (data) {
                Cache.Notice = null;
            }
        })
    sessionStorage.setItem("Notice", Cache.Notice);
    return Cache.Notice;
}

//获取留言类型
Cache.GetMsgType = function () {
    Cache.MsgType = sessionStorage.getItem("MsgType");
    if (Cache.MsgType)
        return $.parseJSON(Cache.MsgType);
    else
        $.ajax({
            url: "/Api/Info/GetMessageType",
            type: "post",
            async: false,//同步
            beforeSend: function () { },
            complete: function () { },
            success: function (data) {
                if (data.flag == 1)
                    Cache.MsgType = $.parseJSON(data.msg);
                else
                    Cache.MsgType = null;
            },
            error: function (data) {
                Cache.MsgType = null;
            }
        })
    sessionStorage.setItem("MsgType", JSON.stringify(Cache.MsgType));
    return Cache.MsgType;
}

//获取当前登录用户信息
Cache.GetUserInfo = function () {
    Cache.UserInfo = sessionStorage.getItem("UserInfo");
    if (Cache.UserInfo)
        return $.parseJSON(Cache.UserInfo);
    else
        $.ajax({
            url: "/Default/GetUserInfro",
            type: "post",
            async: false,//同步
            beforeSend: function () { },
            complete: function () { },
            success: function (data) {
                if (data.flag == 1)
                    Cache.UserInfo = $.parseJSON(data.msg);

                else
                    Cache.UserInfo = null;
            },
            error: function (data) {
                Cache.UserInfo = null;
            }
        })
    sessionStorage.setItem("UserInfo", JSON.stringify(Cache.UserInfo));
    return Cache.UserInfo;
}


//获取友情链接
Cache.GetFriendUrl = function () {
    Cache.FriendUrl = sessionStorage.getItem("FriendUrl");
    if (Cache.FriendUrl)
        return $.parseJSON(Cache.FriendUrl);
    else
        $.ajax({
            url: "/Api/Info/GetFriendUrl",
            type: "post",
            async: false,//同步
            beforeSend: function () { },
            complete: function () { },
            success: function (data) {
                if (data.flag == 1)
                    Cache.FriendUrl = data.msg;
                else
                    Cache.FriendUrl = null;
            },
            error: function (data) {
                Cache.FriendUrl = null;
            }
        })
    sessionStorage.setItem("FriendUrl", JSON.stringify(Cache.FriendUrl));
    return Cache.FriendUrl;
}

//更新当前登录用户信息
Cache.UpDataUserInfo = function () {
    sessionStorage.removeItem("UserInfo");
    Cache.GetUserInfo();
}

//更新当前登录用户信息
Cache.UpdateUI = function (UserInfo) {
    Cache.UserInfo = $.parseJSON(sessionStorage.getItem("UserInfo"));
    if (Cache.UserInfo) {
        Cache.UserInfo.iLevel = UserInfo.iLevel;
        Cache.UserInfo.dLevelTIme = UserInfo.dLevelTIme;
        Cache.UserInfo.iGold = UserInfo.iGold;
        sessionStorage.setItem("UserInfo", JSON.stringify(Cache.UserInfo));
    }
    else
        Cache.GetUserInfo();
}

//预留方法
Cache.ready = function () {
    Cache.UpNoReadCount();
    if (!browser.versions.mobile)
        Cache.AddBack();
    timename = setTimeout(function () { layer.closeAll('loading');}, 100);
}


//更新未读条数
Cache.UpNoReadCount = function () {
    $.ajax({
        url: "/Default/GetNoReadCount",
        type: "post",
        beforeSend: function () { },
        complete: function () { },
        success: function (data) {
            if (data.flag == 1) {
                var UserInfo = Cache.GetUserInfo();
                UserInfo.noReadCount = $.parseJSON(data.msg).noReadCount;
                sessionStorage.setItem("UserInfo", JSON.stringify(UserInfo));
            }
          
        }
    })
}

//添加背景特效
Cache.AddBack = function () {
    $("body").prepend('<canvas id="c_n30" style="position: fixed;width: 100%;height: 100%; opacity: 90;"></canvas>' +
        ' <script src="../../../../Scripts/h5/StarrySky/script.js"></script>');
}

var browser = {
    versions: function () {
        var u = window.navigator.userAgent;
        return {
            trident: u.indexOf('Trident') > -1, //IE内核
            presto: u.indexOf('Presto') > -1, //opera内核
            webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
            gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1, //火狐内核
            mobile: !!u.match(/AppleWebKit.*Mobile.*/) || !!u.match(/AppleWebKit/), //是否为移动终端
            ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
            android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, //android终端或者uc浏览器
            iPhone: u.indexOf('iPhone') > -1 || u.indexOf('Mac') > -1, //是否为iPhone或者安卓QQ浏览器
            iPad: u.indexOf('iPad') > -1, //是否为iPad
            webApp: u.indexOf('Safari') == -1,//是否为web应用程序，没有头部与底部
            weixin: u.indexOf('MicroMessenger') == -1 //是否为微信浏览器
        };
    }()
}