using LIU.Common;
using LIU.Model;
using LIU.Model.EBook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.IDAL.EBook
{
    public interface IChapterDAL:IBaseDAL<Chapter>, IRegister
    {
        /// <summary>
        /// 获取已经采集到最大的章节
        /// </summary>
        /// <param name="gNovelKey">小说主键</param>
        /// <returns></returns>
        int getMaxChapter(Guid gNovelKey);
    }
}

