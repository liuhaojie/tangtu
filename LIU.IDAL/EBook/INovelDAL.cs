using LIU.Common;
using LIU.Model;
using LIU.Model.EBook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.IDAL.EBook
{
    public interface INovelDAL:IBaseDAL<Novel>, IRegister
    {

    }
}

