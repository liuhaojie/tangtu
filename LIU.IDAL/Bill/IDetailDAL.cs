﻿using LIU.Common;
using LIU.Model.Bill;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.IDAL.Bill
{
    public interface IDetailDAL : IBaseDAL<Detail>, IRegister
    {
    }
}
