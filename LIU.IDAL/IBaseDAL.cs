﻿using LIU.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace LIU.IDAL
{
    /// <summary>
    /// 操作数据基类接口
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IBaseDAL<T> where T : class
    {
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        T GetModel(Expression<Func<T, bool>> where);

        /// <summary>
        /// 获取实体类
        /// </summary>
        /// <param name="where">lambda查询条件</param>
        /// <param name="orderBy">lambda排序条件</param>
        /// <param name="isAscending">是否升序</param>
        /// <returns></returns>
        T GetModel(Expression<Func<T, bool>> where, Expression<Func<T, object>> orderBy, bool isAscending);

        /// <summary>
        /// 添加实体
        /// </summary>
        /// <param name="model"></param>
        void Add(T model);

        /// <summary>
        /// 删除实体
        /// </summary>
        /// <param name="model"></param>
        void Delete(T model);

        /// <summary>
        /// 根据条件删除
        /// </summary>
        /// <param name="where"></param>
        void Delete(Expression<Func<T, bool>> where);

        /// <summary>
        /// 更新实体
        /// </summary>
        /// <param name="model"></param>
        void Update(T model);

        /// <summary>
        /// 获取实体数组
        /// </summary>
        /// <param name="where">查询条件</param>
        /// <returns></returns>
        List<T> GetList(Expression<Func<T, bool>> where);

        /// <summary>
        /// 获取分页数组
        /// </summary>
        /// <param name="where">查询条件</param>
        /// <param name="orderBys">排序字段</param>
        /// <param name="ph">分页帮助类</param>
        /// <param name="isAsc">是否升序</param>
        /// <returns></returns>
        List<T> GetList(Expression<Func<T, bool>> where, Expression<Func<T, object>> orderBys, PageHelper ph, bool isAsc);

        /// <summary>
        /// 获取分页的总行数
        /// </summary>
        /// <param name="where">查询条件</param>
        /// <returns></returns>
        int RowCount(Expression<Func<T, bool>> where);

        /// <summary>
        /// 获取最大或者最小值
        /// </summary>
        /// <param name="where">排序条件</param>
        /// <param name="orderBys">排序规则</param>
        /// <param name="isMax">默认获取最大</param>
        /// <returns></returns>
        T GetMaxOrMin(Expression<Func<T, bool>> where, Expression<Func<T, object>> orderBys, bool isMax = true);

        /// <summary>
        /// 执行sql语句
        /// </summary>
        /// <typeparam name="TT"></typeparam>
        /// <param name="procName">语句</param>
        /// <param name="paras">参数</param>
        /// <returns></returns>
        List<TT> ExcetProc<TT>(string procName, params SqlParameter[] paras);

        /// <summary>
        /// 执行sql语句
        /// </summary>
        /// <typeparam name="TT"></typeparam>
        /// <param name="procName">语句</param>
        /// <param name="paras">参数</param>
        /// <returns></returns>
        int ExcetProc(string procName, params SqlParameter[] paras);

        /// <summary>
        /// SqlBulkCopy 导入快速
        /// </summary>
        /// <param name="dataTables"></param>
        void SqlBulkCopy(DataTable[] dataTables);

        /// <summary>
        /// EF SQL 语句返回 dataTable
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        DataTable SqlQueryDataTable(string sql, params SqlParameter[] parameters);
    }
}
