﻿using LIU.Common;
using LIU.Model.Dy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.IDAL.Dy
{
    public interface IKeyWordDAL : IBaseDAL<KeyWord>, IRegister
    {
    }
}
