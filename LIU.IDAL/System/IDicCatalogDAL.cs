using LIU.Common;
using LIU.Model;
using LIU.Model.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIU.IDAL.System
{
    public interface IDicCatalogDAL:IBaseDAL<DicCatalog>, IRegister
    {

    }
}

